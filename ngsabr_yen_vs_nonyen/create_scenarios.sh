#!/bin/bash

# 3 days
DURATION=259200

python3 -m tvgutil.tools.create_rr_scenario --gs 3 --sats 3 --output "3g3s.02.scenario.json"
python3 -m tvgutil.tools.create_rr_tvg --rr 0 --duration $DURATION --minelev 10 --uplinkrate 256000 --downlinkrate 1000000 --output "3g3s.02.ptvg.json" "3g3s.02.scenario.json"
python3 -m tvgutil.tools.convert_ptvg_to_ftvg --output "3g3s.02.ftvg.json" "3g3s.02.ptvg.json"

python3 -m tvgutil.tools.create_rr_scenario --gs 5 --sats 5 --output "5g5s.02.scenario.json"
python3 -m tvgutil.tools.create_rr_tvg --rr 0 --duration $DURATION --minelev 10 --uplinkrate 256000 --downlinkrate 1000000 --output "5g5s.02.ptvg.json" "5g5s.02.scenario.json"
python3 -m tvgutil.tools.convert_ptvg_to_ftvg --output "5g5s.02.ftvg.json" "5g5s.02.ptvg.json"
