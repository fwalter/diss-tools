#!/bin/bash

# This script compares routing performance of Yen and Non-Yen CGR.

TEST="T1_yen_cmp"

WORKING_DIR="results_$TEST"

SCENARIOS=(
    "3g3s+low"
    "3g3s+high"
    "5g5s+low"
    "5g5s+high"
)

ALGOS=(
    CGR-limit-ending
    CGR-limit-depleted
    CGR-yen-00100
    CGR-yen-00500
    CGR-yen-01000
    CGR-yen-05000
    CGR-yen-10000
    CGR-yen-20000
)

RUNS=10

WORKERS=24

# FTVG -> PTVG
TVG_CONVERSION_FLAGS="--link_block_size 12000"

# Flags for the transmission plan
# 3g3s
# LOW: 10*86400*0.2/12750*16777216*2/45428592000.0*2
# HIGH: 10*86400*0.2/510*16777216*2/45428592000.0*2
# 5g5s
# LOW: 10*86400*0.2/3025*16777216*2/192301908000.0*2
# HIGH: 10*86400*0.2/121*16777216*2/192301908000.0*2
TPLAN_FLAGS=(
    "--maxgenpercent 20 --interval 12750 --intervaldev 3187 --lifetime 864000 --minsize 16777216"
    "--maxgenpercent 20 --interval 510 --intervaldev 127 --lifetime 864000 --minsize 16777216"
    "--maxgenpercent 20 --interval 3025 --intervaldev 756 --lifetime 864000 --minsize 16777216"
    "--maxgenpercent 20 --interval 121 --intervaldev 30 --lifetime 864000 --minsize 16777216"
)

# Flags for the simulator, see T07
SIM_FLAGS=(
    "--rx_channels 5 --tx_channels 5 --buffersize 17179869184 --graph_update_min_interval 43200 --link_block_size 12000 --algo ngsabr"
    "--rx_channels 5 --tx_channels 5 --buffersize 17179869184 --graph_update_min_interval 43200 --link_block_size 12000 --algo ngsabr --volume_limited"
    "--rx_channels 5 --tx_channels 5 --buffersize 17179869184 --graph_update_min_interval 43200 --link_block_size 12000 --algo yengsabr --max_routes_yen 100"
    "--rx_channels 5 --tx_channels 5 --buffersize 17179869184 --graph_update_min_interval 43200 --link_block_size 12000 --algo yengsabr --max_routes_yen 500"
    "--rx_channels 5 --tx_channels 5 --buffersize 17179869184 --graph_update_min_interval 43200 --link_block_size 12000 --algo yengsabr --max_routes_yen 1000"
    "--rx_channels 5 --tx_channels 5 --buffersize 17179869184 --graph_update_min_interval 43200 --link_block_size 12000 --algo yengsabr --max_routes_yen 5000"
    "--rx_channels 5 --tx_channels 5 --buffersize 17179869184 --graph_update_min_interval 43200 --link_block_size 12000 --algo yengsabr --max_routes_yen 10000"
    "--rx_channels 5 --tx_channels 5 --buffersize 17179869184 --graph_update_min_interval 43200 --link_block_size 12000 --algo yengsabr --max_routes_yen 20000"
)

COMMAND="$1"

set -euxo pipefail

if [ -z "$COMMAND" ]; then
    COMMAND="run"
fi

for scenario in "${SCENARIOS[@]}"; do
    base_scenario="$(echo "$scenario" | cut -d '+' -f 1)"
    if [[ ! -r "$base_scenario.02.scenario.json" || ! -r "$base_scenario.02.ftvg.json" ]]; then
        echo "$base_scenario.02.scenario.json and $base_scenario.02.ftvg.json have to exist!" >&2
        exit 1
    fi
done

mkdir -p "$WORKING_DIR"

setup() {
    # Remove all empty result files to have a clean setup
    find "$WORKING_DIR" -type f -size 0 -delete

    for ((s = 0; s < ${#SCENARIOS[@]}; s++)); do
        local scenario="${SCENARIOS[$s]}"
        local tplan_flags="${TPLAN_FLAGS[$s]}"

        local prefix="$WORKING_DIR/$scenario"
        local base_scenario="$(echo "$scenario" | cut -d '+' -f 1)"

        # Prepare factual P-TVG
        [ -r "$prefix.05.ptvg.json" ] || \
            python helpers/03_convert_ftvg_to_ptvg.py \
                $TVG_CONVERSION_FLAGS \
                --output "$prefix.05.ptvg.json" \
                "$base_scenario.02.ftvg.json"

        for run in $(seq $RUNS); do
            [ -r "$prefix.06.transmission_plan_$run.json" ] || \
                python -m tvgutil.tools.create_transmission_plan \
                    $tplan_flags \
                    --output "$prefix.06.transmission_plan_$run.json" \
                    "$base_scenario.02.ftvg.json"
        done

        [ -r "$prefix.02.ftvg.json" ] || cp "$base_scenario.02.ftvg.json" \
            "$prefix.02.ftvg.json"
        [ -r "$prefix.02.scenario.json" ] || cp "$base_scenario.02.scenario.json" \
            "$prefix.02.scenario.json"
    done
}

job() {
    local scenario="$1"
    local base_scenario="$(echo "$scenario" | cut -d '+' -f 1)"
    local algo="$2"
    local run="$3"
    local prefix="$WORKING_DIR/$scenario"

    for ((a = 0; a < ${#ALGOS[@]}; a++)); do
        if [[ "${ALGOS[$a]}" = "$algo" ]]; then
            local sim_flags="${SIM_FLAGS[$a]}"
            break
        fi
    done

    [ -r "$prefix.06.transmission_plan_$run.json" ] || exit 1

    [ -r "$prefix.07.stats.$algo.$run.json" ] && return 0

    python 06_simulation.py \
        $sim_flags \
        --ptvg "$prefix.05.ptvg.json" \
        --tplan "$prefix.06.transmission_plan_$run.json" \
        --savestats "$prefix.07.stats.$algo.$run.json" \
        "$prefix.02.ftvg.json"
}

teardown() {
    bash draw_plots.sh \
        "$WORKING_DIR" \
        "$WORKING_DIR/plots_$TEST" \
        "${SCENARIOS[@]}"
}

case "$COMMAND" in
    run)
        setup
        python test_process_pool_executor.py \
            --workers $WORKERS \
            --scenarios "${SCENARIOS[@]}" \
            --algorithms "${ALGOS[@]}" \
            --runs $(seq $RUNS) \
            --progress \
            --logfile "$WORKING_DIR/test_executor.log" \
            "$0"
        teardown
        ;;
    clean)
        rm -rvf --one-file-system --preserve-root "$WORKING_DIR"
        ;;
    cleanrouting)
        rm -vf --one-file-system --preserve-root $WORKING_DIR/*06.transmission_plan*json
        rm -vf --one-file-system --preserve-root $WORKING_DIR/*07.stats*json
        rm -vf --one-file-system --preserve-root $WORKING_DIR/*08.plotstats*json
        ;;
    job)
        job "${@:2}"
        ;;
    *)
        echo "Usage: $0 [<run|clean|job>] [<job-args>]" >&2
        exit 1
        ;;
esac
