#!/bin/bash

set -eux

python3 -m tvgutil.tools.create_rr_scenario --gs 5 --sats 5 --output "01_scenario.json"
python3 -m tvgutil.tools.create_rr_tvg --rr 0 --duration 86400 --minelev 10 --uplinkrate 9600 --downlinkrate 64000 --output "02_ptvg.json" "01_scenario.json"
python3 -m tvgutil.tools.create_transmission_plan --interval 60 --minsize 500000 --lifetime 43200 --intervaldev 10 --maxgenpercent 2 --output "04_tplan.json" "02_ptvg.json"
python3 -m tvgutil.tools.convert_ptvg_to_ftvg --output "03_ftvg.json" "02_ptvg.json"
time python ../diss/06_simulation.py -a ngsabr -t 04_tplan.json -p 02_ptvg.json 03_ftvg.json --progress --buffersize -1
time python ../diss/06_simulation.py -a yengsabr -t 04_tplan.json -p 02_ptvg.json 03_ftvg.json --progress --buffersize -1 --max_routes_yen 100
time python ../diss/06_simulation.py -a yengsabr -t 04_tplan.json -p 02_ptvg.json 03_ftvg.json --progress --buffersize -1 --max_routes_yen 200
time python ../diss/06_simulation.py -a yengsabr -t 04_tplan.json -p 02_ptvg.json 03_ftvg.json --progress --buffersize -1 --max_routes_yen 300
