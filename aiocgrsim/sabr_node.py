# encoding: utf-8

"""A Node allowing to use SABR."""

import collections
import dataclasses
import itertools
import logging
import math
import queue
import random

from typing import Optional

import igraph

from tvgutil.contact_plan import SimplePredictedContactTuple

from predictutil import StartTimeBasedDict

from . import sabr

ASSERT_RANGE = 20

logger = logging.getLogger(__name__)

# NOTE: See NOTEs in sabr.py
# NOTE: Does NOT support earliest arrival time propagation during Dijkstra
#   run, meaning missing support for more than one nested contact (hot spots).
#   It shall be noted that hot spot routes COULD be supported by 1) ensuring
#   all hot spots are interconnected and 2) preventing edges between
#   hot-spot-to-hot-spot-contacts.


@dataclasses.dataclass
class SABRGraphData(sabr.SABRGraphDataBase):

    # IMPORTANT NOTE: contact_plan is indexed by the vertex id (vid) to
    # facilitate easy lookup.

    # This structure contains the graph object.
    graph: Optional[igraph.Graph] = dataclasses.field(
        default=None,
        compare=False,
    )

    # Mapping of contact identifier to vertex ID (as in list)
    # Its keys are SimplePredictedContactTuple, its values int.
    contact_vid_mapping: collections.OrderedDict = dataclasses.field(
        default_factory=collections.OrderedDict,
        compare=False,
    )


# NOTE: Possible optimizations that could be implemented:
#   - clean graphs on contact end (delete vertex)
#   - consider converting contact identifiers to hashable attr.s
class SABRNode(sabr.SABRNodeBase):
    """A Node allowing to use SABR."""

    # True: Use SCGR "limiting contact", False: Use Yen's KSP algorithm.
    use_limiting_contact = True

    def initialize_cgr_data(self, ptvg, update_min_interval, own_eid):
        # Pre-initialize graph data dict while merging based on timestamp
        generation_start_times = []
        for edge, predicted_contact_list in ptvg.edges.items():
            for predicted_contact in predicted_contact_list:
                for generation in predicted_contact.generations:
                    generation_start_times.append(generation.valid_from)

        # Initialize empty instances based on user-defined granularity
        graph_data = {}
        last_ts = float("-inf")
        for ts in sorted(generation_start_times):
            if ts - last_ts > update_min_interval:
                graph_data[ts] = SABRGraphData(
                    start_time=ts,
                )
                last_ts = ts

        logger.debug("Will generate %d graph(s) for node %s",
                     len(graph_data), own_eid)

        # Add ContactGraph to instances
        for gen_start_time, gd in graph_data.items():
            gd.graph = igraph.Graph(directed=True)

            # We first create ``ContactIdentifier`` tuples that provide
            # information to the Dijkstra route search from the PTVG.
            # We, then, add a mapping of the ``ContactIdentifier`` tuples
            # to new ``CGRContact`` instances.
            vertex_list = []
            for edge, predicted_contact_list in ptvg.edges.items():
                tx_node, rx_node = edge
                for predicted_contact in predicted_contact_list:
                    # A hashable tuple representation, used for indexing
                    # (This calculates an average bit rate.)
                    contact_identifier = predicted_contact.to_simple(
                        generation_at=gen_start_time,
                        characteristics_at=None,
                    )
                    if tx_node == own_eid:
                        if rx_node not in gd.neighbor_contact_lists:
                            gd.neighbor_contact_lists[rx_node] = []
                        gd.neighbor_contact_lists[rx_node].append(
                            contact_identifier
                        )
                    gd.contact_plan.append(contact_identifier)
                    gd.contact_vid_mapping[contact_identifier] = len(
                        gd.contact_plan
                    ) - 1
                    vertex_list.append(str(predicted_contact))
            gd.graph.add_vertices(vertex_list)
            for _ in range(ASSERT_RANGE):
                i = random.randrange(len(vertex_list))
                assert gd.graph.vs[i]["name"] == vertex_list[i]

            # Add notional vertices
            for node in ptvg.vertices:
                contact_identifier = SimplePredictedContactTuple(
                    tx_node=node,
                    rx_node=node,
                    start_time=0,
                    end_time=math.inf,
                    bit_rate=math.inf,
                    probability=1.0,
                    delay=0,
                )
                gd.contact_plan.append(contact_identifier)
                gd.contact_vid_mapping[contact_identifier] = len(
                    gd.contact_plan
                ) - 1
                gd.graph.add_vertex(name=node)

            self._add_edges_to_contact_graph(
                gd.graph,
                gd.contact_plan,
                gd.contact_vid_mapping,
            )

        logger.info(
            "Initialized %d graph(s) for node %s", len(graph_data), self.eid,
        )

        return StartTimeBasedDict(graph_data)

    def _add_edges_to_contact_graph(self, graph, vid_contact_list,
                                    contact_vid_mapping):
        # NOTE: Based on pydtnsim.contact_graph
        todo_out = set(vid_contact_list)

        # Now that we have all nodes, start generating the edges which is quite
        # expensive but we only have to do it once for all nodes and all times
        # (as long as the contact plan is not changing)
        edge_seq = []
        cost_seq = []
        for vid, contact in enumerate(vid_contact_list):
            # Remove the currently investigated node
            todo_out.remove(contact)
            for candidate in todo_out:
                # Check if the end node of the first contact is the start node
                # of the second contact and the next contact is not returning
                # to the initial node
                if (contact.rx_node == candidate.tx_node
                        and contact.tx_node != candidate.rx_node):
                    # If that is the case, evaluate if the timing adds up
                    if candidate.end_time > contact.start_time:
                        # Add edge from contact to candidate
                        # (directed, by adding link to candidate to
                        # successor list of contact), also add contact to list
                        # of predecessors of candidate
                        edge_seq.append((
                            vid,
                            contact_vid_mapping[candidate],
                        ))
                        cost_seq.append(
                            _get_sabr_cost(contact, candidate)
                        )
                # Also check if the end node of the second contact is the
                # start node of the first contact and the next contact is not
                # returning to the initial node
                elif (candidate.rx_node == contact.tx_node
                      and candidate.tx_node != contact.rx_node):
                    # If that is the case, evaluate if the timing adds up
                    if contact.end_time > candidate.start_time:
                        # Add edge from contact to candidate
                        # (directed, by adding link to candidate to
                        # successor list of contact), also add contact to list
                        # of predecessors of successor_candidate
                        edge_seq.append((
                            contact_vid_mapping[candidate],
                            vid,
                        ))
                        cost_seq.append(
                            _get_sabr_cost(candidate, contact)
                        )

        graph.add_edges(edge_seq)
        graph.es["cost"] = cost_seq

        for _ in range(ASSERT_RANGE):
            i = random.randrange(len(edge_seq))
            assert graph.es[i].tuple == edge_seq[i]

    def route_generator_factory(self, gd, source_eid, dest_eid, excluded_eids):
        root_contact = SimplePredictedContactTuple(
            tx_node=source_eid,
            rx_node=source_eid,
            start_time=0,
            end_time=math.inf,
            bit_rate=math.inf,
            probability=1.0,
            delay=0,
        )
        destination_contact = SimplePredictedContactTuple(
            tx_node=dest_eid,
            rx_node=dest_eid,
            start_time=0,
            end_time=math.inf,
            bit_rate=math.inf,
            probability=1.0,
            delay=0,
        )

        root_vid = gd.contact_vid_mapping[root_contact]
        destination_vid = gd.contact_vid_mapping[destination_contact]

        if self.use_limiting_contact:
            return _route_generator_wrapper(
                gd.contact_plan,
                _limit_contact_igraph(
                    gd.graph,
                    root_vid,
                    destination_vid,
                    math.inf,
                    "cost",
                    gd.contact_plan,
                ),
            )
        else:
            # 3.2.6.10
            return _route_generator_wrapper(
                gd.contact_plan,
                _yen_igraph(
                    gd.graph,
                    root_vid,
                    destination_vid,
                    math.inf,
                    "cost",
                ),
            )


def _route_generator_wrapper(vid_contact_list, gen):
    for vid_path, _ in gen:
        contact_path = [vid_contact_list[vid] for vid in vid_path]
        # The bundle has to take at least one contact to be delivered.
        # Else we should not be even getting here...
        assert len(contact_path) >= 3
        # Assert first contact is notional
        assert math.isinf(contact_path[0].end_time)
        # Assert last contact is notional
        assert math.isinf(contact_path[-1].end_time)
        # Remove notional contacts
        contact_path = contact_path[1:-1]
        yield contact_path


# 3.2.6.10
def _get_sabr_cost(edge_head_contact, edge_tail_contact):
    # OPTIMIZATION: Edges with a negligible bit rate are unavailable.
    # NOTE: Without this, a divide by zero happens.
    # TODO: Figure out where SABR in ION excludes zero-capacity routes...
    if edge_tail_contact.bit_rate < 1e-6:
        return math.inf

    head_contact_earliest_arrival_time = sabr._get_earliest_arrival_time(
        edge_head_contact,
        # WARNING: The real ETT cannot be computed for a contact in advance
        #   as it depends on the route which leads to the vertex (contact)!
        0,
    )
    # WARNING: SABR (3.2.6.10): "For this purpose, the cost of edge N shall be
    #   the earliest arrival time of the contact that is the vertex in which
    #   edge N terminates." -- As the EAT is a timestamp this leads to very
    #   bad performance (routes with fewer hops get a huge preference, delays
    #   rise, sim. runtime rises, ...) -- try it out yourself.
    #   For now, this assumes an error in the spec.
    return sabr._get_earliest_arrival_time(
        edge_tail_contact,
        head_contact_earliest_arrival_time,
    ) - head_contact_earliest_arrival_time


def _path_cost(graph, path, weights=None):
    pathcost = 0
    for i in range(len(path)):
        if i > 0:
            eid = graph.get_eid(path[i - 1], path[i], error=True)
            edge = graph.es[eid]
            if weights is not None:
                pathcost += edge[weights]
            else:
                # just count the number of edges
                pathcost += 1
    return pathcost


# Adopted from: https://gist.github.com/ALenfant/5491853
# TODO: Write a test for this function!
# TODO: Evaluate for probcgr to split Yen's set "B" (see Wiki) in two like
#   MaxProp does -- highly probable vs. fast -- select alternatingly?
def _yen_igraph(graph, source, target, num_k, weights):

    # Shortest path from the source to the target
    A = [
        graph.get_shortest_paths(
            source,
            to=target,
            weights=weights,
            output="vpath",
        )[0]
    ]
    A_costs = [_path_cost(graph, A[0], weights)]

    yield A[0], A_costs[0]

    # Initialize the heap to store the potential kth shortest path
    B = queue.PriorityQueue()

    iterator = itertools.count(1) if math.isinf(num_k) else range(1, num_k)
    for k in iterator:
        # The spur node ranges from the first node to the next to last node in
        # the shortest path
        for i in range(len(A[k - 1]) - 1):
            # Spur node is retrieved from the previous k-shortest path, k − 1
            spurNode = A[k - 1][i]
            # The sequence of nodes from the source to the spur node of the
            # previous k-shortest path
            rootPath = A[k - 1][:i]

            # We store the removed edges
            removed_edges = []

            for path in A:
                if len(path) - 1 > i and rootPath == path[:i]:
                    # Remove the links that are part of the previous shortest
                    # paths which share the same root path
                    eid = graph.get_eid(path[i], path[i + 1], error=False)
                    if eid == -1:
                        continue  # edge already deleted
                    edge = graph.es[eid]
                    removed_edges.append(
                        (path[i], path[i + 1], edge.attributes())
                    )
                    edge[weights] = math.inf

            # Calculate the spur path from the spur node to the sink
            spurPath = graph.get_shortest_paths(
                spurNode,
                to=target,
                weights=weights,
                output="vpath",
            )[0]

            if len(spurPath) > 0:
                # Entire path is made up of the root path and spur path
                totalPath = rootPath + spurPath
                totalPathCost = _path_cost(graph, totalPath, weights)
                # Add the potential k-shortest path to the heap
                B.put((totalPathCost, totalPath))

            # Add back the edges that were removed from the graph
            for removed_edge in removed_edges:
                node_start, node_end, cost = removed_edge
                eid = graph.get_eid(node_start, node_end, error=True)
                edge = graph.es[eid]
                edge.update_attributes(cost)

        # Sort the potential k-shortest paths by cost
        # B is already sorted
        # Add the lowest cost path becomes the k-shortest path.
        while True:
            try:
                cost_, path_ = B.get(timeout=0)
            except queue.Empty:
                # No more routes
                return
            if path_ not in A:
                # We found a new path to add
                A.append(path_)
                A_costs.append(cost_)
                break

        yield path_, cost_


# This uses the same "limiting contact" approach as S-CGR.
def _limit_contact_igraph(graph, source, target, num_k, weights,
                          vid_contact_list):
    excluded_vertices = []

    while True:
        # Excude edges to excluded vertices by setting the cost to infinite.
        exclusion_list = []
        for vid in excluded_vertices:
            excluded_edges = graph.incident(vid, mode=igraph.IN)
            for eid in excluded_edges:
                edge = graph.es[eid]
                exclusion_list.append((eid, edge.attributes()))
                edge[weights] = math.inf

        vid_path = graph.get_shortest_paths(
            source,
            to=target,
            weights=weights,
            output="vpath",
        )[0]

        contact_path = [vid_contact_list[vid] for vid in vid_path]
        # Determine the time at which the route becomes invalid.
        route_end_time = math.inf
        limiting_contact_index = None
        for i, contact in enumerate(contact_path):
            if contact.end_time >= route_end_time:
                continue
            route_end_time = contact.end_time
            limiting_contact_index = i
        excluded_vertices.append(vid_path[limiting_contact_index])

        # Restore edge weights.
        for eid, cost in exclusion_list:
            edge = graph.es[eid]
            edge.update_attributes(cost)

        # NOTE: Cost is not supported currently.
        yield vid_path, math.inf
