# encoding: utf-8

"""Module providing a probabilistic Contact Graph Routing algorithm."""

import math

from pydtnsim import ContactIdentifier
from pydtnsim.routing import dijkstra
from pydtnsim.routing import cgr_utils
from pydtnsim.routing.scgr import (
    Neighbor,
    Route,
    RouteListEntry,
    AvgRouteDeliveryTime,
)


def probscgr(bundle,
             source_node,
             contact_graph,
             route_list,
             contact_list,
             current_time,
             limbo,
             target_confidence,
             route_min_confidence,
             whole_route_prob,
             default_limiting_contact):
    """Core routing function of Prob-SCGR implementation.

     Enqueues a bundle (packet) into a contact queue base on CGR.

    Args:
        bundle (Packet): The Packet object that should be routed.
        source_node (string): The originating node (i.e. the node where the
            routing decision is performed)
        contact_graph (dict): The contact graph object that provides the
            topological information.
        route_list (list): Cache list to store already found routes inbetween
            routing decisions (whenever possible).
        contact_list (list): List of :mod:`Contact` objects that will be used
            for enqueuing the packet.
        current_time (int): The time of the routing decision (in ms).
        limbo (list): A node-based list where unroutable packets are enqueued
            (and in the best case considered again at a later point)
        hot_spots (list): The list of hot spots n the network. Required to
            prevent a frequent cause for loops. Defaults to none.

    """
    # Reset the list of the excluded nodes
    excluded_nodes = []

    # Check if the bundle prevents returning it to the sender node, if that is
    # the case, add sender to list of excluded nodes
    if not bundle.return_to_sender and bundle.hops:
        excluded_nodes = [bundle.hops[-1]]
    else:
        excluded_nodes = []

    # Identify the neighbor with the best feasible route to the destination
    neighbors = identify_best_feasible_routes(
        source_node, bundle, contact_graph, route_list, excluded_nodes,
        current_time, contact_list,
        target_confidence=target_confidence,
        route_min_confidence=route_min_confidence,
        whole_route_prob=whole_route_prob,
        default_limiting_contact=default_limiting_contact,
    )

    # Check if feasible next hop has been found, if so enqueue the packet
    if neighbors:
        enq_ctr = 0
        for neighbor in neighbors:
            enq_ctr += 1 if contact_list[neighbor.contact].enqueue_packet(
                bundle, neighbor.route
            ) else 0
        return enq_ctr

    limbo.append(bundle)
    return 0


def identify_best_feasible_routes(source_node,
                                  bundle,
                                  contact_graph,
                                  route_list,
                                  excluded_nodes,
                                  current_time,
                                  contact_list,
                                  target_confidence,
                                  route_min_confidence,
                                  whole_route_prob,
                                  default_limiting_contact,
                                  single_hop_prob=False):
    """Get the best feasible routes for the specific time and packet."""
    # Load already found routes from dictionary or initialize otherwise
    if bundle.end_node in route_list:
        # Extract already computed values
        route_list_ref = route_list[bundle.end_node].list
    else:
        # Create route list entry
        route_list_ref = []
        route_list[bundle.end_node] = RouteListEntry(
            list=route_list_ref,
            excluded_contacts=[],
            avg_rdt=AvgRouteDeliveryTime()
        )

    while True:
        returned_contacts = []
        returned_routes = []
        # ### Probability that _all_ selected routes fail
        failure_prob = 1.0
        for route in route_list_ref:
            # If route to time (i.e. minimal end time of all individual route
            # contacts) is in the past, ignore the route
            if route.to_time <= current_time:
                continue

            # If route arrival time (the time that the bundle would reach the
            # desination) is larger than the deadline, ignore the route (i.e.
            # we are considering the deadlines to be hard deadlines)
            if route.edt >= bundle.deadline:
                continue

            # If the route's capacity is smaller than the bundle/packet size,
            # ignore the route
            if route.capacity < bundle.size:
                continue

            # If the next (i.e. first) hop of a route is in the excluded_nodes
            # list, ignore the route
            if route.transmission_plan[0].to_node in excluded_nodes:
                continue

            first_contact = contact_list[route.transmission_plan[0]]

            # If the queue for a particular contact is already full (i.e.
            # cannot house the currently routed bundle), ignore the route
            if not first_contact.is_capacity_sufficient(bundle):
                continue

            # ### Probability calculation
            if whole_route_prob:
                route_success_prob = 1.0
                for cont in route.transmission_plan:
                    route_success_prob *= contact_list[cont].get_confidence()
            else:
                route_success_prob = first_contact.get_confidence()
            if route_success_prob < route_min_confidence:
                continue

            # Found a route
            returned_routes.append(Neighbor(
                contact=route.transmission_plan[0],
                node_id=route.next_hop,
                route=route))
            returned_contacts.append(route.transmission_plan[0])
            failure_prob *= (1 - route_success_prob)
            if (1 - failure_prob) >= target_confidence:
                return returned_routes

        # Reload one more route
        new_route = generate_next_route(
            contact_graph, source_node, bundle.end_node,
            route_list[bundle.end_node].excluded_contacts, current_time,
            route_list[bundle.end_node].avg_rdt)

        # Goal cannot be achieved -> best effort (use all we have)
        if new_route is None:
            return returned_routes

        # ###
        # Determine limiting contact: contact with least probability
        limit_contact = None
        if default_limiting_contact:
            for contact in new_route.transmission_plan:
                if contact.to_time == new_route.to_time:
                    limit_contact = contact
                    break
        else:
            # probabilistic LC
            mincnf = 1.01
            for contact in new_route.transmission_plan:
                # NOTE that this may remove routes in a non-ideal way if using
                # differing message sizes...
                cnf = contact_list[contact].get_confidence()
                if cnf < mincnf:
                    mincnf = cnf
                    limit_contact = contact
        # ###

        # Raise error if we couldn't find a contact matching to the calculated
        # to_time
        if limit_contact is None:
            raise ValueError("No limiting contact found")

        # Just make sure that we get sound routes
        assert (new_route.edt >= max(
            [c.from_time for c in new_route.transmission_plan]))

        route_list_ref.append(new_route)
        route_list[bundle.end_node].excluded_contacts.append(
            limit_contact)


def generate_next_route(contact_graph, source_node, destination_node,
                        excluded_contacts, current_time, avg_rdt):
    """Generate an additional (next best) route to the destination."""
    # Generate root_contact terminal node definition
    root_contact = ContactIdentifier(
        from_node=source_node,
        to_node=source_node,
        from_time=0,
        to_time=math.inf,
        datarate=math.inf,
        delay=0)
    # Generate bundle's destination terminal node definition
    destination_contact = ContactIdentifier(
        from_node=destination_node,
        to_node=destination_node,
        from_time=0,
        to_time=math.inf,
        datarate=math.inf,
        delay=0)

    if avg_rdt.mean >= 0:
        lookahead_time = int(current_time + avg_rdt.mean * 1.5)
    else:
        lookahead_time = 86400

    # Determine the shortest route to the bundle's destination in the
    # contact graph representation of the contact plan with a lookahead
    # window limited to the 120% of the previously observed mean delivery
    # times.
    transmission_plan, distance = dijkstra.get_best_route(
        root_contact,
        destination_contact,
        contact_graph,
        cgr_utils.cgr_neighbor_function,
        current_time,
        hashes=contact_graph.hashes,
        suppressed_contacts=excluded_contacts,
        lookahead_time=lookahead_time)

    # If no route has been found in the lookahead window, fall back to the
    # version without the window.
    if transmission_plan is None:
        # Determine the shortest route to the bundle's destination in the
        # contact graph representation of the contact plan without any
        # lookahead window.
        transmission_plan, distance = dijkstra.get_best_route(
            root_contact,
            destination_contact,
            contact_graph,
            cgr_utils.cgr_neighbor_function,
            current_time,
            hashes=contact_graph.hashes,
            suppressed_contacts=excluded_contacts)

        if transmission_plan is None:
            # End the route finding process if no route is found. This means no
            # more routes are available in the contact graph
            return None

        avg_rdt.window_miss += 1
        avg_rdt.count += 1
        avg_rdt.mean += (distance - current_time) / avg_rdt.count
    else:
        avg_rdt.window_hit += 1
        avg_rdt.count += 1
        avg_rdt.mean += (distance - current_time) / avg_rdt.count

    # Remove unnecessary root and terminal nodes from the route
    transmission_plan = transmission_plan[1:-1]

    # Generate route characteristics and add this information to the route
    edt, cap, to_time = cgr_utils.cgr_get_route_characteristics(
        transmission_plan, distance)

    # Assign the found route as option for the bundle's destination to the
    # route list
    route = Route(
        transmission_plan=transmission_plan,
        next_hop=transmission_plan[0].to_node,
        edt=edt,
        capacity=cap,
        to_time=to_time,
        hops=len(transmission_plan))

    # Supress the limit contact as we have already found the best route for
    # that limiting contact and worse routes with that limiting contact
    # should not be considered (realised by setting the suppressed flag in
    # the working area of the contact)

    # Return the route list with the updated routes for the bundle's
    # destination node
    return route
