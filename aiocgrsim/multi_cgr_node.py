# encoding: utf-8

"""Module providing a node allowing multiple routes via a PTVG-based router."""

import asyncio

from .cgr_node import CGRNode


class MultiCGRNode(CGRNode):
    """A Node implementation allowing to run probabilistic CGR."""

    def __init__(self, eid, buffer_size, event_dispatcher,
                 routing_algorithm, ptvg, graph_update_min_interval, **kwargs):
        super().__init__(eid, buffer_size, event_dispatcher, routing_algorithm,
                         ptvg, graph_update_min_interval, **kwargs)
        self.sched_refcount = {}

    def store_and_schedule(self, message, tx_node, re_schedule=False):
        """Store the provided message in the buffer and try to schedule it."""
        loop_time = asyncio.get_running_loop().time()
        assert loop_time >= message.start_time
        if re_schedule and loop_time > message.deadline:
            # Can be the case under re-scheduling
            self._drop_copy(message)
            return
        assert loop_time <= message.deadline
        if (message in self.sched_refcount and self.sched_refcount[message]
                and not re_schedule):
            # We have it already -> reject
            self.event_dispatcher.message_rejected(self, message)
            return
        limbo_len = len(self.limbo)
        # get the correct graph...
        cd = self.pydtnsim_cgr_data.get_entry_and_drop_predecessors(loop_time)
        # NOTE: This will call contact.enqueue_packet!
        route_count = self.routing_algorithm(
            message,
            self.eid,
            cd.graph,  # this is the graph!
            cd.route_list,
            cd.contact_dict,
            loop_time,
            self.limbo,
        )
        assert len(self.limbo) >= limbo_len
        if route_count:
            cur_ref = self.sched_refcount.get(message, 0)
            assert cur_ref == 0 or re_schedule
            self.sched_refcount[message] = cur_ref + route_count
            if not re_schedule:
                self.buf.add(message)
            # Message has been scheduled
            self.event_dispatcher.message_scheduled(self, message)
            # In case get_messages is waiting, unblock it.
            self.set_message_scheduled()
        else:
            if re_schedule:
                self._drop_copy(message)
            # Message has been added to limbo -> reject
            self.event_dispatcher.message_rejected(self, message)

    def _message_transmitted(self, message, rx_node):
        self._drop_copy(message)

    def _drop_copy(self, message):
        if self.sched_refcount[message]:
            self.sched_refcount[message] -= 1
        if not self.sched_refcount[message]:
            # Message was sent via all channels, drop it
            if self.buf.contains(message):
                self.event_dispatcher.message_deleted(self, message)
                # message may be dropped due to expiry
                self.buf.remove(message)
