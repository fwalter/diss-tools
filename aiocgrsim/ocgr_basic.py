# encoding: utf-8

"""Module providing the pydtnsim-compatible opportunistic CGR algorithm."""

# NOTE: Based on cgr_basic router from pydtnsim, check there for documentation.

from collections import namedtuple

from pydtnsim.routing.cgr_basic import load_route_list

Neighbor = namedtuple('Neighbor', [
    'contact',
    'node_id',
    'route',
    'max_confidence',
])


def ocgr(bundle,
         source_node,
         contact_graph,
         route_list,
         contact_list,
         current_time,
         limbo,
         target_confidence):
    """Opportunistic CGR (O-CGR) routing function."""
    # NOTE: In this function we expect that the route list is reseted manually
    # whenever the contact graph changes, thus no reset mechanism is
    # implemented.

    # Check if the bundle prevents returning it to the sender node, if that is
    # the case, add sender to list of excluded nodes
    if not bundle.return_to_sender and bundle.hops:
        excluded_nodes = [bundle.hops[-1]]
    else:
        excluded_nodes = []

    # Identify feasible proximate neighbor nodes
    # NOTE: This is a list of neighboring network nodes plus one best route
    # (regarding EDT) for each of them
    proximate_nodes = identify_proximate_node_list(
        source_node, bundle, contact_graph, route_list, excluded_nodes,
        current_time, contact_list)

    selected_routes = []
    delivery_confidence = 0
    while proximate_nodes and delivery_confidence < target_confidence:
        # Reset next hop optimization variable
        next_hop = None
        # Iterate over all returned proximate nodes (or rather, routes) and
        # select the best (first based on EDT, then on Hops)
        for neighbor in proximate_nodes:

            # If no value was set yet, take the first available one
            if not next_hop:
                next_hop = neighbor

            # If the edt of the iterated node is better than the saved value,
            # update the saved optimization variable
            elif neighbor.route.edt < next_hop.route.edt:
                next_hop = neighbor
            # Otherwise continue
            elif neighbor.route.edt > next_hop.route.edt:
                continue

            # If the hops of the iterated node are less than the saved value,
            # update the saved optimization variable
            elif neighbor.route.hops < next_hop.route.hops:
                next_hop = neighbor
            elif neighbor.route.hops > next_hop.route.hops:
                continue

            # Consistency feature for SCGR. This condition is not existent in
            # vanilla cgr and is added to provide the same routing decisions as
            # SCGR. Basically, before deciding on the basis of the hash value
            # we also make the start time of the next contact (i.e. when the
            # packet is leaving the node) a parameter.
            elif neighbor.contact.from_time < next_hop.contact.from_time:
                next_hop = neighbor
            elif neighbor.contact.from_time > next_hop.contact.from_time:
                continue

            # If all characteristics were identical before, make decision based
            # on the hashes of the node id (names).
            elif hash(neighbor.node_id) < hash(next_hop.node_id):
                next_hop = neighbor

        if not next_hop:
            continue

        selected_routes.append(next_hop)
        proximate_nodes.remove(next_hop)

        # ### O-CGR
        # "bundle delivery confidence K is increased by increment J, given by
        # J = 1 - ((1 - K) * (1 - D)) where D is the maximum confidence"
        ocgr_d = next_hop.max_confidence ** bundle.size  # D
        delivery_confidence += 1 - ((1 - delivery_confidence) * (1 - ocgr_d))

    # Check if feasible next hop has been found, if so enqueue the packet for
    # the contact
    for next_hop in selected_routes:
        # Enqueue
        contact_list[next_hop.contact].enqueue_packet(bundle, next_hop.route,
                                                      None)
    if not selected_routes:
        limbo.append(bundle)
    return len(selected_routes)


def identify_proximate_node_list(source_node, bundle, contact_graph,
                                 route_list, excluded_nodes, current_time,
                                 contact_list):
    """Compile a list of feasible neighbor nodes."""
    proximate_nodes = list()

    # If the route list for a particular destination is empty, (re-)load the
    # route list by invoking load_route_list for that destination
    if bundle.end_node not in route_list.keys() or route_list[bundle.
                                                              end_node] == []:
        route_list[bundle.end_node] = load_route_list(
            contact_graph, source_node, bundle.end_node, current_time)

    for route in route_list[bundle.end_node]:
        # If route to time (i.e. minimal end time of all individual route
        # contacts) is in the past, ignore the route
        if route.to_time <= current_time:
            continue

        # If route arrival time (the time that the bundle would reach the
        # desination) is larger than the deadline, ignore the route (i.e. we
        # are considering the deadlines to be hard deadlines)
        if route.edt >= bundle.deadline:
            continue

        # If the route's capacity is smaller than the bundle/packet size,
        # ignore the route
        if route.capacity < bundle.size:
            continue

        # If the next (i.e. first) hop of a route is in the excluded_nodes
        # list, ignore the route
        if route.transmission_plan[0].to_node in excluded_nodes:
            continue

        # ###
        first_contact = contact_list[route.transmission_plan[0]]

        # If the queue for a particular contact is already full (i.e. cannot
        # house the currently routed bundle), ignore the route
        if not first_contact.is_capacity_sufficient(bundle):
            continue

        # The route confidence is the expected/estimated delivery probability
        # over the first contact for a message/bundle of the given size.
        route_confidence = first_contact.get_confidence()

        # Iterate over all already detected proximate nodes to optimize the
        # characteristics of the proximate nodes if they are also used by the
        # investigated route and the routes characteristics are better
        for proximate_node in proximate_nodes:
            # Only consider updating node characteristics if the proximate node
            # is the first hop of the investigated route
            if proximate_node.node_id == route.next_hop:
                # ### O-CGR: Update maximum confidence of the route
                # "D is the maximum confidence value among all
                # pertinent routes for which this node is the entry node"
                max_confidence = max(
                    proximate_node.max_confidence,
                    route_confidence,
                )

                # If the arrival time of the investigated route is better than
                # the values saved for the current route of the proximate node,
                # then replace the characteristics with the new route by
                # deleting and reinserting the list item
                if proximate_node.route.edt > route.edt:
                    proximate_nodes.remove(proximate_node)
                    proximate_nodes.append(
                        Neighbor(
                            contact=route.transmission_plan[0],
                            node_id=route.next_hop,
                            route=route,
                            max_confidence=max_confidence))

                # If the arrival time of the investigated route is worse,
                # ignore route and continue
                elif proximate_node.route.edt < route.edt:
                    continue

                # If more hops are required for the currently linked proximate
                # node route, then update the proximate node characteristics to
                # the current route by deleting and reinserting the list item
                elif proximate_node.route.hops > route.hops:
                    proximate_nodes.remove(proximate_node)
                    proximate_nodes.append(
                        Neighbor(
                            contact=route.transmission_plan[0],
                            node_id=route.next_hop,
                            route=route,
                            max_confidence=max_confidence))

                # If less hops are required with the currently linked route,
                # continue
                elif proximate_node.route.hops < route.hops:
                    continue

                # As final optimization option, we use a hash of the node
                # identifier (the smaller the better)
                elif hash(proximate_node.node_id) > hash(route.next_hop):
                    proximate_nodes.remove(proximate_node)
                    proximate_nodes.append(
                        Neighbor(
                            contact=route.transmission_plan[0],
                            node_id=route.next_hop,
                            route=route,
                            max_confidence=max_confidence))

                # Stop iterating the proximate node list because the
                # corresponding proximate node (to the first hop of the route)
                # was found
                break

        # If the next hop of the route is not element of the proximate_nodes
        # list
        if (route.next_hop not in [
                neighbor.node_id for neighbor in proximate_nodes
        ]):
            proximate_node = Neighbor(
                contact=route.transmission_plan[0],
                node_id=route.next_hop,
                route=route,
                max_confidence=route_confidence)
            proximate_nodes.append(proximate_node)

    return proximate_nodes
