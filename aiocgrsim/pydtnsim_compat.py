# encoding: utf-8

"""Module providing support for pydtnsim's Contact Graph Routing algorithm."""

import asyncio
import dataclasses

from aiodtnsim import Message, EventHandler

from tvgutil.contact_plan import PredictedContact

from pydtnsim import ContactIdentifier


@dataclasses.dataclass(frozen=True)
class CGRPacket(Message):
    """An aiodtnsim.Message implementation supporting pydtnsim's CGR."""

    # TODO: SABR: "If the bundle is a non-critical bundle that was previously
    # forwarded to a node that refused custody (and is now being re-forwarded
    # due to that custody refusal), then backward propagation of the bundle
    # (that is, transmission of the bundle back to the node from which it was
    # directly received) is authorized [...]" -- NOTE we assume custody
    # acceptance ALWAYS (else, we would need "probe bundles" as well)
    return_to_sender: bool = False
    is_critical: bool = False
    # identifier: object = None
    hops: list = dataclasses.field(default_factory=list, compare=False)

    def add_planned_route(self, route):
        """Add a determined route. Not implemented."""

    def add_alternative_route(self, route):
        """Add an alternative route. Not implemented."""

    # NOTE that in pydtnsim, the unit of all time-related values
    # is ms, but this is not relevant for the algorithms as long as the
    # same unit is used everywhere.

    # A mapping of pydtnsim attribute names to aiodtnsim
    # attribute names follows.

    ATTR_MAPPING = {
        "source_node": "source",
        "end_node": "destination",
    }

    def __getattr__(self, name):
        """Map attribute names from pydtnsim.Packet to our names."""
        try:
            return getattr(self, CGRPacket.ATTR_MAPPING[name])
        except KeyError:
            raise AttributeError()


@dataclasses.dataclass
class CGRContact:
    """A simulator-independent replacement for pydtnsim.Contact."""

    source: str
    peer: str
    start_time: float
    end_time: float

    predicted_contact: PredictedContact = dataclasses.field(
        default_factory=PredictedContact,
        repr=False,
    )

    def __post_init__(self):
        """Perform post-initialization steps for dataclass."""
        # pylint does not know the dataclass __post_init__ method
        # pylint: disable=attribute-defined-outside-init
        self.queue = []
        self.next_enqueue_time = self.start_time
        # pylint: enable=attribute-defined-outside-init

    def is_capacity_sufficient(self, cgr_packet):
        """Check if the packet arrives before the end time if scheduled now."""
        return self._get_arrival_time(cgr_packet.size) <= self.end_time

    def enqueue_packet(self, cgr_packet, booked_route, best_route=None):
        """Store the given packet in the contact's queue."""
        arrival_time = self._get_arrival_time(cgr_packet.size)
        if arrival_time > self.end_time:
            return False
        self.queue.append(cgr_packet)
        self.next_enqueue_time = arrival_time
        return True

    def _get_arrival_time(self, msg_size):
        time = asyncio.get_running_loop().time()
        earliest_departure = max(time, self.next_enqueue_time)
        # As in "normal" CGR, we use the average transmission rate over the
        # contact, that is calculated using the volume determined from the
        # characteristics-intervals.
        gen = self.predicted_contact.get_generation_at(time)
        avg_bit_rate = gen.get_avg_bit_rate(self.predicted_contact)
        if avg_bit_rate == 0:
            return float("inf")
        # NOTE: Computed by pydtnsim this way (though, only for the check)
        transmit_duration = (
            int((msg_size / avg_bit_rate) * 1000) + 1
        ) / 1000
        return earliest_departure + transmit_duration

    def get_confidence(self):
        time = asyncio.get_running_loop().time()
        gen = self.predicted_contact.get_generation_at(time)
        return gen.probability


class CGRMessagePathEventHandler(EventHandler):
    """An EventHandler for updating the hop list in CGRPacket instances.

    This should always be used when employing a pydtnsim-based routing
    algorithm. These algorithms sometimes make use of the packet's ``hops``
    attribute which would not be updated otherwise.

    """

    def message_scheduled(self, time, node, message):
        """Add the current node to the message's hop list on scheduling."""
        # NOTE: pydtnsim adds hop only after scheduling
        message.hops.append(node.eid)


def preprocess_simpy_dtn_contact_plan(contact_plan):
    """Make a pydtnsim contact plan usable for aiodtnsim.

    Currently, this function only converts millisecond-based timestamps to
    second-based timestamps and adjusts the data rate accordingly.

    """
    processed_contacts = []
    for contact in contact_plan.plan["contacts"]:
        # ms -> s conversion for all fields
        processed_contacts.append(ContactIdentifier(
            from_node=contact.from_node,
            to_node=contact.to_node,
            from_time=(contact.from_time / 1000),
            to_time=(contact.to_time / 1000),
            datarate=(contact.datarate * 1000),  # bit/ms -> bit/s
            delay=(contact.delay / 1000),
        ))
    contact_plan.plan["contacts"] = processed_contacts
