# encoding: utf-8

"""A Node allowing to use pydtnsim's CGR implementation."""

import asyncio
import collections
import logging
import dataclasses

from typing import Optional

from aiodtnsim.simulation.scheduling_event_node import SchedulingEventNode
from aiodtnsim.timeutil import duration_until

from pydtnsim.contact_graph import ContactGraph
from pydtnsim.contact_plan import ContactIdentifier, ContactPlan

from predictutil import StartTimeBasedDict

from .pydtnsim_compat import CGRContact

logger = logging.getLogger(__name__)


@dataclasses.dataclass
class CGRGraphData:

    # Validity interval of the graph (prediction).
    start_time: float
    # This structure contains pydtnsim ContactIdentifiers to run Dijkstra.
    graph: Optional[ContactGraph] = dataclasses.field(
        default=None,
        compare=False,
    )
    # This dict associates every ContactIdentifier to a CGRContact.
    contact_dict: collections.OrderedDict = dataclasses.field(
        default_factory=collections.OrderedDict,
        compare=False,
    )
    # This dict caches pre-calculated CGR routes based on the graph.
    # Its keys are node eids, its values RouteListEntry.
    route_list: collections.OrderedDict = dataclasses.field(
        default_factory=collections.OrderedDict,
        compare=False,
    )


class CGRNode(SchedulingEventNode):
    """A Node allowing to use pydtnsim's CGR implementation."""

    def __init__(self, eid, buffer_size, event_dispatcher, routing_algorithm,
                 ptvg, graph_update_min_interval, **kwargs):
        """Initialize the CGR Node.

        Args:
            - eid: The Node's unique identifier.
            - buffer_size: The assumed simulated maximum buffer size in bits.
                           Can be set to -1 to assume an infinite buffer size.
            - event_dispatcher: The aiodtnsim event dispatcher to monitor the
                                simulation process.
            - routing_algorithm: A pydtnsim-compatible routing algorithm.
            - ptvg: The predicted time-varying graph.
            - graph_update_min_interval: The minimum interval, in seconds,
                                         between two graph updates.
            - link_factory: The (optional) callable to obtain a Link.

        """
        super().__init__(eid, buffer_size, event_dispatcher, **kwargs)
        self.routing_algorithm = routing_algorithm
        self.ptvg = ptvg
        # A list of bundles that are left behind
        self.limbo = []
        cgr_contact_dict = self._get_cgr_contacts(self.ptvg)
        self._initialize_graphs(
            self.ptvg,
            cgr_contact_dict,
            graph_update_min_interval,
        )
        self._initialize_queues(
            cgr_contact_dict,
            eid,
        )

    def _get_cgr_contacts(self, ptvg):
        # The contact_dict allows us to get the actual CGR contact
        # class, which supports things like scheduling data.
        # NOTE: The renaming of attributes comes from ``pydtnsim``.
        # Unfortunately, we cannot use ``pydtnsim.Contact``
        # itself because it is coupled tightly to the simulator instance
        # (makes calls into the managing object instance at runtime).
        return {
            (edge, predicted_contact.start_time): CGRContact(
                source=predicted_contact.tx_node,
                peer=predicted_contact.rx_node,
                start_time=predicted_contact.start_time,
                end_time=predicted_contact.end_time,
                predicted_contact=predicted_contact,
            )
            for edge, predicted_contact_list in ptvg.edges.items()
            for predicted_contact in predicted_contact_list
        }

    def _initialize_graphs(self, ptvg, cgr_contact_dict, update_min_interval):
        generation_start_times = []
        # Pre-initialize graph data dict while merging based on timestamp
        for edge, predicted_contact_list in ptvg.edges.items():
            for predicted_contact in predicted_contact_list:
                for generation in predicted_contact.generations:
                    generation_start_times.append(generation.valid_from)
        # Initialize empty instances based on user-defined granularity
        graph_data = {}
        last_ts = float("-inf")
        for ts in sorted(generation_start_times):
            if ts - last_ts > update_min_interval:
                graph_data[ts] = CGRGraphData(
                    start_time=ts,
                )
                last_ts = ts
        # Add pydtnsim ContactGraph to instances
        for gen_start_time, gd in graph_data.items():
            # Create a pydtnsim ContactGraph
            pydtnsim_contact_list = []
            # We first create ``ContactIdentifier`` tuples that provide
            # information to the Dijkstra route search from the PTVG.
            # We, then, add a mapping of the ``ContactIdentifier`` tuples
            # to new ``CGRContact`` instances which mock pydtnsim's
            # ``Contact``.
            for edge, predicted_contact_list in ptvg.edges.items():
                for predicted_contact in predicted_contact_list:
                    gen = predicted_contact.get_generation_at(
                        gen_start_time
                    )
                    contact_identifier = ContactIdentifier(
                        from_node=predicted_contact.tx_node,
                        to_node=predicted_contact.rx_node,
                        from_time=predicted_contact.start_time,
                        to_time=predicted_contact.end_time,
                        datarate=gen.get_avg_bit_rate(predicted_contact),
                        delay=gen.get_avg_delay(predicted_contact),
                    )
                    pydtnsim_contact_list.append(contact_identifier)
                    gd.contact_dict[contact_identifier] = cgr_contact_dict[
                        (edge, predicted_contact.start_time)
                    ]

            # Initialize empty pydtnsim ContactPlan instance
            pydtnsim_contact_plan = ContactPlan(1, 0, None, None)
            # Fill in data from PTVG
            pydtnsim_contact_plan.plan["nodes"] = list(ptvg.vertices)
            pydtnsim_contact_plan.plan["contacts"] = pydtnsim_contact_list
            # Convert contact plan to contact graph
            gd.graph = ContactGraph(pydtnsim_contact_plan)

        logger.info(
            "Initialized %d graph(s) for node %s", len(graph_data), self.eid,
        )
        self.pydtnsim_cgr_data = StartTimeBasedDict(graph_data)

    def _initialize_queues(self, cgr_contact_dict, eid):
        # A mapping of the possible receivers of data to the predictions
        rx_nodes_to_contacts = {}
        for key, cgr_contact in cgr_contact_dict.items():
            (tx_node, rx_node), start_time = key
            if tx_node != eid:
                continue
            if rx_node not in rx_nodes_to_contacts:
                rx_nodes_to_contacts[rx_node] = []
            rx_nodes_to_contacts[rx_node].append(cgr_contact)
        for _, contacts in rx_nodes_to_contacts.items():
            contacts.sort(key=lambda c: c.start_time)
        self.rx_nodes_to_contacts = rx_nodes_to_contacts

    def store_and_schedule(self, message, tx_node, re_schedule=False):
        """Store the provided message in the buffer and try to schedule it."""
        loop_time = asyncio.get_running_loop().time()
        assert loop_time >= message.start_time
        if re_schedule and loop_time > message.deadline:
            # Can be the case unde re-scheduling
            self.event_dispatcher.message_dropped(self, message)
            self.buf.remove(message)
            return
        assert loop_time <= message.deadline
        limbo_len = len(self.limbo)
        # get the correct graph...
        cd = self.pydtnsim_cgr_data.get_entry_and_drop_predecessors(loop_time)
        # NOTE: This will call contact.enqueue_packet!
        self.routing_algorithm(
            message,
            self.eid,
            cd.graph,  # this is the graph!
            cd.route_list,
            cd.contact_dict,
            loop_time,
            self.limbo,
        )
        assert len(self.limbo) >= limbo_len
        if len(self.limbo) == limbo_len:
            # Accepted!
            if not re_schedule:
                self.buf.add(message)
            # Message has been scheduled
            self.event_dispatcher.message_scheduled(self, message)
            # In case get_messages is waiting, unblock it.
            self.set_message_scheduled()
        else:
            if re_schedule:
                self.buf.remove(message)
            # Message has been added to limbo -> reject
            self.event_dispatcher.message_rejected(self, message)

    async def get_messages(self, rx_node):
        """Asynchronously yield messages to be transmitted."""
        # we run in an endless loop - even if no messages can be sent
        # anymore, we await the next reception event
        contacts = self.rx_nodes_to_contacts[rx_node.eid]
        while True:
            await self._wait_until_transmission_can_start(contacts)
            for contact in contacts:
                # We send all messages queued for the neighbor node for
                # contacts that started in the past.
                # TODO: Check whether there should be a queue just per neighbor
                time = asyncio.get_running_loop().time()
                if contact.start_time > time:
                    break
                while contact.queue:
                    message = contact.queue.pop(0)
                    if not self.buf.contains(message):
                        # message may be dropped due to expiry
                        continue
                    try:
                        yield message, None
                    except GeneratorExit:
                        # Re-schedule aborted message
                        self.store_and_schedule(message, None, True)
                        raise
                    else:
                        self._message_transmitted(message, rx_node)

    async def _wait_until_transmission_can_start(self, contacts):
        # XXX This may be simplified/optimized! Probably it is sufficient to
        # always yield all messages for a given rx_node, regardless whether
        # they are scheduled in the future.
        # TODO: Check behavior in ION / SABR
        while True:
            # Search for next transmission time - check if we can start
            next_transmission_time = None
            for contact in contacts:
                if contact.queue:
                    # this assumes contacts are ordered by start_time
                    next_transmission_time = contact.start_time
                    break
            if next_transmission_time is None:
                # no contacts with rx_node which have data scheduled, wait
                await self.wait_until_new_message_scheduled()
            elif next_transmission_time > asyncio.get_running_loop().time():
                # wait until next contact is scheduled or a msg is received
                try:
                    await asyncio.wait_for(
                        self.wait_until_new_message_scheduled(),
                        timeout=duration_until(next_transmission_time),
                    )
                except asyncio.TimeoutError:
                    # If we wait, we _only_ return if there was a timeout
                    return
            else:
                # next_transmission_time is in the past
                return

    def _message_transmitted(self, message, rx_node):
        if self.buf.contains(message):
            self.event_dispatcher.message_deleted(self, message)
            # message may be dropped due to expiry
            self.buf.remove(message)
