#!/usr/bin/env python
# encoding: utf-8

import sys
import json

import networkx as nx
import matplotlib.pyplot as plt

from tvgutil import tvg

if len(sys.argv) < 2:
    print("Usage: {} <TVG file> [file]".format(sys.argv[0]))
    sys.exit(1)

with open(sys.argv[1], "r") as tvg_file:
    tvg_json = json.load(tvg_file)

xtvg = tvg.from_serializable(tvg_json)

# Create empty DiGraph object
G = nx.DiGraph()

# Add all nodes in the topology to G
for node in xtvg.vertices:
    G.add_node(node)

# Add edges between nodes that share at least one contact
for vertices, contacts in xtvg.edges.items():
    G.add_edge(vertices[0], vertices[1])


pos = nx.spring_layout(G, k=0.55)
pos_labels = {}
for k, v in pos.items():
    pos_labels[k] = (v[0], v[1] - 0.13)

fig = plt.figure()

nx.draw(
    G,
    pos=pos,
    node_color="0.2",
    edge_color="0.55",
)
nx.draw_networkx_labels(
    G,
    pos=pos_labels,
)
plt.axis("off")

if len(sys.argv) > 2:
    fig.set_size_inches(8, 5)
    plt.savefig(sys.argv[2])
else:
    plt.show()
