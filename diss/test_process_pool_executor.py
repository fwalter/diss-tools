#!/usr/bin/env python
# encoding: utf-8

"""Script to run a configurable simulation with aiodtnsim.

Example invocation:

    python test_process_pool_executor.py \
        --workers 24 \
        --scenarios "10gs10sat" "20gs20sat" \
        --algorithms "epidemic" "snw" \
        --runs 1 2 3 4 5 \
        "my-test-script.sh"

"""

import argparse
import asyncio
import collections
import datetime
import logging
import os
import signal
import sys
import textwrap
import tqdm

JobResult = collections.namedtuple("JobResult", [
    "scenario",
    "algo",
    "run",
    "returncode",
    "stdout",
    "stderr",
    "start_dt",
    "complete_dt",
])

logger = logging.getLogger(__name__)


async def execute_job(script, scenario, algo, run):
    logger.info(f"[{scenario}] Executing run #{run} for algo {algo}...")
    cmdline = [
        script,
        "job",
        scenario,
        algo,
        run,
    ]
    logger.debug(f"[{scenario}] Command: %s", " ".join(cmdline))
    process = await asyncio.create_subprocess_exec(
        *cmdline,
        stdout=asyncio.subprocess.PIPE,
        stderr=asyncio.subprocess.PIPE,
    )
    stdout, stderr = await process.communicate()
    if process.returncode == 0:
        logger.info(f"[{scenario}] Run #{run} for algo {algo} succeeded.")
    else:
        logger.warning(f"[{scenario}] Run #{run} for algo {algo} failed with "
                       f"code {process.returncode}.")
    return process.returncode, stdout, stderr


async def worker(script, queue, pbar=None):
    logger.debug(f"Spawned new worker.")
    results = []
    while True:
        job = await queue.get()
        # We immediately unblock the queue to allow that the next elements are
        # added to it by the distributor.
        queue.task_done()
        if not job:
            break
        scenario, algo, run = job
        start_dt = datetime.datetime.utcnow()
        returncode, stdout, stderr = await execute_job(
            script=script,
            scenario=scenario,
            algo=algo,
            run=run,
        )
        results.append(JobResult(
            scenario=scenario,
            algo=algo,
            run=run,
            returncode=returncode,
            stdout=stdout.decode("ascii", "ignore"),
            stderr=stderr.decode("ascii", "ignore"),
            start_dt=start_dt,
            complete_dt=datetime.datetime.utcnow(),
        ))
        if pbar is not None:
            pbar.update(1)
            pbar.refresh()
    return results


async def distribute_jobs(queue, scenarios, algos, runs, worker_count,
                          exit_signal):
    # Send jobs to workers
    for run in runs:
        # Add all jobs for one run to the queue.
        for scenario in scenarios:
            for algo in algos:
                await queue.put((
                    scenario,
                    algo,
                    run,
                ))
        # Wait until everything was taken out of the queue (a worker is ready).
        await queue.join()
        if exit_signal.is_set():
            break
    # Send termination requests
    for _ in range(worker_count):
        await queue.put(None)
    return None


async def execute_jobs(script, scenarios, algos, runs, worker_count, pbar,
                       exit_signal):
    # Job queue
    queue = asyncio.Queue()
    # Initialize workers
    worker_tasks = [
        worker(
            script=script,
            queue=queue,
            pbar=pbar,
        )
        for _ in range(worker_count)
    ]
    # The tasks to be executed include the distributor and all workers.
    tasks = asyncio.gather(
        distribute_jobs(
            queue=queue,
            scenarios=scenarios,
            algos=algos,
            runs=runs,
            worker_count=worker_count,
            exit_signal=exit_signal,
        ),
        *worker_tasks,
    )
    # Wait for tasks to finish
    task_results = await tasks
    # Aggregate results: The first element is of the job distributor, drop it.
    results = []
    for result in task_results[1:]:
        results.extend(result)
    # Evaluate results
    return all(r.returncode == 0 for r in results), results


async def handle_termination_request(task, exit_signal):
    # NOTE: Using cancel here _could_ work, but if the script is a Bash
    # script, its children will not die on termination. Probably the signal
    # is not propagated.
    logger.warning("Termination requested, waiting for run to end...")
    exit_signal.set()


def _main(args):
    _initialize_logger(args.verbose)

    script = os.path.abspath(args.SCRIPT)

    assert os.path.isfile(script), "SCRIPT has to exist"
    assert os.access(script, os.X_OK), "SCRIPT has to be executable"

    job_count = len(args.scenarios) * len(args.algorithms) * len(args.runs)

    logger.info(f"Executing {job_count} job(s) via {args.workers} workers...")

    executor_start_dt = datetime.datetime.utcnow()
    pbar = tqdm.tqdm(total=job_count) if args.progress else None

    exit_signal = asyncio.Event()

    loop = asyncio.get_event_loop()

    task = asyncio.Task(execute_jobs(
        script=script,
        scenarios=args.scenarios,
        algos=args.algorithms,
        runs=args.runs,
        worker_count=args.workers,
        pbar=pbar,
        exit_signal=exit_signal,
    ))

    def signal_handler(signum, frame):
        asyncio.run_coroutine_threadsafe(handle_termination_request(
            task,
            exit_signal,
        ), loop)

    signal.signal(signal.SIGUSR1, signal_handler)

    try:
        ok, results = loop.run_until_complete(task)
    finally:
        if pbar is not None:
            pbar.close()

    executor_end_dt = datetime.datetime.utcnow()
    if ok:
        logger.info("All jobs have been executed successfully.")
        logger.info(f"Total duration: {executor_end_dt - executor_start_dt}")
    else:
        failed_jobs = [
            job_result
            for job_result in results
            if job_result.returncode != 0
        ]
        logger.warning(f"{len(failed_jobs)} job(s) failed!")

    if args.logfile:
        # Write out results of all runs to detailed logfile.
        with open(args.logfile, "w") as log_file:
            log_file.write(
                f"Process pool execution results for script '{script}', "
                f"executed via {args.workers} worker(s), "
                f"started: {executor_start_dt} (UTC), "
                f"completed: {executor_end_dt} (UTC), "
                f"duration: {executor_end_dt - executor_start_dt}\n"
            )
            for job_result in results:
                log_file.write("\n----------\n\n")
                if job_result.returncode == 0:
                    log_file.write("OK:   ")
                else:
                    log_file.write("FAIL: ")
                log_file.write(
                    f"Job results for scenario = {job_result.scenario}, "
                    f"algo = {job_result.algo}, run = {job_result.run}, "
                    f"started at {job_result.start_dt} (UTC), "
                    f"finished at {job_result.complete_dt} (UTC), "
                    f"took {job_result.complete_dt - job_result.start_dt}, "
                    f"rc = {job_result.returncode}\n"
                )
                log_file.write("> STDOUT:\n")
                log_file.write(textwrap.indent(job_result.stdout, "    "))
                log_file.write("> STDERR:\n")
                log_file.write(textwrap.indent(job_result.stderr, "    "))

    if not ok:
        # If anything failed, fail as well.
        sys.exit(1)


def _get_argument_parser():
    parser = argparse.ArgumentParser(
        description="execute a simulator and aggregator",
    )
    parser.add_argument(
        "SCRIPT",
        help="a script allowing to execute jobs",
    )
    parser.add_argument(
        "-s", "--scenarios",
        nargs="+", required=True,
        help="the scenarios to be simulated",
    )
    parser.add_argument(
        "-a", "--algorithms",
        nargs="+", required=True,
        help="the algorithms to be simulated",
    )
    parser.add_argument(
        "-r", "--runs",
        nargs="+", default=[str(i) for i in range(1, 11)],
        help="the names / IDs for the runs to be executed, default: {1..10}",
    )
    parser.add_argument(
        "-w", "--workers",
        type=int, default=4,
        help="the amount of workers to be used",
    )
    parser.add_argument(
        "-p", "--progress",
        action="store_true",
        help="show a progress bar",
    )
    parser.add_argument(
        "-l", "--logfile",
        type=str, default=None,
        help="output file for detailed log information",
    )
    parser.add_argument(
        "-v", "--verbose",
        action="count",
        default=0,
        help="increase log output verbosity",
    )
    return parser


def _initialize_logger(verbosity):
    log_level = {
        0: logging.WARN,
        1: logging.INFO,
    }.get(verbosity, logging.DEBUG)

    # Simulator-wide logging to the console
    logging.basicConfig(
        level=log_level,
        format="%(asctime)s %(levelname)s: %(message)s",
    )


if __name__ == "__main__":
    _main(_get_argument_parser().parse_args())
