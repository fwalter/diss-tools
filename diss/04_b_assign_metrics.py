#!/usr/bin/env python
# encoding: utf-8

import argparse
import json
import sys


def kb_set_latest(kb):
    general_knowledge = {}
    for node, node_kb in kb["metrics"].items():
        assert len(node_kb) == 1, "Please use an unmodified KB!"
        for rx_node, tx_nodes_dict in node_kb.items():
            for tx_node, metric_data in tx_nodes_dict.items():
                for valid_from, conf_mi, vol_mi in metric_data:
                    edge = (tx_node, rx_node)
                    if general_knowledge.get(edge, [0])[0] < valid_from:
                        general_knowledge[edge] = (
                            valid_from,
                            conf_mi,
                            vol_mi
                        )
    new_kb = {}
    for node, node_kb in kb["metrics"].items():
        if node not in new_kb:
            new_kb[node] = {}
        for edge, knowledge in general_knowledge.items():
            tx_node, rx_node = edge
            if rx_node not in new_kb[node]:
                new_kb[node][rx_node] = {}
            _, conf_mi, vol_mi = knowledge
            new_kb[node][rx_node][tx_node] = [(0, conf_mi, vol_mi)]
    return new_kb


def kb_set_instant_global(kb):
    general_knowledge = {}
    for node, node_kb in kb["metrics"].items():
        assert len(node_kb) == 1, "Please use an unmodified KB!"
        for rx_node, tx_nodes_dict in node_kb.items():
            for tx_node, metric_data in tx_nodes_dict.items():
                for valid_from, conf_mi, vol_mi in metric_data:
                    edge = (tx_node, rx_node)
                    if edge not in general_knowledge:
                        general_knowledge[edge] = []
                    general_knowledge[edge].append((
                        valid_from,
                        conf_mi,
                        vol_mi
                    ))
    new_kb = {}
    for node, node_kb in kb["metrics"].items():
        if node not in new_kb:
            new_kb[node] = {}
        for edge, knowledge_items in general_knowledge.items():
            tx_node, rx_node = edge
            if rx_node not in new_kb[node]:
                new_kb[node][rx_node] = {}
            new_kb[node][rx_node][tx_node] = list(sorted(
                knowledge_items,
                key=lambda x: x[0],
            ))
    return new_kb


def kb_set_reverse_edge(kb):
    general_knowledge = {}
    for node, node_kb in kb["metrics"].items():
        assert len(node_kb) == 1, "Please use an unmodified KB!"
        for rx_node, tx_nodes_dict in node_kb.items():
            for tx_node, metric_data in tx_nodes_dict.items():
                for valid_from, conf_mi, vol_mi in metric_data:
                    edge = (tx_node, rx_node)
                    if edge not in general_knowledge:
                        general_knowledge[edge] = []
                    general_knowledge[edge].append((
                        valid_from,
                        conf_mi,
                        vol_mi
                    ))
    new_kb = {}
    for node, node_kb in kb["metrics"].items():
        if node not in new_kb:
            new_kb[node] = {}
        for edge, knowledge_items in general_knowledge.items():
            tx_node, rx_node = edge
            if rx_node not in new_kb[node]:
                new_kb[node][rx_node] = {}
            if node not in edge:
                continue
            new_kb[node][rx_node][tx_node] = list(sorted(
                knowledge_items,
                key=lambda x: x[0],
            ))
    return new_kb


KB_UPDATERS = {
    "latest": kb_set_latest,  # perfect knowledge - latest prediction known
    "global": kb_set_instant_global,  # instant global knowledge of any pred.
    "reverse": kb_set_reverse_edge,  # both nodes of an edge know the same
}


def _main(args):
    kb = json.load(args.KBFILE)
    updater = KB_UPDATERS[args.strategy]
    kb["metrics"] = updater(kb)
    json.dump(
        kb,
        args.output,
        indent=(4 if args.indent else None),
    )
    args.output.write("\n")
    args.output.close()


def _get_argument_parser():
    parser = argparse.ArgumentParser(
        description="update the metric knowledge base",
    )
    parser.add_argument(
        "KBFILE",
        type=argparse.FileType("r"),
        help="the knowledge base file",
    )
    parser.add_argument(
        "-s", "--strategy",
        choices=KB_UPDATERS.keys(),
        default="latest",
        help="the strategy applied to update the knowledge base",
    )
    parser.add_argument(
        "-o", "--output",
        type=argparse.FileType("w"),
        default=sys.stdout,
        help="the output JSON file for the updated knowledge base",
    )
    parser.add_argument(
        "-i", "--indent",
        action="store_true",
        help="specify this if you want pretty JSON",
    )
    return parser


if __name__ == "__main__":
    _main(_get_argument_parser().parse_args())
