#!/usr/bin/env python3
# encoding: utf-8

import sys
import argparse
import json
import math
import random
import datetime
import functools
import collections
from operator import itemgetter

import joblib
import ephem
import numpy as np
import scipy.special
import tqdm

from tvgutil import contact_plan, tvg
from predictutil import (
    predict_rr0_for_scenario,
    get_trajectory_predictors_for_scenario,
    StartTimeBasedDict,
)

TIME_RESOLUTION = 1
APT_RESOLUTION = 0.5

LINK_PARAMETERS = {
    # Widespread RS(255, 223) code as used by Voyager probes
    "255rs223": (223 / 255, 255, 16),
    # https://public.ccsds.org/Pubs/231x1o1.pdf
    # see also: http://pretty-good-codes.org/ldpc.html
    # NOTE that we assume the correctable bits == the minimum distance
    "ccsds256ldpc128": (128 / 256, 256, 24),
    "ccsds128ldpc64": (64 / 128, 128, 14),
    "nofail": (1, 1, 1),
    "none": (1, 1, 0),
}

ContactBERInfo = collections.namedtuple("ContactBERInfo", [
    "station",
    "satellite",
    "starttime",
    "endtime",
    "max_elevation",
    "timestamps",
    "line_bers_down",
    "line_bers_up",
    "distance_func",
])


def _ber_after_coding(ber_before_coding, uncoded_bit_rate,
                      fec_block_size, fec_correctable_bits):
    p_successful_block_transfer = 0
    # add up the probabilities for successful decoding
    for i in range(fec_correctable_bits + 1):
        p_successful_block_transfer += (
            # `block size - i` bits are correct
            (1 - ber_before_coding) ** (fec_block_size - i) *
            # i bits are wrong
            ber_before_coding ** i *
            # count of combinations possible with i wrong bits
            # as we can correct arbitrary combinations
            scipy.special.comb(fec_block_size, i)
        )
    # Get a BER back
    return 1 - (p_successful_block_transfer ** (1 / fec_block_size))


def _get_doi(line_data):
    doi = []
    for i, line in enumerate(line_data):
        first = line[44:86]
        second = line[134:176]
        # check if line is inverted (in case we encountered an IR image or
        # the minute marker...)
        if first.mean() < .5:
            first = 1 - first
        if second.mean() < .5:
            second = 1 - second
        # add combined line to doi
        doi.append(np.concatenate((first, second)))
        assert len(doi[-1] == 84)
    return np.array(doi, ndmin=2, dtype=float)


def _resample_image(image, lines_to_combine):
    new_image = [
        image[i * lines_to_combine:(i + 1) * lines_to_combine]
        for i in range(len(image) // lines_to_combine)
    ]
    return np.array(new_image, ndmin=2, dtype=float)


def _estimate_apt_space_pixel_ber(pixels):

    # estimation kernel == gauss kernel
    def q(value):
        return .5 * scipy.special.erfc(value / np.sqrt(2))

    # rescale such that a transmitted 0 maps to -1 and 1 to 1
    in_data = (pixels * 2 - 1).flatten()
    # bandwidth, optimized for gauss kernel
    h_n = (3 / (4 * len(in_data))) ** (1 / 5) * np.std(in_data)

    # we have only one value and, thus, need to assume the BER is symmetric...
    # (uplink / downlink)
    p_err = 1 / len(in_data) * sum(q(x / h_n) for x in in_data)

    return p_err


def _preprocess_contact_dict(contact_dict, soft_start, soft_end):
    timestamps = np.arange(soft_start, soft_end, APT_RESOLUTION)
    dates = [
        datetime.datetime.utcfromtimestamp(ts)
        for ts in timestamps
    ]

    img_part = contact_dict["linedata"]
    has_sync = contact_dict["has_sync"]

    assert abs(
        len(img_part) * APT_RESOLUTION -
        len(contact_dict["measured_snrs"])
    ) < 10  # 10 seconds
    assert len(img_part) < len(dates)
    assert abs(len(img_part) - len(dates)) < 30 / APT_RESOLUTION  # 30 seconds
    assert len(img_part) == len(has_sync)

    # align dates to measurements
    # NOTE: the >= 2 sec SNR setup time is main factor, so we remove at start
    new_start = len(dates) - len(img_part)
    dates = dates[new_start:]
    timestamps = timestamps[new_start:]

    return img_part, has_sync, dates, timestamps


def _get_sat_distance_light_sec(sat_name, tle_array,
                                station_lat, station_lon, station_alt_m,
                                unixtime):
    gsobj = ephem.Observer()
    gsobj.lat = str(station_lat)
    gsobj.lon = str(station_lon)
    gsobj.date = datetime.datetime.utcfromtimestamp(unixtime)
    satobj = ephem.readtle(sat_name, *tle_array)
    satobj.compute(gsobj)
    return satobj.range / 299_792_458


def _get_scenario_dict(data):
    gs = {
        contact_dict["station_name"]: {
            "id": contact_dict["station_name"],
            "lat": contact_dict["station_lat"],
            "lon": contact_dict["station_lng"],
            "alt": contact_dict["station_alt"],
        }
        for contact_dict in data
    }
    sat = {
        contact_dict["satellite_info"]["name"]: []
        for contact_dict in data
    }
    for contact_dict in data:
        sat[contact_dict["satellite_info"]["name"]].append(
            (
                contact_dict["start_unix"],
                contact_dict["tle"],
            )
        )
    return {
        "gslist": [val for _, val in gs.items()],
        "satlist": [
            {
                "id": key,
                "tle": next(iter(sorted(val, key=itemgetter(0))))[1],
                "tle_list": list(sorted(val, key=itemgetter(0))),
            }
            for key, val in sat.items()
        ],
        "time_offset": int(
            min(contact_dict["start_unix"] for contact_dict in data) - 1
        ),
    }


def _get_factual_contacts(scenario, preprocessed, duration,
                          min_elev=0, resampling_factor=1):
    scenario_start = scenario["time_offset"]
    scenario_end = scenario_start + duration
    gs_list = scenario["gslist"]
    sat_list = scenario["satlist"]
    print("Predicting new RR0 contacts...", file=sys.stderr)
    rr0_contacts = predict_rr0_for_scenario(
        gs_list,
        sat_list,
        scenario_start,
        scenario_end,
        min_elev,
    )
    # NOTE: We use the predictors from the scenario
    traj_predictors = get_trajectory_predictors_for_scenario(
        gs_list,
        sat_list,
    )
    # NOTE: Elevation VARiability -- random +/- allowed in degrees
    EVAR = 2.0
    print("Associating new RR0 contacts with observations...", file=sys.stderr)
    return_value = []
    for gs, sat, start, end in tqdm.tqdm(rr0_contacts):
        tp = traj_predictors[frozenset((gs, sat))]
        _, max_elev = tp.predict_max_elevation(start, end)
        max_elev = max_elev * 180 / math.pi
        candidates = [
            tup
            for tup in preprocessed[(gs, sat)]
            if tup.max_elevation > max_elev - EVAR
        ]
        assert candidates, "no candidates found, please provide more contacts"
        cbd = random.choice(candidates)
        # build associative bisectable dicts to allow searching for elev
        source_elev = -100
        inc_dict = {}
        dec_dict = {}
        for i, ts in enumerate(cbd.timestamps):
            source_elev_new = float(tp.predict_elevation(ts))
            if source_elev_new > source_elev:
                inc_dict[source_elev_new] = i // resampling_factor
            else:
                dec_dict[source_elev_new] = i // resampling_factor
            source_elev = source_elev_new
        inc_dict_b = StartTimeBasedDict(inc_dict)
        dec_dict_b = StartTimeBasedDict(dec_dict)
        # get minimum elevations below which we set BER to .5
        min_inc = min(inc_dict.keys())
        min_dec = min(dec_dict.keys())
        # build new ts array
        delta_ts = cbd.timestamps[1] - cbd.timestamps[0]
        new_timestamps = list(np.arange(start, end, delta_ts))
        # associate elevs
        dest_elev = -100
        new_line_bers_down = []
        new_line_bers_up = []
        delta = (cbd.timestamps[1] - cbd.timestamps[0]) * resampling_factor
        for t in np.arange(start, end, delta):
            dest_elev_new = float(tp.predict_elevation(t))
            if dest_elev_new > dest_elev:
                edict = inc_dict_b
                min_elev = min_inc
            else:
                edict = dec_dict_b
                min_elev = min_dec
            dest_elev = dest_elev_new
            # get BER
            if dest_elev < min_elev:
                ber_down = .5
                ber_up = .5
            else:
                ber_down = cbd.line_bers_down[edict.get_entry_for(dest_elev)]
                ber_up = cbd.line_bers_up[edict.get_entry_for(dest_elev)]
            new_line_bers_down.append(ber_down)
            new_line_bers_up.append(ber_up)
        return_value.append(
            ContactBERInfo(
                station=gs,
                satellite=sat,
                starttime=start,
                endtime=end,
                max_elevation=max_elev,
                timestamps=new_timestamps,
                line_bers_down=new_line_bers_down,
                line_bers_up=new_line_bers_up,
                distance_func=cbd.distance_func,
            )
        )
    return return_value


def _determine_bers(contact_dict, bergranularity, fec_block_size,
                    fec_correctable_b, overall_bit_rate_down,
                    overall_bit_rate_up, resampling_factor):
    cid = contact_dict["id"]
    station = contact_dict["station_name"]
    satellite = contact_dict["satellite_info"]["name"]
    soft_start = contact_dict["start_unix"]
    soft_end = contact_dict["end_unix"]
    max_elevation = contact_dict["max_altitude"]

    try:
        img_part, has_sync, dates, timestamps = _preprocess_contact_dict(
            contact_dict,
            soft_start,
            soft_end,
        )
    except AssertionError as e:
        print(f"Contact #{cid} {station} <-> {satellite}, "
              f"interval [{soft_start}; {soft_end}] "
              f"unusable, assertion failed: {e}", file=sys.stderr)
        return None
    except KeyError:
        print(f"Contact #{cid} {station} <-> {satellite}, "
              f"interval [{soft_start}; {soft_end}] "
              f"unusable, was not analyzed properly!", file=sys.stderr)
        return None

    resampled_doi = _resample_image(
        _get_doi(img_part),
        resampling_factor,
    )
    # Clip the timestamps array to match the resampled lines
    timestamps = timestamps[0:(len(resampled_doi) * resampling_factor)]
    line_bers_down = [
        _ber_after_coding(
            _estimate_apt_space_pixel_ber(line),
            overall_bit_rate_down,
            fec_block_size,
            fec_correctable_b,
        )
        for line in resampled_doi
    ]
    line_bers_up = [
        _ber_after_coding(
            _estimate_apt_space_pixel_ber(line),
            overall_bit_rate_up,
            fec_block_size,
            fec_correctable_b,
        )
        for line in resampled_doi
    ]

    distance_func = functools.partial(
        _get_sat_distance_light_sec,
        satellite,
        contact_dict["tle"],
        contact_dict["station_lat"],
        contact_dict["station_lng"],
        contact_dict["station_alt"],
    )

    starttime = timestamps[0]
    endtime = timestamps[-1] + bergranularity

    return ContactBERInfo(
        station=station,
        satellite=satellite,
        starttime=starttime,
        endtime=endtime,
        max_elevation=max_elevation,
        timestamps=timestamps,
        line_bers_down=line_bers_down,
        line_bers_up=line_bers_up,
        distance_func=distance_func,
    )


def _main(args):
    print("Unpickling input data...", file=sys.stderr)
    data = joblib.load(args.INFILE)

    fec_rate, fec_block_size, fec_correctable_b = LINK_PARAMETERS[args.fec]
    overall_bit_rate_down = args.bitrate_downlink
    overall_bit_rate_up = (
        args.bitrate_uplink if args.bitrate_uplink else overall_bit_rate_down
    )
    net_bit_rate_down = overall_bit_rate_down * fec_rate
    net_bit_rate_up = overall_bit_rate_up * fec_rate

    # use e.g. 20 lines per step if bergranularity == 10 seconds
    resampling_factor = int(args.bergranularity / APT_RESOLUTION)

    # First generate a scenario file compatible with the RR scenario generator
    print("Deriving Ring Road scenario...", file=sys.stderr)
    scenario = _get_scenario_dict(data)
    scenario["bit_rate_downlink"] = net_bit_rate_down
    scenario["bit_rate_uplink"] = net_bit_rate_up
    json.dump(
        scenario,
        args.scenario,
        indent=(4 if args.indent else None),
    )
    args.scenario.write("\n")
    args.scenario.close()

    # Second, generate the F-TVG
    print("Processing contact plans and determining BER...", file=sys.stderr)
    ftup = joblib.Parallel(n_jobs=args.workers)(
        joblib.delayed(_determine_bers)(
            contact_dict,
            bergranularity=args.bergranularity,
            fec_block_size=fec_block_size,
            fec_correctable_b=fec_correctable_b,
            overall_bit_rate_down=overall_bit_rate_down,
            overall_bit_rate_up=overall_bit_rate_up,
            resampling_factor=resampling_factor,
        )
        for contact_dict in tqdm.tqdm(data)
    )

    if args.custom_duration:
        print("WARNING: Applying UNREALISTIC custom duration", file=sys.stderr)
        preprocessed = {}
        for cbd in ftup:
            if not cbd:
                continue
            if (cbd.station, cbd.satellite) not in preprocessed:
                preprocessed[(cbd.station, cbd.satellite)] = []
            preprocessed[(cbd.station, cbd.satellite)].append(cbd)
        ftup = _get_factual_contacts(
            scenario,
            preprocessed,
            args.custom_duration,
            args.min_elevation,
            resampling_factor,
        )

    print("Creating FCP...", file=sys.stderr)

    fcp = []
    for cbd in ftup:

        # Analysis may have failed for the given element
        if not cbd:
            continue

        if all(x >= args.max_ber for x in cbd.line_bers_up):
            continue

        if all(x >= args.max_ber for x in cbd.line_bers_down):
            continue

        chars_up = [
            contact_plan.ContactCharacteristics(
                starting_at=cbd.timestamps[i * resampling_factor],
                bit_rate=net_bit_rate_up,
                # estimated BER over interval
                bit_error_rate=ber,
                delay=cbd.distance_func(cbd.timestamps[i * resampling_factor]),
            )
            for i, ber in enumerate(cbd.line_bers_up)
        ]
        c1 = contact_plan.Contact(
            tx_node=cbd.station,
            rx_node=cbd.satellite,
            start_time=cbd.starttime,
            end_time=cbd.endtime,
            characteristics=chars_up,
        )

        chars_down = [
            contact_plan.ContactCharacteristics(
                starting_at=cbd.timestamps[i * resampling_factor],
                bit_rate=net_bit_rate_down,
                # estimated BER over interval
                bit_error_rate=ber,
                delay=cbd.distance_func(cbd.timestamps[i * resampling_factor]),
            )
            for i, ber in enumerate(cbd.line_bers_down)
        ]
        c2 = contact_plan.Contact(
            tx_node=cbd.satellite,
            rx_node=cbd.station,
            start_time=cbd.starttime,
            end_time=cbd.endtime,
            characteristics=chars_down,
        )

        fcp.append(c1)
        fcp.append(c2)

    print("Writing TVG data...", file=sys.stderr)
    json.dump(
        tvg.to_serializable(
            tvg.from_contact_plan(fcp, contact_plan.Contact)
        ),
        args.ftvg,
        indent=(4 if args.indent else None),
    )
    args.ftvg.write("\n")
    args.ftvg.close()
    print(
        f"Wrote F-TVG with {len(fcp)} contacts.",
        file=sys.stderr,
    )


def _get_argument_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "INFILE",
        help="the input filename, generated by APT analysis",
    )
    parser.add_argument(
        "-f", "--ftvg",
        type=argparse.FileType("w"),
        required=True,
        help="the output filename for the F-TVG",
    )
    parser.add_argument(
        "-s", "--scenario",
        type=argparse.FileType("w"),
        required=True,
        help="the output filename for the scenario file",
    )
    parser.add_argument(
        "-i", "--indent",
        action="store_true",
        help="indent the JSON output",
    )
    parser.add_argument(
        "-r", "--bitrate-downlink",
        type=float, default=4160,
        help="the assumed downlink bit rate (default: 4.16 kbit/s)",
    )
    parser.add_argument(
        "-R", "--bitrate-uplink",
        type=float, default=None,
        help="the assumed uplink bit rate (default: same as downlink)",
    )
    parser.add_argument(
        "-g", "--bergranularity",
        type=int, default=10,
        help="the assumed granularity of BER windows in s (default: 10 s)",
    )
    parser.add_argument(
        "--custom-duration",
        type=int, default=None,
        # NOTE: this is not fully random, as the sat/gs pair stays the same
        help="use custom, specified duration for scenario - NOTE: randomly associates generated contacts to observations",
    )
    parser.add_argument(
        "--min-elevation",
        type=float, default=0.,
        help="the min. elevation if using custom contact duration (default: 0)",
    )
    parser.add_argument(
        "--max-ber",
        type=float, default=0.1,
        help="the max to allow contacts with all BERs greater (default: 0.1)",
    )
    parser.add_argument(
        "--fec",
        choices=LINK_PARAMETERS.keys(), default="255rs223",
        help="the used FEC scheme (default: Voyager RS(255,223)",
    )
    parser.add_argument(
        "-w", "--workers",
        type=int, default=4,
        help="the amount of workers used for parallel processing",
    )
    return parser


if __name__ == "__main__":
    _main(_get_argument_parser().parse_args())
