#!/bin/bash

# This script compares routing performance of different confidence predictors.
# It compares:
#   - Epidemic
#   - SnW
#   - ProbCGR with EWMA predictor, w=0.1
#   - ProbCGR with ratio predictor
#   - CGR

TEST="T06_confrouting"

WORKING_DIR="results_$TEST"

SCENARIOS=(
    "S1+low"
    "S1-p5i+low"
    "S1-p3b+low"
    "S1+high"
    "S1-p5i+high"
    "S1-p3b+high"
    "S1+extreme"
    "S1-p5i+extreme"
    "S1-p3b+extreme"
)
PLOT_SCENARIOS1=(
    "S1+low"
    "S1-p5i+low"
    "S1-p3b+low"
)
PLOT_SCENARIOS2=(
    "S1+high"
    "S1-p5i+high"
    "S1-p3b+high"
)
PLOT_SCENARIOS3=(
    "S1+extreme"
    "S1-p5i+extreme"
    "S1-p3b+extreme"
)

ALGOS=(
    Epidemic
    SprayAndWait
    CGR
    CGR-Prob-30-30
    CGR-Prob-10-80
)

RUNS=3

WORKERS=8
SETUP_WORKERS=24

# Algos and flags to run an observe-distribute-predict step
OBSV_ALGOS=(
    CGR
    CGR-Prob-30-30
    CGR-Prob-10-80
)
# Note that 10800 is just the metric update interval, predictions are performed every 43200s at minimum (see below)
OBSV_FLAGS=(
    "--min_metric_update_interval 10800 --confidence_predictor ratio --volume_predictor minelev --min_elevation 10 --link_block_size 12000"
    "--min_metric_update_interval 10800 --confidence_predictor ratio --volume_predictor minelev --min_elevation 10 --link_block_size 12000"
    "--min_metric_update_interval 10800 --confidence_predictor ratio --volume_predictor minelev --min_elevation 10 --link_block_size 12000"
)

# Flags for the prediction script
PREDICT_FLAGS="--min_predict_interval 43200 --max_predict_interval 86400"

# Flags for the transmission plan, per-scenario as listed above
# Explanation: see the T07 script!
# LOW == 2% load (200 KiB messages)
# HIGH == 50% load (2 MiB messages)
# EXTREME == 100% load (2 MiB messages)
# walker: 5174 bidi contacts, cvol: 830794757462.3237 bits
# - LOW: 10*86400*0.8/273*1638400*2/830794757462.3237*2
# - HIGH: 10*86400*0.8/112*16777216*2/830794757462.3237*2
# - EXTREME: 10*86400*0.8/56*16777216*2/830794757462.3237*2
# 5i: 630719229773.1573
# - LOW: 10*86400*0.8/359*1638400*2/630719229773.1573*2
# - HIGH: 10*86400*0.8/147*16777216*2/630719229773.1573*2
# - EXTREME: 10*86400*0.8/73*16777216*2/630719229773.1573*2
# 3b: 637760609975.6685
# - LOW: 10*86400*0.8/355*1638400*2/637760609975.6685*2
# - HIGH: 10*86400*0.8/145*16777216*2/637760609975.6685*2
# - EXTREME: 10*86400*0.8/72*16777216*2/637760609975.6685*2
TPLAN_FLAGS=(
    "--maxgenpercent 80 --interval 273 --intervaldev 68 --lifetime 864000 --minsize 1638400 --invalidsrc LEO-01 LEO-02 LEO-03 LEO-11 LEO-12 LEO-13 LEO-21 LEO-22 LEO-23 GS-01 GS-02 GS-03 GS-04 GS-05 --invaliddst LEO-01 LEO-02 LEO-03 LEO-11 LEO-12 LEO-13 LEO-21 LEO-22 LEO-23 GS-01 GS-02 GS-03 GS-04 GS-05"
    "--maxgenpercent 80 --interval 359 --intervaldev 89 --lifetime 864000 --minsize 1638400 --invalidsrc LEO-01 LEO-02 LEO-03 LEO-11 LEO-12 LEO-13 LEO-21 LEO-22 LEO-23 GS-01 GS-02 GS-03 GS-04 GS-05 --invaliddst LEO-01 LEO-02 LEO-03 LEO-11 LEO-12 LEO-13 LEO-21 LEO-22 LEO-23 GS-01 GS-02 GS-03 GS-04 GS-05"
    "--maxgenpercent 80 --interval 355 --intervaldev 89 --lifetime 864000 --minsize 1638400 --invalidsrc LEO-01 LEO-02 LEO-03 LEO-11 LEO-12 LEO-13 LEO-21 LEO-22 LEO-23 GS-01 GS-02 GS-03 GS-04 GS-05 --invaliddst LEO-01 LEO-02 LEO-03 LEO-11 LEO-12 LEO-13 LEO-21 LEO-22 LEO-23 GS-01 GS-02 GS-03 GS-04 GS-05"
    "--maxgenpercent 80 --interval 112 --intervaldev 28 --lifetime 864000 --minsize 16777216 --invalidsrc LEO-01 LEO-02 LEO-03 LEO-11 LEO-12 LEO-13 LEO-21 LEO-22 LEO-23 GS-01 GS-02 GS-03 GS-04 GS-05 --invaliddst LEO-01 LEO-02 LEO-03 LEO-11 LEO-12 LEO-13 LEO-21 LEO-22 LEO-23 GS-01 GS-02 GS-03 GS-04 GS-05"
    "--maxgenpercent 80 --interval 147 --intervaldev 36 --lifetime 864000 --minsize 16777216 --invalidsrc LEO-01 LEO-02 LEO-03 LEO-11 LEO-12 LEO-13 LEO-21 LEO-22 LEO-23 GS-01 GS-02 GS-03 GS-04 GS-05 --invaliddst LEO-01 LEO-02 LEO-03 LEO-11 LEO-12 LEO-13 LEO-21 LEO-22 LEO-23 GS-01 GS-02 GS-03 GS-04 GS-05"
    "--maxgenpercent 80 --interval 145 --intervaldev 36 --lifetime 864000 --minsize 16777216 --invalidsrc LEO-01 LEO-02 LEO-03 LEO-11 LEO-12 LEO-13 LEO-21 LEO-22 LEO-23 GS-01 GS-02 GS-03 GS-04 GS-05 --invaliddst LEO-01 LEO-02 LEO-03 LEO-11 LEO-12 LEO-13 LEO-21 LEO-22 LEO-23 GS-01 GS-02 GS-03 GS-04 GS-05"
    "--maxgenpercent 80 --interval 56 --intervaldev 14 --lifetime 864000 --minsize 16777216 --invalidsrc LEO-01 LEO-02 LEO-03 LEO-11 LEO-12 LEO-13 LEO-21 LEO-22 LEO-23 GS-01 GS-02 GS-03 GS-04 GS-05 --invaliddst LEO-01 LEO-02 LEO-03 LEO-11 LEO-12 LEO-13 LEO-21 LEO-22 LEO-23 GS-01 GS-02 GS-03 GS-04 GS-05"
    "--maxgenpercent 80 --interval 73 --intervaldev 18 --lifetime 864000 --minsize 16777216 --invalidsrc LEO-01 LEO-02 LEO-03 LEO-11 LEO-12 LEO-13 LEO-21 LEO-22 LEO-23 GS-01 GS-02 GS-03 GS-04 GS-05 --invaliddst LEO-01 LEO-02 LEO-03 LEO-11 LEO-12 LEO-13 LEO-21 LEO-22 LEO-23 GS-01 GS-02 GS-03 GS-04 GS-05"
    "--maxgenpercent 80 --interval 72 --intervaldev 18 --lifetime 864000 --minsize 16777216 --invalidsrc LEO-01 LEO-02 LEO-03 LEO-11 LEO-12 LEO-13 LEO-21 LEO-22 LEO-23 GS-01 GS-02 GS-03 GS-04 GS-05 --invaliddst LEO-01 LEO-02 LEO-03 LEO-11 LEO-12 LEO-13 LEO-21 LEO-22 LEO-23 GS-01 GS-02 GS-03 GS-04 GS-05"
)

# Flags for the simulator, per algo - 2 GiB of buffer provided
SIM_FLAGS=(
    "--rx_channels 5 --tx_channels 5 --buffersize 17179869184 --graph_update_min_interval 43200 --link_block_size 12000 --algo epidemic"
    "--rx_channels 5 --tx_channels 5 --buffersize 17179869184 --graph_update_min_interval 43200 --link_block_size 12000 --algo snw"
    "--rx_channels 5 --tx_channels 5 --buffersize 17179869184 --graph_update_min_interval 43200 --link_block_size 12000 --algo ngsabr --max_order_routes 6"
    "--rx_channels 5 --tx_channels 5 --buffersize 17179869184 --graph_update_min_interval 43200 --link_block_size 12000 --algo probngsabr --no-always_drop_prob --route_min_confidence 0.3 --target_confidence 0.3 --max_order_routes 6 --no-first_contact_prob --no-first_contact_filter"
    "--rx_channels 5 --tx_channels 5 --buffersize 17179869184 --graph_update_min_interval 43200 --link_block_size 12000 --algo probngsabr --no-always_drop_prob --route_min_confidence 0.1 --target_confidence 0.8 --max_order_routes 6 --no-first_contact_prob --no-first_contact_filter --no_alt_for_looping"
)

COMMAND="$1"

set -euxo pipefail

if [ -z "$COMMAND" ]; then
    COMMAND="run"
fi

for scenario in "${SCENARIOS[@]}"; do
    base_scenario="$(echo "$scenario" | cut -d '+' -f 1)"
    if [[ ! -r "$base_scenario.02.scenario.json" || ! -r "$base_scenario.02.ftvg.json" ]]; then
        echo "$base_scenario.02.scenario.json and $base_scenario.02.ftvg.json have to exist!" >&2
        exit 1
    fi
done

mkdir -p "$WORKING_DIR"

setup() {
    # Remove all empty result files to have a clean setup
    find "$WORKING_DIR" -type f -size 0 -delete

    for ((s = 0; s < ${#SCENARIOS[@]}; s++)); do
        local scenario="${SCENARIOS[$s]}"
        local tplan_flags="${TPLAN_FLAGS[$s]}"

        local prefix="$WORKING_DIR/$scenario"
        local base_scenario="$(echo "$scenario" | cut -d '+' -f 1)"
        local base_prefix="$WORKING_DIR/$base_scenario"

        for ((i = 0; i < ${#OBSV_ALGOS[@]}; i++)); do
            local obsv_algo="${OBSV_ALGOS[$i]}"
            local obsv_flags="${OBSV_FLAGS[$i]}"

            if [ -r "$base_scenario.02.ptvg.json" ]; then
                [ -r "$base_prefix.03.$obsv_algo.kb.observed.json" ] || \
                    python 03_observe_contacts.py -v \
                        $obsv_flags \
                        --ptvg "$base_scenario.02.ptvg.json" \
                        --output "$base_prefix.03.$obsv_algo.kb.observed.json" \
                        "$base_scenario.02.scenario.json" \
                        "$base_scenario.02.ftvg.json"
            else
                [ -r "$base_prefix.03.$obsv_algo.kb.observed.json" ] || \
                    python 03_observe_contacts.py -v \
                        $obsv_flags \
                        --output "$base_prefix.03.$obsv_algo.kb.observed.json" \
                        "$base_scenario.02.scenario.json" \
                        "$base_scenario.02.ftvg.json"
            fi
            [ -r "$base_prefix.03.$obsv_algo.kb.dist.json" ] || \
                python 04_a_distribute_metrics.py \
                    --output "$base_prefix.03.$obsv_algo.kb.dist.json" \
                    --progress \
                    --savestats "$base_prefix.04.$i.$obsv_algo.diststats.json" \
                    "$base_scenario.02.ftvg.json" \
                    "$base_prefix.03.$obsv_algo.kb.observed.json"
            [ -r "$base_prefix.05.$obsv_algo.ftvg.json" ] || \
                python helpers/05_apply_dist_overhead_to_ftvg.py \
                    --output "$base_prefix.05.$obsv_algo.ftvg.json" \
                    "$base_scenario.02.ftvg.json" \
                    "$base_prefix.04.$i.$obsv_algo.diststats.json"
            [ -r "$base_prefix.05.$obsv_algo.ptvg.json" ] || \
                python 05_predict_ptvg.py -v \
                    $PREDICT_FLAGS \
                    --output "$base_prefix.05.$obsv_algo.ptvg.json" \
                    --workers "$SETUP_WORKERS" \
                    "$base_prefix.03.$obsv_algo.kb.dist.json"

            [ -r "$prefix.05.$obsv_algo.ptvg.json" ] || cp "$base_prefix.05.$obsv_algo.ptvg.json" \
                "$prefix.05.$obsv_algo.ptvg.json"
            [ -r "$prefix.05.$obsv_algo.ftvg.json" ] || cp "$base_prefix.05.$obsv_algo.ftvg.json" \
                "$prefix.05.$obsv_algo.ftvg.json"
        done

        for run in $(seq $RUNS); do
            [ -r "$prefix.06.transmission_plan_$run.json" ] || \
                python -m tvgutil.tools.create_transmission_plan \
                    $tplan_flags \
                    --output "$prefix.06.transmission_plan_$run.json" \
                    "$base_scenario.02.ftvg.json"
        done

        [ -r "$prefix.02.ftvg.json" ] || cp "$base_scenario.02.ftvg.json" \
            "$prefix.02.ftvg.json"
        [ -r "$prefix.02.scenario.json" ] || cp "$base_scenario.02.scenario.json" \
            "$prefix.02.scenario.json"
    done
}

job() {
    local scenario="$1"
    local base_scenario="$(echo "$scenario" | cut -d '+' -f 1)"
    local algo="$2"
    local run="$3"
    local prefix="$WORKING_DIR/$scenario"
    local ptvg="$prefix.05.$algo.ptvg.json"
    local dropplan="$base_scenario.02.dropplan.json"

    for ((a = 0; a < ${#ALGOS[@]}; a++)); do
        if [[ "${ALGOS[$a]}" = "$algo" ]]; then
            local sim_flags="${SIM_FLAGS[$a]}"
            break
        fi
    done

    [ -r "$prefix.06.transmission_plan_$run.json" ] || exit 1

    [ -r "$prefix.07.stats.$algo.$run.json" ] && return 0

    if [ -r "$dropplan" ]; then
        local dplan="--dropplan $dropplan"
    else
        local dplan=""
    fi

    if [ -r "$ptvg" ]; then
        python 06_simulation.py \
            $sim_flags \
            --ptvg "$ptvg" \
            $dplan \
            --tplan "$prefix.06.transmission_plan_$run.json" \
            --savestats "$prefix.07.stats.$algo.$run.json" \
            "$prefix.05.$algo.ftvg.json"
    else
        python 06_simulation.py \
            $sim_flags \
            $dplan \
            --tplan "$prefix.06.transmission_plan_$run.json" \
            --savestats "$prefix.07.stats.$algo.$run.json" \
            "$base_scenario.02.ftvg.json"
    fi
}

teardown() {
    bash draw_plots.sh \
        "$WORKING_DIR" \
        "$WORKING_DIR/plots_${TEST}_1" \
        "${PLOT_SCENARIOS1[@]}"
    bash draw_plots.sh \
        "$WORKING_DIR" \
        "$WORKING_DIR/plots_${TEST}_2" \
        "${PLOT_SCENARIOS2[@]}"
    bash draw_plots.sh \
        "$WORKING_DIR" \
        "$WORKING_DIR/plots_${TEST}_3" \
        "${PLOT_SCENARIOS3[@]}"
}

case "$COMMAND" in
    run)
        setup
        python test_process_pool_executor.py \
            --workers $WORKERS \
            --scenarios "${SCENARIOS[@]}" \
            --algorithms "${ALGOS[@]}" \
            --runs $(seq $RUNS) \
            --progress \
            --logfile "$WORKING_DIR/test_executor.log" \
            "$0"
        teardown
        ;;
    clean)
        rm -rvf --one-file-system --preserve-root "$WORKING_DIR"
        ;;
    cleanrouting)
        rm -vf --one-file-system --preserve-root $WORKING_DIR/*06.transmission_plan*json
        rm -vf --one-file-system --preserve-root $WORKING_DIR/*07.stats*json
        rm -vf --one-file-system --preserve-root $WORKING_DIR/*08.plotstats*json
        ;;
    job)
        job "${@:2}"
        ;;
    *)
        echo "Usage: $0 [<run|clean|job>] [<job-args>]" >&2
        exit 1
        ;;
esac
