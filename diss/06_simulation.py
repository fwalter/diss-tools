#!/usr/bin/env python
# encoding: utf-8

"""Script to run a configurable simulation with aiodtnsim."""

import argparse
import json
import sys
import asyncio
import logging
import functools
import math

import tqdm

import tvgutil.contact_plan
import tvgutil.tvg

import aiodtnsim
import aiodtnsim.reports
import aiodtnsim.reports.logging
import aiodtnsim.timeutil

from aiodtnsim.reports.message_ops_report import MessageOpsReport
from aiodtnsim.simulation.event_loop import DESEventLoop

import pydtnsim.contact_plan
import pydtnsim.routing.scgr
import pydtnsim.routing.cgr_basic

from aiocgrsim.cgr_node import CGRNode
from aiocgrsim.multi_cgr_node import MultiCGRNode
from aiocgrsim.pydtnsim_compat import (
    CGRPacket,
    CGRMessagePathEventHandler,
)
from aiocgrsim.sprayandwait_node import SprayAndWaitNode
from aiocgrsim.epidemic_node import EpidemicNode
from aiocgrsim.one_epidemic_node import ONEEpidemicNode
from aiocgrsim.one_sprayandwait_node import ONESprayAndWaitNode
from aiocgrsim.simplified_epidemic_node import SimplifiedEpidemicNode

import aiocgrsim.ocgr_basic
import aiocgrsim.probscgr
import aiocgrsim.sabr
import aiocgrsim.sabr_node
import aiocgrsim.sabr_nodegraph_node
import aiocgrsim.sabr_nodegraph_node_yen
import aiocgrsim.sabr_probcgr_node

from predictutil.impaired_cla_link import (
    IdealRedundancyImpairedCLALink,
    SimulatedIdealRedundancyImpairedCLALink,
)

ROUTERS = {
    "cgr": (CGRNode, pydtnsim.routing.cgr_basic.cgr, [
        "graph_update_min_interval",
    ], []),
    "scgr": (CGRNode, pydtnsim.routing.scgr.cgr, [
        "graph_update_min_interval",
    ], []),
    "sabr": (aiocgrsim.sabr_node.SABRNode, aiocgrsim.sabr.sabr, [
        "graph_update_min_interval",
        "max_order_routes",
        "re_schedule_delayed",
        "re_schedule_overbooked",
        "reject_looping",
        "no_alt_for_looping",
    ], []),
    "ngsabr": (aiocgrsim.sabr_nodegraph_node.NGSABRNode,
               aiocgrsim.sabr.sabr, [
        "graph_update_min_interval",
        "max_order_routes",
        "re_schedule_delayed",
        "re_schedule_overbooked",
        "reject_looping",
        "no_alt_for_looping",
        "volume_limited",
    ], []),
    "yengsabr": (aiocgrsim.sabr_nodegraph_node_yen.NGSABRNodeYen,
                 aiocgrsim.sabr.sabr, [
        "graph_update_min_interval",
        "max_order_routes",
        "re_schedule_delayed",
        "re_schedule_overbooked",
        "reject_looping",
        "no_alt_for_looping",
        "volume_limited",
        "max_routes_yen",
    ], []),
    "probcgr": (MultiCGRNode, aiocgrsim.probscgr.probscgr, [
        "graph_update_min_interval",
    ], [
        "target_confidence",
        "route_min_confidence",
        "whole_route_prob",
        "default_limiting_contact",
    ]),
    "probngsabr": (aiocgrsim.sabr_probcgr_node.ProbNGSABRNode,
                   aiocgrsim.sabr_probcgr_node.probsabr, [
        "graph_update_min_interval",
        "max_order_routes",
        "route_min_confidence",
        "always_drop_prob",
        "re_schedule_delayed",
        "re_schedule_overbooked",
        "reject_looping",
        "no_alt_for_looping",
        "first_contact_filter",
        "volume_limited",
    ], [
        "target_confidence",
        "first_contact_prob",
    ]),
    "ocgr": (MultiCGRNode, aiocgrsim.ocgr_basic.ocgr, [
        "graph_update_min_interval",
    ], [
        "target_confidence",
    ]),
    "epidemic": (EpidemicNode, None, [
        "vector_entry_size",
        "deliverable_first",
        "send_once",
        "max_hops",
    ], []),
    "simplified_epidemic": (SimplifiedEpidemicNode, None, [], []),
    "one_epidemic": (ONEEpidemicNode, None, [], []),
    "snw": (SprayAndWaitNode, None, [
        "initial_copies",
        "deliverable_first",
        "send_once",
    ], []),
    "one_snw": (ONESprayAndWaitNode, None, [
        "initial_copies",
    ], []),
    # no PRoPHET yet, though in STINT17 we have shown it is always between SnW
    # and Epidemic in our scenario (though, no RRp was used)
}

CMD_PARAMS = [
    (
        "initial_copies", int, 6,
        "Spray and Wait: The initial amount of copies injected"
    ),
    (
        "deliverable_first", bool, True,
        "Epidemic / Spray and Wait: Sent messages addressed to neighbor first"
    ),
    (
        "send_once", bool, True,
        "Epidemic / Spray and Wait: Send messages only once during a contact"
    ),
    (
        "vector_entry_size", int, 32,
        "Epidemic: The size (bits) of an entry in the 'summary vector'"
    ),
    (
        "max_hops", int, 6,
        "Epidemic: The maximum hop count"
    ),
    (
        "graph_update_min_interval", float, 0.0,
        "CGR-based: The minimum time (seconds) between two graph updates"
    ),
    (
        "target_confidence", float, 0.7,
        "ProbCGR/OCGR: The target confidence threshold"
    ),
    (
        "route_min_confidence", float, 0.1,
        "ProbCGR: The minimum confidence for a route to be considered"
    ),
    (
        "whole_route_prob", bool, True,
        "ProbCGR: Whether or not to use all confidence metrics on the route"
    ),
    (
        "default_limiting_contact", bool, False,
        "ProbCGR: Whether or not to use the \"normal\" limiting contact algo"
    ),
    (
        "first_contact_prob", bool, True,
        "ProbCGR: Use only the first contact for the route confidence"
    ),
    (
        "first_contact_filter", bool, False,
        "ProbCGR: Use only the first contact for the confidence filter"
    ),
    (
        "always_drop_prob", bool, False,
        "ProbCGR: Always set the least-probable contact as limiting contact"
    ),
    (
        "re_schedule_delayed", bool, True,
        "SABR: Re-schedule bundles after end of scheduled contact"
    ),
    (
        "re_schedule_overbooked", bool, False,
        "SABR: Re-schedule bundles if predicted volume decreases below "
        "scheduled volume"
    ),
    (
        "reject_looping", bool, False,
        "SABR: Reject bundles scheduled once already"
    ),
    (
        "no_alt_for_looping", bool, False,
        "SABR: Do not provide alt. routes for looping bundles"
    ),
    (
        "max_order_routes", int, 6,
        "SABR: Count of applicable routes obtined for ordering"
    ),
    (
        "volume_limited", bool, False,
        "NG-SABR: Determine the limiting contact using minimum volume"
    ),
    (
        "max_routes_yen", int, 1000,
        "SABR w/ Yen: Maximum number of K best routes passed to Yen's algo"
    ),
]


async def drop_all_msgs(node, ts):
    await aiodtnsim.timeutil.sleep_until(ts)
    while True:
        msg = node.buf.pop_edf()
        if not msg:
            break
        node.event_dispatcher.message_dropped(node, msg)


def _main(args):
    # Create and register event loop and logging facilities
    loop = DESEventLoop(start_time=0)
    logger = _initialize_logger(args.verbose, loop)
    aiodtnsim.reports.logging.logger = logger
    asyncio.set_event_loop(loop)

    print(":: Reading TVGs and transmission plan...")

    # Load TVGs and transmission plan
    ftvg = tvgutil.tvg.from_serializable(json.load(args.FTVG))
    args.FTVG.close()
    transmission_plan = json.load(args.tplan) if args.tplan else []
    args.tplan.close()

    # Initialize simulation monitoring/reporting
    event_dispatcher = aiodtnsim.EventDispatcher()
    event_dispatcher.add_subscriber(aiodtnsim.reports.logging.LoggingReport())
    ms_report = aiodtnsim.reports.MessageStatsReport()
    event_dispatcher.add_subscriber(ms_report)
    msg_ops_report = MessageOpsReport()
    event_dispatcher.add_subscriber(msg_ops_report)

    # Determine used node_impl and msg_impl classes
    router_node_impl, cgr_algo, node_params, algo_params = ROUTERS[args.algo]
    if cgr_algo:
        ptvg_json = json.load(args.ptvg)
        args.ptvg.close()
        if "contact_type" in ptvg_json:
            simple_ptvg = tvgutil.tvg.from_serializable(ptvg_json)
            multi_ptvg = {
                node: simple_ptvg
                for node in simple_ptvg.vertices
            }
        else:
            multi_ptvg = {
                node: tvgutil.tvg.from_serializable(ptvg)
                for node, ptvg in ptvg_json.items()
            }

        # Free some memory
        del ptvg_json

        def cgr_node_impl(eid, *arg, **kwarg):
            return router_node_impl(
                eid,
                *arg,
                routing_algorithm=functools.partial(
                    cgr_algo,
                    **{k: getattr(args, k) for k in algo_params},
                ),
                ptvg=multi_ptvg[eid],
                **kwarg,
            )

        node_impl = cgr_node_impl
        msg_impl = CGRPacket
        # We use an EventHandler to add the hops to the messages for CGR
        event_dispatcher.add_subscriber(CGRMessagePathEventHandler())
    else:
        assert not algo_params
        if args.ptvg:
            print("WARNING: P-TVG will not be used in opportunistic router",
                  file=sys.stderr)
        node_impl = router_node_impl
        msg_impl = aiodtnsim.Message

    def get_new_link(contact):
        cls = (
            IdealRedundancyImpairedCLALink if args.monte_carlo_link else
            SimulatedIdealRedundancyImpairedCLALink
        )
        return cls(
            contact,
            block_size=args.link_block_size,
        )

    print(":: Initializing nodes...")

    # Initialize nodes
    sim_nodes = {
        eid: node_impl(
            eid,
            args.buffersize,
            event_dispatcher,
            link_factory=get_new_link,
            tx_channels=args.tx_channels,
            rx_channels=args.rx_channels,
            **{k: getattr(args, k) for k in node_params},
        )
        for eid in ftvg.vertices
    }

    if cgr_algo:
        # Free some memory
        del multi_ptvg

    if args.dropplan:
        print(":: Scheduling message dropping...")
        dropplan = json.load(args.dropplan)
        for node, times in dropplan.items():
            for ts in times:
                asyncio.ensure_future(drop_all_msgs(sim_nodes[node], ts))


    print(":: Scheduling contacts...")

    # Initialize and schedule contacts
    min_start = float("inf")
    max_end = float("-inf")
    for _, contact_list in ftvg.edges.items():
        for factual_contact in contact_list:
            sim_contact = aiodtnsim.Contact(
                tx_node=sim_nodes[factual_contact.tx_node],
                rx_node=sim_nodes[factual_contact.rx_node],
                start_time=factual_contact.start_time,
                end_time=factual_contact.end_time,
                # our Link uses Contact.param, though this is constant and
                # needed for the report(s)
                bit_rate=factual_contact.to_simple().bit_rate,
                delay=float("nan"),  # our Link uses Contact.param
                param=factual_contact,
            )
            min_start = min(min_start, factual_contact.start_time)
            max_end = max(max_end, factual_contact.end_time)
            asyncio.ensure_future(
                sim_contact.tx_node.schedule_contact(sim_contact)
            )

    # Free some memory
    del ftvg
    del contact_list
    del factual_contact

    print(":: Scheduling messages...")

    # Schedule messages
    for message_parameters in transmission_plan:
        # message_parameters is a list with the same first elements
        message = msg_impl(*message_parameters)
        assert message.deadline > message.start_time
        asyncio.ensure_future(
            sim_nodes[message.source].schedule_injection(message)
        )

    # Free some memory
    del transmission_plan

    sim_duration = int(math.ceil(max_end - min_start))
    print(":: Firing up event loop!")
    print(f"This run will simulate {round(sim_duration / 3600, 2)} hours.")

    if args.progress:
        sim_duration = int(math.ceil(max_end - min_start))
        progress_step = sim_duration / 100
        pbar = tqdm.tqdm(total=100)

        async def progress():
            await asyncio.sleep(min_start - loop.time())
            for _ in range(100):
                await asyncio.sleep(progress_step)
                pbar.update(1)

        asyncio.ensure_future(progress())

    # Run simulation by firing up the event loop
    loop.run_forever()

    if args.progress:
        pbar.close()

    # Print report results
    ms_report.print()
    if args.savestats:
        json.dump(
            ms_report.serializable(),
            args.savestats,
        )
        args.savestats.write("\n")
        args.savestats.close()
    if args.savemsgops:
        msg_ops_report.print(args.savemsgops)
        args.savemsgops.close()


def _get_argument_parser():
    parser = argparse.ArgumentParser(
        description="perform a flexible routing simulation, with error rate",
    )
    parser.add_argument(
        "FTVG",
        type=argparse.FileType("r"),
        help="the factual TVG (F-TVG) JSON file",
    )
    parser.add_argument(
        "-a", "--algo",
        choices=ROUTERS.keys(), default="epidemic",
        help="the routing algorithm to be used (default: epidemic)",
    )
    parser.add_argument(
        "-b", "--buffersize",
        type=float, default=-1,
        help="the node buffer size, in bit (default: unlimited)",
    )
    parser.add_argument(
        "-t", "--tplan",
        type=argparse.FileType("r"),
        default=None,
        help="the transmission plan JSON file, empty by default",
    )
    parser.add_argument(
        "-p", "--ptvg",
        type=argparse.FileType("r"),
        default=None,
        help="the predicted TVG (P-TVG) JSON file, used for CGR",
    )
    parser.add_argument(
        "-s", "--savestats",
        type=argparse.FileType("w"),
        default=None,
        help="save message stats into the specified file",
    )
    parser.add_argument(
        "--savemsgops",
        type=argparse.FileType("w"),
        default=None,
        help="save all message operations into the specified file",
    )
    for opt, opt_type, default, desc in CMD_PARAMS:
        if opt_type == bool:
            bool_opt = parser.add_mutually_exclusive_group(required=False)
            bool_opt.add_argument(
                f"--{opt}",
                dest=opt,
                action="store_true",
                help=f"enable: {desc} (default = {default})",
            )
            bool_opt.add_argument(
                f"--no-{opt}",
                dest=opt,
                action="store_false",
                help=f"disable: {desc} (default = {default})",
            )
            parser.set_defaults(**{opt: default})
        else:
            parser.add_argument(
                f"--{opt}",
                type=opt_type, default=default,
                help=f"{desc} (default = {default})",
            )
    parser.add_argument(
        "--link_block_size",
        type=int, default=(1500 * 8),
        help="the block size for link-layer transmission (default: 1.5 kB)",
    )
    parser.add_argument(
        "--tx_channels",
        type=int, default=None,
        help="the amount of TX channels per node (default: infinite)",
    )
    parser.add_argument(
        "--rx_channels",
        type=int, default=None,
        help="the amount of RX channels per node (default: infinite)",
    )
    parser.add_argument(
        "--monte_carlo_link",
        action="store_true",
        help=(
            "simulate random block transmission over the link if "
            "condition msg_size >> block_size does NOT hold"
        ),
    )
    parser.add_argument(
        "--dropplan",
        type=argparse.FileType("r"),
        default=None,
        help="a JSON file containing planned (whole-buffer) message drops",
    )
    parser.add_argument(
        "--progress",
        action="store_true",
        help="show a progress bar",
    )
    parser.add_argument(
        "-v", "--verbose",
        action="count",
        default=0,
        help="increase log output verbosity",
    )
    return parser


def _initialize_logger(verbosity: int, event_loop: asyncio.AbstractEventLoop):

    def _add_time_to_record(record):
        record.sim_time = asyncio.get_running_loop().time()
        return True

    log_level = {
        0: logging.WARN,
        1: logging.INFO,
    }.get(verbosity, logging.DEBUG)

    # Simulator-wide logging to the console
    logging.basicConfig(
        level=log_level,
        format="%(asctime)s %(levelname)s: %(message)s",
    )

    # Specific logging for reporting the simulation progress
    logger = logging.getLogger(sys.argv[0])
    handler = logging.StreamHandler()
    handler.setLevel(log_level)
    formatter = logging.Formatter("%(sim_time).2f %(levelname)s: %(message)s")
    handler.setFormatter(formatter)
    logger.addFilter(_add_time_to_record)
    logger.addHandler(handler)
    logger.propagate = False

    return logger


if __name__ == "__main__":
    _main(_get_argument_parser().parse_args())
