#!/usr/bin/env python
# encoding: utf-8

import os
import json
import argparse

import numpy as np

# Use another font
# import matplotlib
# matplotlib.rcParams['text.usetex'] = False
# matplotlib.rcParams['font.family'] = "Open Sans"

# Fix PDF fonts for IEEE validator (WARNING: makes PDFs BIG!)
# import matplotlib
# matplotlib.rcParams['pdf.fonttype'] = 42
# matplotlib.rcParams['ps.fonttype'] = 42

COLORS = [
    "gainsboro",
    "silver",
    "darkgrey",
    "grey",
    "#5e5e5e",
    "#4a4a4a",
    "#3a3a3a",
    "#2c2c2c",
]
TEXTCOLORS = [
    "black",
    "black",
    "black",
    "black",
    "white",
    "white",
    "white",
    "white",
]
PERCENTILES_ARRAY = [0, 5, 10, 20, 50, 80, 90, 95, 100]
PERCENTILES_RANGE = (2.5, 97.5)
#PERCENTILES_RANGE = (10, 90)


def _preprocess_data_entry(e, factor=1, use_abs=False, subtract_from=None):
    if subtract_from is not None:
        e = subtract_from - e
    e = e * factor
    return abs(e) if use_abs else e


def _single_plot(ax, data, algos, xticks, plot_name, ymin=None, ymax=None,
                 title=None, xlabel=None, ylabel=None, plot_std=True,
                 percentile=True, log=False, legend_col=1, headroom=0.2):
    if not title:
        title = plot_name

    ind = np.arange(len(xticks))
    bar_width = 0.75 / len(algos)

    rects = []
    for n_algo, algo in enumerate(algos):
        means = [np.mean(data[algo][xtick]) for xtick in xticks]
        stds = [np.std(data[algo][xtick]) for xtick in xticks]
        if plot_std:
            if percentile:
                pct_hi, pct_lo = PERCENTILES_RANGE
                assert 100 - pct_hi == pct_lo
                stds_up = [
                    np.percentile(data[algo][xtick], pct_hi) -
                    np.mean(data[algo][xtick])
                    for xtick in xticks
                ]
                stds_lo = [
                    np.mean(data[algo][xtick]) -
                    np.percentile(data[algo][xtick], pct_lo)
                    for xtick in xticks
                ]
            else:
                stds_up = stds_lo = stds
        else:
            stds_up = stds_lo = None
        for n_xtick, xtick in enumerate(xticks):
            pcts = [
                np.percentile(data[algo][xtick], PERCENTILES_ARRAY)
                for xtick in xticks
            ]
            print("{} ({}, {}): mean = {}, std = {}, {}".format(
                plot_name,
                algo,
                xtick,
                round(means[n_xtick], 4),
                round(stds[n_xtick], 4),
                ", ".join([
                    "{}%: {}".format(pct, round(val, 4))
                    for pct, val in zip(
                        PERCENTILES_ARRAY,
                        pcts[n_xtick],
                    )
                ]),
            ))
        rect = ax.bar(
            ind + n_algo * bar_width,
            means,
            bar_width,
            color=COLORS[n_algo % len(COLORS)],
            yerr=(None if not plot_std else [stds_lo, stds_up]),
            capsize=3,
            log=log,
        )
        rects.append(rect)
    # Set axes min/max
    if ymin and ymax:
        ax.set_ylim(bottom=ymin, top=ymax)
    elif ymin:
        ax.set_ylim(bottom=ymin)
    elif ymax:
        ax.set_ylim(top=ymax)
    else:
        ymin, ymax = ax.get_ylim()
        ymax += (ymax - ymin) * headroom  # add some headroom for legend
        ax.set_ylim(bottom=ymin, top=ymax)
    # Label the plot
    if xlabel:
        ax.set_xlabel(xlabel)
    if ylabel:
        ax.set_ylabel(ylabel)
    # Set ticks
    ax.set_xticks(ind + (len(algos) - 1) * (bar_width / 2))
    ax.set_xticklabels(xticks)
    ax.legend(
        rects,
        algos,
        loc="upper right",
        ncol=legend_col,
    )


def _main(args):
    assert len(args.FILE) % len(args.algos) == 0
    assert (not args.xticks or
            len(args.FILE) / len(args.xticks) == len(args.algos))
    print("Plotting: {}".format(args.PLOT))

    tick_count = int(len(args.FILE) / len(args.algos))
    xticks = args.xticks or list(range(tick_count))
    result_data = [
        json.load(fileobj)
        for fileobj in args.FILE
    ]
    data = {
        algo: {
            xtick: [
                _preprocess_data_entry(
                    e,
                    args.factor,
                    args.abs,
                    args.subtract_from,
                )
                for e in result_data[
                    len(args.algos) * xtick_i + algo_i
                ][args.PLOT]
            ]
            for xtick_i, xtick in enumerate(xticks)
        }
        for algo_i, algo in enumerate(args.algos)
        if algo not in args.exclude_algos
    }

    import matplotlib.pyplot as plt
    fig = plt.figure()
    ax = fig.add_subplot(111)

    _single_plot(
        ax,
        data,
        [algo for algo in args.algos if algo not in args.exclude_algos],
        xticks,
        args.PLOT,
        ymin=args.ymin,
        ymax=args.ymax,
        title=args.title,
        xlabel=args.xlabel,
        ylabel=args.ylabel,
        plot_std=(not args.nostd),
        percentile=args.percentile,
        log=args.log,
        legend_col=args.legend_columns,
        headroom=args.headroom,
    )

    if args.outdir:
        fig.set_size_inches(6, 3.45)
        plt.tight_layout()
        # If an output file has been specified, store the plot
        plt.savefig(
            os.path.join(args.outdir, "{}{}.pdf".format(
                args.prefix,
                args.plot_name or args.PLOT,
            ))
        )
    else:
        plt.show()


def _get_argument_parser():
    parser = argparse.ArgumentParser(description="Plot Tool")
    parser.add_argument(
        "PLOT",
        help="the name of the dict element to be plotted",
    )
    parser.add_argument(
        "FILE",
        nargs="+",
        type=argparse.FileType("r"),
        help="the input JSON files containing results, grouped by algorithm",
    )
    parser.add_argument(
        "--outdir",
        default=None,
        help="put plots to the specified directory",
    )
    parser.add_argument(
        "--prefix",
        default="",
        help="a prefix to add in front of the plot filename",
    )
    parser.add_argument(
        "--algos",
        nargs="*",
        default=["<algo1>"],
        help="the algorithm names to be plotted",
    )
    parser.add_argument(
        "--exclude-algos",
        nargs="*",
        default=[],
        help="algorithm names contained but to be filtered out of the data",
    )
    parser.add_argument(
        "--title",
        default=None,
        help="the plot title, defaults to the file name",
    )
    parser.add_argument(
        "--xticks",
        nargs="*",
        default=[],
        help="the x-axis tick labels",
    )
    parser.add_argument(
        "--ymin",
        type=float, default=None,
        help="the minimum for the y-axis",
    )
    parser.add_argument(
        "--ymax",
        type=float,
        default=None,
        help="the maximum for the y-axis",
    )
    parser.add_argument(
        "--xlabel",
        default="",
        help="the x-axis label",
    )
    parser.add_argument(
        "--ylabel",
        default="",
        help="the y-axis label",
    )
    parser.add_argument(
        "--nostd",
        action="store_true",
        help="do not compute a standard deviation",
    )
    parser.add_argument(
        "--factor",
        type=float,
        default=1,
        help="an optional value to scale the data points",
    )
    parser.add_argument(
        "--abs",
        action="store_true",
        help="use absolute values (abs(...) of each entry)",
    )
    parser.add_argument(
        "--percentile",
        action="store_true",
        help="use 5 to 95 percentiles for error bars",
    )
    parser.add_argument(
        "--log",
        action="store_true",
        help="use a log scale",
    )
    parser.add_argument(
        "--subtract-from",
        type=float,
        default=None,
        help="subtract values from given number (before factor)",
    )
    parser.add_argument(
        "--legend-columns",
        type=int,
        default=1,
        help="the amount of columns used for the legend (default: 1)",
    )
    parser.add_argument(
        "--headroom",
        type=float,
        default=0.55,
        help="legend headroom factor (default: 0.55)",
    )
    parser.add_argument(
        "--plot-name",
        default=None,
        help="an alternative name for the plot file (default: stat name)",
    )
    return parser


if __name__ == "__main__":
    _main(_get_argument_parser().parse_args())
