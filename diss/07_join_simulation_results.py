#!/usr/bin/env python
# encoding: utf-8

import sys
import argparse
import json

import numpy as np

from aiodtnsim.reports.message_stats_report import print_stat


def _main(args):
    result = {
        "runtimes": [],
        "delays": [],
        "tx_counts_delivered": [],
        "tx_counts_all": [],
        "rx_counts_delivered": [],
        "rx_counts_all": [],
        "tx_volumes_delivered": [],
        "tx_volumes_all": [],
        "rx_volumes_delivered": [],
        "rx_volumes_all": [],
        "scheduling_counts": [],
        "replica_counts": [],
        "rescheduling_count": [],
        "contact_utilization_total": [],
        "buffer_utilization_total": [],
        # tuples: tx_eid, rx_eid, start, end, ratio, bits
        "contact_utilization": [],
        # tuples: eid, time, buffer used (bits)
        "buffer_utilization": [],
    }
    result_rates = {
        "delivery_rate": [],
        "initial_scheduling_success_rate": [],
        "energy_efficiency": [],
        # technically not rates but a single value per run
        "tx_count_all_total": [],
        "tx_count_delivered_total": [],
        "tx_counts_all_per_bdl": [],
    }

    for in_file in args.INPUT_FILE:
        data = json.load(in_file)
        in_file.close()
        _preprocess(data)
        for k, v in result.items():
            v.extend(data[k])
        _compute_rates(result_rates, data)

    if not args.quiet:
        _print_stats(result, result_rates)

    result.update(result_rates)
    json.dump(
        result,
        args.outfile,
        indent=(4 if args.indent else None),
    )
    args.outfile.write("\n")
    args.outfile.close()


def _preprocess(data):
    data["contact_utilization_total"] = [
        cu
        for _, _, _, _, cu, _ in data["contact_utilization"]
    ]
    data["buffer_utilization_total"] = [
        bu
        for bu_list in data["buffer_utilization"].values()
        for _, bu in bu_list
    ]
    data["buffer_utilization"] = [
        (node, time, bu)
        for node, bu_list in data["buffer_utilization"].items()
        for time, bu in bu_list
    ]
    data["rescheduling_count"] = [
        sum(
            max(0, e - 1)
            for e in data["scheduling_counts"]
        )
    ]


def _compute_rates(result_rates, data):
    result_rates["delivery_rate"].append(
        len(data["delays"]) / len(data["tx_counts_all"])
    )
    result_rates["initial_scheduling_success_rate"].append(
        sum(1 for c in data["scheduling_counts"] if c == 1) /
        len(data["scheduling_counts"])
    )
    result_rates["energy_efficiency"].append(
        len(data["delays"]) / sum(data["replica_counts"])
    )
    result_rates["tx_count_all_total"].append(sum(data["tx_counts_all"]))
    result_rates["tx_counts_all_per_bdl"].append(
        sum(data["tx_counts_all"]) / len(data["tx_counts_all"])
    )
    result_rates["tx_count_delivered_total"].append(
        sum(data["tx_counts_delivered"])
    )


def _print_stats(result, result_rates):
    print("> Totals:", file=sys.stderr)
    print_stat("Average sim. runtime", result["runtimes"])
    d_times = result["delays"]
    total_count = len(result["tx_counts_all"])
    sched_counts = result["scheduling_counts"]
    print(f"Delivered: {len(d_times)} of {total_count} "
          f"({round(len(d_times) / total_count * 100, 2)} %)",
          file=sys.stderr)
    drate_mean = np.mean(result_rates["delivery_rate"])
    drate_std = np.std(result_rates["delivery_rate"])
    print(f"Overall delivery rate: {round(drate_mean * 100, 2)} % "
          f"(std = {round(drate_std * 100, 4)} %)", file=sys.stderr)
    issrate_mean = np.mean(result_rates["initial_scheduling_success_rate"])
    issrate_std = np.std(result_rates["initial_scheduling_success_rate"])
    print("Overall initial scheduling success rate: "
          f"{round(issrate_mean * 100, 2)} % "
          f"(std = {round(issrate_std * 100, 4)} %)", file=sys.stderr)
    if sched_counts:
        print(f"Average sched. count: {round(np.mean(sched_counts), 2)} "
              f"(std = {round(np.std(sched_counts), 4)})",
              file=sys.stderr)
        schedrate = sum(1 for c in sched_counts if c == 1)
        print(f"Rate of msgs. initially scheduled successfully: "
              f"{schedrate} of {len(sched_counts)} "
              f"({round(schedrate / len(sched_counts) * 100, 2)} %)",
              file=sys.stderr)
    tx_counts = result["tx_counts_delivered"]
    if d_times and sum(tx_counts) > 0:
        print_stat("Average delivery delay", d_times)
        # Amount and Volume of Messages Delivered
        rx_counts = result["rx_counts_delivered"]
        tx_volumes = result["tx_volumes_delivered"]
        rx_volumes = result["rx_volumes_delivered"]
        print_stat("Average delivered TX count", tx_counts)
        print_stat("Average delivered RX count", rx_counts)
        print_stat("Average delivered TX volume", tx_volumes)
        print_stat("Average delivered RX volume", rx_volumes)
        # Loss Ratio / Retransmissions & Energy Efficiency
        loss_ratio_pct = round(
            100 - np.mean(rx_counts) / np.mean(tx_counts) * 100, 2
        )
        print(f"=> Link loss ratio for delivered msg.: {loss_ratio_pct} %")
        replicas = result["replica_counts"]
        print_stat("Average msg. replica count", replicas)
        tx_counts_all = result["tx_counts_all"]
        rx_counts_all = result["rx_counts_all"]
        tx_volumes_all = result["tx_volumes_all"]
        rx_volumes_all = result["rx_volumes_all"]
        print_stat("Average TX count", tx_counts_all)
        print_stat("Average RX count", rx_counts_all)
        print_stat("Average TX volume", tx_volumes_all)
        print_stat("Average RX volume", rx_volumes_all)
        loss_ratio_all_pct = round(
            100 - np.mean(rx_counts_all) / np.mean(tx_counts_all) * 100, 2
        )
        print(f"=> Overall link loss ratio: {loss_ratio_all_pct} %")
        eeff_pct = round(len(d_times) / sum(replicas) * 100, 2)
        print(f"=> Energy efficiency (delivered): {eeff_pct} %")
        # Contact Utilization
        contact_utilization = result["contact_utilization_total"]
        print_stat("Contact utilization (%)",
                   np.array(contact_utilization) * 100)
        # Buffer Utilization, overall (after every contact)
        buffer_utilization = result["buffer_utilization_total"]
        print_stat("Buffer utilization", np.array(buffer_utilization))
    elif sum(tx_counts) == 0:
        print("No messages have been transmitted.", file=sys.stderr)
    else:
        print("No messages have been delivered.", file=sys.stderr)


def _get_argument_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "INPUT_FILE",
        type=argparse.FileType("r"),
        nargs="*",
        help="the input files to be joined together"
    )
    parser.add_argument(
        "-o", "--outfile",
        type=argparse.FileType("w"),
        default=sys.stdout,
        help="the output filename",
    )
    parser.add_argument(
        "-i", "--indent",
        action="store_true",
        help="indent the JSON output",
    )
    parser.add_argument(
        "-q", "--quiet",
        action="store_true",
        help="do not print total statistics",
    )
    return parser


if __name__ == "__main__":
    _main(_get_argument_parser().parse_args())
