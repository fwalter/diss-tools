#!/bin/bash

NAME="$1"
TEST="T01_confaccu"
WORKERS=4

set -euo pipefail

WORKING_DIR="results_$TEST"

SCENARIOS=(
    "S1-p3b"
    "S1-p5i"
)

# Algos and flags to run an observe-distribute-predict step
OBSV_ALGOS=(
    ewma_3e-1
    ewma_1e-1
    ratio
)
# Note that 10800 is just the metric update interval, predictions are performed every 43200s at minimum (see below)
OBSV_FLAGS=(
    "--min_metric_update_interval 10800 --confidence_predictor ewma --w_obs 0.3 --volume_predictor minelev --min_elevation 10 --link_block_size 12000"
    "--min_metric_update_interval 10800 --confidence_predictor ewma --w_obs 0.1 --volume_predictor minelev --min_elevation 10 --link_block_size 12000"
    "--min_metric_update_interval 10800 --confidence_predictor ratio --volume_predictor minelev --min_elevation 10 --link_block_size 12000"
)

# Flags for the prediction script
PREDICT_FLAGS="--min_predict_interval 43200 --max_predict_interval 86400"

PLOT_SCENARIOS=(
    "S1-p3b"
    "S1-p3b"
    "S1-p3b"
    "S1-p3b"
    "S1-p5i"
    "S1-p5i"
    "S1-p5i"
    "S1-p5i"
)
PLOT_ARGS1="--group_by_tx --time_resolution 43200 --no_subtract --only_last_prediction --markers"
PLOT_NAMES=(
     "prob-nodist"
     "prob-dist"
     "noprob-nodist"
     "noprob-dist"
     "intermittent-nodist"
     "intermittent-dist"
     "nonintermittent-nodist"
     "nonintermittent-dist"
)
PLOT_ARGS2=(
     "--txnodefilter LEO-11"
     "--txnodefilter LEO-11 --prednodefilter LEO-21"
     "--txnodefilter LEO-01"
     "--txnodefilter LEO-01 --prednodefilter LEO-21"
     "--txnodefilter GS-01"
     "--txnodefilter GS-01 --prednodefilter GS-07"
     "--txnodefilter GS-06"
     "--txnodefilter GS-06 --prednodefilter GS-07"
)

for scenario in "${SCENARIOS[@]}"; do
    base_scenario="$(echo "$scenario" | cut -d '+' -f 1)"
    if [[ ! -r "$base_scenario.02.scenario.json" || ! -r "$base_scenario.02.ftvg.json" ]]; then
        echo "$base_scenario.02.scenario.json and $base_scenario.02.ftvg.json have to exist!" >&2
        exit 1
    fi
done

mkdir -p "$WORKING_DIR"

set -x

# Remove all empty result files to have a clean setup
find "$WORKING_DIR" -type f -size 0 -delete

for ((s = 0; s < ${#SCENARIOS[@]}; s++)); do
    scenario="${SCENARIOS[$s]}"

    prefix="$WORKING_DIR/$scenario"
    base_scenario="$(echo "$scenario" | cut -d '+' -f 1)"
    base_prefix="$WORKING_DIR/$base_scenario"

    for ((i = 0; i < ${#OBSV_ALGOS[@]}; i++)); do
        obsv_algo="${OBSV_ALGOS[$i]}"
        obsv_flags="${OBSV_FLAGS[$i]}"

        if [ -r "$base_scenario.02.ptvg.json" ]; then
            [ -r "$base_prefix.03.$obsv_algo.kb.observed.json" ] || \
                python 03_observe_contacts.py -v \
                    $obsv_flags \
                    --ptvg "$base_scenario.02.ptvg.json" \
                    --output "$base_prefix.03.$obsv_algo.kb.observed.json" \
                    "$base_scenario.02.scenario.json" \
                    "$base_scenario.02.ftvg.json"
        else
            [ -r "$base_prefix.03.$obsv_algo.kb.observed.json" ] || \
                python 03_observe_contacts.py -v \
                    $obsv_flags \
                    --output "$base_prefix.03.$obsv_algo.kb.observed.json" \
                    "$base_scenario.02.scenario.json" \
                    "$base_scenario.02.ftvg.json"
        fi
        [[ -r "$base_prefix.03.$obsv_algo.kb.dist.json" && -r "$base_prefix.04.$i.$obsv_algo.diststats.json" ]] || \
            python 04_a_distribute_metrics.py \
                --output "$base_prefix.03.$obsv_algo.kb.dist.json" \
                --progress \
                --savestats "$base_prefix.04.$i.$obsv_algo.diststats.json" \
                "$base_scenario.02.ftvg.json" \
                "$base_prefix.03.$obsv_algo.kb.observed.json"
        [ -r "$base_prefix.05.$obsv_algo.ptvg.json" ] || \
            python 05_predict_ptvg.py -v \
                $PREDICT_FLAGS \
                --output "$base_prefix.05.$obsv_algo.ptvg.json" \
                --workers "$WORKERS" \
                "$base_prefix.03.$obsv_algo.kb.dist.json"

        cp "$base_prefix.05.$obsv_algo.ptvg.json" "$base_prefix.05.xalgo-$i.ptvg.json"
    done
done

for ((s = 0; s < ${#PLOT_SCENARIOS[@]}; s++)); do
    scenario="${PLOT_SCENARIOS[$s]}"
    prefix="$WORKING_DIR/$scenario"
    base_scenario="$(echo "$scenario" | cut -d '+' -f 1)"
    base_prefix="$WORKING_DIR/$base_scenario"
    plot_name="${PLOT_NAMES[$s]}"
    plot_args2="${PLOT_ARGS2[$s]}"

    [ -r "$base_prefix.plot.$plot_name.pdf" ] || python helpers/06_plot_confidence_prediction_accuracy.py -n "${OBSV_ALGOS[@]}" $PLOT_ARGS1 $plot_args2 --outfile "$base_prefix.plot.$plot_name.pdf" -- "$base_scenario.02.ftvg.json" $base_prefix.05.xalgo-*.ptvg.json
done

PLOT_DIR="$WORKING_DIR/dist_plots"

mkdir -p "$PLOT_DIR"

out="--outdir $PLOT_DIR"
xlabel="scenario"
algos="${OBSV_ALGOS[@]}"

SUMMARY_FILE="$PLOT_DIR/stats_summary.txt"
echo "Scenarios: ${SCENARIOS[@]}" > "$SUMMARY_FILE"
echo "Algos: $(echo "$algos" | tr '\n' ' ')" >> "$SUMMARY_FILE"
echo -e "Date: $(date)\n" >> "$SUMMARY_FILE"

filelist=()
for scenario in "${SCENARIOS[@]}"; do
    base_scenario="$(echo "$scenario" | cut -d '+' -f 1)"
    base_prefix="$WORKING_DIR/$base_scenario"
    for ((i = 0; i < ${#OBSV_ALGOS[@]}; i++)); do
        obsv_algo="${OBSV_ALGOS[$i]}"
        python3 07_join_simulation_results.py $base_prefix.04.$i.$obsv_algo.diststats.json \
                -o $base_prefix.04.joined.$i.$obsv_algo.diststats.json
    done
    filelist+=($base_prefix.04.joined.*.diststats.json)
done

python 08_plot_results.py $out \
    --title "Bundle Transmission Count" \
    --xlabel "$xlabel" --xticks ${SCENARIOS[@]} \
    --algos $algos \
    --ylabel "transmissions per bundle" \
    --percentile \
    "tx_counts_all" \
    ${filelist[@]} | tee -a "$SUMMARY_FILE"

echo >> "$SUMMARY_FILE"

python 08_plot_results.py $out \
    --title "Bundle Transmission Volume" \
    --xlabel "$xlabel" --xticks ${SCENARIOS[@]} \
    --algos $algos \
    --ylabel "avg. transmitted volume per bundle / KiB" \
    --factor 0.0001220703125 --percentile \
    "tx_volumes_all" \
    ${filelist[@]} | tee -a "$SUMMARY_FILE"

echo >> "$SUMMARY_FILE"

python 08_plot_results.py $out \
    --title "Bundle Replica Count" \
    --xlabel "$xlabel" --xticks ${SCENARIOS[@]} \
    --algos $algos \
    --ylabel "average replicas per bundle" \
    --percentile \
    "replica_counts" \
    ${filelist[@]} | tee -a "$SUMMARY_FILE"

echo >> "$SUMMARY_FILE"

python 08_plot_results.py $out \
    --title "Contact Utilization" \
    --xlabel "$xlabel" --xticks ${SCENARIOS[@]} \
    --factor 100 --algos $algos \
    --ylabel "average contact utilization / %" \
    --percentile \
    "contact_utilization_total" \
    ${filelist[@]} | tee -a "$SUMMARY_FILE"

echo >> "$SUMMARY_FILE"

python 08_plot_results.py $out \
    --title "Buffer Utilization" \
    --xlabel "$xlabel" --xticks ${SCENARIOS[@]} \
    --factor 0.0001220703125 --algos $algos \
    --ylabel "average buffer utilization / KiB" \
    --percentile \
    "buffer_utilization_total" \
    ${filelist[@]} | tee -a "$SUMMARY_FILE"

echo >> "$SUMMARY_FILE"
