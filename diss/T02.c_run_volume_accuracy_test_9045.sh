#!/bin/bash

NAME="9045-6s-15g"
TEST="T02_volaccu"
WORKERS=4

WORKING_DIR="results_${TEST}_${NAME}"

if [[ "$1" = "clean" ]]; then
    set -x
    rm -r "$WORKING_DIR"
    exit 0
fi

set -euo pipefail

if [[ ! -r "$NAME.02.scenario.json" || ! -r "$NAME.02.ftvg.json" ]]; then
    echo "$NAME.02.scenario.json and $NAME.02.ftvg.json have to exist!" >&2
    exit 1
fi

# This test run generates un-distributed predictions for assessing their
# accuracy. It checks the accuracy of the volume prediction over time.
# The result is a set of P-TVGs for different algorithms.

set -x

mkdir -p "$WORKING_DIR"
# Remove all empty result files to have a clean setup
find "$WORKING_DIR" -type f -size 0 -delete

PREFIX="$WORKING_DIR/$NAME"

OBSV_FLAGS_ALGO_1="--min_metric_update_interval 43200 --confidence_predictor disabled --volume_predictor minelev --min_elevation 10"
OBSV_FLAGS_ALGO_2="--min_metric_update_interval 43200 --confidence_predictor disabled --volume_predictor minelev --min_elevation 20"
OBSV_FLAGS_ALGO_3="--min_metric_update_interval 43200 --confidence_predictor disabled --volume_predictor twostate --link_block_size 12000 --averaging_window_size 10"
OBSV_FLAGS_ALGO_4="--min_metric_update_interval 43200 --confidence_predictor disabled --volume_predictor tristate --link_block_size 12000 --averaging_window_size 10 --az_granularity 45"

[ -r $PREFIX.A1.03.kb.observed.json ] || python 03_observe_contacts.py $OBSV_FLAGS_ALGO_1 --workers $WORKERS -o $PREFIX.A1.03.kb.observed.json -v --ptvg $NAME.02.ptvg.json $NAME.02.scenario.json $NAME.02.ftvg.json
[ -r $PREFIX.A1.05.ptvg.json ] || python 05_predict_ptvg.py -o $PREFIX.A1.05.ptvg.json --workers $WORKERS -v $PREFIX.A1.03.kb.observed.json

[ -r $PREFIX.A2.03.kb.observed.json ] || python 03_observe_contacts.py $OBSV_FLAGS_ALGO_2 --workers $WORKERS -o $PREFIX.A2.03.kb.observed.json -v --ptvg $NAME.02.ptvg.json $NAME.02.scenario.json $NAME.02.ftvg.json
[ -r $PREFIX.A2.05.ptvg.json ] || python 05_predict_ptvg.py -o $PREFIX.A2.05.ptvg.json --workers $WORKERS -v $PREFIX.A2.03.kb.observed.json

[ -r $PREFIX.A3.03.kb.observed.json ] || python 03_observe_contacts.py $OBSV_FLAGS_ALGO_3 --workers $WORKERS -o $PREFIX.A3.03.kb.observed.json -v --ptvg $NAME.02.ptvg.json $NAME.02.scenario.json $NAME.02.ftvg.json
[ -r $PREFIX.A3.05.ptvg.json ] || python 05_predict_ptvg.py -o $PREFIX.A3.05.ptvg.json --workers $WORKERS -v $PREFIX.A3.03.kb.observed.json

[ -r $PREFIX.A4.03.kb.observed.json ] || python 03_observe_contacts.py $OBSV_FLAGS_ALGO_4 --workers $WORKERS -o $PREFIX.A4.03.kb.observed.json -v --ptvg $NAME.02.ptvg.json $NAME.02.scenario.json $NAME.02.ftvg.json
[ -r $PREFIX.A4.05.ptvg.json ] || python 05_predict_ptvg.py -o $PREFIX.A4.05.ptvg.json --workers $WORKERS -v $PREFIX.A4.03.kb.observed.json

PLOT_ARGS="--time_resolution 43200 --link_block_size 12000 --only_last_prediction --local -n minelev-10deg minelev-20deg twostate threestate --legend-columns 2 --legend-headroom --markers --workers $WORKERS"

[ -r $PREFIX.plot.all.absolute.pdf ] || python helpers/06_plot_volume_prediction_accuracy.py --rxnodefilter LEO-01 LEO-11 LEO-21 LEO-31 LEO-41 LEO-51 $PLOT_ARGS --outfile $PREFIX.plot.all.absolute.pdf $NAME.02.ftvg.json $PREFIX.A1.05.ptvg.json $PREFIX.A2.05.ptvg.json $PREFIX.A3.05.ptvg.json $PREFIX.A4.05.ptvg.json
[ -r $PREFIX.plot.gs01.absolute.pdf ] || python helpers/06_plot_volume_prediction_accuracy.py --rxnodefilter GS-01 $PLOT_ARGS --outfile $PREFIX.plot.gs01.absolute.pdf $NAME.02.ftvg.json $PREFIX.A1.05.ptvg.json $PREFIX.A2.05.ptvg.json $PREFIX.A3.05.ptvg.json $PREFIX.A4.05.ptvg.json
[ -r $PREFIX.plot.gs02.absolute.pdf ] || python helpers/06_plot_volume_prediction_accuracy.py --rxnodefilter GS-02 $PLOT_ARGS --outfile $PREFIX.plot.gs02.absolute.pdf $NAME.02.ftvg.json $PREFIX.A1.05.ptvg.json $PREFIX.A2.05.ptvg.json $PREFIX.A3.05.ptvg.json $PREFIX.A4.05.ptvg.json
[ -r $PREFIX.plot.gs03.absolute.pdf ] || python helpers/06_plot_volume_prediction_accuracy.py --rxnodefilter GS-03 $PLOT_ARGS --outfile $PREFIX.plot.gs03.absolute.pdf $NAME.02.ftvg.json $PREFIX.A1.05.ptvg.json $PREFIX.A2.05.ptvg.json $PREFIX.A3.05.ptvg.json $PREFIX.A4.05.ptvg.json
[ -r $PREFIX.plot.gs04.absolute.pdf ] || python helpers/06_plot_volume_prediction_accuracy.py --rxnodefilter GS-04 $PLOT_ARGS --outfile $PREFIX.plot.gs04.absolute.pdf $NAME.02.ftvg.json $PREFIX.A1.05.ptvg.json $PREFIX.A2.05.ptvg.json $PREFIX.A3.05.ptvg.json $PREFIX.A4.05.ptvg.json
[ -r $PREFIX.plot.gs10.absolute.pdf ] || python helpers/06_plot_volume_prediction_accuracy.py --rxnodefilter GS-10 $PLOT_ARGS --outfile $PREFIX.plot.gs10.absolute.pdf $NAME.02.ftvg.json $PREFIX.A1.05.ptvg.json $PREFIX.A2.05.ptvg.json $PREFIX.A3.05.ptvg.json $PREFIX.A4.05.ptvg.json

[ -r $PREFIX.plot.all.relative.pdf ] || python helpers/06_plot_volume_prediction_accuracy.py --relative --rxnodefilter LEO-01 LEO-11 LEO-21 LEO-31 LEO-41 LEO-51 $PLOT_ARGS --outfile $PREFIX.plot.all.relative.pdf $NAME.02.ftvg.json $PREFIX.A1.05.ptvg.json $PREFIX.A2.05.ptvg.json $PREFIX.A3.05.ptvg.json $PREFIX.A4.05.ptvg.json
[ -r $PREFIX.plot.gs01.relative.pdf ] || python helpers/06_plot_volume_prediction_accuracy.py --relative --rxnodefilter GS-01 $PLOT_ARGS --outfile $PREFIX.plot.gs01.relative.pdf $NAME.02.ftvg.json $PREFIX.A1.05.ptvg.json $PREFIX.A2.05.ptvg.json $PREFIX.A3.05.ptvg.json $PREFIX.A4.05.ptvg.json
[ -r $PREFIX.plot.gs02.relative.pdf ] || python helpers/06_plot_volume_prediction_accuracy.py --relative --rxnodefilter GS-02 $PLOT_ARGS --outfile $PREFIX.plot.gs02.relative.pdf $NAME.02.ftvg.json $PREFIX.A1.05.ptvg.json $PREFIX.A2.05.ptvg.json $PREFIX.A3.05.ptvg.json $PREFIX.A4.05.ptvg.json
[ -r $PREFIX.plot.gs03.relative.pdf ] || python helpers/06_plot_volume_prediction_accuracy.py --relative --rxnodefilter GS-03 $PLOT_ARGS --outfile $PREFIX.plot.gs03.relative.pdf $NAME.02.ftvg.json $PREFIX.A1.05.ptvg.json $PREFIX.A2.05.ptvg.json $PREFIX.A3.05.ptvg.json $PREFIX.A4.05.ptvg.json
[ -r $PREFIX.plot.gs04.relative.pdf ] || python helpers/06_plot_volume_prediction_accuracy.py --relative --rxnodefilter GS-04 $PLOT_ARGS --outfile $PREFIX.plot.gs04.relative.pdf $NAME.02.ftvg.json $PREFIX.A1.05.ptvg.json $PREFIX.A2.05.ptvg.json $PREFIX.A3.05.ptvg.json $PREFIX.A4.05.ptvg.json
[ -r $PREFIX.plot.gs10.relative.pdf ] || python helpers/06_plot_volume_prediction_accuracy.py --relative --rxnodefilter GS-10 $PLOT_ARGS --outfile $PREFIX.plot.gs10.relative.pdf $NAME.02.ftvg.json $PREFIX.A1.05.ptvg.json $PREFIX.A2.05.ptvg.json $PREFIX.A3.05.ptvg.json $PREFIX.A4.05.ptvg.json
