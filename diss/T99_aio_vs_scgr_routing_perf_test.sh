#!/bin/bash

TEST="T99_aio_scgr"

WORKING_DIR="results_$TEST"

SCENARIOS=(
    "S1+low"
    "S1+high"
    "S2+low"
    "S2+high"
)

ALGOS=(
    CGR-aiodtnsim
    CGR-pydtnsim
)

# Algos which need a factual P-TVG
PTVG_ALGOS=(
    CGR-aiodtnsim
    CGR-pydtnsim
)

RUNS=10

WORKERS=6
SETUP_WORKERS=24

# FTVG -> PTVG
TVG_CONVERSION_FLAGS="--link_block_size 12000"
# 03-flags for generating the "soft" PTVG
OBSV_FLAGS_SOFT_FRAME="--min_metric_update_interval 43200 --confidence_predictor disabled --volume_predictor factual --link_block_size 12000 --only_factual"
# "latest" prevents unnecessary generations and assigns always the best value -- no difference here...
METRIC_ASSIGNMENT_FLAGS="--strategy latest"
# Flags for the transmission plan - see T07
TPLAN_FLAGS=(
    "--maxgenpercent 80 --interval 273 --intervaldev 68 --lifetime 864000 --minsize 1638400 --invalidsrc LEO-01 LEO-02 LEO-03 LEO-11 LEO-12 LEO-13 LEO-21 LEO-22 LEO-23 --invaliddst LEO-01 LEO-02 LEO-03 LEO-11 LEO-12 LEO-13 LEO-21 LEO-22 LEO-23"
    "--maxgenpercent 80 --interval 112 --intervaldev 28 --lifetime 864000 --minsize 16777216 --invalidsrc LEO-01 LEO-02 LEO-03 LEO-11 LEO-12 LEO-13 LEO-21 LEO-22 LEO-23 --invaliddst LEO-01 LEO-02 LEO-03 LEO-11 LEO-12 LEO-13 LEO-21 LEO-22 LEO-23"
    "--maxgenpercent 80 --interval 223 --intervaldev 56 --lifetime 864000 --minsize 1638400 --invalidsrc LEO-01 LEO-11 LEO-21 LEO-31 LEO-41 LEO-51 --invaliddst LEO-01 LEO-11 LEO-21 LEO-31 LEO-41 LEO-51"
    "--maxgenpercent 80 --interval 92 --intervaldev 23 --lifetime 864000 --minsize 16777216 --invalidsrc LEO-01 LEO-11 LEO-21 LEO-31 LEO-41 LEO-51 --invaliddst LEO-01 LEO-11 LEO-21 LEO-31 LEO-41 LEO-51"
)

# Flags for the simulator, per algo - 2 GiB of buffer provided
SIM_FLAGS=(
    "--rx_channels 5 --tx_channels 5 --buffersize 17179869184 --graph_update_min_interval 43200 --link_block_size 12000 --algo ngsabr"
    "--rx_channels 5 --tx_channels 5 --buffersize 17179869184 --graph_update_min_interval 43200 --link_block_size 12000 --algo scgr"
)

COMMAND="$1"

set -euo pipefail

if [ -z "$COMMAND" ]; then
    COMMAND="run"
fi

for scenario in "${SCENARIOS[@]}"; do
    base_scenario="$(echo "$scenario" | cut -d '+' -f 1)"
    if [[ ! -r "$base_scenario.02.scenario.json" || ! -r "$base_scenario.02.ftvg.json" ]]; then
        echo "$base_scenario.02.scenario.json and $base_scenario.02.ftvg.json have to exist!" >&2
        exit 1
    fi
done

mkdir -p "$WORKING_DIR"

setup() {
    set -x

    # Remove all empty result files to have a clean setup
    find "$WORKING_DIR" -type f -size 0 -delete

    for ((s = 0; s < ${#SCENARIOS[@]}; s++)); do
        local scenario="${SCENARIOS[$s]}"
        local tplan_flags="${TPLAN_FLAGS[$s]}"

        local prefix="$WORKING_DIR/$scenario"
        local base_scenario="$(echo "$scenario" | cut -d '+' -f 1)"
        local base_prefix="$WORKING_DIR/$base_scenario"

        # Prepare accurate P-TVG
        [ -r "$prefix.05._CGR-factual.ptvg.json" ] || \
            python helpers/03_convert_ftvg_to_ptvg.py \
                $TVG_CONVERSION_FLAGS \
                --output "$prefix.05._CGR-factual.ptvg.json" \
                "$base_scenario.02.ftvg.json"

        # Provide P-TVG to all algos needing it
        for algo in "${PTVG_ALGOS[@]}"; do
            [ -r "$prefix.05.$algo.ptvg.json" ] || \
                cp "$prefix.05._CGR-factual.ptvg.json" "$prefix.05.$algo.ptvg.json"
        done

        for run in $(seq $RUNS); do
            [ -r "$prefix.06.transmission_plan_$run.json" ] || \
                python -m tvgutil.tools.create_transmission_plan \
                    $tplan_flags \
                    --output "$prefix.06.transmission_plan_$run.json" \
                    "$base_scenario.02.ftvg.json"
        done

        [ -r "$prefix.02.ftvg.json" ] || cp "$base_scenario.02.ftvg.json" \
            "$prefix.02.ftvg.json"
        [ -r "$prefix.02.scenario.json" ] || cp "$base_scenario.02.scenario.json" \
            "$prefix.02.scenario.json"
    done
}

job() {
    local scenario="$1"
    local algo="$2"
    local run="$3"
    local prefix="$WORKING_DIR/$scenario"
    local ptvg="$prefix.05.$algo.ptvg.json"

    for ((a = 0; a < ${#ALGOS[@]}; a++)); do
        if [[ "${ALGOS[$a]}" = "$algo" ]]; then
            local sim_flags="${SIM_FLAGS[$a]}"
            break
        fi
    done

    set -x

    [ -r "$prefix.06.transmission_plan_$run.json" ] || exit 1

    [ -r "$prefix.07.stats.$algo.$run.json" ] && return 0

    if [ -r "$ptvg" ]; then
        python 06_simulation.py \
            $sim_flags \
            --ptvg "$ptvg" \
            --tplan "$prefix.06.transmission_plan_$run.json" \
            --savestats "$prefix.07.stats.$algo.$run.json" \
            "$prefix.02.ftvg.json"
    else
        python 06_simulation.py \
            $sim_flags \
            --tplan "$prefix.06.transmission_plan_$run.json" \
            --savestats "$prefix.07.stats.$algo.$run.json" \
            "$prefix.02.ftvg.json"
    fi
}

teardown() {
    set -x

    exec bash draw_plots.sh \
        "$WORKING_DIR" \
        "$WORKING_DIR/plots_$TEST" \
        "${SCENARIOS[@]}"
}

case "$COMMAND" in
    run)
        setup
        python test_process_pool_executor.py \
            --workers $WORKERS \
            --scenarios "${SCENARIOS[@]}" \
            --algorithms "${ALGOS[@]}" \
            --runs $(seq $RUNS) \
            --progress \
            --logfile "$WORKING_DIR/test_executor.log" \
            "$0"
        teardown
        ;;
    clean)
        rm -rvf --one-file-system --preserve-root "$WORKING_DIR"
        ;;
    cleanrouting)
        rm -vf --one-file-system --preserve-root $WORKING_DIR/*06.transmission_plan*json
        rm -vf --one-file-system --preserve-root $WORKING_DIR/*07.stats*json
        rm -vf --one-file-system --preserve-root $WORKING_DIR/*08.plotstats*json
        ;;
    job)
        job "${@:2}"
        ;;
    *)
        echo "Usage: $0 [<run|clean|job>] [<job-args>]" >&2
        exit 1
        ;;
esac
