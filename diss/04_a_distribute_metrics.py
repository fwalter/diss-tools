#!/usr/bin/env python
# encoding: utf-8

"""Script to exchange metrics epidemically via a simulator instance."""

import argparse
import json
import sys
import asyncio
import logging
import math
import random

import cbor2
import tqdm

import tvgutil.tvg

import aiodtnsim
import aiodtnsim.reports
import aiodtnsim.reports.logging

import aiodtnsim.simulation.event_loop

from aiodtnsim.simulation.scheduling_event_node import SchedulingEventNode

from aiodtnsim.reports.message_stats_report import print_stat

from predictutil.impaired_cla_link import (
    SimulatedIdealRedundancyImpairedCLALink,
)

logger = logging.getLogger(__name__)


class KBUpdateHandler(aiodtnsim.EventHandler):
    """An event handler providing message routing statistics."""

    def __init__(self, node_eid, node_kb):
        self.node_eid = node_eid
        self.node_kb = node_kb
        self.known = {}
        self.dist_delays = []

    def message_received(self, time, rx_node, message, tx_node):
        if rx_node.eid != self.node_eid or message.source == self.node_eid:
            return
        m_tx_node, m_rx_node, conf_mi, vol_mi = message.data
        tup = (message.source, m_tx_node, m_rx_node)
        if tup not in self.known:
            self.known[tup] = set([float("-inf")])
        if message.start_time in self.known[tup]:
            return
        # Only add if it is the latest information
        if message.start_time > max(self.known[tup]):
            if m_rx_node not in self.node_kb:
                self.node_kb[m_rx_node] = {}
            if m_tx_node not in self.node_kb[m_rx_node]:
                self.node_kb[m_rx_node][m_tx_node] = []
            self.node_kb[m_rx_node][m_tx_node].append((
                time,
                conf_mi,
                vol_mi,
            ))
        self.known[tup].add(message.start_time)
        cur_time = asyncio.get_running_loop().time()
        self.dist_delays.append((
            message.source,
            m_tx_node,
            m_rx_node,
            message.start_time,
            cur_time - message.start_time,
        ))

    def get_dist_delays(self):
        return self.dist_delays


class MetricDistributionNode(SchedulingEventNode):

    newest_information_about = {}
    sent_to_node = {}

    def store_and_schedule(self, message, tx_node):
        # Check whether msg is the newest information.
        tx_node, rx_node, conf_mi, vol_mi = message.data
        edge = tx_node, rx_node
        if edge in self.newest_information_about:
            newest_msg = self.newest_information_about[edge]
            if newest_msg in self.buf:
                if message.start_time <= newest_msg.start_time:
                    assert tx_node is not None
                    self.event_dispatcher.message_rejected(self, message)
                    return
                self.event_dispatcher.message_dropped(self, newest_msg)
                self.buf.remove(newest_msg)
        # Record that msg is the newest information.
        self.newest_information_about[edge] = message
        # Record that the tx_node knows the message already.
        if tx_node:
            if tx_node not in self.sent_to_node:
                self.sent_to_node[tx_node] = set()
            if message not in self.sent_to_node[tx_node]:
                self.sent_to_node[tx_node].add(message)
        # Add to buffer normally.
        super().store_and_schedule(message, tx_node)

    async def get_messages(self, rx_node):
        if rx_node not in self.sent_to_node:
            self.sent_to_node[rx_node] = set()
        sent = self.sent_to_node[rx_node]
        to_be_sent = [
            m for m in self.buf
            if m not in sent and m.source != rx_node
        ]
        random.shuffle(to_be_sent)
        for message in to_be_sent:
            if message not in self.buf:
                continue
            if message.deadline < asyncio.get_running_loop().time():
                self.event_dispatcher.message_dropped(self, message)
                self.buf.remove(message)
                continue
            yield message, None
            # If the message was not sent out successfully, we will not
            # get here but yield will throw.
            sent.add(message)


def _main(args):
    # Create and register event loop and logging facilities
    loop = aiodtnsim.simulation.event_loop.DESEventLoop(start_time=0)
    logger = _initialize_logger(args.verbose, loop)
    aiodtnsim.reports.logging.logger = logger
    asyncio.set_event_loop(loop)

    # Load F-TVG and KB
    ftvg = tvgutil.tvg.from_serializable(json.load(args.FTVG))
    kb = json.load(args.KBFILE)

    # Initialize simulation monitoring/reporting
    event_dispatcher = aiodtnsim.EventDispatcher()
    event_dispatcher.add_subscriber(aiodtnsim.reports.logging.LoggingReport())
    ms_report = aiodtnsim.reports.MessageStatsReport()
    event_dispatcher.add_subscriber(ms_report)
    kb_updaters = []
    for eid in ftvg.vertices:
        node_kb_updater = KBUpdateHandler(eid, kb["metrics"][eid])
        event_dispatcher.add_subscriber(node_kb_updater)
        kb_updaters.append(node_kb_updater)

    def get_new_link(contact):
        return SimulatedIdealRedundancyImpairedCLALink(
            contact,
            block_size=args.link_block_size,
        )

    # Initialize nodes
    sim_nodes = {
        eid: MetricDistributionNode(
            eid,
            buffer_size=-1,
            event_dispatcher=event_dispatcher,
            link_factory=get_new_link,
        )
        for eid in ftvg.vertices
    }

    # Initialize and schedule contacts
    min_start = float("inf")
    max_end = float("-inf")
    for _, contact_list in ftvg.edges.items():
        for factual_contact in contact_list:
            simple_contact = factual_contact.to_simple()
            sim_contact = aiodtnsim.Contact(
                tx_node=sim_nodes[simple_contact.tx_node],
                rx_node=sim_nodes[simple_contact.rx_node],
                start_time=simple_contact.start_time,
                end_time=simple_contact.end_time,
                bit_rate=simple_contact.bit_rate,
                delay=simple_contact.delay,
                param=factual_contact,
            )
            min_start = min(min_start, factual_contact.start_time)
            max_end = max(max_end, factual_contact.end_time)
            asyncio.ensure_future(
                sim_contact.tx_node.schedule_contact(sim_contact)
            )

    # Schedule messages
    for node, node_kb in kb["metrics"].items():
        assert len(node_kb) == 1, "Please use an unmodified KB!"
        for rx_node, tx_nodes_dict in node_kb.items():
            for tx_node, metric_data in tx_nodes_dict.items():
                # TODO: Allow reducing the update frequency...
                for valid_from, conf_mi, vol_mi in metric_data:
                    msg_data = tx_node, rx_node, conf_mi, vol_mi
                    # `node` has the knowledge and spreads it via this msg.
                    message = aiodtnsim.Message(
                        start_time=valid_from,
                        source=node,
                        destination="<any>",
                        # NOTE: We estimate the size using a CBOR serializer.
                        size=(len(cbor2.dumps(msg_data)) * 8),
                        deadline=(valid_from + args.max_lifetime),
                        data=msg_data,
                    )
                    asyncio.ensure_future(
                        sim_nodes[message.source].schedule_injection(message)
                    )

    sim_duration = int(math.ceil(max_end - min_start))
    print("aiodtnsim :: Firing up event loop!", file=sys.stderr)
    print(f"This run will simulate {round(sim_duration / 3600, 2)} hours.",
          file=sys.stderr)

    if args.progress:
        sim_duration = int(math.ceil(max_end - min_start))
        progress_step = sim_duration / 100
        pbar = tqdm.tqdm(total=100)

        async def progress():
            await asyncio.sleep(min_start - loop.time())
            for _ in range(100):
                await asyncio.sleep(progress_step)
                pbar.update(1)

        asyncio.ensure_future(progress())

    # Run simulation by firing up the event loop
    loop.run_forever()

    if args.progress:
        pbar.close()

    # Print report results
    ms_report.print()
    # Calculate and print distribution delay
    dd_stats = []
    for kb_updater in kb_updaters:
        dd_stats.extend(kb_updater.get_dist_delays())
    dist_delays = [delay for _, _, _, _, delay in dd_stats]
    print_stat("** Average distribution delay", dist_delays)

    # Store statistics to file - updated by our own delay stats
    if args.savestats:
        ms_report_data = ms_report.serializable()
        ms_report_data.update({
            "delays": dist_delays,
            "raw_delay_tuples": dd_stats,
        })
        json.dump(
            ms_report_data,
            args.savestats,
        )
        args.savestats.write("\n")
        args.savestats.close()

    print("Dumping output to JSON file...", file=sys.stderr)
    json.dump(
        kb,
        args.output,
        indent=(4 if args.indent else None),
    )
    args.output.write("\n")
    args.output.close()


def _get_argument_parser():
    parser = argparse.ArgumentParser(
        description="use a DES run to update node knowledge of topology",
    )
    parser.add_argument(
        "FTVG",
        type=argparse.FileType("r"),
        help="the factual TVG (F-TVG) JSON file",
    )
    parser.add_argument(
        "KBFILE",
        type=argparse.FileType("r"),
        help="the knowledge base file",
    )
    parser.add_argument(
        "-o", "--output",
        type=argparse.FileType("w"),
        default=sys.stdout,
        help="the output JSON file for the updated knowledge base",
    )
    parser.add_argument(
        "--progress",
        action="store_true",
        help="show a progress bar",
    )
    parser.add_argument(
        "--max_lifetime",
        type=int,
        default=43200,
        help="the maximum lifetime of metric messages, default: 0.5 days",
    )
    parser.add_argument(
        "--link_block_size",
        type=int, default=(1500 * 8),
        help="the block size for link-layer transmission (default: 1.5 kB)",
    )
    parser.add_argument(
        "-s", "--savestats",
        type=argparse.FileType("w"),
        default=None,
        help="save message stats into the specified file",
    )
    parser.add_argument(
        "-v", "--verbose",
        action="count",
        default=0,
        help="increase log output verbosity",
    )
    parser.add_argument(
        "-i", "--indent",
        action="store_true",
        help="specify this if you want pretty JSON",
    )
    return parser


def _initialize_logger(verbosity: int, event_loop: asyncio.AbstractEventLoop):

    def _add_time_to_record(record):
        record.sim_time = asyncio.get_running_loop().time()
        return True

    log_level = {
        0: logging.WARN,
        1: logging.INFO,
    }.get(verbosity, logging.DEBUG)

    # Simulator-wide logging to the console
    logging.basicConfig(
        level=log_level,
        format="%(asctime)s %(levelname)s: %(message)s",
    )

    # Specific logging for reporting the simulation progress
    logger = logging.getLogger(sys.argv[0])
    handler = logging.StreamHandler()
    handler.setLevel(log_level)
    formatter = logging.Formatter("%(sim_time).2f %(levelname)s: %(message)s")
    handler.setFormatter(formatter)
    logger.addFilter(_add_time_to_record)
    logger.addHandler(handler)

    return logger


if __name__ == "__main__":
    _main(_get_argument_parser().parse_args())
