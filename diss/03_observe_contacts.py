#!/usr/bin/env python3
# encoding: utf-8

import sys
import json
import argparse
import logging
import math
import random
from operator import attrgetter

import joblib
import tqdm

from tvgutil import contact_plan, tvg

from predictutil import (
    predict_rr0_for_scenario,
    get_trajectory_predictors_for_scenario,
    get_contact,
    get_pcp_for_scenario,
)
from predictutil.confidence import CONFIDENCE_PREDICTORS
from predictutil.volume import VOLUME_PREDICTORS

logger = logging.getLogger(__name__)


def _process_job(edge_conf_predictor, edge_vol_predictor, edge_contacts,
                 min_metric_update_interval, scenario_end_time):
    # The first record is valid from time 0.0 and represents a state where
    # nothing has been observed yet.
    cur_list = [(
        0.0,
        # See comment below.
        edge_conf_predictor.export_metric_info(0),
        edge_vol_predictor.export_metric_info(0),
    )]

    updates = []
    next_update_t = math.inf  # in the first iteration, this will not run!
    for f_contact in edge_contacts:
        while f_contact.end_time > next_update_t:
            # fill up until contact
            updates.append((next_update_t, None))
            next_update_t += min_metric_update_interval
        updates.append((f_contact.end_time, f_contact))
        next_update_t = f_contact.end_time + min_metric_update_interval
    while scenario_end_time > next_update_t:
        # fill up rest
        updates.append((next_update_t, None))
        next_update_t += min_metric_update_interval

    for t, f_contact in updates:
        if f_contact:
            edge_conf_predictor.observe_contact(f_contact)
            edge_vol_predictor.observe_contact(f_contact)
        # NOTE: Even if nothing new has been observed, the metrics are
        # exported. This is e.g. relevant in case of confidence metrics - they
        # are updated by what was _predicted_ which we do not know here.
        cur_list.append((
            t,
            # WARNING: It HAS to be ensured that no references to changing
            # data are returned by exporting the metrics EXCEPT when this is
            # intended behavior (e.g. the "factual" volume predictor).
            # Else the wrong metric information could be stored!
            edge_conf_predictor.export_metric_info(at_time=t),
            edge_vol_predictor.export_metric_info(at_time=t),
        ))
    return cur_list


def _main(args):
    _initialize_logger(args.verbose)
    # First get all contacts from the graph
    # NOTE: It is important that the provided FTVG has its contacts already
    # merged according to predicted time windows!
    ftvg = tvg.from_serializable(json.load(args.FTVGFILE))
    fcp = tvg.to_contact_plan(ftvg)
    print("Loaded FCP with {} unidirectional observations.".format(
        len(fcp)
    ), file=sys.stderr)

    scenario = json.load(args.SCENARIOFILE)
    if args.start_at_first_contact:
        scenario_start = min(fcp, key=attrgetter("start_time")).start_time
    else:
        scenario_start = scenario["time_offset"]
    if args.duration:
        scenario_end = scenario_start + args.duration
    else:
        scenario_end = max(fcp, key=attrgetter("end_time")).end_time

    # Assumption: We have an FCP + a scenario file describing what the nodes
    # "see" and generate a "knowledge base" file describing what the nodes
    # saw/know from observations.

    if args.ptvg:
        ptvg = tvg.from_serializable(json.load(args.ptvg))
        pcp = tvg.to_contact_plan(ptvg)
    else:
        print("Predicting PCP...", file=sys.stderr)
        pcp = get_pcp_for_scenario(
            scenario,
            scenario_start,
            scenario_end,
            ftvg_filter=(ftvg if args.only_factual else None),
        )
        ptvg = tvg.from_contact_plan(pcp)

    print("Loaded P-TVG with {} uni-directional contact(s).".format(
        len(pcp)
    ), file=sys.stderr)

    # 2. Run modular predictors for capacity and probability metrics (import)

    conf_predictor_factory, conf_predictor_arglist = CONFIDENCE_PREDICTORS[
        args.confidence_predictor
    ]
    conf_predictor_args = {
        k: getattr(args, k)
        for k in conf_predictor_arglist
        if getattr(args, k) is not None
    }
    vol_predictor_factory, vol_predictor_arglist = VOLUME_PREDICTORS[
        args.volume_predictor
    ]
    vol_predictor_args = {
        k: getattr(args, k)
        for k in vol_predictor_arglist
        if getattr(args, k) is not None
    }

    print("Initializing trajectory predictors...", file=sys.stderr)
    trajectory_predictors = get_trajectory_predictors_for_scenario(scenario)

    # Sanity checks
    for contact in random.sample(pcp, min(len(pcp), 20)):
        tp = trajectory_predictors[frozenset(
            (contact.tx_node, contact.rx_node)
        )]
        start_offset = tp.get_offset(contact.start_time)
        end_offset = tp.get_offset(contact.end_time)
        assert start_offset == end_offset, "Do scenario and P-TVG match?"
        start_elev = tp.predict_elevation(contact.start_time)
        end_elev = tp.predict_elevation(contact.end_time)
        assert abs(start_elev) < .01, "Do scenario and P-TVG match?"
        # This might be the case if the F-TVG requires mocking for the
        # provided scenario.
        assert abs(end_elev) < .01, "Forgot to provide the right P-TVG?"
        max_elev_t, max_elev = tp.predict_max_elevation(
            contact.start_time,
            contact.end_time,
        )
        assert start_elev < max_elev > end_elev, "BUG in TP"
        assert contact.start_time < max_elev_t < contact.end_time, "BUG in TP"

    print("Preparing knowledge base...", file=sys.stderr)
    metrics = {}  # knowledge: [node][rx][tx] -> [tuple(from, conf_mi, vol_mi)]
    for rx_node in ptvg.vertices:
        metrics[rx_node] = {}
        metrics[rx_node][rx_node] = {}
    jobs = []
    for edge, p_contacts in ptvg.edges.items():
        job_conf_predictor = conf_predictor_factory(
            p_contacts,
            trajectory_predictors[frozenset(edge)],
            **conf_predictor_args,
        )
        job_vol_predictor = vol_predictor_factory(
            trajectory_predictors[frozenset(edge)],
            **vol_predictor_args,
        )
        jobs.append((
            edge,
            joblib.delayed(_process_job)(
                edge_conf_predictor=job_conf_predictor,
                edge_vol_predictor=job_vol_predictor,
                edge_contacts=ftvg.edges.get(edge, []),
                min_metric_update_interval=args.min_metric_update_interval,
                scenario_end_time=scenario_end,
            ),
        ))

    print("Observing contacts...", file=sys.stderr)
    job_results = joblib.Parallel(n_jobs=args.workers)(
        job for _, job in tqdm.tqdm(jobs)
    )

    print("Storing results...", file=sys.stderr)
    for (edge, _), cur_list in zip(jobs, job_results):
        # The receiving node of the edge (edge[1]) now knows more about the
        # edge contacts -> assign metric info to its knowledge base.
        # FORMAT: [node][rx][tx]
        metrics[edge[1]][edge[1]][edge[0]] = cur_list

    json.dump(
        {
            "scenario": scenario,
            "base_ptvg": tvg.to_serializable(ptvg),
            "confidence_predictor": args.confidence_predictor,
            "volume_predictor": args.volume_predictor,
            "confidence_predictor_args": conf_predictor_args,
            "volume_predictor_args": vol_predictor_args,
            "metrics": metrics,
        },
        args.output,
        indent=(4 if args.indent else None),
    )
    args.output.write("\n")
    args.output.close()


def _initialize_logger(verbosity):
    log_level = {
        0: logging.WARN,
        1: logging.INFO,
    }.get(verbosity, logging.DEBUG)
    logging.basicConfig(
        level=log_level,
        format="%(asctime)s %(levelname)s: %(message)s",
    )


def _get_argument_parser():
    parser = argparse.ArgumentParser(
        description="Ring Road prediction knowledge base generation tool")
    parser.add_argument("SCENARIOFILE", type=argparse.FileType("r"),
                        help="the base scenario file")
    parser.add_argument("FTVGFILE", type=argparse.FileType("r"),
                        help="the F-TVG file inferred via analysis")
    parser.add_argument("-p", "--ptvg",
                        type=argparse.FileType("r"), default=None,
                        help="the P-TVG file -- if provided, do not use SGP4")
    parser.add_argument("-g", "--gs", type=int, default=None,
                        help="'taken' gs count (default=all)")
    parser.add_argument("-s", "--sats", type=int, default=None,
                        help="'taken' sat count (default=all)")
    parser.add_argument("-d", "--duration", type=float, default=None,
                        help="scenario duration, in seconds (default=Emax)")
    parser.add_argument("--start-at-first-contact", action="store_true",
                        help="assume first factual contact as start time")
    parser.add_argument("--confidence_predictor", default="ratio",
                        choices=CONFIDENCE_PREDICTORS.keys(),
                        help="approach used to predict contact confidence")
    parser.add_argument("--volume_predictor", default="minelev",
                        choices=VOLUME_PREDICTORS.keys(),
                        help="approach used to predict contact volume")
    parser.add_argument("--only_factual", action="store_true",
                        help="if specified, only yield contacts for which a "
                             "factual contact could be found (unrealistic!)")
    parser.add_argument("--min_metric_update_interval", type=int, default=7200,
                        help="the minimum interval, in seconds, for metrics"
                             "to be exported / updated -- needed for prob.!")
    predopts = set()
    for _, opts in CONFIDENCE_PREDICTORS.values():
        predopts |= set(opts)
    for _, opts in VOLUME_PREDICTORS.values():
        predopts |= set(opts)
    for opt in predopts:
        parser.add_argument("--" + opt, type=float, default=None,
                            help=(opt + " prediction param"))
    parser.add_argument("-o", "--output",
                        type=argparse.FileType("w"), default=sys.stdout,
                        help="output JSON file for the pred. knowledge base")
    parser.add_argument("-i", "--indent", action="store_true",
                        help="specify this if you want pretty JSON")
    parser.add_argument(
        "-w", "--workers",
        type=int, default=4,
        help="the amount of workers used for parallel processing",
    )
    parser.add_argument(
        "-v", "--verbose",
        action="count",
        default=0,
        help="increase log output verbosity",
    )
    return parser


if __name__ == "__main__":
    _main(_get_argument_parser().parse_args())
