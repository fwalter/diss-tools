#!/bin/sh
python -m tvgutil.tools.create_gs_list --groundstations 25 --prefix "GS-" --shapefile ne_110m_land.shp --output gslist.json -i
python -m tvgutil.tools.create_rr_scenario --gs 25 --sats 9 --satdbfile walker931.tle --gsfile gslist.json -i -o walker931_scenario.json --noresetids
python -m tvgutil.tools.fix_rr_scenario_tle_checksums < walker931_scenario.json > walker931_scenario_fixed.json
python 03_plot_gs_sat_scenario.py walker931_scenario_fixed.json --orbit-count 1
