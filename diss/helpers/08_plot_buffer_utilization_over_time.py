#!/usr/bin/env python3
# encoding: utf-8

import argparse
import json

# Add colorblind-friendly color cycle
# See https://gist.github.com/thriveth/8560036
import cycler
CB_color_cycle = ["#377eb8", "#ffb01e", "#4daf4a",
                  "#616161", "#a65628", "#984ea3",
                  "#f781bf", "#e41a1c", "#dede00"]

import matplotlib
matplotlib.rcParams["axes.prop_cycle"] = cycler.cycler(color=CB_color_cycle)

matplotlib.rcParams["text.usetex"] = False
matplotlib.rcParams["font.family"] = "Open Sans"

import numpy as np
import matplotlib.pyplot as plt

MB_FACTOR = 8 * 1024 * 1024  # MiB

MARKERS = [
    "D",
    "x",
    "o",
    "s",
    "v",
    "^",
    "<",
    ">",
    "d",
]


def _main(args):
    print("Loading statistics...")
    algo_stats = {}
    nodes = set()
    min_t = float("inf")
    max_t = float("-inf")
    for i, statfile in enumerate(args.STATFILE):
        with open(statfile, "r") as f:
            data = json.load(f)
            algo_name = (
                args.algonames[i] if i < len(args.algonames)
                else f"<algo #{i}>"
            )
            assert algo_name not in algo_stats
            algo_stats[algo_name] = data["buffer_utilization"]
            nodes |= {n for n, _, _ in data["buffer_utilization"]}
            min_t = min(
                min_t,
                min(t for _, t, _ in data["buffer_utilization"]),
            )
            max_t = max(
                max_t,
                max(t for _, t, _ in data["buffer_utilization"]),
            )

    algolist = "\n    ".join(algo_stats.keys())
    nodelist = "\n    ".join(nodes)
    print(
        f"Loaded stats for {len(algo_stats)} algo(s) "
        f"and {len(nodes)} node(s)\n"
        f"-> Algos:\n    {algolist}\n-> Nodes:\n    {nodelist}"
    )

    if args.nodefilter:
        nodefilterlist = "\n    ".join(args.nodefilter)
        print(f"Filtering for nodes: \n    {nodefilterlist}")
    if args.node_prefix:
        print(f"Filtering for node prefix: {args.node_prefix}*")

    if args.percentile:
        pct_lo = args.pct_min
        pct_hi = 100 - pct_lo

    scenario_start = min_t
    scenario_end = max_t

    print("Checking out probabilities...")
    time_resolution = args.time_resolution
    times = np.arange(scenario_start, scenario_end, time_resolution)
    plot_data = {algo: [[] for _ in times] for algo in algo_stats}
    for algo, items in algo_stats.items():
        for node, time, buff_util in items:
            if args.nodefilter and node not in args.nodefilter:
                continue
            elif args.node_prefix and not node.startswith(args.node_prefix):
                continue
            buff_util /= MB_FACTOR
            index = int((time - scenario_start) / time_resolution)
            plot_data[algo][index].append(buff_util)

    overall_means = {
        algo: np.mean([
            bu / MB_FACTOR
            for _, _, bu in e
        ])
        for algo, e in algo_stats.items()
    }

    prop_cycle = plt.rcParams["axes.prop_cycle"]
    colors = prop_cycle.by_key()["color"]

    print("Plotting...")
    xval = [round((t - scenario_start) / 3600, 2) for t in times]
    fig, ax1 = plt.subplots(1, 1)
    for i, algo in enumerate(plot_data):
        ax1.axhline(y=overall_means[algo], color=colors[i], alpha=0.3)
    for i, (algo, time_stats) in enumerate(plot_data.items()):
        if args.percentile:
            yerr = (
                [
                    (
                        np.mean(e) - np.percentile(e, pct_lo)
                        if e else float("nan")
                    )
                    for e in time_stats
                ],
                [
                    (
                        np.percentile(e, pct_hi) - np.mean(e)
                        if e else float("nan")
                    )
                    for e in time_stats
                ],
            )
        else:
            yerr = [(np.std(e) if e else float("nan")) for e in time_stats]
        if args.no_err:
            ax1.plot(
                xval,
                [(np.mean(e) if e else float("nan")) for e in time_stats],
                label=algo,
                marker=(MARKERS[i % len(MARKERS)] if args.markers else None),
            )
        else:
            ax1.errorbar(
                xval,
                [(np.mean(e) if e else float("nan")) for e in time_stats],
                yerr=yerr,
                label=algo,
                capsize=2,
                marker=(MARKERS[i % len(MARKERS)] if args.markers else None),
            )
    if args.log:
        ax1.set_yscale("log")
        ax1.set_ylim(bottom=1)
    ax1.set_xlabel("time from simulation start / hours")
    ax1.set_ylabel(
        ("average " if args.no_err else "") +
        "buffer utilization / MiB"
    )
    ax1.legend(
        loc=args.legend_location,
        ncol=args.legend_columns,
    )

    if args.outfile:
        fig.set_size_inches(6, 3.45)
        plt.tight_layout()
        plt.savefig(args.outfile)
    else:
        plt.show()


def _get_argument_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "STATFILE",
        nargs="+",
        help="the (joined) input statistics files",
    )
    parser.add_argument(
        "-o", "--outfile",
        help="the output filename",
    )
    parser.add_argument(
        "-n", "--algonames",
        nargs="*", default=[],
        help="the algorithm names",
    )
    parser.add_argument(
        "--nodefilter",
        nargs="*", default=[],
        help="filter for showing only the specified nodes",
    )
    parser.add_argument(
        "--node-prefix",
        default=None,
        help="filter for showing only nodes with specified prefix",
    )
    parser.add_argument(
        "--time-resolution",
        type=float, default=3600,
        help="the time in seconds between two samples (default: 3600 s)",
    )
    parser.add_argument(
        "--percentile",
        action="store_true",
        help="use 95th percentile confidence interval for error bars",
    )
    parser.add_argument(
        "--pct-min",
        type=float, default=2.5,
        help="the start of the percentile range (default: 2.5)",
    )
    parser.add_argument(
        "--log",
        action="store_true",
        help="use logarithmic scale",
    )
    parser.add_argument(
        "--no-err",
        action="store_true",
        help="do not use error bars",
    )
    parser.add_argument(
        "--markers",
        action="store_true",
        help="add markers",
    )
    parser.add_argument(
        "--legend-columns",
        type=int,
        default=1,
        help="the amount of columns used for the legend (default: 1)",
    )
    parser.add_argument(
        "--legend-location",
        default="upper right",
        help="the legend location passed to matplotlib (default: upper left)",
    )
    return parser


if __name__ == "__main__":
    _main(_get_argument_parser().parse_args())
