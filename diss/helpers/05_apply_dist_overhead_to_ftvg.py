#!/usr/bin/env python3
# encoding: utf-8

import argparse
import json
import sys

from tvgutil import tvg

from aiodtnsim.reports.message_stats_report import print_stat


def contact_factory(graph, contact, new_start, new_end):
    # build a new contact with the same properties as contact,
    # but different interval
    return graph.contact_type(
        contact.tx_node,
        contact.rx_node,
        new_start,
        new_end,
        (
            contact.characteristics
            if hasattr(contact, "characteristics")
            else contact.generations
        ),
    )


def _main(args):
    print("Loading TVG file...", file=sys.stderr)
    graph = tvg.from_serializable(json.load(args.INFILE))
    contacts = [c for ev, ec in graph.edges.items() for c in ec]
    start = min(c.start_time for c in contacts)
    end = max(c.end_time for c in contacts)
    print(
        f"Found {len(contacts)} contacts of {len(graph.vertices)} nodes "
        f"({len(graph.edges)} edges) in [{start}, {end}] "
        f"({(end - start) / 86400} day(s)).",
        file=sys.stderr
    )
    print("Loading diststats...", file=sys.stderr)
    diststats = json.load(args.DISTSTATS)
    util_stats = diststats["contact_utilization"]

    print("Calculating blocked contact times...", file=sys.stderr)
    down_intervals_by_edge = {e: [] for e in graph.edges}
    for tx_node, rx_node, start, end, time_util, _ in util_stats:
        edge = tx_node, rx_node
        duration = end - start
        blocked_duration = min(duration, time_util * duration)
        down_intervals_by_edge[edge].append(
            (start, start + blocked_duration)
        )

    print("Applying blocked contact times...", file=sys.stderr)
    dropped = split = shortened = 0
    for edge, edge_contacts in graph.edges.items():
        edge_di = down_intervals_by_edge[edge]
        for down_start, down_end in edge_di:
            i = 0
            # NOTE: using "boring" iteration because modifying edge_contacts
            while i < len(edge_contacts):
                ct = edge_contacts[i]
                if not (ct.end_time > down_start and ct.start_time < down_end):
                    i += 1
                    continue
                # overlapping with the "down interval"
                assert down_end >= down_start
                if down_end < ct.end_time:
                    if down_start > ct.start_time:
                        new_intv = [
                            (ct.start_time, down_start),
                            (down_end, ct.end_time),
                        ]
                    else:
                        new_intv = [
                            (down_end, ct.end_time),
                        ]
                elif down_start > ct.start_time:
                    assert down_end >= ct.end_time
                    new_intv = [
                        (ct.start_time, down_start),
                    ]
                else:
                    new_intv = []
                # apply
                if not new_intv:
                    del edge_contacts[i]
                    dropped += 1
                elif len(new_intv) == 1:
                    edge_contacts[i] = contact_factory(
                        graph,
                        ct,
                        new_intv[0][0],
                        new_intv[0][1],
                    )
                    i += 1
                    shortened += 1
                else:
                    assert len(new_intv) == 2
                    c1 = contact_factory(
                        graph,
                        ct,
                        new_intv[0][0],
                        new_intv[0][1],
                    )
                    c2 = contact_factory(
                        graph,
                        ct,
                        new_intv[1][0],
                        new_intv[1][1],
                    )
                    edge_contacts[i] = c2
                    edge_contacts.insert(i, c1)
                    i += 2
                    split += 1

    # This script should only shorten or drop!
    assert split == 0

    print(f"Shortened {shortened} contact(s), dropped {dropped} contact(s)",
          file=sys.stderr)
    print_stat(
        "Average time reduction",
        [
            time_util * (end - start)
            for _, _, start, end, time_util, _ in util_stats
        ],
        1,
        " s",
    )

    print("Writing TVG file...", file=sys.stderr)
    json.dump(
        tvg.to_serializable(graph),
        args.output,
        indent=(4 if args.indent else None),
    )
    args.output.write("\n")
    args.output.close()


def _get_argument_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "INFILE",
        type=argparse.FileType("r"),
        default=sys.stdin,
        help="the input F-TVG",
    )
    parser.add_argument(
        "DISTSTATS",
        type=argparse.FileType("r"),
        default=sys.stdin,
        help="the distribution stats file",
    )
    parser.add_argument(
        "-o", "--output",
        type=argparse.FileType("w"),
        default=sys.stdout,
        help="the output file for the F-TVG (default: stdout)",
    )
    parser.add_argument(
        "-i", "--indent",
        action="store_true",
        help="indent the JSON output",
    )

    return parser


if __name__ == "__main__":
    _main(_get_argument_parser().parse_args())
