#!/usr/bin/env python3
# encoding: utf-8

import argparse
import json
import random
import sys

import numpy as np

from tvgutil import tvg


def contact_factory(graph, contact, new_start, new_end):
    # build a new contact with the same properties as contact,
    # but different interval
    return graph.contact_type(
        contact.tx_node,
        contact.rx_node,
        new_start,
        new_end,
        (
            contact.characteristics
            if hasattr(contact, "characteristics")
            else contact.generations
        ),
    )


def _main(args):
    print("Loading TVG file...", file=sys.stderr)
    graph = tvg.from_serializable(json.load(args.INFILE))
    contacts = [c for ev, ec in graph.edges.items() for c in ec]
    start = min(c.start_time for c in contacts)
    end = max(c.end_time for c in contacts)
    print(
        f"Found {len(contacts)} contacts of {len(graph.vertices)} nodes "
        f"({len(graph.edges)} edges) in [{start}, {end}] "
        f"({(end - start) / 86400} day(s)).",
        file=sys.stderr
    )

    if args.list_nodes:
        print("Node list with contact counts:", file=sys.stderr)
        for node in sorted(graph.vertices):
            ccount = sum(
                1
                for edge, cl in graph.edges.items()
                for _ in cl
                if node in edge
            )
            print(f"> {node}: {ccount}", file=sys.stderr)
        sys.exit(0)

    print("Calculating selected probabilistic effects...", file=sys.stderr)
    down_intervals_by_edge = {e: [] for e in graph.edges}
    node_down_starttimes = {v: [] for v in graph.vertices}

    # MTBF/MTTR
    for node in args.failing_nodes:
        assert node in graph.vertices, f"'{node}' is no known vertex"
        down_intv = []
        cur_t = start
        while cur_t < end:
            fail_at = cur_t + np.random.exponential(args.node_mtbf) * 3600
            repair_at = fail_at + np.random.exponential(args.node_mttr) * 3600
            down_intv.append((fail_at, repair_at))
            node_down_starttimes[node].append(fail_at)
            cur_t = repair_at
        for edge, edge_di in down_intervals_by_edge.items():
            if node not in edge:
                continue
            edge_di.extend(down_intv)
        print(
            f"> Node '{node}' fails {len(down_intv)} time(s): {down_intv}",
            file=sys.stderr
        )

    # Randomly-unreliable nodes
    checked_edges = set()
    for node in args.unreliable_contact_nodes:
        assert node in graph.vertices, f"'{node}' is no known vertex"
        dropped_ct = 0
        for edge, edge_di in down_intervals_by_edge.items():
            if node not in edge:
                continue
            if not args.unreliability_is_unidirectional:
                return_edge = (edge[1], edge[0])
                if return_edge in checked_edges:
                    continue
                checked_edges.add(edge)
            for contact in graph.edges[edge]:
                if random.random() < args.contact_probability:
                    # Contact will be kept.
                    continue
                # Contact will be missed.
                interval = (contact.start_time, contact.end_time)
                edge_di.append(interval)
                dropped_ct += 1
                if not args.unreliability_is_unidirectional:
                    down_intervals_by_edge[return_edge].append(interval)
                    dropped_ct += 1
        print(
            f"> Node '{node}' misses {dropped_ct} unidirectional contact(s).",
            file=sys.stderr
        )

    # e.g. solar-powered stations unavailable at night
    for node in args.regularly_down_nodes:
        assert node in graph.vertices, f"'{node}' is no known vertex"
        down_intv = []
        period = (args.on_duration + args.off_duration) * 3600
        cur_t = start + random.random() * period  # random offset
        while cur_t < end:
            fail_at = cur_t + args.on_duration * 3600
            repair_at = fail_at + args.off_duration * 3600
            down_intv.append((fail_at, repair_at))
            cur_t = repair_at
        print(
            f"> Node '{node}' is down in {len(down_intv)} interval(s): "
            f"{down_intv}",
            file=sys.stderr
        )
        for edge, edge_di in down_intervals_by_edge.items():
            if node not in edge:
                continue
            edge_di.extend(down_intv)

    # e.g. ground stations breaking at a specific time
    for node_i, node in enumerate(args.permanently_failing_nodes):
        assert node in graph.vertices, f"'{node}' is no known vertex"
        if args.permanent_failure_offset:
            if len(args.permanent_failure_offset) > 1:
                failure_offset = args.permanent_failure_offset[node_i]
            else:
                failure_offset = args.permanent_failure_offset[0]
        else:
            failure_offset = np.random.exponential(args.permanent_failure_mttf)
        print(
            f"> Node '{node}' breaks at offset {round(failure_offset, 4)} h.",
            file=sys.stderr
        )
        failure_time = start + failure_offset * 3600
        node_down_starttimes[node].append(failure_time)
        for edge, edge_di in down_intervals_by_edge.items():
            if node not in edge:
                continue
            edge_di.append((failure_time, float("inf")))

    print("Applying selected probabilistic effects...", file=sys.stderr)
    dropped = split = shortened = 0
    for edge, edge_contacts in graph.edges.items():
        edge_di = down_intervals_by_edge[edge]
        for down_start, down_end in edge_di:
            i = 0
            # NOTE: using "boring" iteration because modifying edge_contacts
            while i < len(edge_contacts):
                ct = edge_contacts[i]
                if not (ct.end_time > down_start and ct.start_time < down_end):
                    i += 1
                    continue
                # overlapping with the "down interval"
                assert down_end >= down_start
                if down_end < ct.end_time:
                    if down_start > ct.start_time:
                        new_intv = [
                            (ct.start_time, down_start),
                            (down_end, ct.end_time),
                        ]
                    else:
                        new_intv = [
                            (down_end, ct.end_time),
                        ]
                elif down_start > ct.start_time:
                    assert down_end >= ct.end_time
                    new_intv = [
                        (ct.start_time, down_start),
                    ]
                else:
                    new_intv = []
                # apply
                if not new_intv:
                    del edge_contacts[i]
                    dropped += 1
                elif len(new_intv) == 1:
                    edge_contacts[i] = contact_factory(
                        graph,
                        ct,
                        new_intv[0][0],
                        new_intv[0][1],
                    )
                    i += 1
                    shortened += 1
                else:
                    assert len(new_intv) == 2
                    c1 = contact_factory(
                        graph,
                        ct,
                        new_intv[0][0],
                        new_intv[0][1],
                    )
                    c2 = contact_factory(
                        graph,
                        ct,
                        new_intv[1][0],
                        new_intv[1][1],
                    )
                    edge_contacts[i] = c2
                    edge_contacts.insert(i, c1)
                    i += 2
                    split += 1
    print(
        f"> Contacts dropped: {dropped}, split: {split}, "
        f"shortened: {shortened}",
        file=sys.stderr
    )

    print("Checking for edges with too few contacts...", file=sys.stderr)
    contact_count = sum(1 for cl in graph.edges.values() for c in cl)
    pairs = list(graph.edges.keys())
    for ek in pairs:
        edge_length = len(graph.edges[ek])
        if edge_length < args.mincontacts:
            print(
                f"> Dropping edge {ek} (len = {edge_length})",
                file=sys.stderr
            )
            contact_count -= edge_length
            del graph.edges[ek]
    print(
        f"Cleaned graph has {contact_count} contacts "
        f"({len(graph.vertices)} nodes, {len(graph.edges)} edges).",
        file=sys.stderr
    )
    print("Checking for isolated nodes...", file=sys.stderr)
    nodes = list(graph.vertices)
    for node in nodes:
        rm = False
        outgoing_cap = sum(
            sum(
                char.bit_rate * (c_end - c_start)
                for c_start, c_end, char in
                c.characteristics_in_time_frame(c.start_time, c.end_time)
            )
            for ev, ec in graph.edges.items()
            for c in ec
            if ev[0] == node
        )
        if outgoing_cap < args.minout or outgoing_cap == 0:
            print(
                f"> Node '{node}' has not enough outgoing capacity.",
                file=sys.stderr
            )
            rm = True
        incoming_cap = sum(
            sum(
                char.bit_rate * (c_end - c_start)
                for c_start, c_end, char in
                c.characteristics_in_time_frame(c.start_time, c.end_time)
            )
            for ev, ec in graph.edges.items()
            for c in ec
            if ev[1] == node
        )
        if incoming_cap < args.minin or incoming_cap == 0:
            print(
                f"> Node '{node}' has not enough incoming capacity.",
                file=sys.stderr
            )
            rm = True
        if rm:
            print(f"> Dropping node {node}", file=sys.stderr)
            del graph.vertices[node]
            for ek in list(graph.edges.keys()):
                if ek[0] == node or ek[1] == node:
                    print(f"> Dropping edge {ek}", file=sys.stderr)
                    contact_count -= len(graph.edges[ek])
                    del graph.edges[ek]

    print("Rebuilding vertex-associations...", file=sys.stderr)
    for vk in graph.vertices:
        graph.vertices[vk] = {
            neigh for neigh in graph.vertices if (vk, neigh) in graph.edges
        }
    print(
        f"Final graph has {contact_count} contacts "
        f"({len(graph.vertices)} nodes, {len(graph.edges)} edges).",
        file=sys.stderr
    )

    print("Writing TVG file...", file=sys.stderr)
    json.dump(
        tvg.to_serializable(graph),
        args.output,
        indent=(4 if args.indent else None),
    )
    args.output.write("\n")
    args.output.close()

    if args.dropplan_out:
        print("Writing dropplan...", file=sys.stderr)
        json.dump(
            node_down_starttimes,
            args.dropplan_out,
            indent=(4 if args.indent else None),
        )
        args.dropplan_out.write("\n")
        args.dropplan_out.close()


def _get_argument_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "INFILE",
        type=argparse.FileType("r"),
        default=sys.stdin,
        help="the input (F/P-)TVG",
    )
    parser.add_argument(
        "-l", "--list-nodes",
        action="store_true",
        help="only list possible nodes",
    )

    parser.add_argument(
        "-n", "--failing-nodes",
        nargs="*",
        default=[],
        help="nodes which may break/fail over time",
    )
    parser.add_argument(
        "--node-mtbf",
        type=float,
        default=3,
        help="mean time between node failures (MTBF) in hours, default: 3",
    )
    parser.add_argument(
        "--node-mttr",
        type=float,
        default=0.1,
        help="mean time to repair a node (MTTR) in hours, default: 0.1",
    )

    parser.add_argument(
        "-u", "--unreliable-contact-nodes",
        nargs="*",
        default=[],
        help="nodes which have unreliable contacts",
    )
    parser.add_argument(
        "--contact-probability",
        type=float,
        default=.5,
        help="contact probability with unreliable nodes, default: 0.5",
    )
    parser.add_argument(
        "--unreliability-is-unidirectional",
        action="store_true",
        help="apply contact probability independently for up- and downlink",
    )

    parser.add_argument(
        "-r", "--regularly-down-nodes",
        nargs="*",
        default=[],
        help="nodes which have a fixed downtime pattern",
    )
    parser.add_argument(
        "--off-duration",
        type=float,
        default=12,
        help="off-duration for regularly-offline nodes in hours, default: 12",
    )
    parser.add_argument(
        "--on-duration",
        type=float,
        default=12,
        help="on-duration for regularly-offline nodes in hours, default: 12",
    )

    parser.add_argument(
        "-p", "--permanently-failing-nodes",
        nargs="*",
        default=[],
        help="nodes which fail permanently after a fixed amount of time",
    )
    parser.add_argument(
        "--permanent-failure-offset",
        type=float,
        nargs="*",
        default=[],  # NOTE: may be a single value or len() == len(-r)
        help="time offset(s), in hours, to the permanent failure",
    )
    parser.add_argument(
        "--permanent-failure-mttf",
        type=float,
        default=240,
        help="MTTF, in hours, to the permanent failure, default: 240",
    )

    parser.add_argument(
        "--minout",
        type=float, default=1e-10,
        help="minimum effective outgoing capacity for a node, in bits",
    )
    parser.add_argument(
        "--minin",
        type=float, default=1e-10,
        help="minimum effective incoming capacity for a node, in bits",
    )
    parser.add_argument(
        "--mincontacts",
        type=int, default=1,
        help="minimum contact count for kept nodes (default: 1)",
    )

    parser.add_argument(
        "-o", "--output",
        type=argparse.FileType("w"),
        default=sys.stdout,
        help="the output file for the (F/P-)TVG (default: stdout)",
    )
    parser.add_argument(
        "--dropplan-out",
        type=argparse.FileType("w"),
        default=None,
        help="write times when a node fails to the specified JSON file",
    )
    parser.add_argument(
        "-i", "--indent",
        action="store_true",
        help="indent the JSON output",
    )

    return parser


if __name__ == "__main__":
    _main(_get_argument_parser().parse_args())
