#!/usr/bin/env python3
# encoding: utf-8

import sys
import argparse
import json

import numpy as np

from tvgutil import contact_plan, tvg

from predictutil import get_fc_volume

from aiodtnsim.reports.message_stats_report import print_stat


def _fmt(val):
    return round(val, 4)


def _main(args):
    graph = tvg.from_serializable(json.load(args.INFILE))
    assert graph.contact_type == contact_plan.Contact
    contacts = [c for ev, ec in graph.edges.items() for c in ec]
    start = min(c.start_time for c in contacts)
    end = max(c.end_time for c in contacts)
    print(
        f"Found {len(contacts)} contacts of {len(graph.vertices)} nodes "
        f"({len(graph.edges)} edges) in [{start}, {end}] "
        f"({(end - start) / 86400} day(s)).",
        file=sys.stderr
    )
    volumes = {
        edge: [get_fc_volume(c, args.link_block_size) for c in ec]
        for edge, ec in graph.edges.items()
    }
    all_volumes = [v for ev in volumes.values() for v in ev]
    print(f"overall sum = {_fmt(sum(all_volumes))}")
    print_stat("overall", all_volumes)
    edge_volumes = [sum(ev) for ev in volumes.values()]
    print_stat("per-edge", edge_volumes)


def _get_argument_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "INFILE",
        type=argparse.FileType("r"),
        default=sys.stdin,
        help="the input F-TVG",
    )
    parser.add_argument(
        "-b", "--link-block-size",
        type=int,
        default=(1500 * 8),
        help="the assumed link-layer block size",
    )
    return parser


if __name__ == "__main__":
    _main(_get_argument_parser().parse_args())
