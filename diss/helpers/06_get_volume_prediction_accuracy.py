#!/usr/bin/env python3
# encoding: utf-8

import argparse
import json

import numpy as np

from tvgutil import tvg

from predictutil import get_fc_volume

FACTOR = 1 / 8 / 1024  # KiB


def _get_contact(cp, start_time, end_time):
    for c in cp:
        if end_time > c.start_time and start_time < c.end_time:
            return c
    return None


def _main(args):
    with open(args.FTVGFILE, "r") as f:
        ftvg = tvg.from_serializable(json.load(f))
    fcp = tvg.to_contact_plan(ftvg)
    with open(args.PTVGFILE, "r") as f:
        data = json.load(f)
        if "contact_type" in data:
            multi_ptvg = {"<node>": tvg.from_serializable(data)}
        else:
            multi_ptvg = {
                eid: tvg.from_serializable(sptvg)
                for eid, sptvg in data.items()
            }
    multi_pcp = {
        eid: tvg.to_contact_plan(ptvg)
        for eid, ptvg in multi_ptvg.items()
    }
    print(f"{len(fcp)} factual and {len(next(iter(multi_pcp.values())))} "
          f"predicted contact(s) in {len(multi_pcp)} plans loaded")

    def _fcp_cap(contact, block_size):
        return get_fc_volume(contact, block_size) * FACTOR

    def _pcp_cap(contact):
        # FIXME: Which generation to use?
        gen = contact.get_generation_at(contact.start_time)
        cap = gen.get_volume(contact)
        return cap * FACTOR

    for eid, pcp in multi_pcp.items():
        print(f"\n>>> GRAPH INFORMATION for {eid}:")

        fcp_caps = [_fcp_cap(c, args.link_block_size) for c in fcp]
        pcp_caps = [_pcp_cap(c) for c in pcp]

        print(f"Total network contact volume: factual = {sum(fcp_caps)}, "
              f"predicted = {sum(pcp_caps)}")

        cap_diffs = []
        non_factual_caps = []
        for i, pc in enumerate(pcp):
            fc = _get_contact(fcp, pc.start_time, pc.end_time)
            if fc:
                cap_diffs.append(
                    _pcp_cap(pc) -
                    _fcp_cap(fc, args.link_block_size)
                )
            else:
                non_factual_caps.append(_pcp_cap(pc))

        print("Volume difference (p-f) for correctly-predicted contacts:")
        print(f"> Mean:   {np.mean(cap_diffs)}")
        print(f"> Std.:   {np.std(cap_diffs)}")
        print(f"> Median: {np.median(cap_diffs)}")
        print(f"> Max.:   {max(cap_diffs)}")
        print(f"> Min.:   {min(cap_diffs)}")
        print(f"Additional capacity in network: {sum(non_factual_caps)}")
        print("=> NOTE: Values are in KiB!")


def _get_argument_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "FTVGFILE",
        help="the input F-TVG",
    )
    parser.add_argument(
        "PTVGFILE",
        help="the input P-TVG",
    )
    parser.add_argument(
        "--link_block_size",
        type=int, default=(1500 * 8),
        help="the block size for link-layer transmission (default: 1.5 kB)",
    )
    return parser


if __name__ == "__main__":
    _main(_get_argument_parser().parse_args())
