#!/usr/bin/env python
# encoding: utf-8

from __future__ import (
    unicode_literals,
    print_function,
    absolute_import,
    division,
)

import sys
import argparse
import json
import datetime

import numpy as np

import matplotlib
matplotlib.use("Qt5Agg")
import matplotlib.pyplot as plt

from tvgutil import contact_plan, tvg

from predictutil import get_fc_volume


def _main(args):
    with open(args.INFILE, "r") as f:
        ftvg = tvg.from_serializable(json.load(f))
    cp = tvg.to_contact_plan(ftvg)
    print("{} contact(s) loaded".format(len(cp)))
    stats = {
        "volume": [],
        "bit_rate": [],
        "duration": [],
    }
    plt.subplot(2, 1, 1)
    for i, contact in enumerate(cp):
        d = contact.end_time - contact.start_time
        stats["duration"].append(d)
        stats["bit_rate"].append(contact.to_simple().bit_rate)
        stats["volume"].append(get_fc_volume(contact, args.blocksize))
        plt.plot([contact.start_time, contact.end_time], [i, i], color='k', linestyle='-')
    plt.gca().ticklabel_format(useOffset=False)
    plt.xlabel("time")
    plt.ylabel("index")
    plt.subplot(2, 3, 4)
    plt.hist(stats["duration"], bins=40)
    plt.gca().ticklabel_format(useOffset=False)
    plt.xlabel("duration")
    plt.subplot(2, 3, 5)
    plt.hist(stats["bit_rate"], bins=40)
    plt.gca().ticklabel_format(useOffset=False)
    plt.xlabel("bit_rate")
    plt.subplot(2, 3, 6)
    plt.hist(stats["volume"], bins=40)
    plt.gca().ticklabel_format(useOffset=False)
    plt.xlabel("volume")
    plt.show()


def _get_argument_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "INFILE",
        help="the input FCP",
    )
    parser.add_argument(
        "--blocksize",
        type=float,
        default=(1500 * 8),
        help="the link block size to determine the volumes (default: 1.5 kB)",
    )
    return parser


if __name__ == "__main__":
    _main(_get_argument_parser().parse_args())
