#!/usr/bin/env python3
# encoding: utf-8

import sys
import argparse
import json
import numpy

from tvgutil import tvg


def _main(args):
    graph = tvg.from_serializable(json.load(args.INFILE))
    contacts = [c for ev, ec in graph.edges.items() for c in ec]
    start = min(c.start_time for c in contacts)
    end = max(c.end_time for c in contacts)
    print(
        f"Found {len(contacts)} contacts of {len(graph.vertices)} nodes "
        f"({len(graph.edges)} edges) in [{start}, {end}] "
        f"({(end - start) / 86400} day(s)).",
        file=sys.stderr
    )
    c_confs = []
    for _, ec in graph.edges.items():
        for c in ec:
            # Whole contact time frame
            char_iter = c.characteristics_in_time_frame(
                c.start_time,
                c.end_time,
            )
            exp_capacity = 0.0  # bits expected to be delivered
            total_capacity = 0.0
            for c_start, c_end, char in char_iter:
                # slot packet delivery confidence
                msg_confi = (1 - char.bit_error_rate) ** args.msg_size
                cur_capacity = (c_end - c_start) * char.bit_rate
                total_capacity += cur_capacity
                exp_capacity += cur_capacity * msg_confi
            confidence = exp_capacity / total_capacity
            c_confs.append(confidence)
    print(
        f"Average confidence: {numpy.mean(c_confs)} "
        f"(std = {numpy.std(c_confs)})",
        file=sys.stderr
    )


def _get_argument_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "INFILE",
        type=argparse.FileType("r"),
        default=sys.stdin,
        help="the input (F/P-)TVG",
    )
    parser.add_argument(
        "-s", "--msg_size",
        type=float, default=20000,
        help="the assumed msg. size in bits (default: 20k)",
    )
    return parser


if __name__ == "__main__":
    _main(_get_argument_parser().parse_args())
