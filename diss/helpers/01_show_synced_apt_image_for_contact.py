#!/usr/bin/env python
# encoding: utf-8

from __future__ import (
    unicode_literals,
    print_function,
    absolute_import,
    division,
)

import argparse
import datetime
import os
import sys
import math

import json
import numpy as np
import matplotlib
import matplotlib.dates

matplotlib.use("Qt5Agg")
from matplotlib import pyplot as plt

from capacityutil import (
    satnogs_api,
    contact_estimator,
    signal_analysis,
)

SATNOGS_NETWORKS = {
    "dev": satnogs_api.DEV_NETWORK_URL,
    "prod": satnogs_api.NETWORK_URL,
}


def _main(args):
    data = None
    if args.file:
        filedata = json.load(args.file)
        downloadpath = filedata["download_path"]
        observations = filedata["observations"]
        for gsid, satid, obs_tuples in observations:
            for obs, obsdata in obs_tuples:
                if obs["id"] == args.observation:
                    data = obsdata
                    break
            if data:
                break
    elif not args.downloadpath:
        downloadpath = os.curdir
    data = data or satnogs_api.fetch_observation_data(
        args.observation,
        network_url=SATNOGS_NETWORKS[args.network],
        download_path=(args.downloadpath or downloadpath),
    )
    print("Received data: ")
    print(data)
    if not os.path.isfile(data["file"]):
        print("Warning: Audio data missing at " + data["file"])
    if data["vetted_status"] != "good":
        print("Warning: Using contact which was not vetted as 'good'")
    sat, obs = contact_estimator.get_sat_and_observer(
        data["satellite_info"]["name"],
        data["tle"],
        data["station_lat"],
        data["station_lng"],
    )

    unsynced, synced, has_sync, off, corr = signal_analysis.decode_apt_image(
        ogg_file=data["file"],
        dest_path=(args.downloadpath or downloadpath),
        force=args.reanalyze,
    )

    # NOTE: observation process leading to shorter OGG than observation time:
    # - satnogsclient.observer.observer spawns GNU Radio script via
    #   satnogsclient.upsat.gnuradio_handler
    # - script takes around 2 seconds (NC10) to start up and begin observation
    # - script Popen object is assigned to rigctl (WorkerFreq) object which
    #   is attached via socket
    # - thread _communicate_tracking_info is started, checks time every 100 ms
    # - satnogsclient.observer.worker.Worker.trackstop sends SIGINT to
    #   GNU Radio script
    # - stopping process makes poll_gnu_proc_status in
    #   satnogsclient.observer.observer.observer return
    # - file is saved continuously by GR script, contents in buffers are
    #   probably dropped upon killing the process (should be in range of ms)
    #   see also: https://www.gnuradio.org/blog/2017-01-05-buffers/
    # - file is read here
    # - 1 line at start and 1 line at end (= roughly 1 second or less) get
    #   skipped on decoding

    # measurement example for NC10 observation:
    # * 914.5 s image, 919 s contact
    # * setup time according to log: ~2-3 seconds
    # * 1 s removed by APT decoding -> ~1 s difference

    # NOTE: the above means:
    # 1) we should assume the image at the end of the observation time frame
    # 2) we should implement something to accommodate for inaccurate clock!

    print(
        "Seconds in image: ",
        len(synced) / 2,
        "- seconds in contact: ",
        data["end_unix"] - data["start_unix"],
    )

    if args.clip:
        synced = np.clip(synced, 0, 1)

    plt.imshow(synced, interpolation="nearest", cmap=args.colorscheme)
    plt.tight_layout()

    if args.storedata:
        json.dump(data, args.storedata)
        args.storedata.close()
    if args.storeimg:
        plt.savefig(args.storeimg)

    plt.show()


def _get_argument_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-o", "--observation",
        type=int,
        required=True,
        help="the observation to be used",
    )
    parser.add_argument(
        "-f", "--file",
        type=argparse.FileType("r"),
        default=None,
        help="use input file containing pre-fetched SatNOGS data",
    )
    parser.add_argument(
        "-n", "--network",
        choices=SATNOGS_NETWORKS.keys(),
        default="dev",
        help="the SatNOGS network to be used",
    )
    parser.add_argument(
        "-s", "--colorscheme",
        default="gray",
        help="override the used colorscheme",
    )
    parser.add_argument(
        "-c", "--clip",
        action="store_true",
        help="clip to [0; 1] value range",
    )
    parser.add_argument(
        "-r", "--reanalyze",
        action="store_true",
        help="force reanalysis of audio file",
    )
    parser.add_argument(
        "--downloadpath",
        default=None,
        help="the path for downloading received data",
    )
    parser.add_argument(
        "--storedata",
        type=argparse.FileType("w"),
        default=None,
        help="store fetched data to the given file",
    )
    parser.add_argument(
        "--storeimg",
        type=str, default=None,
        help="store decoded image to the given file",
    )
    return parser


if __name__ == "__main__":
    _main(_get_argument_parser().parse_args())
