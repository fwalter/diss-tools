#!/usr/bin/env python3
# encoding: utf-8

import sys
import argparse
import json

from tvgutil import tvg
from predictutil import get_contact

MSG_SIZE = 12000  # 1.5 kBytes


def _fccap(fc):
    char_iter = fc.characteristics_in_time_frame(
        fc.start_time,
        fc.end_time,
    )
    exp_capacity = 0.0  # bits expected to be delivered
    total_capacity = 0.0
    for c_start, c_end, char in char_iter:
        # slot packet delivery confidence
        msg_confi = (1 - char.bit_error_rate) ** MSG_SIZE
        cur_capacity = (c_end - c_start) * char.bit_rate
        total_capacity += cur_capacity
        exp_capacity += cur_capacity * msg_confi
    return exp_capacity, total_capacity


def _ctos(contact):
    typename = type(contact).__name__
    if typename == "Contact":
        expc, maxc = _fccap(contact)
        char_str = f"exp_cap={round(expc, 2)}, max_cap={round(maxc, 2)}"
    else:
        assert typename == "PredictedContact"
        gen_char = []
        for gen in contact.generations:
            gen_char.append(
                f"(Pr={round(gen.probability, 4)}, "
                f"V={round(gen.get_volume(contact), 2)}, "
                f"d_avg={round(gen.get_avg_delay(contact), 3)}) "
                f"from {round(gen.valid_from, 2)}"
            )
        char_str = ", ".join(gen_char)
    return (
        f"{typename} {contact.tx_node} -> {contact.rx_node} @ "
        f"[{round(contact.start_time, 2)}, {round(contact.end_time, 2)}]; " +
        char_str
    )


def _graph_diff(graph_a, graph_b):
    for graph, name in ((graph_a, "A"), (graph_b, "B")):
        contacts = [c for ev, ec in graph.edges.items() for c in ec]
        start = min(c.start_time for c in contacts)
        end = max(c.end_time for c in contacts)
        print(
            f"{name}: {len(contacts)} contacts of {len(graph.vertices)} nodes "
            f"({len(graph.edges)} edges) in [{start}, {end}] "
            f"({(end - start) / 86400} day(s)).", file=sys.stderr
        )

    edge_set_a = set(graph_a.edges.keys())
    edge_set_b = set(graph_b.edges.keys())
    if edge_set_a != edge_set_b:
        not_in_a = edge_set_b - edge_set_a
        not_in_b = edge_set_a - edge_set_b
        print(f"Edge sets differ:\n"
              f"  -> {not_in_a} not in A\n"
              f"  -> {not_in_b} not in B")
        sys.exit(1)

    diff = False
    for (e_a, ec_a), (e_b, ec_b) in zip(graph_a.edges.items(),
                                        graph_b.edges.items()):
        for c_a in ec_a:
            c_b = get_contact(ec_b, c_a.start_time, c_a.end_time)
            if c_b is None:
                diff = True
                print(f"< {_ctos(c_a)}")
                continue
            if c_a != c_b:
                diff = True
                print(f"< {_ctos(c_a)}")
                print(f"> {_ctos(c_b)}")
        # Check contacts not in A
        for c_b in ec_b:
            c_a = get_contact(ec_a, c_b.start_time, c_b.end_time)
            if c_a is None:
                diff = True
                print(f"> {_ctos(c_b)}")
            if c_a != c_b:
                assert diff

    if not diff:
        print("No differences found!", file=sys.stderr)
    return diff


def _main(args):
    tvg_json_a = json.load(args.A)
    tvg_json_b = json.load(args.B)
    if "contact_type" in tvg_json_a:
        simple_tvg_a = tvg.from_serializable(tvg_json_a)
        tvg_list_a = [simple_tvg_a]
        simple_tvg_b = tvg.from_serializable(tvg_json_b)
        tvg_list_b = [simple_tvg_b]
        nodes = [None]
    else:
        # Attempt to load as multi-TVGprint(
        print("Detected a multi-TVG!", file=sys.stderr)
        if set(tvg_json_a.keys()) != set(tvg_json_b.keys()):
            print(f"Graph keys differ: A={len(tvg_json_a.keys())}, "
                  f"B={len(tvg_json_b.keys())}")
            sys.exit(1)
        tvg_list_a = []
        tvg_list_b = []
        nodes = []
        for node in tvg_json_a:
            tvg_list_a.append(tvg.from_serializable(tvg_json_a[node]))
            tvg_list_b.append(tvg.from_serializable(tvg_json_b[node]))
            nodes.append(node)
    if len(tvg_list_a) != len(tvg_list_b):
        print(f"Graph count differs: A={len(tvg_list_a)}, B={len(tvg_list_b)}")
        sys.exit(1)
    diff = False
    for graph_a, graph_b, node in zip(tvg_list_a, tvg_list_b, nodes):
        if node:
            print(f":: Inspecting knowledge of {node}...", file=sys.stderr)
        diff |= _graph_diff(graph_a, graph_b)
    sys.exit(1 if diff else 0)


def _get_argument_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "A",
        type=argparse.FileType("r"),
        default=sys.stdin,
        help="the first (F/P-)TVG",
    )
    parser.add_argument(
        "B",
        type=argparse.FileType("r"),
        default=sys.stdin,
        help="the second (F/P-)TVG",
    )
    return parser


if __name__ == "__main__":
    _main(_get_argument_parser().parse_args())
