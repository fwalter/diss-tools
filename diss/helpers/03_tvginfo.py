#!/usr/bin/env python3
# encoding: utf-8

import sys
import argparse
import json

from tvgutil import tvg


def _main(args):
    graph = tvg.from_serializable(json.load(args.INFILE))

    contacts = [c for ev, ec in graph.edges.items() for c in ec]
    start = min(c.start_time for c in contacts)
    end = max(c.end_time for c in contacts)
    print(
        f"Found {len(contacts)} contacts of {len(graph.vertices)} nodes "
        f"({len(graph.edges)} edges) in [{start}, {end}] "
        f"({(end - start) / 86400} day(s)).",
        file=sys.stderr
    )

    print("Node list with contact counts:", file=sys.stderr)
    for node in sorted(graph.vertices):
        ccount = sum(
            1
            for edge, cl in graph.edges.items()
            for _ in cl
            if node in edge
        )
        print(f"> {node}: {ccount}", file=sys.stderr)


def _get_argument_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "INFILE",
        type=argparse.FileType("r"),
        default=sys.stdin,
        help="the input (F/P-)TVG",
    )
    return parser


if __name__ == "__main__":
    _main(_get_argument_parser().parse_args())
