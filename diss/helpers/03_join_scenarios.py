#!/usr/bin/env python3
# encoding: utf-8

import sys
import argparse
import json


def _main(args):
    joined_scenario = {
        "gslist": [],
        "satlist": [],
        "time_offset": None,
        "bit_rate_uplink": None,
        "bit_rate_downlink": None,
    }

    for i, scenario_file in enumerate(args.INFILE):
        scenario = json.load(scenario_file)

        if i != 0:
            if abs(joined_scenario["bit_rate_uplink"] -
                   scenario["bit_rate_uplink"]) > 1e-3:
                print(f"#{i}: bit_rate_uplink differs", file=sys.stderr)
            if abs(joined_scenario["bit_rate_downlink"] -
                   scenario["bit_rate_downlink"]) > 1e-3:
                print(f"#{i}: bit_rate_uplink differs", file=sys.stderr)
            joined_scenario["time_offset"] = min(
                joined_scenario["time_offset"],
                scenario["time_offset"],
            )
        else:
            joined_scenario["bit_rate_uplink"] = (
                scenario["bit_rate_uplink"]
            )
            joined_scenario["bit_rate_downlink"] = (
                scenario["bit_rate_downlink"]
            )
            joined_scenario["time_offset"] = (
                scenario["time_offset"]
            )

        for gs in scenario["gslist"]:
            gsid = gs["id"]
            found = False
            for existing_gs in joined_scenario["gslist"]:
                if gsid == existing_gs["id"]:
                    assert abs(gs["lat"] - existing_gs["lat"]) < 1e-3, (
                        f"#{i}: GS {gsid} exists with other coordinates"
                    )
                    assert abs(gs["lon"] - existing_gs["lon"]) < 1e-3, (
                        f"#{i}: GS {gsid} exists with other coordinates"
                    )
                    assert abs(gs["alt"] - existing_gs["alt"]) < 1e-3, (
                        f"#{i}: GS {gsid} exists with other coordinates"
                    )
                    found = True
                    break
            if not found:
                joined_scenario["gslist"].append(gs)

        for sat in scenario["satlist"]:
            found = False
            for existing_sat in joined_scenario["satlist"]:
                if sat["id"] == existing_sat["id"]:
                    existing_sat["tle_list"].extend(sat["tle_list"])
                    existing_sat["tle_list"].sort(key=lambda e: e[0])
                    found = True
                    break
            if not found:
                joined_scenario["satlist"].append(sat)

    print("Dumping scenario file...", file=sys.stderr)
    json.dump(
        joined_scenario,
        args.output,
        indent=(4 if args.indent else None),
    )
    args.output.write("\n")
    args.output.close()


def _get_argument_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "INFILE",
        nargs="+",
        type=argparse.FileType("r"),
        default=[sys.stdin],
        help="the input (F/P-)TVGs",
    )
    parser.add_argument(
        "-o", "--output",
        type=argparse.FileType("w"),
        default=sys.stdout,
        help="the output (F/P-)TVG",
    )
    parser.add_argument(
        "-i", "--indent",
        action="store_true",
        help="indent the JSON output",
    )
    return parser


if __name__ == "__main__":
    _main(_get_argument_parser().parse_args())
