#!/usr/bin/env python3
# encoding: utf-8

import argparse
import json
import sys
from operator import attrgetter

from tvgutil import tvg


def _main(args):
    data = json.load(args.INFILE)
    args.INFILE.close()
    if "contact_type" in data:
        multi_tvg = {"<node>": tvg.from_serializable(data)}
    else:
        multi_tvg = {
            eid: tvg.from_serializable(g)
            for eid, g in data.items()
        }

    for node, g in multi_tvg.items():
        cp = tvg.to_contact_plan(g)
        start_offset = (
            min(cp, key=attrgetter("start_time")).start_time -
            args.target_offset
        )
        for edge, contacts in g.edges.items():
            for contact in contacts:
                contact.start_time -= start_offset
                contact.end_time -= start_offset
                try:
                    gens = contact.generations
                except AttributeError:
                    for char in contact.characteristics:
                        if char.starting_at != 0:
                            char.starting_at -= start_offset
                else:
                    for gen in gens:
                        if gen.valid_from != 0:
                            gen.valid_from -= start_offset
                        for char in gen.characteristics:
                            if char.starting_at != 0:
                                char.starting_at -= start_offset

    json.dump(
        {node: tvg.to_serializable(g) for node, g in multi_tvg.items()},
        args.output,
        indent=4,
    )
    args.output.write("\n")
    args.output.close()


def _get_argument_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "INFILE",
        type=argparse.FileType("r"),
        default=sys.stdin,
        help="the input (F/P-)TVG",
    )
    parser.add_argument(
        "-o", "--output",
        type=argparse.FileType("w"),
        default=sys.stdout,
        help="the output (F/P-)TVG",
    )
    parser.add_argument(
        "--target-offset",
        type=float,
        default=1.,
        help="the resulting start offset (start of first contact)",
    )
    return parser


if __name__ == "__main__":
    _main(_get_argument_parser().parse_args())
