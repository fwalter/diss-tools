#!/usr/bin/env python3
# coding: utf-8

import argparse
import datetime
import json
import math

# Add colorblind-friendly color cycle
# See https://gist.github.com/thriveth/8560036
import cycler
CB_color_cycle = ["#377eb8", "#ffb01e", "#4daf4a",
                  "#a65628", "#984ea3",  # no grey b/c map is grey
                  "#f781bf", "#e41a1c", "#dede00"]

import matplotlib
matplotlib.rcParams["axes.prop_cycle"] = cycler.cycler(color=CB_color_cycle)

matplotlib.rcParams["text.usetex"] = False
matplotlib.rcParams["font.family"] = "Open Sans"

SAT_COLORS = CB_color_cycle

import ephem
import shapefile as shp  # pyshp package
import numpy as np
import numpy.ma as ma

# matplotlib.rcParams['pdf.fonttype'] = 42
# matplotlib.rcParams['ps.fonttype'] = 42
import matplotlib.pyplot as plt

from matplotlib.lines import Line2D


def _main(args):
    scenario = json.load(args.SCENARIOFILE)

    gs_list = scenario["gslist"]
    sat_list = scenario["satlist"]

    fig = plt.figure()
    fig.set_size_inches(args.width, args.height)

    # Plot landmasses
    sf = shp.Reader("ne_110m_land.shp")
    for shape in sf.shapeRecords():
        x = [i[0] for i in shape.shape.points[:]]
        y = [i[1] for i in shape.shape.points[:]]
        # mask skips longer than 20°
        mc = ma.array(x)
        for i in range(1, len(x)):
            if math.sqrt((x[i] - x[i - 1]) ** 2 + (y[i] - y[i - 1]) ** 2) > 20:
                mc[i - 1] = ma.masked
        plt.fill(mc, y, color="#D0D0D0", zorder=0)
        plt.plot(mc, y, linewidth=1, color="#000000", zorder=1)

    plt.tight_layout()

    # Satellite tracks
    sats = [
        (sat["id"], ephem.readtle(sat["id"], *sat["tle"]))
        for sat in sat_list
    ]
    start_offset = None
    for sat_i, (name, sat) in enumerate(sats):
        track_x = []
        track_y = []
        mask = []
        t = scenario["time_offset"]
        record = 0
        lastlon = 0
        sat.compute(
            datetime.datetime.fromtimestamp(t, tz=datetime.timezone.utc)
        )
        lastlon = float(sat.sublong) * 180 / math.pi
        while True:
            t += 1.
            sat.compute(datetime.datetime.fromtimestamp(
                t,
                tz=datetime.timezone.utc,
            ))
            lon = float(sat.sublong) * 180 / math.pi
            lat = float(sat.sublat) * 180 / math.pi
            if record:
                if np.sign(lastlon) != np.sign(lon) and abs(round(lon)) == 180:
                    if record == args.orbit_count:
                        break
                    mask.append(ma.masked)
                    record += 1
                else:
                    mask.append(None)
                track_x.append(lon)
                track_y.append(lat)
            elif np.sign(lastlon) != np.sign(lon) and abs(round(lon)) == 180:
                record = 1
                start = round(lon)
                if start_offset is None:
                    start_offset = (
                        t -
                        scenario["time_offset"] +
                        args.sat_offset
                    )
            lastlon = lon
        track_x = ma.masked_array(track_x)
        for i, v in enumerate(mask):
            if v is not None:
                track_x[i] = v
        track_plot = plt.plot(
            track_x,
            track_y,
            label=name,
            linestyle="-",
            zorder=3,
            color=SAT_COLORS[
                int(sat_i / args.divide_satcolor) % len(SAT_COLORS)
            ],
        )
        sat.compute(datetime.datetime.fromtimestamp(
            scenario["time_offset"] + start_offset,
            tz=datetime.timezone.utc,
        ))
        track_start = (
            float(sat.sublong) * 180 / math.pi,
            float(sat.sublat) * 180 / math.pi
        )
        plt.scatter(
            [track_start[0]], [track_start[1]],
            marker="o",
            color=track_plot[0].get_color(),
            zorder=4,
        )

    # GSs
    for i, gs_desc in enumerate(gs_list):
        plt.scatter(
            [gs_desc["lon"]], [gs_desc["lat"]],
            marker="s",
            color="#000000",
            zorder=2,
        )
        if not args.no_labels and not args.simple_legend:
            plt.annotate(
                gs_desc["id"],
                xy=(gs_desc["lon"], gs_desc["lat"]),
                xytext=tuple(
                    float(f)
                    for f in args.textloc[i % len(args.textloc)].split(",")
                ),
                textcoords="offset points",
                ha="left",
                va="bottom",
                bbox=dict(
                    # boxstyle="round,pad=0.5",
                    fc="white",
                    alpha=0.92,
                ),
                arrowprops=dict(
                    arrowstyle="-",  # ->
                    connectionstyle="arc3,rad=0",
                ),
                zorder=5,
            )

    if args.simple_legend:
        legend_elements = [
            #Line2D([0], [0], color=SAT_COLORS[0], lw=4, label="Sat."),
            Line2D([0], [0], marker="o", color="w", label="Satellite",
                   markerfacecolor="#000000", markersize=10),
            Line2D([0], [0], marker="s", color="w", label="GS",
                   markerfacecolor="#000000", markersize=10),
        ]
        plt.legend(
            handles=legend_elements,
            loc=args.legend_location,
            ncol=args.legend_columns,
        )
    elif not args.no_labels:
        plt.legend(
            loc=args.legend_location,
            ncol=args.legend_columns,
        )
    plt.tight_layout()
    if args.outfile:
        plt.savefig(args.outfile)
    else:
        plt.show()


def _get_argument_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "SCENARIOFILE",
        type=argparse.FileType("r"),
        help="the scenario file",
    )
    parser.add_argument(
        "--textloc",
        nargs="+", default=["6,12"],
        help="custom text locations",
    )
    parser.add_argument(
        "--orbit-count",
        type=float,
        default=3,
        help="amount of orbits plotted",
    )
    parser.add_argument(
        "--sat-offset",
        type=int,
        default=2700,
        help="offset of the satellite marker on the line, in seconds",
    )
    parser.add_argument(
        "--width",
        type=float,
        default=6,
        help="width of the plot in inches (default: 6)",
    )
    parser.add_argument(
        "--height",
        type=float,
        default=3.45,
        help="height of the plot in inches (default: 3.45)",
    )
    parser.add_argument(
        "--legend-columns",
        type=int,
        default=1,
        help="the amount of columns used for the legend (default: 1)",
    )
    parser.add_argument(
        "--no-labels",
        action="store_true",
        help="hide labels of GS and legend for sats",
    )
    parser.add_argument(
        "--divide-satcolor",
        type=int,
        default=1,
        help="divide the sat index to determine the color (default: 1)",
    )
    parser.add_argument(
        "--simple-legend",
        action="store_true",
        help="generate a simple legend identifying GS and Sat.",
    )
    parser.add_argument(
        "--legend-location",
        default="lower right",
        help="the legend location passed to matplotlib (default: lower right)",
    )
    parser.add_argument(
        "-o", "--outfile",
        help="the output filename",
    )
    return parser


if __name__ == "__main__":
    _main(_get_argument_parser().parse_args())
