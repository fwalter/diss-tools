#!/usr/bin/env python3
# encoding: utf-8

# Tool to apply a new scenario using measurements for another one as basis.

import argparse
import itertools
import json
import math
import operator
import random
import sys

import numpy as np

from tvgutil import contact_plan, tvg

from predictutil import (
    get_contact,
    get_fc_volume,
    get_pcp_for_scenario,
    get_trajectory_predictors_for_scenario,
)


def print_graph_info(name, graph):
    contacts = tvg.to_contact_plan(graph)
    start = min(c.start_time for c in contacts)
    end = max(c.end_time for c in contacts)
    print(
        f"> {name}: {len(contacts)} contacts of {len(graph.vertices)} nodes "
        f"({len(graph.edges)} edges) in [{start}, {end}] "
        f"({(end - start) / 86400} day(s)).",
        file=sys.stderr
    )


def _key_valid(k, valid_keys, invalid_keys):
    return (k in valid_keys or not valid_keys) and k not in invalid_keys


def filter_list(lst, element_key, valid_keys, invalid_keys):
    return [
        e
        for e in lst
        if _key_valid(e[element_key], valid_keys, invalid_keys)
    ]


def _main(args):
    assert args.duration < 60, "remember --duration is in DAYS"

    # 1. Load 2 scenarios and orig. F-TVG

    print("Loading TVGs...", file=sys.stderr)

    # NOTE: It is important that the provided FTVG has its contacts already
    # merged according to predicted time windows!
    orig_ftvg_unmodified = tvg.from_serializable(json.load(args.ORIGINAL_FTVG))
    orig_fcp_unmodified = tvg.to_contact_plan(orig_ftvg_unmodified)
    print_graph_info("Original F-TVG", orig_ftvg_unmodified)

    orig_scenario_unmodified = json.load(args.ORIGINAL_SCENARIO)
    new_scenario = json.load(args.NEW_SCENARIO)

    # 2. a) Filter nodes from old scenario

    print("Filtering nodes...", file=sys.stderr)

    orig_scenario = {
        "gslist": filter_list(
            orig_scenario_unmodified["gslist"],
            "id",
            args.valid_gs or orig_ftvg_unmodified.vertices,
            args.invalid_gs,
        ),
        "satlist": filter_list(
            orig_scenario_unmodified["satlist"],
            "id",
            args.valid_sat or orig_ftvg_unmodified.vertices,
            args.invalid_sat,
        ),
        "time_offset": orig_scenario_unmodified["time_offset"],
        "bit_rate_uplink": orig_scenario_unmodified["bit_rate_uplink"],
        "bit_rate_downlink": orig_scenario_unmodified["bit_rate_downlink"],
    }

    # 2. b) Filter original F-TVG for edges available in the filtered scenario
    # and, by that, in the generated "original" P-TVG...

    orig_fcp_without_volfilter = [
        fc
        for fc in orig_fcp_unmodified
        if (
            (
                _key_valid(fc.tx_node, args.valid_gs, args.invalid_gs) or
                _key_valid(fc.tx_node, args.valid_sat, args.invalid_sat)
            ) and
            (
                _key_valid(fc.rx_node, args.valid_gs, args.invalid_gs) or
                _key_valid(fc.rx_node, args.valid_sat, args.invalid_sat)
            )
        )
    ]
    orig_ftvg_without_volfilter = tvg.from_contact_plan(
        orig_fcp_without_volfilter,
        contact_type=contact_plan.Contact,
    )

    print_graph_info("Filtered original F-TVG", orig_ftvg_without_volfilter)

    # 2. c) Remove empty / low-volume FCs

    if args.volume_filter:
        orig_fcp = [
            fc
            for fc in orig_fcp_without_volfilter
            if (
                get_fc_volume(fc, block_size=args.link_block_size) >=
                args.volume_filter
            )
        ]
        orig_ftvg = tvg.from_contact_plan(
            orig_fcp,
            contact_type=contact_plan.Contact,
        )
        print_graph_info("Original F-TVG after vol.-filter", orig_ftvg)
    else:
        orig_fcp = orig_fcp_without_volfilter
        orig_ftvg = orig_ftvg_without_volfilter

    # 3. Build mappings OLD NODE -> NEW NODE

    print("Building mappings...", file=sys.stderr)

    orig_gslist = orig_scenario["gslist"]
    orig_satlist = orig_scenario["satlist"]
    new_gslist = new_scenario["gslist"]
    new_satlist = new_scenario["satlist"]

    # Assert the lists are orthogonal
    assert not (
        set(e["id"] for e in orig_gslist) &
        set(e["id"] for e in orig_satlist)
    )
    assert not (
        set(e["id"] for e in new_gslist) &
        set(e["id"] for e in new_satlist)
    )

    # Create GS, sat, and joined mapping as dicts
    if args.assignmode == "roundrobin":
        gs_mapping = {
            new_gs["id"]: orig_gslist[i % len(orig_gslist)]
            for i, new_gs in enumerate(new_gslist)
        }
        sat_mapping = {
            new_sat["id"]: orig_satlist[i % len(orig_satlist)]
            for i, new_sat in enumerate(new_satlist)
        }
    else:
        assert args.assignmode == "random"
        gs_mapping = {
            new_gs["id"]: random.choice(orig_gslist)
            for new_gs in new_gslist
        }
        sat_mapping = {
            new_sat["id"]: random.choice(orig_satlist)
            for new_sat in new_satlist
        }
    node_mapping = {
        **gs_mapping,
        **sat_mapping,
    }

    print(
        "> Node association:",
        {k: v["id"] for k, v in node_mapping.items()},
        file=sys.stderr
    )

    # 4. Assert the sample size is big and diverse enough

    print("Performing sanity checks...", file=sys.stderr)

    elev_thresh, elev_step = np.linspace(0, 90, args.elevation_check_bins,
                                         endpoint=False, retstep=True)
    tps = get_trajectory_predictors_for_scenario(orig_scenario)
    for new_gs, new_sat in itertools.product(new_gslist, new_satlist):
        orig_gs_id = gs_mapping[new_gs["id"]]["id"]
        orig_sat_id = sat_mapping[new_sat["id"]]["id"]
        # 5. a) Check for size
        sample_size = (
            len(orig_ftvg.edges[(orig_gs_id, orig_sat_id)]) +
            len(orig_ftvg.edges[(orig_sat_id, orig_gs_id)])
        )
        assert sample_size >= args.min_sample_size, (
            f"too few samples for ('{orig_gs_id}', '{orig_sat_id}'): "
            f"{sample_size}"
        )
        # 5. b) Check for elevation diversity
        tp = tps[frozenset((orig_gs_id, orig_sat_id))]
        elev_bins = np.zeros_like(elev_thresh)
        for fc in (orig_ftvg.edges[(orig_gs_id, orig_sat_id)] +
                   orig_ftvg.edges[(orig_sat_id, orig_gs_id)]):
            _, elev_rad = tp.predict_max_elevation(fc.start_time, fc.end_time)
            elev_deg = elev_rad * 180 / math.pi
            elev_bin_i = int(elev_deg / elev_step)
            elev_bins[elev_bin_i] += 1
        assert all(cnt >= args.min_contacts_per_bin
                   for cnt in elev_bins[(args.first_checked_bin):]), (
            f"too few contacts for ('{orig_gs_id}', '{orig_sat_id}') in bins: "
            f"{elev_bins}"
        )

    # 5. Update properties of new scenario

    print("Creating reference TVGs...", file=sys.stderr)

    new_scenario["bit_rate_uplink"] = orig_scenario["bit_rate_uplink"]
    new_scenario["bit_rate_downlink"] = orig_scenario["bit_rate_downlink"]

    # 6. generate P-TVGs for old and new

    orig_scenario_start = min(
        orig_fcp,
        key=operator.attrgetter("start_time"),
    ).start_time
    orig_scenario_end = max(
        orig_fcp,
        key=operator.attrgetter("end_time"),
    ).end_time
    orig_pcp = get_pcp_for_scenario(
        orig_scenario,
        orig_scenario_start,
        orig_scenario_end,
        # We always want the old PCP filtered as we only need the soft time
        # frames with elevation == 0 deg for all observations.
        ftvg_filter=orig_ftvg,
        strip_start_end=False,
    )
    orig_ptvg = tvg.from_contact_plan(orig_pcp)
    print_graph_info("Inferred reference P-TVG", orig_ptvg)

    new_unadjusted_pcp = get_pcp_for_scenario(
        new_scenario,
        new_scenario["time_offset"],
        new_scenario["time_offset"] + (args.duration * 86400),
        # No need to filter -- we do not have an F-TVG yet, we build it!
        ftvg_filter=None,
        strip_start_end=True,
    )

    # 7. Generate new F-TVG

    print("Generating new TVGs...", file=sys.stderr)

    # 7. a) Assign the candidate contacts on a bi-directional basis.
    fc_candidates = []
    pc_candidates = []
    known = {}
    for unadj_pc in new_unadjusted_pcp:
        tup = (
            frozenset((unadj_pc.tx_node, unadj_pc.rx_node)),
            unadj_pc.start_time,
        )
        if tup in known:
            candidate_fc, candidate_pc = known[tup]
            fc_candidates.append(candidate_fc)
            pc_candidates.append(candidate_pc)
            continue

        orig_tx_node = node_mapping[unadj_pc.tx_node]["id"]
        orig_rx_node = node_mapping[unadj_pc.rx_node]["id"]

        # 7.1 Get a contact from the original FCP

        # NOTE: Currently only uniquely-dist. random choice is implemented.
        # It _may_ be an option to chosse based on max. elevation, etc.
        # Though, this may bias results and has to be evaluated beforehand.
        candidate_fc = random.choice(
            orig_ftvg.edges[(orig_tx_node, orig_rx_node)]
        )

        # 7.2 Get the PC for that contact -> soft interval

        # Selection is performed by overlap detection.
        candidate_pc = get_contact(
            orig_ptvg.edges[(orig_tx_node, orig_rx_node)],
            candidate_fc.start_time,
            candidate_fc.end_time,
        )
        assert candidate_pc, f"no PC found for {candidate_fc}"

        fc_candidates.append(candidate_fc)
        pc_candidates.append(candidate_pc)
        known[tup] = candidate_fc, candidate_pc

    # 7. b) Build the FCP, PCP, and remapping dict based on the candidates.
    new_fcp = []
    new_pcp = []
    remap_dict = {
        n1: {
            n2: {}
            for n2 in node_mapping
        }
        for n1 in node_mapping
    }
    for i, unadj_pc in enumerate(new_unadjusted_pcp):
        candidate_fc = fc_candidates[i]
        candidate_pc = pc_candidates[i]

        # 7.3 Compute offsets

        # We compute the offsets from the difference of contact start times.
        # NOTE: Centering was attempted but will lead to misalignment with
        # the SGP4 instances in the TP.
        pc_offset = unadj_pc.start_time - candidate_pc.start_time

        # 7.4 Derive a new FC

        fc = contact_plan.Contact(
            tx_node=unadj_pc.tx_node,
            rx_node=unadj_pc.rx_node,
            start_time=(candidate_fc.start_time + pc_offset),
            end_time=(candidate_fc.end_time + pc_offset),
            characteristics=[
                contact_plan.ContactCharacteristics(
                    starting_at=(
                        char.starting_at + pc_offset
                        if char.starting_at > 1.e-3 else 0.0
                    ),
                    bit_rate=char.bit_rate,
                    bit_error_rate=char.bit_error_rate,
                    delay=char.delay,
                )
                for char in candidate_fc.characteristics
            ],
        )
        new_fcp.append(fc)

        # 7.5 Derive a new PC (-> the duration is adjusted)
        # NOTE: generations are not relevant for this "prototype" PC

        candidate_fc_simple = candidate_fc.to_simple()
        pc = contact_plan.PredictedContact.simple(
            unadj_pc.tx_node,
            unadj_pc.rx_node,
            candidate_pc.start_time + pc_offset,
            candidate_pc.end_time + pc_offset,
            bit_rate=candidate_fc_simple.bit_rate,
            probability=candidate_fc_simple.delay,
        )
        new_pcp.append(pc)

        # 7.6 Add offsets to time-based contact remap for mock predictor
        remap_timestamp = pc.start_time
        remap_offset = pc_offset
        remap_dict[pc.tx_node][pc.rx_node][remap_timestamp] = remap_offset

    # 8. Check the plausibility of generated data

    # Ensure offsets are symmetrical - else, the mock TP would not work!
    for n1 in node_mapping:
        for n2 in node_mapping:
            assert remap_dict[n1][n2] == remap_dict[n2][n1]

    # 9. Build mock information base

    new_scenario["mock"] = {
        "orig_sat": sat_mapping,
        "orig_gs": gs_mapping,
        "remap": remap_dict,
    }

    # 10. Store new scenario file and TVGs

    new_ftvg = tvg.from_contact_plan(
        new_fcp,
        contact_type=contact_plan.Contact,
    )
    print_graph_info("Created F-TVG", new_ftvg)
    new_ptvg = tvg.from_contact_plan(new_pcp)
    print_graph_info("Created P-TVG", new_ptvg)

    print("Writing data to files...", file=sys.stderr)

    json.dump(
        tvg.to_serializable(new_ftvg),
        args.ftvg_output,
        indent=(4 if args.indent else None),
    )
    args.ftvg_output.write("\n")
    args.ftvg_output.close()

    json.dump(
        tvg.to_serializable(new_ptvg),
        args.ptvg_output,
        indent=(4 if args.indent else None),
    )
    args.ptvg_output.write("\n")
    args.ptvg_output.close()

    json.dump(
        new_scenario,
        args.scenario_output,
        indent=(4 if args.indent else None),
    )
    args.scenario_output.write("\n")
    args.scenario_output.close()


def _get_argument_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "ORIGINAL_SCENARIO",
        type=argparse.FileType("r"),
        default=sys.stdin,
        help="the original scenario",
    )
    parser.add_argument(
        "ORIGINAL_FTVG",
        type=argparse.FileType("r"),
        default=sys.stdin,
        help="the original F-TVG",
    )
    parser.add_argument(
        "NEW_SCENARIO",
        type=argparse.FileType("r"),
        default=sys.stdin,
        help="the new scenario",
    )

    # GS / Sat assignment args
    parser.add_argument(
        "--valid-gs",
        nargs="*",
        default=[],
        help="a list of valid ground stations (whitelist)",
    )
    parser.add_argument(
        "--invalid-gs",
        nargs="*",
        default=[],
        help="a list of invalid (excluded) ground stations (blacklist)",
    )
    parser.add_argument(
        "--valid-sat",
        nargs="*",
        default=[],
        help="a list of valid satellites (whitelist)",
    )
    parser.add_argument(
        "--invalid-sat",
        nargs="*",
        default=[],
        help="a list of invalid (excluded) satellites (blacklist)",
    )
    parser.add_argument(
        "--assignmode",
        choices=["roundrobin", "random"],
        default="roundrobin",
        help="how to assign GSs and satellites (default: roundrobin)",
    )

    # FTVG volume-filter
    parser.add_argument(
        "--volume-filter",
        type=int,
        default=None,
        help="minimum volume (bits) for a source contact (default: 0)",
    )
    parser.add_argument(
        "--link-block-size",
        type=int,
        default=(1500 * 8),
        help="the block size (bit) for the link-layer (default: 1.5 kB)",
    )

    parser.add_argument(
        "--duration",
        type=float,
        required=True,
        help="the duration of the new scenario, in days",
    )

    # Checker args
    parser.add_argument(
        "--min-sample-size",
        type=int,
        default=10,
        help="the minimum count of unidir. contacts per pair, default: 10",
    )
    parser.add_argument(
        "--elevation-check-bins",
        type=int,
        default=3,
        help="the amount of groups for elevation sample checking, default: 3",
    )
    parser.add_argument(
        "--min-contacts-per-bin",
        type=int,
        default=4,
        help="the minimum count of unidir. contacts elev. group, default: 4",
    )
    parser.add_argument(
        "--first-checked-bin",
        type=int,
        default=0,
        help="allow the specified bin count below sample size, default: 0",
    )

    parser.add_argument(
        "--ftvg-output",
        type=argparse.FileType("w"),
        required=True,
        help="the F-TVG output file",
    )
    parser.add_argument(
        "--ptvg-output",
        type=argparse.FileType("w"),
        required=True,
        help="the P-TVG output file",
    )
    parser.add_argument(
        "--scenario-output",
        type=argparse.FileType("w"),
        required=True,
        help="the scenario output file with re-mapping",
    )

    parser.add_argument(
        "-i", "--indent",
        action="store_true",
        help="indent the JSON output",
    )
    return parser


if __name__ == "__main__":
    _main(_get_argument_parser().parse_args())
