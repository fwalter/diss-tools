#!/usr/bin/env python3
# encoding: utf-8

import sys
import argparse
import json
import datetime

from tvgutil import contact_plan, tvg

from predictutil import get_contact


def _main(args):
    graphs = []
    contact_type = None
    for i, tvgfile in enumerate(args.INFILE):
        graph = tvg.from_serializable(json.load(tvgfile))
        if contact_type is None:
            contact_type = graph.contact_type
        else:
            assert graph.contact_type is contact_type, f"wrong type: {tvgfile}"
        contacts = [c for ev, ec in graph.edges.items() for c in ec]
        start = min(c.start_time for c in contacts)
        end = max(c.end_time for c in contacts)
        print(
            f"#{i} {len(contacts)} contacts of {len(graph.vertices)} nodes "
            f"({len(graph.edges)} edges) in [{start}, {end}] "
            f"({(end - start) / 86400} day(s)).",
            file=sys.stderr
        )
        graphs.append(graph)

    print("Joining edges...", file=sys.stderr)
    edges = {}
    for i, graph in enumerate(graphs):
        for edge, edge_contacts in graph.edges.items():
            if edge not in edges:
                edges[edge] = []
            for contact in edge_contacts:
                assert get_contact(
                    edges[edge],
                    contact.start_time,
                    contact.end_time,
                ) is None, f"invalid: contact {contact} in #{i} overlaps"
            edges[edge].extend(edge_contacts)

    print("Rebuilding TVG...", file=sys.stderr)
    contacts = [c for ev, ec in edges.items() for c in ec]
    graph = tvg.from_contact_plan(contacts, contact_type=contact_type)
    print(
        f"Final graph has {len(contacts)} contacts "
        f"({len(graph.vertices)} nodes, {len(graph.edges)} edges).",
        file=sys.stderr
    )

    print("Dumping TVG file...", file=sys.stderr)
    json.dump(
        tvg.to_serializable(graph),
        args.output,
        indent=(4 if args.indent else None),
    )
    args.output.write("\n")
    args.output.close()


def _get_argument_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "INFILE",
        nargs="+",
        type=argparse.FileType("r"),
        default=[sys.stdin],
        help="the input (F/P-)TVGs",
    )
    parser.add_argument(
        "-o", "--output",
        type=argparse.FileType("w"),
        default=sys.stdout,
        help="the output (F/P-)TVG",
    )
    parser.add_argument(
        "-i", "--indent",
        action="store_true",
        help="indent the JSON output",
    )
    return parser


if __name__ == "__main__":
    _main(_get_argument_parser().parse_args())
