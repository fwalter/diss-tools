#!/usr/bin/env python3
# encoding: utf-8

import sys
import argparse
import json
import datetime

from tvgutil import contact_plan, tvg


def _main(args):
    graph = tvg.from_serializable(json.load(args.INFILE))
    contacts = [c for ev, ec in graph.edges.items() for c in ec]
    start = min(c.start_time for c in contacts)
    end = max(c.end_time for c in contacts)
    print(
        f"Found {len(contacts)} contacts of {len(graph.vertices)} nodes "
        f"({len(graph.edges)} edges) in [{start}, {end}] "
        f"({(end - start) / 86400} day(s)).",
        file=sys.stderr
    )
    start += args.startoffset * 3600
    duration = min(end - start, args.duration * 3600)
    duration *= args.percentage * 0.01  # .01 scales percentage to ratio
    end = start + duration
    print(
        f"Reducing TVG to time interval [{start}, {end}] "
        f"({duration / 86400} day(s)) or {args.maxedgecontacts} "
        "contacts per edge...",
        file=sys.stderr
    )
    contact_count = 0
    for _, ec in graph.edges.items():
        i = 0
        while i < len(ec):
            contact = ec[i]
            if contact.start_time > end or contact.end_time < start:
                del ec[i]
            elif i >= args.maxedgecontacts:
                del ec[i]
            elif args.mode == "drop" and (
                    contact.end_time > end or contact.start_time < start):
                del ec[i]
            else:
                if args.mode == "crop":
                    new_start = contact.start_time
                    new_end = contact.end_time
                    if contact.start_time < start:
                        new_start = start
                    if contact.end_time > end:
                        new_end = end
                    ec[i] = graph.contact_type(
                        contact.tx_node, contact.rx_node, new_start, new_end,
                        (
                            contact.characteristics
                            if hasattr(contact, "characteristics")
                            else contact.generations
                        )
                    )
                i += 1
                contact_count += 1
    print(
        f"Reduced graph to {contact_count} contacts "
        f"({len(graph.vertices)} nodes, {len(graph.edges)} edges).",
        file=sys.stderr
    )
    print("Checking for edges with too few contacts...", file=sys.stderr)
    pairs = list(graph.edges.keys())
    for ek in pairs:
        edge_length = len(graph.edges[ek])
        if edge_length < args.mincontacts:
            print(
                f"> Dropping edge {ek} (len = {edge_length})",
                file=sys.stderr
            )
            contact_count -= edge_length
            del graph.edges[ek]
    print(
        f"Cleaned graph has {contact_count} contacts "
        f"({len(graph.vertices)} nodes, {len(graph.edges)} edges).",
        file=sys.stderr
    )
    print("Checking for isolated nodes...", file=sys.stderr)
    nodes = list(graph.vertices)
    for node in nodes:
        rm = False
        outgoing_cap = sum(
            sum(
                char.bit_rate * (c_end - c_start)
                for c_start, c_end, char in
                c.characteristics_in_time_frame(c.start_time, c.end_time)
            )
            for ev, ec in graph.edges.items()
            for c in ec
            if ev[0] == node
        )
        if outgoing_cap < args.minout or outgoing_cap == 0:
            print(
                f"> Node '{node}' has not enough outgoing capacity.",
                file=sys.stderr
            )
            rm = True
        incoming_cap = sum(
            sum(
                char.bit_rate * (c_end - c_start)
                for c_start, c_end, char in
                c.characteristics_in_time_frame(c.start_time, c.end_time)
            )
            for ev, ec in graph.edges.items()
            for c in ec
            if ev[1] == node
        )
        if incoming_cap < args.minin or incoming_cap == 0:
            print(
                f"> Node '{node}' has not enough incoming capacity.",
                file=sys.stderr
            )
            rm = True
        if rm or node in args.dropnodes:
            print(f"> Dropping node {node}", file=sys.stderr)
            del graph.vertices[node]
            for ek in list(graph.edges.keys()):
                if ek[0] == node or ek[1] == node:
                    print(f"> Dropping edge {ek}", file=sys.stderr)
                    contact_count -= len(graph.edges[ek])
                    del graph.edges[ek]

    print("Rebuilding vertex-associations...", file=sys.stderr)
    for vk in graph.vertices:
        graph.vertices[vk] = {
            neigh for neigh in graph.vertices if (vk, neigh) in graph.edges
        }
    print(
        f"Final graph has {contact_count} contacts "
        f"({len(graph.vertices)} nodes, {len(graph.edges)} edges).",
        file=sys.stderr
    )
    json.dump(
        tvg.to_serializable(graph),
        args.output,
        indent=(4 if args.indent else None),
    )
    args.output.write("\n")
    args.output.close()


def _get_argument_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "INFILE",
        type=argparse.FileType("r"),
        default=sys.stdin,
        help="the input (F/P-)TVG",
    )
    parser.add_argument(
        "-o", "--output",
        type=argparse.FileType("w"),
        default=sys.stdout,
        help="the output (F/P-)TVG",
    )
    parser.add_argument(
        "-i", "--indent",
        action="store_true",
        help="indent the JSON output",
    )
    parser.add_argument(
        "--mode",
        choices=["crop", "keep", "drop"], default="crop",
        help="how to deal with contact intervals exceeding overall time frame",
    )
    parser.add_argument(
        "-s", "--startoffset",
        type=float, default=0,
        help="the start offset of the cropped interval, in hours",
    )
    parser.add_argument(
        "-d", "--duration",
        type=float, default=float("inf"),
        help="the length (duration) of the cropped interval, in hours",
    )
    parser.add_argument(
        "-p", "--percentage",
        type=float, default=100.0,
        help="reduce final interval to the provided percentage",
    )
    parser.add_argument(
        "-c", "--maxedgecontacts",
        type=float, default=float("inf"),
        help="the maximum amount of contacts per edge in the final graph",
    )
    parser.add_argument(
        "--minout",
        type=float, default=1e-10,
        help="minimum effective outgoing capacity for a node, in bits",
    )
    parser.add_argument(
        "--minin",
        type=float, default=1e-10,
        help="minimum effective incoming capacity for a node, in bits",
    )
    parser.add_argument(
        "--msgsize",
        type=float, default=20000,
        help="the assumed msg. size for conf. filtering (bits, default: 20k)",
    )
    parser.add_argument(
        "--mincontacts",
        type=int, default=1,
        help="minimum contact count for kept nodes (default: 1)",
    )
    parser.add_argument(
        "--dropnodes",
        nargs="*", default=[],
        help="nodes to be dropped additionally (default: <none>)",
    )
    return parser


if __name__ == "__main__":
    _main(_get_argument_parser().parse_args())
