#!/usr/bin/env python3
# encoding: utf-8

import argparse
import json

import numpy as np
import matplotlib.cm
import matplotlib.pyplot as plt
import networkx as nx

from tvgutil import tvg


def _main(args):
    print("Loading TVG...")
    with open(args.FTVGFILE, "r") as f:
        ftvg = tvg.from_serializable(json.load(f))

    print("Loading statistics...")
    algo_stats_c = {}
    algo_stats_b = {}
    nodes = set()
    for i, statfile in enumerate(args.STATFILE):
        with open(statfile, "r") as f:
            data = json.load(f)
            algo_name = (
                args.algonames[i] if i < len(args.algonames)
                else f"<algo #{i}>"
            )
            assert algo_name not in algo_stats_c
            assert algo_name not in algo_stats_b
            algo_stats_c[algo_name] = data["contact_utilization"]
            algo_stats_b[algo_name] = data["buffer_utilization"]
            nodes |= {n for n, _, _, _, _, _ in data["contact_utilization"]}

    algolist = "\n    ".join(algo_stats_c.keys())
    nodelist = "\n    ".join(nodes)
    print(
        f"Loaded stats for {len(algo_stats_c)} algo(s) "
        f"and {len(nodes)} node(s)\n"
        f"-> Algos:\n    {algolist}\n-> Nodes:\n    {nodelist}"
    )

    def _xmean(lst):
        if lst:
            return np.mean(lst)
        else:
            return 0

    c_util_by_edge = {
        algo_name: {
            (tx_node, rx_node): (sum if args.absolute else _xmean)([
                (cu_abs if args.absolute else cu_rel)
                for n1, n2, _, _, cu_rel, cu_abs in algo_stats_c[algo_name]
                if n1 == tx_node and n2 == rx_node
            ])
            for tx_node in nodes
            for rx_node in nodes
        }
        for algo_name in algo_stats_c
    }

    buf_util_by_node = {
        algo_name: {
            node: np.mean([
                bu
                for bu_node, _, bu in algo_stats_b[algo_name]
                if bu_node == node
            ])
            for node in nodes
        }
        for algo_name in algo_stats_b
    }

    graph = nx.DiGraph()
    for node in sorted(nodes):
        graph.add_node(node)
    for tx_node, rx_node in sorted(ftvg.edges):
        graph.add_edge(tx_node, rx_node)

    nodelist = graph.nodes()
    edgelist = graph.edges()
    for algo_name in algo_stats_c:
        fig = plt.figure()
        PLT_N_BASE_SZ = 3500
        PLT_E_BASE_SZ = 3
        mean_c_util = np.mean(list(c_util_by_edge[algo_name].values()))
        max_c_util = max(c_util_by_edge[algo_name].values())
        for edge in edgelist:
            if edge not in c_util_by_edge[algo_name]:
                c_util_by_edge[algo_name][edge] = [0]
        edgesizes = [
            np.mean(c_util_by_edge[algo_name][edge]) /
            mean_c_util * PLT_E_BASE_SZ
            for edge in edgelist
        ]
        edgecolors = [
            0.8 - (c_util_by_edge[algo_name][edge] / max_c_util * 0.8)
            for edge in edgelist
        ]
        for eid in nodelist:
            if eid not in buf_util_by_node[algo_name]:
                buf_util_by_node[algo_name][eid] = 0
        mean_b_util = np.mean(list(buf_util_by_node[algo_name].values()))
        max_b_util = max(list(buf_util_by_node[algo_name].values()))
        nodesizes = [
            buf_util_by_node[algo_name][eid] / mean_b_util * PLT_N_BASE_SZ
            for eid in nodelist
        ]
        nodecolors = [
            0.8 - (buf_util_by_node[algo_name][eid] / max_b_util * 0.8)
            for eid in nodelist
        ]
        pos = nx.bipartite_layout(
            graph,
            nodes=list(set(args.partition1) & nodes),
            align="horizontal",
        )
        nx.draw(
            graph,
            node_size=nodesizes,
            pos=pos,
            label=algo_name,
            node_color=nodecolors,
            edge_cmap=matplotlib.cm.get_cmap("gray"),
            cmap="gray",
            width=edgesizes,
            edge_color=edgecolors,
            arrowsize=30.,
            arrowstyle="simple",
            vmin=0,
            vmax=1,
            edge_vmin=0,
            edge_vmax=1,
            connectionstyle='arc3,rad=0.2'
        )
        for node in nodelist:
            PLT_OFF = 0.15
            x, y = pos[node]
            offset = PLT_OFF if node in args.partition1 else -PLT_OFF
            plt.text(
                x=x,
                y=(y + offset),
                s=node,
                horizontalalignment="center",
                verticalalignment="center",
                fontsize=18,
            )

        fig.set_size_inches(14, 8)
        plt.margins(0.1)
        if args.outfile_prefix:
            plt.savefig(args.outfile_prefix + f"_{algo_name}.pdf")
        else:
            plt.show()


def _get_argument_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "FTVGFILE",
        help="the F-TVG file",
    )
    parser.add_argument(
        "STATFILE",
        nargs="+",
        help="the (joined) input statistics files",
    )
    parser.add_argument(
        "-o", "--outfile-prefix",
        help="the output filename prefix",
    )
    parser.add_argument(
        "-n", "--algonames",
        nargs="*", default=[],
        help="the algorithm names",
    )
    parser.add_argument(
        "--partition1",
        nargs="*", default=[
            "NOAA 15", "NOAA 18", "NOAA 19",
            "LEO-01", "LEO-02", "LEO-03", "LEO-04", "LEO-05", "LEO-06",
            "LEO-11", "LEO-12", "LEO-13", "LEO-14", "LEO-15", "LEO-16",
            "LEO-21", "LEO-22", "LEO-23", "LEO-24", "LEO-25", "LEO-26",
            "LEO-31", "LEO-32", "LEO-33", "LEO-34", "LEO-35", "LEO-36",
            "LEO-41", "LEO-42", "LEO-43", "LEO-44", "LEO-45", "LEO-46",
            "LEO-51", "LEO-52", "LEO-53", "LEO-54", "LEO-55", "LEO-56",
            "LEO-61", "LEO-62", "LEO-63", "LEO-64", "LEO-65", "LEO-66",
        ],
        help="nodes in one graph partition, default: NOAA xx and LEO-01 to 66",
    )
    parser.add_argument(
        "--absolute",
        action="store_true",
        help="plot based on absolute utilization",
    )
    return parser


if __name__ == "__main__":
    _main(_get_argument_parser().parse_args())
