#!/usr/bin/env python3
# encoding: utf-8

import argparse
import json
from operator import attrgetter

# Add colorblind-friendly color cycle
# See https://gist.github.com/thriveth/8560036
import cycler
CB_color_cycle = ["#377eb8", "#ffb01e", "#4daf4a",
                  "#616161", "#a65628", "#984ea3",
                  "#f781bf", "#e41a1c", "#dede00"]

import matplotlib
matplotlib.rcParams["axes.prop_cycle"] = cycler.cycler(color=CB_color_cycle)

matplotlib.rcParams["text.usetex"] = False
matplotlib.rcParams["font.family"] = "Open Sans"

import numpy as np
import matplotlib.pyplot as plt
import tqdm

from tvgutil import tvg

from predictutil import get_contact

PCT_LO = 2.5
PCT_HI = 97.5

BARCOLORS_1 = [
    #"#46150f",
    #"#641e16",

    "#616161",

    "#82271d",
    "#a03023",
    "#be392a",
    "#dc4231",
    "#fa4b38",
]
BARCOLORS_2 = [
    #"#134609",
    #"#1c640d",

    "#4daf4a",

    "#248211",
    "#2da016",
    "#35be1a",
    "#3edc1e",
    "#46fa22",
]

MARKERS = [
    "D",
    "x",
    "o",
    "s",
    "v",
    "^",
    "<",
    ">",
    "d",
]


def _estimate_prob(ftvg, ptvg, pc):
    edge = pc.tx_node, pc.rx_node
    return len(ftvg.edges[edge]) / len(ptvg.edges[edge])


def _main(args):
    print("Loading TVGs...")
    with open(args.FTVGFILE, "r") as f:
        ftvg = tvg.from_serializable(json.load(f))
    fcp = tvg.to_contact_plan(ftvg)
    multi_pcps = []
    multi_ptvgs = []
    for ptvgfile in args.PTVGFILE:
        with open(ptvgfile, "r") as f:
            data = json.load(f)
            if "contact_type" in data:
                multi_ptvg = {"<node>": tvg.from_serializable(data)}
            else:
                multi_ptvg = {
                    eid: tvg.from_serializable(sptvg)
                    for eid, sptvg in data.items()
                }
        multi_pcps.append({
            eid: tvg.to_contact_plan(ptvg)
            for eid, ptvg in multi_ptvg.items()
        })
        multi_ptvgs.append(multi_ptvg)
    nodelist = "\n    ".join(sorted(next(iter(multi_ptvg.values())).vertices))
    print(f"{len(fcp)} factual and {len(next(iter(multi_pcps[0].values())))} "
          f"predicted contact(s) in {len(multi_pcps[0])} plans from "
          f"{len(multi_pcps)} files loaded, nodes: \n    {nodelist}")

    if args.txnodefilter:
        nodefilterlist = "\n    ".join(sorted(args.txnodefilter))
        print(f"Filtering for TX nodes: \n    {nodefilterlist}")
    if args.rxnodefilter:
        nodefilterlist = "\n    ".join(sorted(args.rxnodefilter))
        print(f"Filtering for RX nodes: \n    {nodefilterlist}")
    if args.prednodefilter:
        nodefilterlist = "\n    ".join(sorted(args.prednodefilter))
        print(f"Filtering for predicting nodes: \n    {nodefilterlist}")

    scenario_start = min(fcp, key=attrgetter("start_time")).start_time
    scenario_end = max(fcp, key=attrgetter("end_time")).end_time

    print("Looking for missed contacts...")
    time_resolution_missed = (
        args.time_resolution_missed
        if args.time_resolution_missed
        else args.time_resolution
    )
    times_missed = np.arange(
        scenario_start,
        scenario_end,
        time_resolution_missed,
    )
    # WARNING: This requires that all PTVGs predict the same contact intervals!
    ptvg = next(iter(multi_ptvgs[0].values()))
    missed_c = {}
    observed_c = {}
    for edge, ec in tqdm.tqdm(ptvg.edges.items()):
        if args.txnodefilter and edge[0] not in args.txnodefilter:
            continue
        if args.rxnodefilter and edge[1] not in args.rxnodefilter:
            continue
        checked_node = edge[0] if args.group_by_tx else edge[1]
        if checked_node not in missed_c:
            missed_c[checked_node] = [0 for _ in times_missed]
            observed_c[checked_node] = [0 for _ in times_missed]
        for c in ec:
            if get_contact(ftvg.edges[edge], c.start_time, c.end_time):
                observed_c[checked_node][int(
                    (c.start_time - scenario_start) / time_resolution_missed
                )] += 1
            else:
                missed_c[checked_node][int(
                    (c.start_time - scenario_start) / time_resolution_missed
                )] += 1

    print("Checking out probabilities...")
    times = np.arange(scenario_start, scenario_end, args.time_resolution)
    accus = [[[] for _ in times] for _ in multi_pcps]
    meanlines = [0 for _ in multi_pcps]
    items = [
        (algo_i, eid, pc)
        for algo_i, multi_pcp in enumerate(multi_pcps)
        for eid, pcp in multi_pcp.items()
        for pc_i, pc in enumerate(pcp)
    ]
    for algo_i, eid, pc in tqdm.tqdm(items):
        if args.txnodefilter and pc.tx_node not in args.txnodefilter:
            continue
        if args.rxnodefilter and pc.rx_node not in args.rxnodefilter:
            continue
        if not args.prednodefilter and eid != pc.rx_node:
            continue
        if args.prednodefilter and eid not in args.prednodefilter:
            continue
        real_p = (
            args.contact_probability if args.contact_probability
            else _estimate_prob(ftvg, multi_ptvgs[algo_i][eid], pc)
        )
        meanlines[algo_i] = real_p if args.no_subtract else 0
        sub_val = 0 if args.no_subtract else real_p
        for t_i, t in enumerate(times):
            if args.only_last_prediction and (t_i < len(times) - 1 and
                                              t < pc.start_time):
                continue
            if t > pc.end_time and not args.only_last_prediction:
                break
            accus[algo_i][t_i].append(
                pc.get_generation_at(t).probability -
                sub_val
            )
            if args.only_last_prediction:
                break

    print("Plotting...")
    algonames = {i: an for i, an in enumerate(args.algonames)}
    xval = [round((t - scenario_start) / 3600, 2) for t in times]
    fig, (ax1, ax2) = plt.subplots(2, 1, gridspec_kw={"height_ratios": [2, 1]})
    # fig = plt.figure()
    # ax1 = fig.add_subplot(211)
    if not args.no_subtract:
        ax1.axhline(y=0, color="#909090")
    for algo_i, _ in enumerate(multi_pcps):
        if args.percentile:
            yerr = (
                [
                    np.mean(e) - np.percentile(e, PCT_LO)
                    for e in accus[algo_i]
                ],
                [
                    np.percentile(e, PCT_HI) - np.mean(e)
                    for e in accus[algo_i]
                ],
            )
        else:
            yerr = [np.std(e) for e in accus[algo_i]]
        plot_list = ax1.errorbar(
            xval,
            [np.mean(e) for e in accus[algo_i]],
            yerr,
            label=algonames.get(algo_i, "unknown"),
            capsize=2,
            marker=(MARKERS[algo_i % len(MARKERS)] if args.markers else None),
        )
        if args.no_subtract:
            ax1.axhline(
                y=meanlines[algo_i],
                color=plot_list[0].get_color(),
                alpha=0.3,
            )
    ax1.set_xlabel("time from simulation start / hours")
    if args.no_subtract:
        ax1.set_ylabel("predicted confidence")
    else:
        ax1.set_ylabel("predicted confidence - avg. probability")
    ax1.legend(loc="upper right", ncol=2)
    # TODO: Think about showing the "real" probability using a moving
    # average over all contacts as a graph below the predictions.
    # -> This would help seeing why there are variations / outliers.
    # ax2 = ax1.twinx()
    # ax2 = fig.add_subplot(212)
    xval2 = [round((t - scenario_start) / 3600, 2) for t in times_missed]
    mb_width = time_resolution_missed / 3600 / 5
    bottom = [0 for _ in times_missed]
    node_prep = "with" if args.group_by_tx else "by"
    for i, (node, missed) in enumerate(missed_c.items()):
        ax2.bar(
            [x - mb_width / 2 for x in xval2],
            missed,
            label=(f"missed {node_prep} {node} "),
            bottom=bottom,
            width=mb_width,
            color=BARCOLORS_1[i % len(BARCOLORS_1)],
        )
        bottom = [b + missed[i] for i, b in enumerate(bottom)]
    bottom = [0 for _ in times_missed]
    for i, (node, obsv) in enumerate(observed_c.items()):
        ax2.bar(
            [x + mb_width / 2 for x in xval2],
            obsv,
            label=(f"observed {node_prep} {node} "),
            bottom=bottom,
            width=mb_width,
            color=BARCOLORS_2[i % len(BARCOLORS_2)],
        )
        bottom = [b + obsv[i] for i, b in enumerate(bottom)]
    ymin, ymax = ax2.get_ylim()
    ymax += (ymax - ymin) * 0.85  # add some headroom for legend
    ax2.set_ylim(bottom=ymin, top=ymax)
    ax2.set_xlabel("time from simulation start / hours")
    ax2.set_ylabel("#contacts")
    ax2.legend(loc="upper right", ncol=2)
    if args.outfile:
        fig.set_size_inches(6, 3.45)
        plt.tight_layout()
        plt.savefig(args.outfile)
    else:
        plt.show()
    # TODO: Echo measured results (percentiles, MSE) to the console.


def _get_argument_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "FTVGFILE",
        help="the input F-TVG",
    )
    parser.add_argument(
        "PTVGFILE",
        nargs="+",
        help="the input P-TVGs",
    )
    parser.add_argument(
        "-o", "--outfile",
        help="the output filename",
    )
    parser.add_argument(
        "-n", "--algonames",
        nargs="*", default=[],
        help="the algorithm names",
    )
    parser.add_argument(
        "--txnodefilter",
        nargs="*", default=[],
        help="filter for showing only the specified TX nodes",
    )
    parser.add_argument(
        "--rxnodefilter",
        nargs="*", default=[],
        help="filter for showing only the specified RX nodes",
    )
    parser.add_argument(
        "--prednodefilter",
        nargs="*", default=[],
        help="filter for showing only predictions by the specified nodes",
    )
    parser.add_argument(
        "--time_resolution",
        type=float, default=3600,
        help="the time in seconds between two samples (default: 3600 s)",
    )
    parser.add_argument(
        "--time_resolution_missed",
        type=float, default=None,
        help=(
            "the time in seconds between two samples for 'missed contacts' "
            "(default: same as time_resolution)"
        ),
    )
    parser.add_argument(
        "--contact_probability",
        type=float, default=None,
        help="the factual contact probability (default: estimate)",
    )
    parser.add_argument(
        "--only_last_prediction",
        action="store_true",
        help="only show metric for the last prediction of a contact",
    )
    parser.add_argument(
        "--group_by_tx",
        action="store_true",
        help="group bottom graph by tx node (default: rx node)",
    )
    parser.add_argument(
        "--no_subtract",
        action="store_true",
        help="do not subtract by mean",
    )
    parser.add_argument(
        "--percentile",
        action="store_true",
        help="use 95th percentile confidence interval for error bars",
    )
    parser.add_argument(
        "--markers",
        action="store_true",
        help="add markers",
    )
    return parser


if __name__ == "__main__":
    _main(_get_argument_parser().parse_args())
