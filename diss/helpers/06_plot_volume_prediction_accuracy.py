#!/usr/bin/env python3
# encoding: utf-8

import argparse
import json
from operator import attrgetter

# Add colorblind-friendly color cycle
# See https://gist.github.com/thriveth/8560036
import cycler
CB_color_cycle = ["#377eb8", "#ffb01e", "#4daf4a",
                  "#616161", "#a65628", "#984ea3",
                  "#f781bf", "#e41a1c", "#dede00"]

import matplotlib
matplotlib.rcParams["axes.prop_cycle"] = cycler.cycler(color=CB_color_cycle)

matplotlib.rcParams["text.usetex"] = False
matplotlib.rcParams["font.family"] = "Open Sans"

import numpy as np
import matplotlib.pyplot as plt
import joblib
import tqdm

from tvgutil import tvg

from predictutil import get_fc_volume, get_contact

FACTOR = 1 / 8 / 1024 / 1024  # MiB

PCT_LO = 2.5
PCT_HI = 97.5

MARKERS = [
    "D",
    "x",
    "o",
    "s",
    "v",
    "^",
    "<",
    ">",
    "d",
]


def _fcp_cap(contact, block_size):
    return get_fc_volume(contact, block_size) * FACTOR


def _pcp_cap(contact, t):
    gen = contact.get_generation_at(t)
    cap = gen.get_volume(contact)
    return cap * FACTOR


def _get_accus(algo_i, eid, pcp, ftvg, times, txnodefilter, rxnodefilter,
               link_block_size, only_last_prediction, relative, local):
    accus = [[] for _ in times]
    for pc in pcp:
        if txnodefilter and pc.tx_node not in txnodefilter:
            continue
        if rxnodefilter and pc.rx_node not in rxnodefilter:
            continue
        if local and eid != pc.rx_node:
            continue
        fc = get_contact(
            ftvg.edges[(pc.tx_node, pc.rx_node)],
            pc.start_time,
            pc.end_time,
        )
        if not fc:
            # NOTE: We cannot analyze the prediction accuracy for contacts
            # that did not take place. This influences the confidence,
            # not the volume.
            continue
        fc_cap = _fcp_cap(fc, link_block_size)
        for t_i, t in enumerate(times):
            if only_last_prediction and (t_i < len(times) - 1 and
                                         t < pc.start_time):
                continue
            if t > pc.end_time and not only_last_prediction:
                break
            pc_cap = _pcp_cap(pc, t)
            if abs(fc_cap) < 1e-3 and relative:
                print(f"WARNING: excluding failed contact {pc} "
                      f"(estimated {pc_cap})")
                continue
            accus[t_i].append(
                (pc_cap - fc_cap) / fc_cap * 100
                if relative else (pc_cap - fc_cap)
            )
            if only_last_prediction:
                break
    return algo_i, accus


def _main(args):
    print("Loading TVGs...")
    with open(args.FTVGFILE, "r") as f:
        ftvg = tvg.from_serializable(json.load(f))
    fcp = tvg.to_contact_plan(ftvg)
    multi_pcps = []
    for ptvgfile in args.PTVGFILE:
        with open(ptvgfile, "r") as f:
            data = json.load(f)
            if "contact_type" in data:
                multi_ptvg = {"<node>": tvg.from_serializable(data)}
            else:
                multi_ptvg = {
                    eid: tvg.from_serializable(sptvg)
                    for eid, sptvg in data.items()
                }
        multi_pcps.append({
            eid: tvg.to_contact_plan(ptvg)
            for eid, ptvg in multi_ptvg.items()
        })
    nodelist = "\n- ".join(next(iter(multi_ptvg.values())).vertices)
    print(f"{len(fcp)} factual and {len(next(iter(multi_pcps[0].values())))} "
          f"predicted contact(s) in {len(multi_pcps[0])} plans from "
          f"{len(multi_pcps)} files loaded, nodes: \n- {nodelist}")

    if args.txnodefilter:
        nodefilterlist = "\n- ".join(args.txnodefilter)
        print(f"Filtering for TX nodes: \n- {nodefilterlist}")
    if args.rxnodefilter:
        nodefilterlist = "\n- ".join(args.rxnodefilter)
        print(f"Filtering for RX nodes: \n- {nodefilterlist}")
    if args.prednodefilter:
        nodefilterlist = "\n- ".join(sorted(args.prednodefilter))
        print(f"Filtering for predicting nodes: \n- {nodefilterlist}")

    scenario_start = min(fcp, key=attrgetter("start_time")).start_time
    scenario_end = max(fcp, key=attrgetter("end_time")).end_time

    times = np.arange(scenario_start, scenario_end, args.time_resolution)
    accus = [[[] for _ in times] for _ in multi_pcps]

    algonames = {i: an for i, an in enumerate(args.algonames)}

    fig, ax = plt.subplots(1, 1)

    print("Checking out volumes...")
    items = [
        (algo_i, eid, pcp)
        for algo_i, multi_pcp in enumerate(multi_pcps)
        for eid, pcp in multi_pcp.items()
    ]
    accu_results = joblib.Parallel(n_jobs=args.workers)(
        joblib.delayed(_get_accus)(
            algo_i=algo_i,
            eid=eid,
            pcp=pcp,
            ftvg=ftvg,
            times=times,
            txnodefilter=args.txnodefilter,
            rxnodefilter=args.rxnodefilter,
            link_block_size=args.link_block_size,
            only_last_prediction=args.only_last_prediction,
            relative=args.relative,
            local=args.local,
        )
        for algo_i, eid, pcp in tqdm.tqdm(items)
        if not args.prednodefilter or eid in args.prednodefilter
    )
    for algo_i, accu_list in accu_results:
        # NOTE: Elements of the list accus[algo_i] are lists -> add them!
        accus[algo_i] = [a + b for a, b in zip(accus[algo_i], accu_list)]

    print("Plotting...")
    ax.axhline(y=0, color="#909090")
    for algo_i, _ in enumerate(multi_pcps):
        if args.percentile:
            yerr = (
                [
                    np.mean(e) - np.percentile(e, PCT_LO)
                    for e in accus[algo_i]
                ],
                [
                    np.percentile(e, PCT_HI) - np.mean(e)
                    for e in accus[algo_i]
                ],
            )
        else:
            yerr = [np.std(e) for e in accus[algo_i]]
        ax.errorbar(
            [round((t - scenario_start) / 3600, 2) for t in times],
            [np.mean(e) for e in accus[algo_i]],
            yerr,
            label=algonames.get(algo_i, "unknown"),
            capsize=2,
            marker=(MARKERS[algo_i % len(MARKERS)] if args.markers else None),
        )
    ax.set_xlabel("time from simulation start / hours")
    if args.relative:
        ax.set_ylabel("rel. volume diff. (predicted - measured) / %")
    else:
        ax.set_ylabel("volume diff. (predicted - measured) / MiB")
    if args.legend_headroom:
        ymin, ymax = ax.get_ylim()
        ymax += (ymax - ymin) * 0.3  # add some headroom for legend
        ax.set_ylim(bottom=ymin, top=ymax)
    ax.legend(
        loc=args.legend_location,
        ncol=args.legend_columns,
    )
    # TODO: Evaluate showing the average data rate as a graph below.
    # This would allow to see outlying contacts better.

    if args.outfile:
        fig.set_size_inches(6, 3.45)
        plt.tight_layout()
        plt.savefig(args.outfile)
    else:
        plt.show()
    # TODO: Echo measured results (percentiles, MSE) to the console.


def _get_argument_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "FTVGFILE",
        help="the input F-TVG",
    )
    parser.add_argument(
        "PTVGFILE",
        nargs="+",
        help="the input P-TVGs",
    )
    parser.add_argument(
        "-o", "--outfile",
        help="the output filename",
    )
    parser.add_argument(
        "-n", "--algonames",
        nargs="*", default=[],
        help="the algorithm names",
    )
    parser.add_argument(
        "--txnodefilter",
        nargs="*", default=[],
        help="filter for showing only the specified TX nodes",
    )
    parser.add_argument(
        "--rxnodefilter",
        nargs="*", default=[],
        help="filter for showing only the specified RX nodes",
    )
    parser.add_argument(
        "--prednodefilter",
        nargs="*", default=[],
        help="filter for showing only predictions by the specified nodes",
    )
    parser.add_argument(
        "--link_block_size",
        type=int, default=(1500 * 8),
        help="the block size for link-layer transmission (default: 1.5 kB)",
    )
    parser.add_argument(
        "--time_resolution",
        type=float, default=600,
        help="the time in seconds between two samples (default: 600 s)",
    )
    parser.add_argument(
        "--all_nodes",
        action="store_true",
        help="calculate stats for all nodes (default: just local neighbors)",
    )
    parser.add_argument(
        "--only_last_prediction",
        action="store_true",
        help="only use value just before contact starts",
    )
    parser.add_argument(
        "--relative",
        action="store_true",
        help="calculate relative difference (ref: factual)",
    )
    parser.add_argument(
        "--local",
        action="store_true",
        help="calculate only stats for local node as receiver",
    )
    parser.add_argument(
        "--percentile",
        action="store_true",
        help="use 95th percentile confidence interval for error bars",
    )
    parser.add_argument(
        "--markers",
        action="store_true",
        help="add markers",
    )
    parser.add_argument(
        "--legend-columns",
        type=int,
        default=1,
        help="the amount of columns used for the legend (default: 1)",
    )
    parser.add_argument(
        "--legend-location",
        default="upper right",
        help="the legend location passed to matplotlib (default: upper left)",
    )
    parser.add_argument(
        "--legend-headroom",
        action="store_true",
        help="add some headroom for the legend",
    )
    parser.add_argument(
        "-w", "--workers",
        type=int, default=4,
        help="the amount of workers used for parallel processing",
    )
    return parser


if __name__ == "__main__":
    _main(_get_argument_parser().parse_args())
