#!/usr/bin/env python
# encoding: utf-8

"""
Tool to convert an F-TVG representation to a P-TVG.
"""

import argparse
import json
import sys

from tvgutil import contact_plan, tvg

from predictutil import get_fc_volume


def _fc_to_pc(factual_contact, link_block_size):
    volume = get_fc_volume(factual_contact, link_block_size)
    duration = factual_contact.end_time - factual_contact.start_time
    average_bit_rate = volume / duration
    return contact_plan.PredictedContact(
        tx_node=factual_contact.tx_node,
        rx_node=factual_contact.rx_node,
        start_time=factual_contact.start_time,
        end_time=factual_contact.end_time,
        generations=[
            contact_plan.PredictedContactGeneration(
                valid_from=0.0,
                probability=1.0,
                characteristics=[
                    contact_plan.PredictedContactCharacteristics(
                        starting_at=factual_contact.start_time,
                        bit_rate=average_bit_rate,
                        delay=char.delay,
                    )
                    for char in factual_contact.characteristics
                ],
            ),
        ],
    )


def _main(args):
    ftvg = tvg.from_serializable(json.load(args.FILE))
    assert ftvg.contact_type == contact_plan.Contact
    for node_tuple in ftvg.edges:
        # Rewrite edges by PCs
        ftvg.edges[node_tuple] = [
            _fc_to_pc(
                fc,
                args.link_block_size,
            )
            for fc in ftvg.edges[node_tuple]
        ]
    ftvg.contact_type = contact_plan.PredictedContact
    json.dump(
        tvg.to_serializable(ftvg),
        args.output,
        indent=(4 if args.indent else None),
        sort_keys=args.indent,
    )
    args.output.close()
    print("P-TVG with {} vertices and {} edges written.".format(
        len(ftvg.vertices),
        len(ftvg.edges),
    ), file=sys.stderr)


def _get_argument_parser():
    parser = argparse.ArgumentParser(
        description="F-TVG to P-TVG converter")
    parser.add_argument("FILE", type=argparse.FileType("r"),
                        default=sys.stdin,
                        help="the F-TVG input file")
    parser.add_argument("-o", "--output", type=argparse.FileType("w"),
                        default=sys.stdout,
                        help="the output file for the created P-TVG")
    parser.add_argument("-s", "--link_block_size", type=float, default=12000,
                        help="the assumed link-layer block size, in bits")
    parser.add_argument("-i", "--indent", action="store_true",
                        help="specify this if you want pretty JSON")
    return parser


if __name__ == "__main__":
    _main(_get_argument_parser().parse_args())
