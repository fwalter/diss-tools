#!/usr/bin/env python3
# encoding: utf-8

import argparse
import json

import numpy as np
import matplotlib.pyplot as plt
import networkx as nx

from tvgutil import tvg


def _main(args):
    print("Loading TVG...")
    with open(args.FTVGFILE, "r") as f:
        ftvg = tvg.from_serializable(json.load(f))

    print("Loading statistics...")
    algo_stats = {}
    nodes = set()
    for i, statfile in enumerate(args.STATFILE):
        with open(statfile, "r") as f:
            data = json.load(f)
            algo_name = (
                args.algonames[i] if i < len(args.algonames)
                else f"<algo #{i}>"
            )
            assert algo_name not in algo_stats
            algo_stats[algo_name] = data["buffer_utilization"]
            nodes |= {n for n, _, _ in data["buffer_utilization"]}

    algolist = "\n    ".join(algo_stats.keys())
    nodelist = "\n    ".join(nodes)
    print(
        f"Loaded stats for {len(algo_stats)} algo(s) "
        f"and {len(nodes)} node(s)\n"
        f"-> Algos:\n    {algolist}\n-> Nodes:\n    {nodelist}"
    )

    buf_util_by_node = {
        algo_name: {
            node: np.mean([
                bu
                for bu_node, _, bu in algo_stats[algo_name]
                if bu_node == node
            ])
            for node in nodes
        }
        for algo_name in algo_stats
    }

    graph = nx.DiGraph()
    for node in sorted(nodes):
        graph.add_node(node)
    for tx_node, rx_node in sorted(ftvg.edges):
        graph.add_edge(tx_node, rx_node)

    nodelist = graph.nodes()
    for algo_name in algo_stats:
        fig = plt.figure()
        PLT_BASE_SZ = 3500
        mean_util = np.mean(list(buf_util_by_node[algo_name].values()))
        max_util = max(list(buf_util_by_node[algo_name].values()))
        nodesizes = [
            buf_util_by_node[algo_name][eid] / mean_util * PLT_BASE_SZ
            for eid in nodelist
        ]
        nodecolors = [
            0.8 - (buf_util_by_node[algo_name][eid] / max_util * 0.8)
            for eid in nodelist
        ]
        pos = nx.bipartite_layout(
            graph,
            nodes=list(set(args.partition1) & nodes),
            align="horizontal",
        )
        nx.draw(
            graph,
            node_size=nodesizes,
            pos=pos,
            label=algo_name,
            node_color=nodecolors,
            cmap="gray",
            width=3.,
            arrowsize=30.,
            vmin=0,
            vmax=1,
        )
        for node in nodelist:
            PLT_OFF = 0.15
            x, y = pos[node]
            offset = PLT_OFF if node in args.partition1 else -PLT_OFF
            plt.text(
                x=x,
                y=(y + offset),
                s=node,
                horizontalalignment="center",
                verticalalignment="center",
                fontsize=18,
            )

        fig.set_size_inches(8, 6.6)
        plt.margins(0.1)
        if args.outfile_prefix:
            plt.savefig(args.outfile_prefix + f"_{algo_name}.pdf")
        else:
            plt.show()


def _get_argument_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "FTVGFILE",
        help="the F-TVG file",
    )
    parser.add_argument(
        "STATFILE",
        nargs="+",
        help="the (joined) input statistics files",
    )
    parser.add_argument(
        "-o", "--outfile-prefix",
        help="the output filename prefix",
    )
    parser.add_argument(
        "-n", "--algonames",
        nargs="*", default=[],
        help="the algorithm names",
    )
    parser.add_argument(
        "--partition1",
        nargs="*", default=[
            "NOAA 15", "NOAA 18", "NOAA 19",
            "LEO-01", "LEO-02", "LEO-03", "LEO-04", "LEO-05", "LEO-06",
            "LEO-11", "LEO-12", "LEO-13", "LEO-14", "LEO-15", "LEO-16",
            "LEO-21", "LEO-22", "LEO-23", "LEO-24", "LEO-25", "LEO-26",
            "LEO-31", "LEO-32", "LEO-33", "LEO-34", "LEO-35", "LEO-36",
            "LEO-41", "LEO-42", "LEO-43", "LEO-44", "LEO-45", "LEO-46",
            "LEO-51", "LEO-52", "LEO-53", "LEO-54", "LEO-55", "LEO-56",
            "LEO-61", "LEO-62", "LEO-63", "LEO-64", "LEO-65", "LEO-66",
        ],
        help="nodes in one graph partition, default: NOAA xx and LEO-01 to 66",
    )
    return parser


if __name__ == "__main__":
    _main(_get_argument_parser().parse_args())
