#!/bin/bash

set -euxo pipefail

if [ $# -lt 3 ]; then
    echo 'Usage: bash draw_plots.sh <result-dir> <plot-dir> <scenario_name_1> ... <scenario_name_n>' >&2
    exit 1
fi

PLOT_DIR="$2"
SCENARIOS=("${@:3}")

LEGEND_COL=2

RESULTS_DIR="$1"

xlabel="scenario"

algos="$(ls $RESULTS_DIR/${SCENARIOS[0]}.07.stats.* | cut -d '.' -f 4 | sort | uniq)"

rm -rvf --one-file-system --preserve-root "$PLOT_DIR"
mkdir -pv "$PLOT_DIR"
out="--outdir $PLOT_DIR"

echo "Plotting scenarios: ${SCENARIOS[@]}"
echo "for algos: $algos"

filelist=()
for scenario in "${SCENARIOS[@]}"; do
    TEST_PREFIX="$RESULTS_DIR/$scenario"
    # NOTE that the order is important for passing this via glob
    i=1
    for algo in $algos; do
        param_prefix="${TEST_PREFIX}.07.stats.${algo}"
        echo
        for file in ${param_prefix}*.json; do
            if [[ "$(stat --printf="%s" "$file")" == "0" ]]; then
                echo "Removing empty file: $file"
                rm $file
            fi
        done
        echo "=> Statistics for algo $algo:"
        python3 07_join_simulation_results.py ${param_prefix}.*.json \
            -o ${TEST_PREFIX}.08.plotstats.$(printf "%02d\n" $i).json
        i=$[i+1]
    done
    filelist+=(${TEST_PREFIX}.08.plotstats.*.json)
done

SUMMARY_FILE="$PLOT_DIR/stats_summary.txt"
echo "Scenarios: ${SCENARIOS[@]}" > "$SUMMARY_FILE"
echo "Algos: $(echo "$algos" | tr '\n' ' ')" >> "$SUMMARY_FILE"
echo -e "Date: $(date)\n" >> "$SUMMARY_FILE"

echo "> Plotting files:"
echo ${filelist[@]}

python 08_plot_results.py $out \
    --title "Bundle Delivery Delay" \
    --xlabel "$xlabel" --xticks ${SCENARIOS[@]} \
    --algos $algos \
    --ylabel "average delay / hours" \
    --percentile \
    --legend-columns $LEGEND_COL \
    --factor 0.000277777778 \
    "delays" \
    ${filelist[@]} | tee -a "$SUMMARY_FILE"

echo >> "$SUMMARY_FILE"

python 08_plot_results.py $out \
    --title "Bundle Delivery Delay" \
    --xlabel "$xlabel" --xticks ${SCENARIOS[@]} \
    --algos $algos \
    --ylabel "average delay / hours" \
    --percentile \
    --legend-columns $LEGEND_COL \
    --plot-name "delays_log" \
    --log \
    --ymin 0.1 --ymax 10000  \
    --factor 0.000277777778 \
    "delays" \
    ${filelist[@]} | tee -a "$SUMMARY_FILE"

echo >> "$SUMMARY_FILE"

python 08_plot_results.py $out \
    --title "Bundle Delivery Rate" \
    --xlabel "$xlabel" --xticks ${SCENARIOS[@]} \
    --factor 100 --algos $algos \
    --ylabel "delivery probability / %" \
    --percentile \
    --legend-columns $LEGEND_COL \
    "delivery_rate" \
    ${filelist[@]} | tee -a "$SUMMARY_FILE"

echo >> "$SUMMARY_FILE"

python 08_plot_results.py $out \
    --title "Bundle Transmission Count (delivered)" \
    --xlabel "$xlabel" --xticks ${SCENARIOS[@]} \
    --algos $algos \
    --ylabel "average transmissions per bundle" \
    --percentile \
    --legend-columns $LEGEND_COL \
    --log \
    --ymin 0.5 --ymax 1000  \
    "tx_counts_delivered" \
    ${filelist[@]} | tee -a "$SUMMARY_FILE"

echo >> "$SUMMARY_FILE"

python 08_plot_results.py $out \
    --title "Bundle Transmission Count" \
    --xlabel "$xlabel" --xticks ${SCENARIOS[@]} \
    --algos $algos \
    --ylabel "average transmissions per bundle" \
    --percentile \
    --legend-columns $LEGEND_COL \
    --log \
    --ymin 0.5 --ymax 1000  \
    "tx_counts_all" \
    ${filelist[@]} | tee -a "$SUMMARY_FILE"

echo >> "$SUMMARY_FILE"

python 08_plot_results.py $out \
    --title "Bundle Replica Count" \
    --xlabel "$xlabel" --xticks ${SCENARIOS[@]} \
    --algos $algos \
    --ylabel "average replicas per bundle" \
    --percentile \
    --legend-columns $LEGEND_COL \
    --log \
    --ymin 0.5 --ymax 200  \
    "replica_counts" \
    ${filelist[@]} | tee -a "$SUMMARY_FILE"

echo >> "$SUMMARY_FILE"

python 08_plot_results.py $out \
    --title "Bundle Scheduling Count (CGR)" \
    --xlabel "$xlabel" --xticks ${SCENARIOS[@]} \
    --algos $algos \
    --ylabel "avg. sched. events per bundle per node" \
    --percentile \
    --legend-columns $LEGEND_COL \
    --exclude-algos "epidemic" "one_epidemic" "snw" "one_snw" "Epidemic" "SprayAndWait" -- \
    "scheduling_counts" \
    ${filelist[@]} | tee -a "$SUMMARY_FILE"

echo >> "$SUMMARY_FILE"

python 08_plot_results.py $out \
    --title "Initial Scheduling Success Rate (CGR)" \
    --xlabel "$xlabel" --xticks ${SCENARIOS[@]} \
    --factor 100 --algos $algos \
    --ylabel "bundles init. sched. successfully / %" \
    --percentile \
    --legend-columns $LEGEND_COL \
    --exclude-algos "epidemic" "one_epidemic" "snw" "one_snw" "Epidemic" "SprayAndWait" -- \
    "initial_scheduling_success_rate" \
    ${filelist[@]} | tee -a "$SUMMARY_FILE"

echo >> "$SUMMARY_FILE"

python 08_plot_results.py $out \
    --title "Bundles Re-scheduled (CGR)" \
    --xlabel "$xlabel" --xticks ${SCENARIOS[@]} \
    --algos $algos \
    --ylabel "bundles re-scheduled" \
    --percentile \
    --legend-columns $LEGEND_COL \
    --exclude-algos "epidemic" "one_epidemic" "snw" "one_snw" "Epidemic" "SprayAndWait" -- \
    "rescheduling_count" \
    ${filelist[@]} | tee -a "$SUMMARY_FILE"

echo >> "$SUMMARY_FILE"

python 08_plot_results.py $out \
    --title "Bundles Re-scheduled (CGR)" \
    --xlabel "$xlabel" --xticks ${SCENARIOS[@]} \
    --algos $algos \
    --ylabel "bundles re-scheduled" \
    --percentile \
    --plot-name "rescheduling_count_log" \
    --log \
    --ymin 100 --ymax 1000000  \
    --legend-columns $LEGEND_COL \
    --exclude-algos "epidemic" "one_epidemic" "snw" "one_snw" "Epidemic" "SprayAndWait" -- \
    "rescheduling_count" \
    ${filelist[@]} | tee -a "$SUMMARY_FILE"

echo >> "$SUMMARY_FILE"

python 08_plot_results.py $out \
    --title "Contact Utilization" \
    --xlabel "$xlabel" --xticks ${SCENARIOS[@]} \
    --factor 100 --algos $algos \
    --ylabel "average contact utilization / %" \
    --percentile \
    --legend-columns $LEGEND_COL \
    "contact_utilization_total" \
    ${filelist[@]} | tee -a "$SUMMARY_FILE"

echo >> "$SUMMARY_FILE"

python 08_plot_results.py $out \
    --title "Buffer Utilization" \
    --xlabel "$xlabel" --xticks ${SCENARIOS[@]} \
    --factor 0.000000119209289551 --algos $algos \
    --ylabel "average buffer utilization / MiB" \
    --percentile \
    --log \
    --ymin 0.1 --ymax 1000000  \
    --legend-columns $LEGEND_COL \
    "buffer_utilization_total" \
    ${filelist[@]} | tee -a "$SUMMARY_FILE"

echo >> "$SUMMARY_FILE"

python 08_plot_results.py $out \
    --title "Energy Efficiency" \
    --xlabel "$xlabel" --xticks ${SCENARIOS[@]} \
    --factor 100 --algos $algos \
    --ylabel "average energy efficiency (%)" \
    --percentile \
    --legend-columns $LEGEND_COL \
    "energy_efficiency" \
    ${filelist[@]} | tee -a "$SUMMARY_FILE"

echo >> "$SUMMARY_FILE"

python 08_plot_results.py $out \
    --title "Total Amount of Transmissions" \
    --xlabel "$xlabel" --xticks ${SCENARIOS[@]} \
    --algos $algos \
    --ylabel "bundle transmissions" \
    --percentile \
    --legend-columns $LEGEND_COL \
    "tx_count_all_total" \
    ${filelist[@]} | tee -a "$SUMMARY_FILE"

echo >> "$SUMMARY_FILE"

python 08_plot_results.py $out \
    --title "Total Amount of Transmissions (delivered)" \
    --xlabel "$xlabel" --xticks ${SCENARIOS[@]} \
    --algos $algos \
    --ylabel "bundle transmissions" \
    --percentile \
    --legend-columns $LEGEND_COL \
    "tx_count_delivered_total" \
    ${filelist[@]} | tee -a "$SUMMARY_FILE"

echo >> "$SUMMARY_FILE"

python 08_plot_results.py $out \
    --title "Simulation Runtime" \
    --xlabel "$xlabel" --xticks ${SCENARIOS[@]} \
    --algos $algos \
    --ylabel "average simulation runtime / s" \
    --percentile \
    --legend-columns $LEGEND_COL \
    "runtimes" \
    ${filelist[@]} | tee -a "$SUMMARY_FILE"

echo >> "$SUMMARY_FILE"

python 08_plot_results.py $out \
    --title "Simulation Runtime" \
    --xlabel "$xlabel" --xticks ${SCENARIOS[@]} \
    --algos $algos \
    --ylabel "average simulation runtime / s" \
    --percentile \
    --log \
    --ymin 0.1 --ymax 70000000  \
    --plot-name "runtimes_log" \
    --legend-columns $LEGEND_COL \
    "runtimes" \
    ${filelist[@]} | tee -a "$SUMMARY_FILE"

for scenario in "${SCENARIOS[@]}"; do
    TEST_PREFIX="$RESULTS_DIR/$scenario"
    python helpers/08_plot_buffer_utilization_over_time.py -o $PLOT_DIR/buffer_utilization_over_time_$scenario.pdf --time-resolution 43200 --legend-columns $LEGEND_COL --legend-location "lower left" --markers --no-err --log -n $algos -- ${TEST_PREFIX}.08.plotstats.*.json
    python helpers/08_plot_contact_utilization_over_time.py -o $PLOT_DIR/contact_utilization_over_time_$scenario.pdf --time-resolution 43200 --legend-columns $LEGEND_COL --legend-location "lower left" --markers --no-err -n $algos -- $RESULTS_DIR/$scenario.02.ftvg.json ${TEST_PREFIX}.08.plotstats.*.json
    python helpers/08_plot_buffer_utilization_over_time.py -o $PLOT_DIR/buffer_utilization_over_time_pct_err_$scenario.pdf --time-resolution 43200 --legend-columns $LEGEND_COL --legend-location "upper left" -n $algos --percentile -- ${TEST_PREFIX}.08.plotstats.*.json
    python helpers/08_plot_contact_utilization_over_time.py -o $PLOT_DIR/contact_utilization_over_time_pct_err_$scenario.pdf --time-resolution 43200 --legend-columns $LEGEND_COL --legend-location "upper left" -n $algos --percentile -- $RESULTS_DIR/$scenario.02.ftvg.json ${TEST_PREFIX}.08.plotstats.*.json
    # python helpers/08_plot_graph_utilization_over_nodes.py -o $PLOT_DIR/graph_utilization_nodes_rel_$scenario -n $algos -- $RESULTS_DIR/$scenario.02.ftvg.json ${TEST_PREFIX}.08.plotstats.*.json
    # python helpers/08_plot_graph_utilization_over_nodes.py -o $PLOT_DIR/graph_utilization_nodes_abs_$scenario --absolute -n $algos -- $RESULTS_DIR/$scenario.02.ftvg.json ${TEST_PREFIX}.08.plotstats.*.json
done
