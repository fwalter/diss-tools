# Time-cropping because one selected GS has had problems at the start
python helpers/03_crop_tvg.py scenarios/base/base.02.ftvg.json -o base.02.time-cropped.ftvg.json -s 100 -d 500
Found 1282 contacts of 9 nodes (36 edges) in [1578356361.0, 1580254735.0] (21.971921296296298 day(s)).
Reducing TVG to time interval [1578716361.0, 1580254735.0] (17.80525462962963 day(s)) or inf contacts per edge...
Reduced graph to 1030 contacts (9 nodes, 36 edges).
Checking for edges with too few contacts...
Cleaned graph has 1030 contacts (9 nodes, 36 edges).
Checking for isolated nodes...
Rebuilding vertex-associations...
Final graph has 1030 contacts (9 nodes, 36 edges).

$ python helpers/03_tvginfo.py scenarios/walker931-10g/base.02.time-cropped.ftvg.json
Found 1030 contacts of 9 nodes (36 edges) in [1578719231.5, 1580254735.0] (17.77203125 day(s)).
Node list with contact counts:
> DL6KBG (VHF): 82
> F6KKR: 376
> KB9JHU: 98
> MW6CYK: 110
> N5CNB-VHF: 310
> NOAA 15: 356
> NOAA 18: 268
> NOAA 19: 406
> Um Alaish 4: 54
