cp 9045-6s-15g.02.ftvg.json S2.02.ftvg.json
cp 9045-6s-15g.02.ptvg.json S2.02.ptvg.json
cp 9045-6s-15g.02.scenario.json S2.02.scenario.json
cp walker931-10g.02.ftvg.json S1.02.ftvg.json
cp walker931-10g.02.ptvg.json S1.02.ptvg.json
cp walker931-10g.02.scenario.json S1.02.scenario.json
cp walker931-10g.02.ptvg.json S1-p3b.02.ptvg.json
cp walker931-10g.02.ptvg.json S1-p5i.02.ptvg.json
cp walker931-10g.02.scenario.json S1-p3b.02.scenario.json
cp walker931-10g.02.scenario.json S1-p5i.02.scenario.json
cp walker931-10g-3b.02.ftvg.json S1-p3b.02.ftvg.json
cp walker931-10g-5i.02.ftvg.json S1-p5i.02.ftvg.json

for f in S*02.ftvg.json; do echo $f; python helpers/03_get_contact_volume.py $f; done
