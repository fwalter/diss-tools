#!/bin/bash

# This script performs an integration test of the whole pipeline.

TEST="T00_test"

WORKING_DIR="results_$TEST"

SCENARIO_DIR="scenarios/base"

SCENARIOS=(
    "base+test"
)

ALGOS=(
    one_epidemic
    scgr-tristate
    ngsabr-tristate
)

RUNS=2

WORKERS=4
SETUP_WORKERS=4

OBSV_ALGOS=(
    scgr-tristate
    ngsabr-tristate
)
OBSV_FLAGS=(
    "--only_factual --confidence_predictor disabled --volume_predictor tristate --link_block_size 12000 --averaging_window_size 10 --az_granularity 45"
    "--only_factual --confidence_predictor disabled --volume_predictor tristate --link_block_size 12000 --averaging_window_size 10 --az_granularity 45"
)

CROP_FLAGS="--duration 24"
# Flags for the transmission plan - small plan
TPLAN_FLAGS="--maxcount 25 --interval 600 --lifetime 86400 --minsize 2000000 --invalidsrc 'NOAA 15' 'NOAA 18' 'NOAA 19' --invaliddst 'NOAA 15' 'NOAA 18' 'NOAA 19'"
# Flags for the simulator - 100M buffer
SIM_FLAGS="--buffersize 100000000 --graph_update_min_interval 14400 --link_block_size 12000"

COMMAND="$1"

set -euxo pipefail

if [ -z "$COMMAND" ]; then
    COMMAND="run"
fi

for scenario in "${SCENARIOS[@]}"; do
    base_scenario="$(echo "$scenario" | cut -d '+' -f 1)"
    if [[ ! -r "$SCENARIO_DIR/$base_scenario.02.scenario.json" || ! -r "$SCENARIO_DIR/$base_scenario.02.ftvg.json" ]]; then
        echo "$SCENARIO_DIR/$base_scenario.02.scenario.json and $SCENARIO_DIR/$base_scenario.02.ftvg.json have to exist!" >&2
        exit 1
    fi
done

mkdir -p "$WORKING_DIR"

setup() {
    # Remove all empty result files to have a clean setup
    find "$WORKING_DIR" -type f -size 0 -delete

    for scenario in "${SCENARIOS[@]}"; do
        local prefix="$WORKING_DIR/$scenario"
        local base_scenario="$(echo "$scenario" | cut -d '+' -f 1)"
        local base_prefix="$WORKING_DIR/$base_scenario"

        for ((i = 0; i < ${#OBSV_ALGOS[@]}; i++)); do
            local obsv_algo="${OBSV_ALGOS[$i]}"
            local obsv_flags="${OBSV_FLAGS[$i]}"

            [ -r "$base_prefix.02.ftvg.cropped.json" ] || \
                python helpers/03_crop_tvg.py \
                    $CROP_FLAGS \
                    --output "$base_prefix.02.ftvg.cropped.json" \
                    "$SCENARIO_DIR/$base_scenario.02.ftvg.json"

            [ -r "$base_prefix.03.$obsv_algo.kb.observed.json" ] || \
                python 03_observe_contacts.py -v \
                    $obsv_flags \
                    --output "$base_prefix.03.$obsv_algo.kb.observed.json" \
                    "$SCENARIO_DIR/$base_scenario.02.scenario.json" \
                    "$base_prefix.02.ftvg.cropped.json"
            [ -r "$base_prefix.03.$obsv_algo.kb.dist.json" ] || \
                python 04_a_distribute_metrics.py \
                    --output "$base_prefix.03.$obsv_algo.kb.dist.json" \
                    --progress \
                    "$base_prefix.02.ftvg.cropped.json" \
                    "$base_prefix.03.$obsv_algo.kb.observed.json"
            [ -r "$base_prefix.05.$obsv_algo.ptvg.json" ] || \
                python 05_predict_ptvg.py -v \
                    --output "$base_prefix.05.$obsv_algo.ptvg.json" \
                    --workers "$SETUP_WORKERS" \
                    "$base_prefix.03.$obsv_algo.kb.dist.json"

            cp "$base_prefix.02.ftvg.cropped.json" \
                "$prefix.02.ftvg.json"
            cp "$SCENARIO_DIR/$base_scenario.02.scenario.json" \
                "$prefix.02.scenario.json"
            cp "$base_prefix.05.$obsv_algo.ptvg.json" \
                "$prefix.05.$obsv_algo.ptvg.json"
        done

        local load_config="$(echo "$scenario" | cut -d '+' -f 2)"

        case "$load_config" in
            test)
                local tplan_flags="$TPLAN_FLAGS"
                ;;
        esac

        for run in $(seq $RUNS); do
            [ -r "$prefix.06.transmission_plan_$run.json" ] || \
                python -m tvgutil.tools.create_transmission_plan \
                    $tplan_flags \
                    --output "$prefix.06.transmission_plan_$run.json" \
                    "$base_prefix.02.ftvg.cropped.json"
        done
    done
}

job() {
    local scenario="$1"
    local base_scenario="$(echo "$scenario" | cut -d '+' -f 1)"
    local algo="$2"
    local run="$3"
    local prefix="$WORKING_DIR/$scenario"
    local base_prefix="$WORKING_DIR/$base_scenario"
    # example: "cgr-factual" -> "cgr"; "snw" -> "snw"
    local sim_algo="$(echo "$algo" | cut -d "-" -f 1)"
    local ptvg="$prefix.05.$algo.ptvg.json"

    [ -r "$prefix.06.transmission_plan_$run.json" ] || exit 1

    [ -r "$prefix.07.stats.$algo.$run.json" ] && return 0

    if [ -r "$ptvg" ]; then
        python 06_simulation.py \
            $SIM_FLAGS \
            --algo "$sim_algo" \
            --ptvg "$ptvg" \
            --tplan "$prefix.06.transmission_plan_$run.json" \
            --savestats "$prefix.07.stats.$algo.$run.json" \
            "$base_prefix.02.ftvg.cropped.json"
    else
        python 06_simulation.py \
            $SIM_FLAGS \
            --algo "$sim_algo" \
            --tplan "$prefix.06.transmission_plan_$run.json" \
            --savestats "$prefix.07.stats.$algo.$run.json" \
            "$base_prefix.02.ftvg.cropped.json"
    fi
}

teardown() {
    exec bash draw_plots.sh \
        "$WORKING_DIR" \
        "$WORKING_DIR/plots_$TEST" \
        "${SCENARIOS[@]}"
}

case "$COMMAND" in
    run)
        setup
        python test_process_pool_executor.py \
            --workers $WORKERS \
            --scenarios "${SCENARIOS[@]}" \
            --algorithms "${ALGOS[@]}" \
            --runs $(seq $RUNS) \
            --progress \
            --logfile "$WORKING_DIR/test_executor.log" \
            "$0"
        teardown
        ;;
    clean)
        rm -rvf --one-file-system --preserve-root "$WORKING_DIR"
        ;;
    cleanrouting)
        rm -vf --one-file-system --preserve-root $WORKING_DIR/*07.stats*json
        ;;
    job)
        job "${@:2}"
        ;;
    *)
        echo "Usage: $0 [<run|clean|job>] [<job-args>]" >&2
        exit 1
        ;;
esac
