#!/usr/bin/env python3
# encoding: utf-8

import sys
import json
import copy
import math
import argparse
import logging
import random

import joblib
import tqdm

from tvgutil import tvg

from predictutil import (
    get_trajectory_predictors_for_scenario,
    add_characteristics_to_pc,
)
from predictutil.confidence import CONFIDENCE_PREDICTORS
from predictutil.volume import VOLUME_PREDICTORS

logger = logging.getLogger(__name__)


def _process_job(conf_predictor, vol_predictor, valid_from, pc_data):
    result = []
    for start_time, end_time in pc_data:
        conf = conf_predictor.predict_confidence(
            valid_from,
            start_time,
            end_time,
        )
        brf = vol_predictor.predict_data_rate_factors(
            valid_from,
            start_time,
            end_time,
        )
        result.append((conf, brf))
    return result


def _main(args):
    _initialize_logger(args.verbose)
    print("Reading input file...", file=sys.stderr)
    kb = json.load(args.KBFILE)
    conf_predictor_factory, conf_predictor_arglist = CONFIDENCE_PREDICTORS[
        kb["confidence_predictor"]
    ]
    conf_predictor_args = kb["confidence_predictor_args"]
    vol_predictor_factory, vol_predictor_arglist = VOLUME_PREDICTORS[
        kb["volume_predictor"]
    ]
    vol_predictor_args = kb["volume_predictor_args"]
    metrics = kb["metrics"]

    print("Initializing trajectory predictors...", file=sys.stderr)
    trajectory_predictors = get_trajectory_predictors_for_scenario(
        kb["scenario"],
    )

    print("Initializing P-TVGs...", file=sys.stderr)
    base_ptvg = tvg.from_serializable(kb["base_ptvg"])
    base_pcp = tvg.to_contact_plan(base_ptvg)
    multi_ptvg = {
        node: tvg.from_serializable(copy.deepcopy(kb["base_ptvg"]))
        for node in base_ptvg.vertices
    }

    # Sanity checks
    for contact in random.sample(base_pcp, min(len(base_pcp), 20)):
        tp = trajectory_predictors[frozenset(
            (contact.tx_node, contact.rx_node)
        )]
        start_offset = tp.get_offset(contact.start_time)
        end_offset = tp.get_offset(contact.end_time)
        assert start_offset == end_offset, "Do scenario and P-TVG match?"
        start_elev = tp.predict_elevation(contact.start_time)
        end_elev = tp.predict_elevation(contact.end_time)
        assert abs(start_elev) < .01, "Do scenario and P-TVG match?"
        # This might be the case if the F-TVG requires mocking for the
        # provided scenario.
        assert abs(end_elev) < .01, "Forgot to provide the right P-TVG?"
        max_elev_t, max_elev = tp.predict_max_elevation(
            contact.start_time,
            contact.end_time,
        )
        assert start_elev < max_elev > end_elev, "BUG in TP"
        assert contact.start_time < max_elev_t < contact.end_time, "BUG in TP"

    print("Generating individual P-TVG structures...", file=sys.stderr)

    print("Creating Jobs...", file=sys.stderr)
    jobs = []
    for node, ptvg in tqdm.tqdm(multi_ptvg.items()):
        for edge, edge_contacts in ptvg.edges.items():
            # Save bit rate before overwriting it in characteristics
            edge_contact_brs = []
            for pc in edge_contacts:
                pc_bit_rate = pc.to_simple().bit_rate
                edge_contact_brs.append(pc_bit_rate)

            # Add following predictions according to kb
            # FORMAT of metrics: [node][rx][tx]
            cur_metrics = metrics[node].get(edge[1], {}).get(edge[0], [])
            if not cur_metrics or abs(cur_metrics[0][0]) > 1e-3:
                # e.g. after distribution the first element is not starting
                # at 0.0, so we need to prepend an empty call to the list
                cur_metrics[:0] = [(0.0, None, None)]
            last_validity_time = -math.inf
            cur_metrics.sort(key=lambda e: e[0])
            for i, (valid_from, conf_mi, vol_mi) in enumerate(cur_metrics):
                if i == 0:
                    assert valid_from == 0, valid_from
                if valid_from < last_validity_time + args.min_predict_interval:
                    assert i != 0
                    # Check if next element is later than max interval
                    if i + 1 == len(cur_metrics):
                        # last element
                        next_valid_from = math.inf
                    else:
                        next_valid_from = cur_metrics[i + 1][0]
                    max_next = last_validity_time + args.max_predict_interval
                    # only skip if the next "valid_from" is below the maximum
                    if next_valid_from < max_next:
                        continue
                last_validity_time = valid_from
                # New predictors to not break parallelism
                job_conf_predictor = conf_predictor_factory(
                    base_ptvg.edges[edge],
                    trajectory_predictors[frozenset(edge)],
                    **conf_predictor_args,
                )
                job_vol_predictor = vol_predictor_factory(
                    trajectory_predictors[frozenset(edge)],
                    **vol_predictor_args,
                )
                if conf_mi is not None:
                    job_conf_predictor.import_metric_info(conf_mi)
                    job_vol_predictor.import_metric_info(vol_mi)
                else:
                    assert vol_mi is None
                # Add contacts
                job_pc_data = [
                    (pc.start_time, pc.end_time)
                    for pc, pc_bit_rate in zip(edge_contacts, edge_contact_brs)
                    if pc.start_time >= valid_from
                ]
                job_pcs = [
                    (pc, pc_bit_rate)
                    for pc, pc_bit_rate in zip(edge_contacts, edge_contact_brs)
                    if pc.start_time >= valid_from
                ]
                jobs.append((
                    job_pcs,
                    valid_from,
                    joblib.delayed(_process_job)(
                        job_conf_predictor,
                        job_vol_predictor,
                        valid_from,
                        job_pc_data,
                    ),
                ))

    print("Predicting...", file=sys.stderr)
    job_results = joblib.Parallel(n_jobs=args.workers)(
        job for _, _, job in tqdm.tqdm(jobs)
    )

    print("Updating contacts...", file=sys.stderr)
    for job_desc, result in tqdm.tqdm(zip(jobs, job_results)):
        job_pcs, valid_from, _ = job_desc
        # Update all PCs with new predictions
        for pc_desc, res in zip(job_pcs, result):
            pc, pc_bit_rate = pc_desc
            conf, brf = res
            assert pc.start_time >= valid_from
            add_characteristics_to_pc(
                pc,
                predicted_at=valid_from,
                bit_rate=pc_bit_rate,
                confidence=conf,
                bit_rate_factors=brf,
                overwrite=(valid_from == 0),
            )

    print("Serializing data to JSON...", file=sys.stderr)
    json.dump(
        {
            node: tvg.to_serializable(ptvg)
            for node, ptvg in multi_ptvg.items()
        },
        args.output,
        indent=(4 if args.indent else None),
    )
    args.output.write("\n")
    args.output.close()


def _initialize_logger(verbosity):
    log_level = {
        0: logging.WARN,
        1: logging.INFO,
    }.get(verbosity, logging.DEBUG)
    logging.basicConfig(
        level=log_level,
        format="%(asctime)s %(levelname)s: %(message)s",
    )


def _get_argument_parser():
    parser = argparse.ArgumentParser(
        description="Ring Road P-TVG prediction tool",
    )
    parser.add_argument(
        "KBFILE",
        type=argparse.FileType("r"),
        help="the knowledge base file",
    )
    parser.add_argument(
        "-w", "--workers",
        type=int, default=4,
        help="the amount of workers used for parallel processing",
    )
    parser.add_argument(
        "--min_predict_interval",
        type=int, default=0,
        help="the minimum time difference between two predictions",
    )
    parser.add_argument(
        "--max_predict_interval",
        type=int, default=0,
        help="the maximum time difference between two predictions",
    )
    parser.add_argument(
        "-o", "--output",
        type=argparse.FileType("w"),
        default=sys.stdout,
        help="output JSON file for the multi-P-TVG",
    )
    parser.add_argument(
        "-i", "--indent",
        action="store_true",
        help="specify this if you want pretty JSON",
    )
    parser.add_argument(
        "-v", "--verbose",
        action="count",
        default=0,
        help="increase log output verbosity",
    )
    return parser


if __name__ == "__main__":
    _main(_get_argument_parser().parse_args())
