#!/bin/bash

# This script compares routing performance of different volume predictors.
# It compares:
#   - Epidemic
#   - SnW
#   - CGR with minelev volume for minelev={5,10,20}
#   - CGR with two-state predictor
#   - CGR with three-state predictor
#   - CGR with factual volumes

TEST="T07_volroute"

WORKING_DIR="results_$TEST"

SCENARIOS=(
    "S1+low"
    "S1+high"
    "S2+low"
    "S2+high"
)

ALGOS=(
    Epidemic
    SprayAndWait
    CGR-minelev05
    CGR-minelev10
    CGR-minelev20
    CGR-twostate
    CGR-threestate
    CGR-factual
)

RUNS=3

WORKERS=4
SETUP_WORKERS=24

OBSV_ALGOS=(
    CGR-minelev05
    CGR-minelev10
    CGR-minelev20
    CGR-twostate
    CGR-threestate
    CGR-factual
)
OBSV_FLAGS=(
    "--min_metric_update_interval 43200 --only_factual --confidence_predictor disabled --volume_predictor minelev --min_elevation 5"
    "--min_metric_update_interval 43200 --only_factual --confidence_predictor disabled --volume_predictor minelev --min_elevation 10"
    "--min_metric_update_interval 43200 --only_factual --confidence_predictor disabled --volume_predictor minelev --min_elevation 20"
    "--min_metric_update_interval 43200 --only_factual --confidence_predictor disabled --volume_predictor twostate --link_block_size 12000 --averaging_window_size 10"
    "--min_metric_update_interval 43200 --only_factual --confidence_predictor disabled --volume_predictor tristate --link_block_size 12000 --averaging_window_size 10 --az_granularity 45"
    "--min_metric_update_interval 43200 --only_factual --confidence_predictor disabled --volume_predictor factual"
)

# Flags for the transmission plan, per-scenario as listed above
# (MSG VOL == duration_sec * 80% gentime / interval * bit per msg) * (AVG HOPS == 2) / (CONTACT VOL) * (BR_RATIO == 2): e.g. (7*3*86400*0.8*1/200)*2/(570*10*60*0.5*1)*2 == 17%
# LOW = 2% (200 KiB bundles instead 2 MiB)
# MEDIUM = 50%
# HIGH = 100%
# DEV is 1/4 of intv
# walker: 5174 bidi contacts, cvol: 830794757462.3237 bits
# - LOW: 10*86400*0.8/273*1638400*2/830794757462.3237*2
# - HIGH: 10*86400*0.8/112*16777216*2/830794757462.3237*2
# - EXTREME: 10*86400*0.8/56*16777216*2/830794757462.3237*2
# 9045: 5714 bidi contacts, cvol: 1015259915908.1959 bits
# - LOW: 10*86400*0.8/223*1638400*2/1015259915908.1959*2
# - HIGH: 10*86400*0.8/92*16777216*2/1015259915908.1959*2
# - EXTREME: 10*86400*0.8/46*16777216*2/1015259915908.1959*2
TPLAN_FLAGS=(
    "--maxgenpercent 80 --interval 273 --intervaldev 68 --lifetime 864000 --minsize 1638400 --invalidsrc LEO-01 LEO-02 LEO-03 LEO-11 LEO-12 LEO-13 LEO-21 LEO-22 LEO-23 --invaliddst LEO-01 LEO-02 LEO-03 LEO-11 LEO-12 LEO-13 LEO-21 LEO-22 LEO-23"
    "--maxgenpercent 80 --interval 112 --intervaldev 28 --lifetime 864000 --minsize 16777216 --invalidsrc LEO-01 LEO-02 LEO-03 LEO-11 LEO-12 LEO-13 LEO-21 LEO-22 LEO-23 --invaliddst LEO-01 LEO-02 LEO-03 LEO-11 LEO-12 LEO-13 LEO-21 LEO-22 LEO-23"
    "--maxgenpercent 80 --interval 223 --intervaldev 56 --lifetime 864000 --minsize 1638400 --invalidsrc LEO-01 LEO-11 LEO-21 LEO-31 LEO-41 LEO-51 --invaliddst LEO-01 LEO-11 LEO-21 LEO-31 LEO-41 LEO-51"
    "--maxgenpercent 80 --interval 92 --intervaldev 23 --lifetime 864000 --minsize 16777216 --invalidsrc LEO-01 LEO-11 LEO-21 LEO-31 LEO-41 LEO-51 --invaliddst LEO-01 LEO-11 LEO-21 LEO-31 LEO-41 LEO-51"
)

# Flags for the simulator, per algo - 2 GiB of buffer provided
SIM_FLAGS=(
    "--rx_channels 5 --tx_channels 5 --buffersize 17179869184 --graph_update_min_interval 43200 --link_block_size 12000 --algo epidemic"
    "--rx_channels 5 --tx_channels 5 --buffersize 17179869184 --graph_update_min_interval 43200 --link_block_size 12000 --algo snw"
    "--rx_channels 5 --tx_channels 5 --buffersize 17179869184 --graph_update_min_interval 43200 --link_block_size 12000 --algo ngsabr"
    "--rx_channels 5 --tx_channels 5 --buffersize 17179869184 --graph_update_min_interval 43200 --link_block_size 12000 --algo ngsabr"
    "--rx_channels 5 --tx_channels 5 --buffersize 17179869184 --graph_update_min_interval 43200 --link_block_size 12000 --algo ngsabr"
    "--rx_channels 5 --tx_channels 5 --buffersize 17179869184 --graph_update_min_interval 43200 --link_block_size 12000 --algo ngsabr"
    "--rx_channels 5 --tx_channels 5 --buffersize 17179869184 --graph_update_min_interval 43200 --link_block_size 12000 --algo ngsabr"
    "--rx_channels 5 --tx_channels 5 --buffersize 17179869184 --graph_update_min_interval 43200 --link_block_size 12000 --algo ngsabr"
)

COMMAND="$1"

set -euxo pipefail

if [ -z "$COMMAND" ]; then
    COMMAND="run"
fi

for scenario in "${SCENARIOS[@]}"; do
    base_scenario="$(echo "$scenario" | cut -d '+' -f 1)"
    if [[ ! -r "$base_scenario.02.scenario.json" || ! -r "$base_scenario.02.ftvg.json" ]]; then
        echo "$base_scenario.02.scenario.json and $base_scenario.02.ftvg.json have to exist!" >&2
        exit 1
    fi
done

mkdir -p "$WORKING_DIR"

setup() {
    # Remove all empty result files to have a clean setup
    find "$WORKING_DIR" -type f -size 0 -delete

    for ((s = 0; s < ${#SCENARIOS[@]}; s++)); do
        local scenario="${SCENARIOS[$s]}"
        local tplan_flags="${TPLAN_FLAGS[$s]}"

        local prefix="$WORKING_DIR/$scenario"
        local base_scenario="$(echo "$scenario" | cut -d '+' -f 1)"
        local base_prefix="$WORKING_DIR/$base_scenario"

        for ((i = 0; i < ${#OBSV_ALGOS[@]}; i++)); do
            local obsv_algo="${OBSV_ALGOS[$i]}"
            local obsv_flags="${OBSV_FLAGS[$i]}"

            if [ -r "$base_scenario.02.ptvg.json" ]; then
                [ -r "$base_prefix.03.$obsv_algo.kb.observed.json" ] || \
                    python 03_observe_contacts.py -v \
                        $obsv_flags \
                        --ptvg "$base_scenario.02.ptvg.json" \
                        --output "$base_prefix.03.$obsv_algo.kb.observed.json" \
                        "$base_scenario.02.scenario.json" \
                        "$base_scenario.02.ftvg.json"
            else
                [ -r "$base_prefix.03.$obsv_algo.kb.observed.json" ] || \
                    python 03_observe_contacts.py -v \
                        $obsv_flags \
                        --output "$base_prefix.03.$obsv_algo.kb.observed.json" \
                        "$base_scenario.02.scenario.json" \
                        "$base_scenario.02.ftvg.json"
            fi
            if [[ "$(echo "$obsv_algo" | cut -d "-" -f 2)" == "factual" ]]; then
                [ -r "$base_prefix.03.$obsv_algo.kb.dist.json" ] || \
                    python 04_b_assign_metrics.py \
                        --output "$base_prefix.03.$obsv_algo.kb.dist.json" \
                        --strategy latest \
                        "$base_prefix.03.$obsv_algo.kb.observed.json"
                [ -r "$base_prefix.05.$obsv_algo.ftvg.json" ] || cp "$base_scenario.02.ftvg.json" \
                    "$base_prefix.05.$obsv_algo.ftvg.json"
            else
                [[ -r "$base_prefix.03.$obsv_algo.kb.dist.json" && -r "$base_prefix.04.$i.$obsv_algo.diststats.json" ]] || \
                    python 04_a_distribute_metrics.py \
                        --output "$base_prefix.03.$obsv_algo.kb.dist.json" \
                        --progress \
                        --savestats "$base_prefix.04.$i.$obsv_algo.diststats.json" \
                        "$base_scenario.02.ftvg.json" \
                        "$base_prefix.03.$obsv_algo.kb.observed.json"
                [ -r "$base_prefix.05.$obsv_algo.ftvg.json" ] || \
                    python helpers/05_apply_dist_overhead_to_ftvg.py \
                        --output "$base_prefix.05.$obsv_algo.ftvg.json" \
                        "$base_scenario.02.ftvg.json" \
                        "$base_prefix.04.$i.$obsv_algo.diststats.json"
            fi
            [ -r "$base_prefix.05.$obsv_algo.ptvg.json" ] || \
                python 05_predict_ptvg.py -v \
                    --output "$base_prefix.05.$obsv_algo.ptvg.json" \
                    --workers "$SETUP_WORKERS" \
                    "$base_prefix.03.$obsv_algo.kb.dist.json"

            [ -r "$prefix.05.$obsv_algo.ptvg.json" ] || cp "$base_prefix.05.$obsv_algo.ptvg.json" \
                "$prefix.05.$obsv_algo.ptvg.json"
            [ -r "$prefix.05.$obsv_algo.ftvg.json" ] || cp "$base_prefix.05.$obsv_algo.ftvg.json" \
                "$prefix.05.$obsv_algo.ftvg.json"
        done

        for run in $(seq $RUNS); do
            [ -r "$prefix.06.transmission_plan_$run.json" ] || \
                python -m tvgutil.tools.create_transmission_plan \
                    $tplan_flags \
                    --output "$prefix.06.transmission_plan_$run.json" \
                    "$base_scenario.02.ftvg.json"
        done

        [ -r "$prefix.02.ftvg.json" ] || cp "$base_scenario.02.ftvg.json" \
            "$prefix.02.ftvg.json"
        [ -r "$prefix.02.scenario.json" ] || cp "$base_scenario.02.scenario.json" \
            "$prefix.02.scenario.json"
    done
}

job() {
    local scenario="$1"
    local base_scenario="$(echo "$scenario" | cut -d '+' -f 1)"
    local algo="$2"
    local run="$3"
    local prefix="$WORKING_DIR/$scenario"
    local ptvg="$prefix.05.$algo.ptvg.json"

    for ((a = 0; a < ${#ALGOS[@]}; a++)); do
        if [[ "${ALGOS[$a]}" = "$algo" ]]; then
            local sim_flags="${SIM_FLAGS[$a]}"
            break
        fi
    done

    [ -r "$prefix.06.transmission_plan_$run.json" ] || exit 1

    [ -r "$prefix.07.stats.$algo.$run.json" ] && return 0

    if [ -r "$ptvg" ]; then
        python 06_simulation.py \
            $sim_flags \
            --ptvg "$ptvg" \
            --tplan "$prefix.06.transmission_plan_$run.json" \
            --savestats "$prefix.07.stats.$algo.$run.json" \
            "$prefix.05.$algo.ftvg.json"
    else
        python 06_simulation.py \
            $sim_flags \
            --tplan "$prefix.06.transmission_plan_$run.json" \
            --savestats "$prefix.07.stats.$algo.$run.json" \
            "$base_scenario.02.ftvg.json"
    fi
}

teardown() {
    bash draw_plots.sh \
        "$WORKING_DIR" \
        "$WORKING_DIR/plots_$TEST" \
        "${SCENARIOS[@]}"

    PLOT_DIR="$WORKING_DIR/dist_plots_$TEST"

    mkdir -p "$PLOT_DIR"

    local out="--outdir $PLOT_DIR"
    local xlabel="scenario"
    local algos="$(for a in "${OBSV_ALGOS[@]}"; do echo $a | cut -d "-" -f 2 | sed "s/factual//"; done)"

    SUMMARY_FILE="$PLOT_DIR/stats_summary.txt"
    echo "Scenarios: ${SCENARIOS[@]}" > "$SUMMARY_FILE"
    echo "Algos: $(echo "$algos" | tr '\n' ' ')" >> "$SUMMARY_FILE"
    echo -e "Date: $(date)\n" >> "$SUMMARY_FILE"

    filelist=()
    for scenario in "${SCENARIOS[@]}"; do
        base_scenario="$(echo "$scenario" | cut -d '+' -f 1)"
        base_prefix="$WORKING_DIR/$base_scenario"
        for ((i = 0; i < ${#OBSV_ALGOS[@]}; i++)); do
            obsv_algo="${OBSV_ALGOS[$i]}"
            if [[ "$(echo "$obsv_algo" | cut -d "-" -f 2)" != "factual" ]]; then
                    python3 07_join_simulation_results.py $base_prefix.04.$i.$obsv_algo.diststats.json \
                            -o $base_prefix.04.joined.$i.$obsv_algo.diststats.json
            fi
        done
        filelist+=($base_prefix.04.joined.*.diststats.json)
    done

    python 08_plot_results.py $out \
        --title "Bundle Transmission Count" \
        --xlabel "$xlabel" --xticks ${SCENARIOS[@]} \
        --algos $algos \
        --ylabel "average transmissions per bundle" \
        --legend-columns 2 \
        --percentile \
        "tx_counts_all" \
        ${filelist[@]} | tee -a "$SUMMARY_FILE"

    echo >> "$SUMMARY_FILE"

    python 08_plot_results.py $out \
        --title "Bundle Transmission Volume" \
        --xlabel "$xlabel" --xticks ${SCENARIOS[@]} \
        --algos $algos \
        --ylabel "avg. transmitted volume per bundle / KiB" \
        --legend-columns 2 \
        --factor 0.0001220703125 --percentile \
        "tx_volumes_all" \
        ${filelist[@]} | tee -a "$SUMMARY_FILE"

    echo >> "$SUMMARY_FILE"

    python 08_plot_results.py $out \
        --title "Bundle Replica Count" \
        --xlabel "$xlabel" --xticks ${SCENARIOS[@]} \
        --algos $algos \
        --ylabel "average replicas per bundle" \
        --legend-columns 2 \
        --percentile \
        "replica_counts" \
        ${filelist[@]} | tee -a "$SUMMARY_FILE"

    echo >> "$SUMMARY_FILE"

    python 08_plot_results.py $out \
        --title "Contact Utilization" \
        --xlabel "$xlabel" --xticks ${SCENARIOS[@]} \
        --factor 100 --algos $algos \
        --ylabel "average contact utilization / %" \
        --legend-columns 2 \
        --percentile \
        "contact_utilization_total" \
        ${filelist[@]} | tee -a "$SUMMARY_FILE"

    echo >> "$SUMMARY_FILE"

    python 08_plot_results.py $out \
        --title "Buffer Utilization" \
        --xlabel "$xlabel" --xticks ${SCENARIOS[@]} \
        --factor 0.0001220703125 --algos $algos \
        --ylabel "average buffer utilization / KiB" \
        --legend-columns 2 \
        --percentile \
        "buffer_utilization_total" \
        ${filelist[@]} | tee -a "$SUMMARY_FILE"

    echo >> "$SUMMARY_FILE"

    python 08_plot_results.py $out \
        --title "Distribution Delay" \
        --xlabel "$xlabel" --xticks ${SCENARIOS[@]} \
        --algos $algos \
        --ylabel "average distribution delay / s" \
        --legend-columns 2 \
        --percentile \
        "delays" \
        ${filelist[@]} | tee -a "$SUMMARY_FILE"

    echo >> "$SUMMARY_FILE"
}

case "$COMMAND" in
    run)
        setup
        python test_process_pool_executor.py \
            --workers $WORKERS \
            --scenarios "${SCENARIOS[@]}" \
            --algorithms "${ALGOS[@]}" \
            --runs $(seq $RUNS) \
            --progress \
            --logfile "$WORKING_DIR/test_executor.log" \
            "$0"
        teardown
        ;;
    clean)
        rm -rvf --one-file-system --preserve-root "$WORKING_DIR"
        ;;
    cleanrouting)
        rm -vf --one-file-system --preserve-root $WORKING_DIR/*06.transmission_plan*json
        rm -vf --one-file-system --preserve-root $WORKING_DIR/*07.stats*json
        rm -vf --one-file-system --preserve-root $WORKING_DIR/*08.plotstats*json
        ;;
    job)
        job "${@:2}"
        ;;
    *)
        echo "Usage: $0 [<run|clean|job>] [<job-args>]" >&2
        exit 1
        ;;
esac
