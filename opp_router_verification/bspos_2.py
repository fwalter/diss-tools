import sys
import json
import os

with open(sys.argv[1], "r") as f:
    j = json.loads(f.read())
    for i, s in enumerate(j["gslist"]):
        print("{},{}".format(int((float(s["lon"]) + 180) * 4007 / 360), int((90 - float(s["lat"])) * 2000 / 180)))
