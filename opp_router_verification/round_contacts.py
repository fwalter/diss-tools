#!/usr/bin/env python3
# encoding: utf-8

import sys
import argparse
import json
import math

from tvgutil import tvg


def _main(args):
    graph = tvg.from_serializable(json.load(args.INFILE))
    cp = tvg.to_contact_plan(graph)

    for contact in cp:
        contact.start_time = math.ceil(contact.start_time)
        contact.end_time = math.ceil(contact.end_time)

    json.dump(
        tvg.to_serializable(tvg.from_contact_plan(
            cp,
            contact_type=graph.contact_type,
        )),
        args.output,
        indent=(4 if args.indent else None),
    )
    args.output.write("\n")
    args.output.close()


def _get_argument_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "INFILE",
        type=argparse.FileType("r"),
        default=sys.stdin,
        help="the input (F/P-)TVG",
    )
    parser.add_argument(
        "-o", "--output",
        type=argparse.FileType("w"),
        default=sys.stdout,
        help="the output (F/P-)TVG",
    )
    parser.add_argument(
        "-i", "--indent",
        action="store_true",
        help="indent the JSON output",
    )
    return parser


if __name__ == "__main__":
    _main(_get_argument_parser().parse_args())
