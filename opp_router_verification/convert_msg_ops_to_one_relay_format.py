#!/usr/bin/env python3
# encoding: utf-8

import argparse
import json
import sys

from tvgutil import tvg

from predictutil import get_contact

from aiodtnsim.reports.message_stats_report import print_stat


def _main(args):
    print("Loading scenario...", file=sys.stderr)
    scenario = json.load(args.SCENARIO)
    args.SCENARIO.close()
    start_time = scenario["time_offset"]

    # TODO: add cmdline args
    sat_start_offset = 0
    gs_start_offset = len(scenario["satlist"])
    sat_prefix = "sat_node"
    gs_prefix = "gs_node"

    node_map = {
        sat["id"]: sat_prefix + str(i + sat_start_offset)
        for i, sat in enumerate(scenario["satlist"])
    }
    node_map.update({
        gs["id"]: gs_prefix + str(i + gs_start_offset)
        for i, gs in enumerate(scenario["gslist"])
    })

    print("Processing msgops...", file=sys.stderr)
    for line in args.MSGOPS:
        line_split = line.strip().split(" ")
        if not ("inject" in line or "recv" in line or "deliver" in line):
            continue
        for i, field in enumerate(line_split):
            if field in ("inject", "recv", "deliver"):
                op_idx = i
                break
        line_split[0] = str(round(float(line_split[0]) / 1000 - start_time, 2))
        line_split[1:op_idx] = node_map[" ".join(line_split[1:op_idx])].split(" ")
        if line_split[op_idx] == "recv":
            for i, field in enumerate(line_split):
                if field == "from":
                    from_idx = i
                    break
            line_split[from_idx + 1:] = node_map[" ".join(line_split[from_idx + 1:])].split(" ")
        args.output.write(" ".join(line_split))
        args.output.write("\n")
    args.output.close()


def _get_argument_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "SCENARIO",
        type=argparse.FileType("r"),
        help="the scenario file to be used for mapping the nodes",
    )
    parser.add_argument(
        "MSGOPS",
        type=argparse.FileType("r"),
        help="the msg ops file",
    )
    parser.add_argument(
        "-o", "--output",
        type=argparse.FileType("w"),
        default=sys.stdout,
        help="the msg ops output file with ONE identifiers",
    )
    return parser


if __name__ == "__main__":
    _main(_get_argument_parser().parse_args())
