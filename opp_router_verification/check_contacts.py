#!/usr/bin/env python3
# encoding: utf-8

import argparse
import json
import sys

from tvgutil import tvg

from predictutil import get_contact

from aiodtnsim.reports.message_stats_report import print_stat


def _main(args):
    print("Loading scenario...", file=sys.stderr)
    scenario = json.load(args.SCENARIO)
    args.SCENARIO.close()
    node_list = (
        [sat["id"] for sat in scenario["satlist"]] +
        [gs["id"] for gs in scenario["gslist"]]
    )
    start_time = scenario["time_offset"]

    print("Loading TVG...", file=sys.stderr)
    ftvg = tvg.from_serializable(json.load(args.FTVG))
    args.FTVG.close()

    print("Loading report file...", file=sys.stderr)
    contacts = []
    pending_contacts = {}
    for line in args.CONN_ONE_REPORT:
        if line[0] == "#" or line.strip() == "":
            continue
        ts, _, node1, node2, updown = line.strip().split(" ")
        key = (node1, node2)
        if updown == "up":
            assert key not in pending_contacts, f"error: {key} pending already"
            pending_contacts[key] = float(ts)
        else:
            assert updown == "down", f"wrong value: {updown}"
            mapped_node1 = node_list[int(node1)]
            mapped_node2 = node_list[int(node2)]
            contacts.append((
                mapped_node1,
                mapped_node2,
                start_time + pending_contacts[key],
                start_time + float(ts),
            ))
            del pending_contacts[key]
    args.CONN_ONE_REPORT.close()

    duration_diffs = []
    not_found = []
    for node1, node2, start, end in contacts:
        edge = (node1, node2)
        fc = get_contact(ftvg.edges[edge], start, end)
        if not fc:
            not_found.append((node1, node2, start, end))
            continue
        duration_diffs.append(abs(
            (fc.end_time - fc.start_time) -
            (end - start)
        ))
        ftvg.edges[edge].remove(fc)
        reverse_edge = (node2, node1)
        rfc = get_contact(ftvg.edges[reverse_edge], start, end)
        ftvg.edges[reverse_edge].remove(rfc)

    print(f"Found {len(duration_diffs)} of {len(contacts)}", file=sys.stderr)
    print_stat("Duration diff", duration_diffs)
    print(f"Not found ({len(not_found)}): ", not_found, file=sys.stderr)
    fcp = tvg.to_contact_plan(ftvg)
    print(f"Remainder ({len(fcp)}): ", fcp, file=sys.stderr)


def _get_argument_parser():
    parser = argparse.ArgumentParser(
        description="check contacts from ONE report against F-TVG"
    )
    parser.add_argument(
        "SCENARIO",
        type=argparse.FileType("r"),
        help="the scenario file to be used for mapping the nodes",
    )
    parser.add_argument(
        "FTVG",
        type=argparse.FileType("r"),
        help="the F-TVG file",
    )
    parser.add_argument(
        "CONN_ONE_REPORT",
        type=argparse.FileType("r"),
        help="the ONE ConnectivityONEReport",
    )
    return parser


if __name__ == "__main__":
    _main(_get_argument_parser().parse_args())
