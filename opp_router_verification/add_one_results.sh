#!/bin/bash

set -euxo pipefail

TEST="T1_one_verification"
WORKING_DIR="results_$TEST"
REPORT_DIR1=one_reports/scenario2fc_uc
REPORT_DIR2=one_reports/scenario2fc_u
REPORT_DIR3=one_reports/scenario2fc_0
SCENARIO_ALIAS="STINT17_S2_8x90"

# NOTE the relation to $algos in join_stats.sh and draw_plots.sh!
EPIDEMIC_NUMBER1=03
EPIDEMIC_NUMBER2=xx # UNUSED
EPIDEMIC_NUMBER3=04
SNW_NUMBER1=07
SNW_NUMBER2=yy # UNUSED
SNW_NUMBER3=08

mkdir -p "$WORKING_DIR"

prefix="$WORKING_DIR/$SCENARIO_ALIAS"

python msr_to_stats.py $REPORT_DIR1/scenario2_EpidemicRouter_MessageStatsReport.txt $REPORT_DIR1/scenario2_EpidemicRouter_MessageDelayReport.txt -o "$prefix.08.plotstats.$EPIDEMIC_NUMBER1.json"
python msr_to_stats.py $REPORT_DIR1/scenario2_SprayAndWaitRouter_MessageStatsReport.txt $REPORT_DIR1/scenario2_SprayAndWaitRouter_MessageDelayReport.txt -o "$prefix.08.plotstats.$SNW_NUMBER1.json"

#python msr_to_stats.py $REPORT_DIR2/scenario2_EpidemicRouter_MessageStatsReport.txt $REPORT_DIR2/scenario2_EpidemicRouter_MessageDelayReport.txt -o "$prefix.08.plotstats.$EPIDEMIC_NUMBER2.json"
#python msr_to_stats.py $REPORT_DIR2/scenario2_SprayAndWaitRouter_MessageStatsReport.txt $REPORT_DIR2/scenario2_SprayAndWaitRouter_MessageDelayReport.txt -o "$prefix.08.plotstats.$SNW_NUMBER2.json"

python msr_to_stats.py $REPORT_DIR3/scenario2_EpidemicRouter_MessageStatsReport.txt $REPORT_DIR3/scenario2_EpidemicRouter_MessageDelayReport.txt -o "$prefix.08.plotstats.$EPIDEMIC_NUMBER3.json"
python msr_to_stats.py $REPORT_DIR3/scenario2_SprayAndWaitRouter_MessageStatsReport.txt $REPORT_DIR3/scenario2_SprayAndWaitRouter_MessageDelayReport.txt -o "$prefix.08.plotstats.$SNW_NUMBER3.json"
