import sys
import json
import os

os.system("rm -f aa/*; rmdir aa")
with open(sys.argv[1], "r") as f:
    j = json.loads(f.read())
    for i, s in enumerate(j["satlist"]):
        with open("sat.tle", "w") as f2:
            f2.write("ABC\n" + "\n".join(s["tle"]))
        os.system("java -jar ../../../TleToWKT/target/TLEtoWKT-jar-with-dependencies.jar -f sat.tle -d 2017-08-01T00:00:00.000Z -it 8640 -o aa")
        os.system("mv -fv aa/*.wkt sat{}.wkt".format(i + 1))
        os.system("mv -f aa/*-Info.txt sat{}-Info.txt".format(i + 1))
        os.system("rmdir aa; rm sat.tle")
