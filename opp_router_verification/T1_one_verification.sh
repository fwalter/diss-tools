#!/bin/bash

# This script compares aiocgrsim against provided ONE reports.

TEST="T1_one_verification"

WORKING_DIR="results_$TEST"

SCENARIOS=(
    "STINT17_S2_8x90"
)

ALGOS=(
    aiodtnsim_ONE_Epidemic
    aiodtnsim_ONE_SprayAndWait
    aiodtnsim_Epidemic
    aiodtnsim_SprayAndWait
)

RUNS=5

WORKERS=4
SETUP_WORKERS=24

PLOT_ALGOS=(
    ONE_Epidemic
    ONE_SprayAndWait
    aiodtnsim_ONE_Epidemic
    aiodtnsim_ONE_SprayAndWait
    aiodtnsim_Epidemic
    aiodtnsim_SprayAndWait
)

# 1M messages, every 30 sec, for all 3 days, no deviation, only from gs to gs, lifetime one day
TPLAN_FLAGS=(
    "--interval 30 --minsize 1000000 --lifetime 86400 --intervaldev 0 --maxgenpercent 100 --validsrc gs0 gs1 gs2 gs3 gs4 gs6 gs7 gs8 gs9 --validdst gs0 gs1 gs2 gs3 gs4 gs6 gs7 gs8 gs9"
)

# 1G bufs
SIM_FLAGS=(
    "--buffersize 1000000000 --algo one_epidemic"
    "--buffersize 1000000000 --algo one_snw"
    "--buffersize 1000000000 --algo epidemic"
    "--buffersize 1000000000 --algo snw"
)

COMMAND="$1"

set -euxo pipefail

if [ -z "$COMMAND" ]; then
    COMMAND="run"
fi

for scenario in "${SCENARIOS[@]}"; do
    base_scenario="$(echo "$scenario" | cut -d '+' -f 1)"
    if [[ ! -r "$base_scenario.02.scenario.json" || ! -r "$base_scenario.02.ftvg.json" ]]; then
        echo "$base_scenario.02.scenario.json and $base_scenario.02.ftvg.json have to exist!" >&2
        exit 1
    fi
done

mkdir -p "$WORKING_DIR"

setup() {
    # Remove all empty result files to have a clean setup
    find "$WORKING_DIR" -type f -size 0 -delete

    for ((s = 0; s < ${#SCENARIOS[@]}; s++)); do
        local scenario="${SCENARIOS[$s]}"
        local tplan_flags="${TPLAN_FLAGS[$s]}"

        local prefix="$WORKING_DIR/$scenario"
        local base_scenario="$(echo "$scenario" | cut -d '+' -f 1)"

        for run in $(seq $RUNS); do
            [ -r "$prefix.06.transmission_plan_$run.json" ] || \
                python -m tvgutil.tools.create_transmission_plan \
                    $tplan_flags \
                    --output "$prefix.06.transmission_plan_$run.json" \
                    "$base_scenario.02.ftvg.json"
        done

        [ -r "$prefix.02.ftvg.json" ] || cp "$base_scenario.02.ftvg.json" \
            "$prefix.02.ftvg.json"
        [ -r "$prefix.02.scenario.json" ] || cp "$base_scenario.02.scenario.json" \
            "$prefix.02.scenario.json"
    done
}

job() {
    local scenario="$1"
    local base_scenario="$(echo "$scenario" | cut -d '+' -f 1)"
    local algo="$2"
    local run="$3"
    local prefix="$WORKING_DIR/$scenario"
    local ptvg="$prefix.05.$algo.ptvg.json"

    for ((a = 0; a < ${#ALGOS[@]}; a++)); do
        if [[ "${ALGOS[$a]}" = "$algo" ]]; then
            local sim_flags="${SIM_FLAGS[$a]}"
            break
        fi
    done

    [ -r "$prefix.06.transmission_plan_$run.json" ] || exit 1

    [ -r "$prefix.07.stats.$algo.$run.json" ] && return 0

    if [ -r "$ptvg" ]; then
        python 06_simulation.py \
            $sim_flags \
            --ptvg "$ptvg" \
            --tplan "$prefix.06.transmission_plan_$run.json" \
            --savestats "$prefix.07.stats.$algo.$run.json" \
            "$prefix.05.$algo.ftvg.json"
    else
        python 06_simulation.py \
            $sim_flags \
            --tplan "$prefix.06.transmission_plan_$run.json" \
            --savestats "$prefix.07.stats.$algo.$run.json" \
            "$base_scenario.02.ftvg.json"
    fi
}

teardown() {
    bash join_stats.sh \
        "$WORKING_DIR" \
        "$WORKING_DIR/plots_$TEST" \
        "${SCENARIOS[@]}"
    bash add_one_results.sh
    bash draw_plots.sh \
        "$WORKING_DIR" \
        "$WORKING_DIR/plots_$TEST" \
        "${SCENARIOS[@]}"
}

case "$COMMAND" in
    run)
        setup
        python test_process_pool_executor.py \
            --workers $WORKERS \
            --scenarios "${SCENARIOS[@]}" \
            --algorithms "${ALGOS[@]}" \
            --runs $(seq $RUNS) \
            --progress \
            --logfile "$WORKING_DIR/test_executor.log" \
            "$0"
        teardown
        ;;
    clean)
        rm -rvf --one-file-system --preserve-root "$WORKING_DIR"
        ;;
    cleanrouting)
        rm -vf --one-file-system --preserve-root $WORKING_DIR/*06.transmission_plan*json
        rm -vf --one-file-system --preserve-root $WORKING_DIR/*07.stats*json
        rm -vf --one-file-system --preserve-root $WORKING_DIR/*08.plotstats*json
        ;;
    job)
        job "${@:2}"
        ;;
    *)
        echo "Usage: $0 [<run|clean|job>] [<job-args>]" >&2
        exit 1
        ;;
esac
