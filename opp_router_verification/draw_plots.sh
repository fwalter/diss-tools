#!/bin/bash

set -euxo pipefail

if [ $# -lt 3 ]; then
    echo 'Usage: bash draw_plots.sh <result-dir> <plot-dir> <scenario_name_1> ... <scenario_name_n>' >&2
    exit 1
fi

PLOT_DIR="$2"
SCENARIOS=("${@:3}")

LEGEND_COL=2

RESULTS_DIR="$1"

xlabel="scenario"

# WARNING: Statically defined - order is important!
algos="aiodtnsim_Epidemic aiodtnsim_ONE_Epidemic ONE_Epidemic_U+C ONE_Epidemic aiodtnsim_SprayAndWait aiodtnsim_ONE_SprayAndWait ONE_SprayAndWait_U+C ONE_SprayAndWait"

rm -rvf --one-file-system --preserve-root "$PLOT_DIR"
mkdir -pv "$PLOT_DIR"
out="--outdir $PLOT_DIR"

echo "Plotting scenarios: ${SCENARIOS[@]}"
echo "for algos: $algos"

filelist=()
for scenario in "${SCENARIOS[@]}"; do
    TEST_PREFIX="$RESULTS_DIR/$scenario"
    filelist+=(${TEST_PREFIX}.08.plotstats.*.json)
done

SUMMARY_FILE="$PLOT_DIR/stats_summary.txt"
echo "Scenarios: ${SCENARIOS[@]}" > "$SUMMARY_FILE"
echo "Algos: $(echo "$algos" | tr '\n' ' ')" >> "$SUMMARY_FILE"
echo -e "Date: $(date)\n" >> "$SUMMARY_FILE"

echo "> Plotting files:"
echo ${filelist[@]}

python 08_plot_results.py $out \
    --title "Bundle Delivery Delay" \
    --xlabel "$xlabel" --xticks ${SCENARIOS[@]} \
    --algos $algos \
    --ylabel "average delay / hours" \
    --percentile \
    --legend-columns $LEGEND_COL \
    --factor 0.000277777778 \
    "delays" \
    ${filelist[@]} | tee -a "$SUMMARY_FILE"

echo >> "$SUMMARY_FILE"

python 08_plot_results.py $out \
    --title "Bundle Delivery Delay" \
    --xlabel "$xlabel" --xticks ${SCENARIOS[@]} \
    --algos $algos \
    --ylabel "average delay / hours" \
    --percentile \
    --legend-columns $LEGEND_COL \
    --plot-name "delays_log" \
    --log \
    --ymin 0.1 --ymax 1000  \
    --factor 0.000277777778 \
    "delays" \
    ${filelist[@]} | tee -a "$SUMMARY_FILE"

echo >> "$SUMMARY_FILE"

python 08_plot_results.py $out \
    --title "Bundle Delivery Rate" \
    --xlabel "$xlabel" --xticks ${SCENARIOS[@]} \
    --factor 100 --algos $algos \
    --ylabel "delivery probability / %" \
    --percentile \
    --legend-columns $LEGEND_COL \
    "delivery_rate" \
    ${filelist[@]} | tee -a "$SUMMARY_FILE"

echo >> "$SUMMARY_FILE"

python 08_plot_results.py $out \
    --title "Bundle Transmission Count" \
    --xlabel "$xlabel" --xticks ${SCENARIOS[@]} \
    --algos $algos \
    --ylabel "average transmissions per bundle" \
    --percentile \
    --legend-columns $LEGEND_COL \
    --log \
    --ymin 0.5 --ymax 5000  \
    "tx_counts_all" \
    ${filelist[@]} | tee -a "$SUMMARY_FILE"

echo >> "$SUMMARY_FILE"

python 08_plot_results.py $out \
    --title "Bundle Reception Count" \
    --xlabel "$xlabel" --xticks ${SCENARIOS[@]} \
    --algos $algos \
    --ylabel "average receptions per bundle" \
    --percentile \
    --legend-columns $LEGEND_COL \
    --log \
    --ymin 0.5 --ymax 5000  \
    "rx_counts_all" \
    ${filelist[@]} | tee -a "$SUMMARY_FILE"

echo >> "$SUMMARY_FILE"
