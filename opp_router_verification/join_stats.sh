#!/bin/bash

set -euxo pipefail

if [ $# -lt 3 ]; then
    echo 'Usage: bash join_stats.sh <result-dir> <plot-dir> <scenario_name_1> ... <scenario_name_n>' >&2
    exit 1
fi

PLOT_DIR="$2"
SCENARIOS=("${@:3}")

LEGEND_COL=2

RESULTS_DIR="$1"

xlabel="scenario"

# WARNING: Statically defined - order is important!
algos="aiodtnsim_Epidemic aiodtnsim_ONE_Epidemic skip skip aiodtnsim_SprayAndWait aiodtnsim_ONE_SprayAndWait skip skip"

rm -rvf --one-file-system --preserve-root "$PLOT_DIR"
mkdir -pv "$PLOT_DIR"
out="--outdir $PLOT_DIR"

echo "Plotting scenarios: ${SCENARIOS[@]}"
echo "for algos: $algos"

filelist=()
for scenario in "${SCENARIOS[@]}"; do
    TEST_PREFIX="$RESULTS_DIR/$scenario"
    # NOTE that the order is important for passing this via glob
    i=1
    for algo in $algos; do
        if [[ "$algo" == "skip" ]]; then
            i=$[i+1]
            continue
        fi
        param_prefix="${TEST_PREFIX}.07.stats.${algo}"
        echo
        for file in ${param_prefix}*.json; do
            if [[ "$(stat --printf="%s" "$file")" == "0" ]]; then
                echo "Removing empty file: $file"
                rm $file
            fi
        done
        echo "=> Statistics for algo $algo:"
        python3 07_join_simulation_results.py ${param_prefix}.*.json \
            -o ${TEST_PREFIX}.08.plotstats.$(printf "%02d\n" $i).json
        i=$[i+1]
    done
    filelist+=(${TEST_PREFIX}.08.plotstats.*.json)
done
