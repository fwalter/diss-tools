#!/usr/bin/env python3
# encoding: utf-8

import argparse
import json
import sys

from aiodtnsim.reports.message_stats_report import print_stat


def _main(args):
    print("Loading report files...", file=sys.stderr)
    delays = []
    for line in args.DELAY_REPORT:
        if line[0] == "#" or line.strip() == "":
            continue
        delays.append(float(line.strip().split(" ")[0]))
    args.DELAY_REPORT.close()

    stats = args.STATS_REPORT.read().split("\n")[1:-3]
    stats_dict = {
        line.split(": ")[0]: float(line.split(": ")[1])
        for line in stats
    }
    args.STATS_REPORT.close()

    tx_count = int(stats_dict["started"])
    rx_count = int(stats_dict["relayed"])
    message_count = int(stats_dict["created"])
    delivered_count = int(stats_dict["delivered"])

    result = {
        "delays": delays,
        "tx_counts_all": [
            tx_count / message_count
            for _ in range(message_count)
        ],
        "rx_counts_all": [
            rx_count / message_count
            for _ in range(message_count)
        ],
        "delivery_rate": [delivered_count / message_count],
    }

    print(f"Delivered: {delivered_count} of {message_count} "
          f"({round(delivered_count / message_count * 100, 2)} %)",
          file=sys.stderr)
    print_stat("Average delivery delay", delays)
    print("Average TX counts (overall)", result["tx_counts_all"][0],
          file=sys.stderr)
    print("Average RX counts (overall)", result["rx_counts_all"][0],
          file=sys.stderr)

    print("Writing output file...", file=sys.stderr)
    json.dump(
        result,
        args.output,
        indent=(4 if args.indent else None),
    )
    args.output.write("\n")
    args.output.close()


def _get_argument_parser():
    parser = argparse.ArgumentParser(
        description="convert ONE reports to JSON stats"
    )
    parser.add_argument(
        "STATS_REPORT",
        type=argparse.FileType("r"),
        default=sys.stdin,
        help="the ONE MessageStatsReport",
    )
    parser.add_argument(
        "DELAY_REPORT",
        type=argparse.FileType("r"),
        default=sys.stdin,
        help="the ONE MessageDelayReport",
    )
    parser.add_argument(
        "-o", "--output",
        type=argparse.FileType("w"),
        default=sys.stdout,
        help="the output file for the simulation stats (default: stdout)",
    )
    parser.add_argument(
        "-i", "--indent",
        action="store_true",
        help="indent the JSON output",
    )
    return parser


if __name__ == "__main__":
    _main(_get_argument_parser().parse_args())
