#!/usr/bin/env python3
# encoding: utf-8

import argparse
import json
import sys

from tvgutil import tvg

from predictutil import get_contact

from aiodtnsim.reports.message_stats_report import print_stat


def _main(args):
    print("Loading scenario...", file=sys.stderr)
    scenario = json.load(args.SCENARIO)
    args.SCENARIO.close()
    start_time = scenario["time_offset"]

    # TODO: add cmdline args
    sat_start_offset = 0
    gs_start_offset = len(scenario["satlist"])
    sat_prefix = "sat_node"
    gs_prefix = "gs_node"

    node_map = {
        sat["id"]: sat_prefix + str(i + sat_start_offset)
        for i, sat in enumerate(scenario["satlist"])
    }
    node_map.update({
        gs["id"]: gs_prefix + str(i + gs_start_offset)
        for i, gs in enumerate(scenario["gslist"])
    })

    print("Loading TVG...", file=sys.stderr)
    ftvg = tvg.from_serializable(json.load(args.FTVG))
    args.FTVG.close()

    fcp = tvg.to_contact_plan(ftvg)
    for fc in fcp:
        tx_node = node_map[fc.tx_node]
        rx_node = node_map[fc.rx_node]
        start = fc.start_time - start_time
        end = fc.end_time - start_time
        args.output.write(f"{tx_node},{rx_node},{start},{end}\r")
    args.output.close()


def _get_argument_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "SCENARIO",
        type=argparse.FileType("r"),
        help="the scenario file to be used for mapping the nodes",
    )
    parser.add_argument(
        "FTVG",
        type=argparse.FileType("r"),
        help="the F-TVG file",
    )
    parser.add_argument(
        "-o", "--output",
        type=argparse.FileType("w"),
        default=sys.stdout,
        help="the CSV output file",
    )
    return parser


if __name__ == "__main__":
    _main(_get_argument_parser().parse_args())
