#!/bin/bash

set -euxo pipefail

SCENARIO_IN="stint_pubs_rr_scenario_8x90.json"

SCENARIO_ALIAS="STINT17_S2_8x90"

# NOISL, 3 days, 10 deg min elevation, 125k data rate to approximate using half of it due to bidirectionally used connections
# python3 -m tvgutil.tools.create_rr_tvg --rr 0 --halfperiod 1800 --step 100 --duration 259200 --minelev 10 --uplinkrate 125000 --downlinkrate 125000 --islrange 1000 --islrate 125000 --output ptvg_bidi_conn.json "$1"
# NOISL, 3 days, 10 deg min elevation, 250k data rate
python3 -m tvgutil.tools.create_rr_tvg --rr 0 --duration 259200 --minelev 10 --uplinkrate 250000 --downlinkrate 250000 --islrange 1000 --islrate 250000 --output "$SCENARIO_ALIAS.02.ptvg.json" "$SCENARIO_IN"

python3 -m tvgutil.tools.convert_ptvg_to_ftvg --probabilistic --output "$SCENARIO_ALIAS.02.ftvg_accurate.json" "$SCENARIO_ALIAS.02.ptvg.json"

# generate "rounded" TVG to adapt to 1s interval of ONE
python round_contacts.py "$SCENARIO_ALIAS.02.ftvg_accurate.json" -o "$SCENARIO_ALIAS.02.ftvg.json"

cp "$SCENARIO_IN" "$SCENARIO_ALIAS.02.scenario.json"

# NOTE: tplan as follows:
# 1M messages, every 30 sec, for all 3 days, no deviation, only from gs to gs, lifetime one day
# python3 -m tvgutil.tools.create_transmission_plan --interval 30 --minsize 1000000 --lifetime 86400 --intervaldev 0 --maxgenpercent 100 --validsrc gs0 gs1 gs2 gs3 gs4 gs6 gs7 gs8 gs9 --validdst gs0 gs1 gs2 gs3 gs4 gs6 gs7 gs8 gs9 --output tplan.json ptvg.json
# NOTE: buffer size is 1G
