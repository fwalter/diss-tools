all:

# Directory for the "normal" virtual Python envionment
VENV := $(shell pwd)/.venv
PYTHON = $(VENV)/bin/python
GET_PIP = curl -sS https://bootstrap.pypa.io/get-pip.py | $(PYTHON)
PIP = $(VENV)/bin/pip
GR_OOT_BUILD_DIR := $(VENV)/gr-oot-build

.PHONY: virtualenv-without-gr
virtualenv-without-gr:
	@echo "=> Creating Python 3 virtualenv in $(VENV) ..."
	rm --recursive --force --one-file-system --preserve-root "$(VENV)"
	python3 -m venv --without-pip --system-site-packages "$(VENV)"
	@echo "=> Installing latest pip package ..."
	$(GET_PIP)
	@echo "=> Installing Python dependencies ..."
	$(PIP) install -r ./requirements.txt
	@echo "=> Installing capacityutil Python dependencies ..."
	$(PIP) install -r ./capacityutil/requirements.txt
	@echo "=> Installing aiocgrsim Python dependencies ..."
	$(PIP) install -r ./aiocgrsim/requirements.txt
	@echo "=> Installing diss Python dependencies ..."
	$(PIP) install -r ./diss/requirements.txt
	@echo "=> Installing tvgutil into venv ..."
	$(PIP) install -e ./dtn-tvg-util
	@echo "=> Installing aiodtnsim into venv ..."
	$(PIP) install -e ./aiodtnsim
	@echo "=> Installing pydtnsim into venv ..."
	$(PIP) install -e ./pydtnsim
	@echo "=> Installing capacityutil into venv ..."
	$(PYTHON) ./capacityutil/install.py
	@echo "=> Installing predictutil into venv ..."
	$(PYTHON) ./predictutil/install.py
	@echo "=> Installing aiocgrsim into venv ..."
	$(PYTHON) ./aiocgrsim/install.py
	@echo "=> Installing linters ..."
	$(PIP) install flake8 mypy pylint
	@echo "=> To activate the virtualenv: source '$(VENV)/bin/activate'"

.PHONY: virtualenv
virtualenv: virtualenv-without-gr
	@echo "=> Building and installing gr-is_sync ..."
	# NOTE #1: If not specifying CMAKE_PREFIX_PATH, cmake will search $PATH to determine the include path.
	# If you have /bin before /usr/bin in your $PATH, cmake will determine /include as the main system+
	# include directory. On most systems, this does not exist - and the build fails.
	# NOTE #2: CMAKE_INSTALL_PREFIX is intentionally empty so make install does not build any directory
	# structure within DESTDIR, but installs into $DESTDIR/{bin,lib,include,...}.
	mkdir -p "$(GR_OOT_BUILD_DIR)/gr-is_sync/build" && cd "$(GR_OOT_BUILD_DIR)/gr-is_sync/build" && cmake -DCMAKE_PREFIX_PATH=/usr -DCMAKE_INSTALL_PREFIX= ../../../../gr38_oot_modules/gr-is_sync && make && make install DESTDIR=../../..
	@echo "=> Building and installing gr-ogg_source ..."
	mkdir -p "$(GR_OOT_BUILD_DIR)/gr-ogg_source/build" && cd "$(GR_OOT_BUILD_DIR)/gr-ogg_source/build" && cmake -DCMAKE_PREFIX_PATH=/usr -DCMAKE_INSTALL_PREFIX= ../../../../gr38_oot_modules/gr-ogg_source && make && make install DESTDIR=../../..
	@echo "=> Cleaning up GR build directory ..."
	rm --recursive --force --one-file-system --preserve-root "$(GR_OOT_BUILD_DIR)"
	@echo "=> Adding GR SWIG and GRC components to path inside venv ..."
	# Move things installed with usr/ prefix to venv root dir (seems this happens for some SWIG components)
	cp -rv $(VENV)/usr/* $(VENV)/
	rm --recursive --force --one-file-system --preserve-root $(VENV)/usr
	# As gr-satnogs has to be contained in the LD_LIBRARY_PATH, we need an ugly hack in the activate script of the venv.
	echo 'export LD_LIBRARY_PATH="$$LD_LIBRARY_PATH:$(VENV)/lib"' >> "$(VENV)/bin/activate"
	echo 'export GRC_BLOCKS_PATH="$$GRC_BLOCKS_PATH:$(VENV)/share/gnuradio/grc/blocks"' >> "$(VENV)/bin/activate"
	echo 'eval "$$(declare -f deactivate | sed "s/deactivate/orig_deactivate/g")"' >> "$(VENV)/bin/activate"
	echo 'deactivate() { orig_deactivate; export LD_LIBRARY_PATH="$$(echo "$$LD_LIBRARY_PATH" | sed -e "s#:$(VENV)/lib##g")"; export GRC_BLOCKS_PATH="$$(echo "$$GRC_BLOCKS_PATH" | sed -e "s#:$(VENV)/share/gnuradio/grc/blocks##g")"; unset -f deactivate; }' >> "$(VENV)/bin/activate"
	rm -f "$(VENV)/bin/"{activate_this.py,activate.csh,activate.fish}
	@echo "=> To activate the virtualenv: source '$(VENV)/bin/activate'"
