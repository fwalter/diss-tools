#!/usr/bin/env python
# encoding: utf-8

from __future__ import (
    unicode_literals,
    print_function,
    absolute_import,
    division,
)

import os

import numpy as np
import scipy
import scipy.signal
import scipy.stats

from capacityutil.noaa_snr_from_ogg import noaa_snr_from_ogg
from capacityutil.noaa_apt_snr_from_ogg import noaa_apt_snr_from_ogg

DEFAULT_SAMPLE_RATE = 1
SNR_OUTPUT_SAMPLE_RATE = 200
MIN_SYNC_CORRELATION_VALUE = 33


def load_snr_file(path, sample_rate, rate=DEFAULT_SAMPLE_RATE):
    raw = scipy.fromfile(path, dtype=scipy.float32)
    samples_per_sec = sample_rate * rate
    filtered = []
    for i in range(int(len(raw) / samples_per_sec)):
        filtered.append(
            sum(raw[i * samples_per_sec:(i + 1) * samples_per_sec]) /
            samples_per_sec
        )
    return filtered


def determine_snrs(ogg_file, dest_path, force=False, rate=DEFAULT_SAMPLE_RATE,
                   input_sample_rate=48000):
    snr_file = os.path.abspath(os.path.join(
        dest_path,
        os.path.basename(ogg_file) + ".snr.dat",
    ))

    if not os.path.isfile(snr_file) or force:
        if os.path.isfile(snr_file):
            os.remove(snr_file)

        tb = noaa_snr_from_ogg(
            input_file_path=str(ogg_file),
            output_file_path=str(snr_file),
            input_sample_rate=input_sample_rate,
        )
        assert tb.output_sample_rate == SNR_OUTPUT_SAMPLE_RATE
        tb.start()
        tb.wait()

    return load_snr_file(snr_file, SNR_OUTPUT_SAMPLE_RATE, rate)


def identify_best_peaks(samples, kernel, xvalues, npeaks, prominence=1e-5):
    v = kernel(xvalues)
    peaks, properties = scipy.signal.find_peaks(v, prominence=prominence)
    best_peaks_indices = sorted(
        range(len(peaks)),
        key=lambda i: properties["prominences"][i],
        reverse=True,
    )
    return [xvalues[peaks[best_peaks_indices[i]]] for i in range(npeaks)]


def get_gaussian_bimodal_means(samples, bin_count=512,
                               step2_granularity=1.e-5):
    base_min = min(samples)
    base_max = max(samples)
    base_granularity = (base_max - base_min) / bin_count
    base_range = np.arange(base_min, base_max, base_granularity)
    kernel = scipy.stats.gaussian_kde(samples)

    try:
        peaks = identify_best_peaks(samples, kernel, base_range, 2)
    except IndexError:
        return None, None

    peak1 = min(peaks)
    peak2 = max(peaks)

    p1range = np.arange(
        peak1 - base_granularity,
        peak1 + base_granularity,
        step2_granularity,
    )
    p2range = np.arange(
        peak2 - base_granularity,
        peak2 + base_granularity,
        step2_granularity,
    )

    try:
        peak1 = identify_best_peaks(samples, kernel, p1range, 1)[0]
        peak2 = identify_best_peaks(samples, kernel, p2range, 1)[0]
    except IndexError:
        return None, None

    return peak1, peak2


def decode_apt_image(ogg_file, dest_path, force=False, sample_rate=48000):
    raw_file = os.path.abspath(os.path.join(
        dest_path,
        os.path.basename(ogg_file) + ".aptraw.dat",
    ))
    sync_file = os.path.abspath(os.path.join(
        dest_path,
        os.path.basename(ogg_file) + ".synca.dat",
    ))

    if not os.path.isfile(raw_file) or not os.path.isfile(sync_file) or force:
        if os.path.isfile(raw_file):
            os.remove(raw_file)
        if os.path.isfile(sync_file):
            os.remove(sync_file)

        tb = noaa_apt_snr_from_ogg(
            input_file_path=str(ogg_file),
            output_raw_file_path=str(raw_file),
            output_sync_a_file_path=str(sync_file),
            input_sample_rate=sample_rate,
        )
        tb.start()
        tb.wait()

    raw = scipy.fromfile(raw_file, dtype=scipy.float32)
    sync_a = scipy.fromfile(sync_file, dtype=scipy.uint32)
    assert len(raw) == len(sync_a)

    lines = int(len(raw) / 2080)
    sync_a_2d = sync_a[:2080 * lines].reshape(-1, 2080)
    raw_2d = raw[:2080 * lines].reshape(-1, 2080)

    offsets = [sync_a_2d[i].argmax() for i in range(lines)]
    correlation_values = [sync_a_2d[i].max() for i in range(lines)]

    # smooth offsets and interpolate some missing values
    WND = 15  # moving average window
    MAX_SHIFT = 3  # maximum shift wrt. median to consider a value an outlier
    MIN_VALUES = 5  # min. values remaining in WND to calculate mean
    has_sync = [0 for _ in range(lines)]
    offsets_new = [None for _ in range(lines)]
    for i in range(lines - WND):
        wnd = offsets[i:i + WND]
        # filter outliers
        median = np.median(wnd)
        filtered_wnd = [
            off
            for off, corrval in zip(wnd, correlation_values[i:i + WND])
            if (
                abs(off - median) <= MAX_SHIFT and
                corrval >= MIN_SYNC_CORRELATION_VALUE
            )
        ]
        if len(filtered_wnd) >= MIN_VALUES:
            # set middle value in window
            center_i = i + (WND // 2)
            offsets_new[center_i] = np.median(filtered_wnd)
            has_sync[center_i] = int(
                abs(offsets[center_i] - median) <= MAX_SHIFT
            )

    # fill in missing offsets - simply use the next-best value
    for i in range(lines):
        if offsets_new[i] is not None:
            continue
        index_list = [
            (j, abs(i - j))
            for j, v in enumerate(offsets_new)
            if v is not None
        ]
        if not index_list:
            offsets_new[i] = offsets[i]
            continue
        index, index_offset = min(index_list, key=lambda x: x[1])
        offsets_new[i] = offsets_new[index]

    # build "synced" image out of raw data
    # NOTE: we skip a line at start and one at end to prevent dealing with
    #       negative offsets for which we don't have data
    new_lines = []
    scaling_samples = []
    last_off = 0
    for i in range(1, lines - 1):
        # image starts with sync_a, well, two pixels before that to ensure
        # we get all of sync_a in the rect of interest
        base_off = i * 2080 + (int(round(offsets_new[i])) - 40) - 2
        line = raw[base_off:base_off + 2080]
        new_lines.append(line)
        # for lines where we have sync, get the max and min (b/w) from pattern
        if has_sync[i]:
            sync_a = line[2:41]
            sync_b = line[1040 + 2:1040 + 41]
            scaling_samples.append(sync_a)
            scaling_samples.append(sync_b)
        # sanity check: offset has to be advancing
        assert base_off > last_off
        last_off = base_off
    has_sync = np.array(has_sync[1:-1])
    offsets_new = np.array(offsets_new[1:-1])
    correlation_values = np.array(correlation_values[1:-1])

    assert len(new_lines) == len(has_sync)
    assert len(new_lines) == len(correlation_values)

    if scaling_samples:
        scaling_samples = np.array(scaling_samples).flatten()
    else:
        scaling_samples = raw
    pattern_min, pattern_max = get_gaussian_bimodal_means(scaling_samples)

    if pattern_min is None or pattern_max is None:
        # pattern_min = min(scaling_samples)
        # pattern_max = max(scaling_samples)
        raise ValueError("cannot scale image from {}, no min/max found".format(
            ogg_file
        ))

    # scale values to range determined by pattern min/max
    new_lines = np.array(new_lines)
    new_lines -= pattern_min
    new_lines *= 1 / (pattern_max - pattern_min)
    # NOTE: clipping _may_ be performed as follows:
    # new_lines = np.clip(new_lines, 0, 1)

    # returned tuple is as follows: (
    #     unsynced,
    #     synced,
    #     sync_available[line],
    #     max_corr_offset[line],
    #     max_corr_value[line],
    # )
    return raw_2d, new_lines, has_sync, offsets_new, correlation_values
