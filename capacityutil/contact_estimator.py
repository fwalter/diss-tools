#!/usr/bin/env python
# encoding: utf-8

from __future__ import (
    unicode_literals,
    print_function,
    absolute_import,
    division,
)

import ephem
import datetime
import math
import os

import numpy as np
import scipy

DEFAULT_SAMPLE_RATE = 1
SNR_OUTPUT_SAMPLE_RATE = 200


def get_sat_and_observer(sat_name, tle, lat, lon):
    sat = ephem.readtle(str(sat_name), *tle)
    obs = ephem.Observer()
    obs.lat = str(lat)  # pylint: disable=assigning-non-slot
    obs.lon = str(lon)  # pylint: disable=assigning-non-slot
    return sat, obs


def compute(func, satobj, gsobj, frequency_hz, unixtime):
    gsobj.date = datetime.datetime.utcfromtimestamp(unixtime)
    satobj.compute(gsobj)
    return func(satobj, frequency_hz)


def compute_over_contact(func, sat, obs, frequency_hz, starttime, endtime,
                         sample_rate=DEFAULT_SAMPLE_RATE):
    for ts in np.arange(starttime, endtime, 1 / sample_rate):
        yield compute(func, sat, obs, frequency_hz, ts)


def _get_elevation(satobj, _):
    return satobj.alt * 180 / math.pi


def estimate_atmospheric_loss_db(satobj, frequency_hz):
    elevation_deg = satobj.alt * 180 / math.pi
    # estimated using exponential regression of formula from Ippolito tables
    # https://books.google.de/books?id=OiAyBwAAQBAJ&lpg=PT11&ots=6K_IcweFyr&dq=%22Radiowave%20Propagation%20in%20Satellite%20Communications%22&lr&hl=de&pg=PT54#v=onepage&q=%22Radiowave%20Propagation%20in%20Satellite%20Communications%22&f=false
    atmospheric_loss_dbw = 10.1675 * math.exp(-0.2453163 * elevation_deg)
    # rough estimation
    ionospheric_loss_dbw = 0.7 if frequency_hz < 430e6 else (
        0.4 if frequency_hz < 2e9 else 0.1)
    return atmospheric_loss_dbw + ionospheric_loss_dbw


def estimate_path_loss_db(satobj, frequency_hz):
    distance_meters = satobj.range
    return 22 + 20 * math.log10(
        distance_meters * frequency_hz / ephem.c
    )


def get_gs_pointing_error_deg(satobj, _):
    elevation_deg = _get_elevation(satobj, _)
    return 90 - elevation_deg if elevation_deg >= 0 else 90 + elevation_deg


def get_sat_pointing_error_deg(satobj, _):
    altitude_m = satobj.elevation
    distance_m = satobj.range
    EARTH_RADIUS_M = ephem.earth_radius
    if distance_m < altitude_m:
        return 0
    return math.acos(
        (distance_m * distance_m + (EARTH_RADIUS_M + altitude_m) ** 2 -
         EARTH_RADIUS_M * EARTH_RADIUS_M) /
        (2 * distance_m * (EARTH_RADIUS_M + altitude_m))
    ) / math.pi * 180


def estimate_sat_pointing_loss_dipole_db(satobj, _):
    """Get pointing loss for a dipole antenna at 90 degree alignment."""
    pointing_error_deg = get_sat_pointing_error_deg(satobj, _)
    if not pointing_error_deg:
        return 0
    return -10 * math.log10(math.cos(pointing_error_deg / 180 * math.pi))


def estimate_sat_pointing_loss_directional_90deg_db(satobj, _):
    """Get pointing loss for a directional antenna at 90 degree alignment."""
    # NOTE: This is for an example beam width (NOAA is helical 63 deg).
    beam_width_deg = 63
    pointing_error_deg = get_sat_pointing_error_deg(satobj, _)
    if not pointing_error_deg:
        return 0
    tmp = 2 * (pointing_error_deg * (79.76 / beam_width_deg))
    return -10 * math.log10(
        3282.81 * ((math.sin(tmp * math.pi / 180) ** 2) / (tmp ** 2))
    )


def estimate_gs_pointing_loss_dipole_db(satobj, _):
    """Get pointing loss for a dipole antenna at 90 degree alignment."""
    pointing_error_deg = get_gs_pointing_error_deg(satobj, _)
    return -10 * math.log10(math.cos(pointing_error_deg / 180 * math.pi))


def estimate_gs_pointing_loss_turnstile_db(satobj, _):
    """Get pointing loss for a turnstile antenna at 90 degree alignment."""
    # uses: http://www.hep.princeton.edu/~mcdonald/examples/turnstile.pdf
    # with setting E^2 proportional to P => -10 * log10(E^2 / Const.) is loss
    pointing_error_deg = get_gs_pointing_error_deg(satobj, _)
    pointing_error_rad = pointing_error_deg / 180 * math.pi
    return -10 * math.log10(
        math.sqrt((1 + (math.cos(pointing_error_rad) *
                        math.cos(pointing_error_rad))) /
                  2)
    )


def estimate_gs_pointing_loss_directional_90deg_db(satobj, _):
    """Get pointing loss for a directional antenna at 90 degree alignment."""
    # NOTE: This is for an example beam width (3-element Yagi).
    # The beam width is determined by reflector size / Yagi elements.
    beam_width_deg = 67
    pointing_error_deg = get_gs_pointing_error_deg(satobj, _)
    tmp = 2 * (pointing_error_deg * (79.76 / beam_width_deg))
    return -10 * math.log10(
        3282.81 * ((math.sin(tmp * math.pi / 180) ** 2) / (tmp ** 2))
    )


def estimate_pointing_loss_db(satobj, _):
    return (
        estimate_sat_pointing_loss_directional_90deg_db(satobj, _) +
        estimate_gs_pointing_loss_turnstile_db(satobj, _)
    )


def estimate_losses(satobj, frequency_hz):
    atmospheric_loss_db = estimate_atmospheric_loss_db(
        satobj,
        frequency_hz,
    )
    path_loss_db = estimate_path_loss_db(
        satobj,
        frequency_hz,
    )
    # NOTE: currently neglects GS pointing loss
    # TODO: pointing loss (+ whether sat/gs) as cli param
    pointing_loss_db = estimate_pointing_loss_db(
        satobj,
        frequency_hz,
    )
    loss_db = atmospheric_loss_db + path_loss_db + pointing_loss_db
    return loss_db
