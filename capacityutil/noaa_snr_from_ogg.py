#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#
# SPDX-License-Identifier: GPL-3.0
#
# GNU Radio Python Flow Graph
# Title: NOAA SNR from OGG
# Author: Felix Walter
# Description: Get the SNR of NOAA transmissions from an OGG file
# GNU Radio version: 3.8.0.0

from gnuradio import blocks
from gnuradio import filter
from gnuradio.filter import firdes
from gnuradio import gr
import sys
import signal
from argparse import ArgumentParser
from gnuradio.eng_arg import eng_float, intx
from gnuradio import eng_notation
import ogg_source

class noaa_snr_from_ogg(gr.top_block):

    def __init__(self, input_file_path="in.ogg", input_sample_rate=48000, output_file_path="satnogs_cnr.dat"):
        gr.top_block.__init__(self, "NOAA SNR from OGG")

        ##################################################
        # Parameters
        ##################################################
        self.input_file_path = input_file_path
        self.input_sample_rate = input_sample_rate
        self.output_file_path = output_file_path

        ##################################################
        # Variables
        ##################################################
        self.snr_filter_rate = snr_filter_rate = 20
        self.output_sample_rate = output_sample_rate = 200

        ##################################################
        # Blocks
        ##################################################
        self.rational_resampler_xxx_0_0 = filter.rational_resampler_fff(
                interpolation=output_sample_rate,
                decimation=input_sample_rate,
                taps=None,
                fractional_bw=None)
        self.rational_resampler_xxx_0 = filter.rational_resampler_fff(
                interpolation=output_sample_rate,
                decimation=input_sample_rate,
                taps=None,
                fractional_bw=None)
        self.ogg_source_ogg_source_0 = ogg_source.ogg_source(input_file_path, 1, False)
        self.blocks_sub_xx_0 = blocks.sub_ff(1)
        self.blocks_nlog10_ff_0_0 = blocks.nlog10_ff(10, 1, 0)
        self.blocks_nlog10_ff_0 = blocks.nlog10_ff(10, 1, 0)
        self.blocks_multiply_xx_0_0 = blocks.multiply_vff(1)
        self.blocks_multiply_xx_0 = blocks.multiply_vff(1)
        self.blocks_file_sink_0 = blocks.file_sink(gr.sizeof_float*1, output_file_path, False)
        self.blocks_file_sink_0.set_unbuffered(False)
        self.blocks_abs_xx_0_0 = blocks.abs_ff(1)
        self.blocks_abs_xx_0 = blocks.abs_ff(1)
        self.band_pass_filter_0_0_0 = filter.fir_filter_fff(
            1,
            firdes.band_pass(
                1,
                input_sample_rate,
                4.4e3,
                4.6e3,
                200,
                firdes.WIN_HAMMING,
                6.76))
        self.band_pass_filter_0_0 = filter.fir_filter_fff(
            1,
            firdes.band_pass(
                1,
                input_sample_rate,
                2.3e3,
                2.5e3,
                200,
                firdes.WIN_HAMMING,
                6.76))



        ##################################################
        # Connections
        ##################################################
        self.connect((self.band_pass_filter_0_0, 0), (self.blocks_abs_xx_0, 0))
        self.connect((self.band_pass_filter_0_0_0, 0), (self.blocks_abs_xx_0_0, 0))
        self.connect((self.blocks_abs_xx_0, 0), (self.blocks_multiply_xx_0, 1))
        self.connect((self.blocks_abs_xx_0, 0), (self.blocks_multiply_xx_0, 0))
        self.connect((self.blocks_abs_xx_0_0, 0), (self.blocks_multiply_xx_0_0, 0))
        self.connect((self.blocks_abs_xx_0_0, 0), (self.blocks_multiply_xx_0_0, 1))
        self.connect((self.blocks_multiply_xx_0, 0), (self.rational_resampler_xxx_0, 0))
        self.connect((self.blocks_multiply_xx_0_0, 0), (self.rational_resampler_xxx_0_0, 0))
        self.connect((self.blocks_nlog10_ff_0, 0), (self.blocks_sub_xx_0, 0))
        self.connect((self.blocks_nlog10_ff_0_0, 0), (self.blocks_sub_xx_0, 1))
        self.connect((self.blocks_sub_xx_0, 0), (self.blocks_file_sink_0, 0))
        self.connect((self.ogg_source_ogg_source_0, 0), (self.band_pass_filter_0_0, 0))
        self.connect((self.ogg_source_ogg_source_0, 0), (self.band_pass_filter_0_0_0, 0))
        self.connect((self.rational_resampler_xxx_0, 0), (self.blocks_nlog10_ff_0, 0))
        self.connect((self.rational_resampler_xxx_0_0, 0), (self.blocks_nlog10_ff_0_0, 0))

    def get_input_file_path(self):
        return self.input_file_path

    def set_input_file_path(self, input_file_path):
        self.input_file_path = input_file_path

    def get_input_sample_rate(self):
        return self.input_sample_rate

    def set_input_sample_rate(self, input_sample_rate):
        self.input_sample_rate = input_sample_rate
        self.band_pass_filter_0_0.set_taps(firdes.band_pass(1, self.input_sample_rate, 2.3e3, 2.5e3, 200, firdes.WIN_HAMMING, 6.76))
        self.band_pass_filter_0_0_0.set_taps(firdes.band_pass(1, self.input_sample_rate, 4.4e3, 4.6e3, 200, firdes.WIN_HAMMING, 6.76))

    def get_output_file_path(self):
        return self.output_file_path

    def set_output_file_path(self, output_file_path):
        self.output_file_path = output_file_path
        self.blocks_file_sink_0.open(self.output_file_path)

    def get_snr_filter_rate(self):
        return self.snr_filter_rate

    def set_snr_filter_rate(self, snr_filter_rate):
        self.snr_filter_rate = snr_filter_rate

    def get_output_sample_rate(self):
        return self.output_sample_rate

    def set_output_sample_rate(self, output_sample_rate):
        self.output_sample_rate = output_sample_rate


def argument_parser():
    description = 'Get the SNR of NOAA transmissions from an OGG file'
    parser = ArgumentParser(description=description)
    return parser


def main(top_block_cls=noaa_snr_from_ogg, options=None):
    if options is None:
        options = argument_parser().parse_args()
    tb = top_block_cls()

    def sig_handler(sig=None, frame=None):
        tb.stop()
        tb.wait()
        sys.exit(0)

    signal.signal(signal.SIGINT, sig_handler)
    signal.signal(signal.SIGTERM, sig_handler)

    tb.start()
    tb.wait()


if __name__ == '__main__':
    main()
