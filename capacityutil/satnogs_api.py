#!/usr/bin/env python
# encoding: utf-8

from __future__ import (
    unicode_literals,
    print_function,
    absolute_import,
    division,
)

import os
import re
import shutil
import calendar
import logging

import requests
import dateutil.parser
import urllib3.exceptions

NETWORK_URL = "https://network.satnogs.org"
DEV_NETWORK_URL = "https://network-dev.satnogs.org"
DB_URL = "https://db.satnogs.org"

DEFAULT_RETRIES = 3
DEFAULT_TIMEOUT = 5

_TLE_REGEX = re.compile(
    r"<pre>"
    r"(1 [\w\s\.\-\+]+)\s*(<br>|\n)\s*(2 [\w\s\.\-\+]+)"
    r"<\/pre>"
)
_OGG_REGEX = re.compile(
    r"data\-audio=\"([\w\.\-\/\_\:]+\/)([\w\.\-\_]+)\""
)

logger = logging.getLogger(__name__)


def _create_session(retries):
    session = requests.Session()
    adapter = requests.adapters.HTTPAdapter(max_retries=retries)
    session.mount("http://", adapter)
    session.mount("https://", adapter)
    return session


def fetch_tle_for_observation(observation_id, session=None,
                              network_url=NETWORK_URL, timeout=DEFAULT_TIMEOUT,
                              retries=DEFAULT_RETRIES):
    session = session or _create_session(retries)
    r = session.get("{satnogs_url}/observations/{observation_id}/".format(
        satnogs_url=network_url,
        observation_id=observation_id,
    ), timeout=timeout)
    if not r.ok:
        raise requests.exceptions.HTTPError(
            "request to SatNOGS failed - " + str(r.status_code)
        )
    match = _TLE_REGEX.search(r.text)
    return match.group(1), match.group(3)


def fetch_observation_info(observation_id, session=None,
                           network_url=NETWORK_URL, timeout=DEFAULT_TIMEOUT,
                           retries=DEFAULT_RETRIES):
    session = session or _create_session(retries)
    r = session.get("{satnogs_url}/api/observations/{observation_id}/".format(
        satnogs_url=network_url,
        observation_id=observation_id,
    ), timeout=timeout)
    if not r.ok:
        raise requests.exceptions.HTTPError(
            "request to SatNOGS API failed - " + str(r.status_code)
        )
    return r.json()


def fetch_satellite_info(norad_cat_id, session=None,
                         db_url=DB_URL, timeout=DEFAULT_TIMEOUT,
                         retries=DEFAULT_RETRIES):
    session = session or _create_session(retries)
    r = session.get("{satnogs_db_url}/api/satellites/{norad_cat_id}/".format(
        satnogs_db_url=db_url,
        norad_cat_id=norad_cat_id,
    ), timeout=timeout)
    if not r.ok:
        raise requests.exceptions.HTTPError(
            "request to SatNOGS DB API failed - " + str(r.status_code)
        )
    return r.json()


def fetch_transmitter_info(transmitter_uuid, session=None,
                           db_url=DB_URL, timeout=DEFAULT_TIMEOUT,
                           retries=DEFAULT_RETRIES):
    session = session or _create_session(retries)
    r = session.get("{satnogs_db_url}/api/transmitters/{trans_id}/".format(
        satnogs_db_url=db_url,
        trans_id=transmitter_uuid,
    ), timeout=timeout)
    if not r.ok:
        raise requests.exceptions.HTTPError(
            "request to SatNOGS DB API failed - " + str(r.status_code)
        )
    return r.json()


def fetch_ogg_recording(observation_id, session=None, network_url=NETWORK_URL,
                        timeout=DEFAULT_TIMEOUT, download=True,
                        download_retries=DEFAULT_RETRIES, dest_path=None,
                        retries=DEFAULT_RETRIES):
    session = session or _create_session(retries)
    r = session.get("{satnogs_url}/observations/{observation_id}/".format(
        satnogs_url=network_url,
        observation_id=observation_id,
    ), timeout=timeout)
    if not r.ok:
        raise requests.exceptions.HTTPError(
            "request to SatNOGS failed - " + str(r.status_code)
        )
    match = _OGG_REGEX.search(r.text)
    file_url = match.group(1) + match.group(2)
    if not file_url.startswith("http"):
        file_url = network_url + file_url
    file_name = match.group(2)
    if not dest_path:
        dest_path = os.getcwd()
    dest_filepath = os.path.abspath(os.path.join(dest_path, file_name))
    if not download:
        return file_url
    if not os.path.isfile(dest_filepath):
        for retry in range(download_retries):
            logger.info("Downloading OGG file to {} (try {} of {})".format(
                dest_filepath,
                retry + 1,
                download_retries,
            ))
            try:
                r = session.get(file_url, stream=True, timeout=timeout)
                if not r.ok:
                    raise requests.exceptions.HTTPError(
                        "request to SatNOGS failed - " + str(r.status_code)
                    )
                with open(dest_filepath, "wb") as f:
                    r.raw.decode_content = True
                    shutil.copyfileobj(r.raw, f)
                break
            except urllib3.exceptions.ReadTimeoutError:
                continue
    return dest_filepath


def fetch_observations(gs_id, sat_norad_cat_id, max_count, date_range=None,
                       session=None, network_url=NETWORK_URL,
                       retries=DEFAULT_RETRIES, timeout=DEFAULT_TIMEOUT):
    session = session or _create_session(retries)
    result = []
    url = ("{satnogs_url}/api/observations/" +
           "?satellite__norad_cat_id={norad_cat_id}&ground_station={gs_id}" +
           "&start={start}&end={end}").format(
        satnogs_url=network_url,
        norad_cat_id=sat_norad_cat_id,
        gs_id=gs_id,
        start=(date_range[0] if date_range else ""),
        end=(date_range[1] if date_range else ""),
    )
    # depaginate
    while not max_count or len(result) < max_count:
        r = session.get(url, timeout=timeout)
        if not r.ok:
            raise requests.exceptions.HTTPError(
                "request to SatNOGS API failed - " + str(r.status_code)
            )
        result += [
            x for x in r.json() if (
                x["vetted_status"] in ("good", "bad", "unknown") and
                (x["payload"] is not None or x["archive_url"] is not None)
            )
        ]
        if "next" not in r.links:
            break
        url = r.links["next"]["url"]
    return result[0:max_count] if max_count else result


def fetch_observation_data(observation_id, session=None,
                           network_url=NETWORK_URL, db_url=DB_URL,
                           retries=DEFAULT_RETRIES, timeout=DEFAULT_TIMEOUT,
                           include_audio=True, download=True,
                           download_path=None):
    session = session or _create_session(retries)
    data = fetch_observation_info(
        observation_id,
        session,
        network_url,
        timeout,
    )
    data["start_unix"] = calendar.timegm(
        dateutil.parser.parse(data["start"]).utctimetuple()
    )
    data["end_unix"] = calendar.timegm(
        dateutil.parser.parse(data["end"]).utctimetuple()
    )
    data["tle"] = fetch_tle_for_observation(
        observation_id,
        session,
        network_url,
        timeout,
    )
    data["satellite_info"] = fetch_satellite_info(
        data["norad_cat_id"],
        session,
        db_url,
        timeout,
    )
    data["transmitter_info"] = fetch_transmitter_info(
        data["transmitter"],
        session,
        db_url,
        timeout,
    )
    if include_audio:
        data["file"] = fetch_ogg_recording(
            observation_id,
            session,
            network_url,
            timeout,
            download,
            retries,
            download_path,
        )
    return data
