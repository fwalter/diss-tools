options:
  parameters:
    author: Felix Walter
    category: '[GRC Hier Blocks]'
    cmake_opt: ''
    comment: ''
    copyright: ''
    description: Get the SNR of NOAA transmissions from an OGG file
    gen_cmake: 'On'
    gen_linking: dynamic
    generate_options: no_gui
    hier_block_src_path: '.:'
    id: noaa_snr_from_ogg
    max_nouts: '0'
    output_language: python
    placement: (0,0)
    qt_qss_theme: ''
    realtime_scheduling: ''
    run: 'True'
    run_command: /home/felix/.virtualenvs/satnogs/bin/python -u {filename}
    run_options: run
    sizing_mode: fixed
    thread_safe_setters: ''
    title: NOAA SNR from OGG
    window_size: 2048,1080
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [7, 3]
    rotation: 0
    state: enabled

blocks:
- name: output_sample_rate
  id: variable
  parameters:
    comment: Sample rate in data written to file sink
    value: '200'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [271, 450]
    rotation: 0
    state: enabled
- name: snr_filter_rate
  id: variable
  parameters:
    comment: Rate for power moving average
    value: '20'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [502, 450]
    rotation: 0
    state: enabled
- name: band_pass_filter_0_0
  id: band_pass_filter
  parameters:
    affinity: ''
    alias: ''
    beta: '6.76'
    comment: ''
    decim: '1'
    gain: '1'
    high_cutoff_freq: 2.5e3
    interp: '1'
    low_cutoff_freq: 2.3e3
    maxoutbuf: '0'
    minoutbuf: '0'
    samp_rate: input_sample_rate
    type: fir_filter_fff
    width: '200'
    win: firdes.WIN_HAMMING
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [247, 51]
    rotation: 0
    state: enabled
- name: band_pass_filter_0_0_0
  id: band_pass_filter
  parameters:
    affinity: ''
    alias: ''
    beta: '6.76'
    comment: 'The noise needs to be filtered with the same

      band pass width as the signal.'
    decim: '1'
    gain: '1'
    high_cutoff_freq: 4.6e3
    interp: '1'
    low_cutoff_freq: 4.4e3
    maxoutbuf: '0'
    minoutbuf: '0'
    samp_rate: input_sample_rate
    type: fir_filter_fff
    width: '200'
    win: firdes.WIN_HAMMING
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [247, 219]
    rotation: 0
    state: enabled
- name: blocks_abs_xx_0
  id: blocks_abs_xx
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    maxoutbuf: '0'
    minoutbuf: '0'
    type: float
    vlen: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [481, 111]
    rotation: 0
    state: enabled
- name: blocks_abs_xx_0_0
  id: blocks_abs_xx
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    maxoutbuf: '0'
    minoutbuf: '0'
    type: float
    vlen: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [461, 279]
    rotation: 0
    state: enabled
- name: blocks_file_sink_0
  id: blocks_file_sink
  parameters:
    affinity: ''
    alias: ''
    append: 'False'
    comment: ''
    file: output_file_path
    type: float
    unbuffered: 'False'
    vlen: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1238, 285]
    rotation: 0
    state: enabled
- name: blocks_multiply_xx_0
  id: blocks_multiply_xx
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    maxoutbuf: '0'
    minoutbuf: '0'
    num_inputs: '2'
    type: float
    vlen: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [650, 95]
    rotation: 0
    state: enabled
- name: blocks_multiply_xx_0_0
  id: blocks_multiply_xx
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    maxoutbuf: '0'
    minoutbuf: '0'
    num_inputs: '2'
    type: float
    vlen: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [625, 263]
    rotation: 0
    state: enabled
- name: blocks_nlog10_ff_0
  id: blocks_nlog10_ff
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    k: '0'
    maxoutbuf: '0'
    minoutbuf: '0'
    n: '10'
    vlen: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1004, 61]
    rotation: 0
    state: enabled
- name: blocks_nlog10_ff_0_0
  id: blocks_nlog10_ff
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    k: '0'
    maxoutbuf: '0'
    minoutbuf: '0'
    n: '10'
    vlen: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1000, 216]
    rotation: 0
    state: enabled
- name: blocks_sub_xx_0
  id: blocks_sub_xx
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    maxoutbuf: '0'
    minoutbuf: '0'
    num_inputs: '2'
    type: float
    vlen: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1109, 143]
    rotation: 0
    state: enabled
- name: input_file_path
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: ''
    short_id: ''
    type: ''
    value: '"in.ogg"'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [7, 219]
    rotation: 180
    state: enabled
- name: input_sample_rate
  id: parameter
  parameters:
    alias: ''
    comment: Sample rate coming from the data source
    hide: none
    label: ''
    short_id: ''
    type: ''
    value: '48000'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [23, 450]
    rotation: 0
    state: enabled
- name: note_0
  id: note
  parameters:
    alias: ''
    comment: ''
    note: Signal
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [396, 23]
    rotation: 0
    state: true
- name: note_1
  id: note
  parameters:
    alias: ''
    comment: ''
    note: BW-matched noise
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [394, 350]
    rotation: 0
    state: true
- name: ogg_source_ogg_source_0
  id: ogg_source_ogg_source
  parameters:
    affinity: ''
    alias: ''
    channels: '1'
    comment: ''
    filename: input_file_path
    maxoutbuf: '0'
    minoutbuf: '0'
    repeat: 'False'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [34, 147]
    rotation: 0
    state: true
- name: output_file_path
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: ''
    short_id: ''
    type: ''
    value: '"satnogs_cnr.dat"'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1005, 315]
    rotation: 180
    state: enabled
- name: rational_resampler_xxx_0
  id: rational_resampler_xxx
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    decim: input_sample_rate
    fbw: '0'
    interp: output_sample_rate
    maxoutbuf: '0'
    minoutbuf: '0'
    taps: ''
    type: fff
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [789, 83]
    rotation: 0
    state: enabled
- name: rational_resampler_xxx_0_0
  id: rational_resampler_xxx
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    decim: input_sample_rate
    fbw: '0'
    interp: output_sample_rate
    maxoutbuf: '0'
    minoutbuf: '0'
    taps: ''
    type: fff
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [789, 251]
    rotation: 0
    state: enabled

connections:
- [band_pass_filter_0_0, '0', blocks_abs_xx_0, '0']
- [band_pass_filter_0_0_0, '0', blocks_abs_xx_0_0, '0']
- [blocks_abs_xx_0, '0', blocks_multiply_xx_0, '0']
- [blocks_abs_xx_0, '0', blocks_multiply_xx_0, '1']
- [blocks_abs_xx_0_0, '0', blocks_multiply_xx_0_0, '0']
- [blocks_abs_xx_0_0, '0', blocks_multiply_xx_0_0, '1']
- [blocks_multiply_xx_0, '0', rational_resampler_xxx_0, '0']
- [blocks_multiply_xx_0_0, '0', rational_resampler_xxx_0_0, '0']
- [blocks_nlog10_ff_0, '0', blocks_sub_xx_0, '0']
- [blocks_nlog10_ff_0_0, '0', blocks_sub_xx_0, '1']
- [blocks_sub_xx_0, '0', blocks_file_sink_0, '0']
- [ogg_source_ogg_source_0, '0', band_pass_filter_0_0, '0']
- [ogg_source_ogg_source_0, '0', band_pass_filter_0_0_0, '0']
- [rational_resampler_xxx_0, '0', blocks_nlog10_ff_0, '0']
- [rational_resampler_xxx_0_0, '0', blocks_nlog10_ff_0_0, '0']

metadata:
  file_format: 1
