import site
import os

tools_dir = os.path.abspath(os.path.join(__file__, '../..'))
try:
    site_dir = site.getsitepackages()[0]
except AttributeError:
    import glob
    site_dir = glob.glob(site.PREFIXES[0] + "/lib/python*/site-packages")[0]


def install():
    with open(os.path.join(site_dir, 'capacityutil.pth'), 'w') as fd:
        fd.write(tools_dir)


if __name__ == '__main__':
    install()
