/* -*- c++ -*- */
/*
 * Copyright 2019 gr-is_sync author.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef INCLUDED_IS_SYNC_IS_SYNC_A_H
#define INCLUDED_IS_SYNC_IS_SYNC_A_H

#include <is_sync/api.h>
#include <gnuradio/sync_block.h>

namespace gr {
  namespace is_sync {

    /*!
     * \brief <+description of block+>
     * \ingroup is_sync
     *
     */
    class IS_SYNC_API is_sync_a : virtual public gr::sync_block
    {
     public:
      typedef boost::shared_ptr<is_sync_a> sptr;

      /*!
       * \brief Return a shared_ptr to a new instance of is_sync::is_sync_a.
       *
       * To avoid accidental use of raw pointers, is_sync::is_sync_a's
       * constructor is in a private implementation
       * class. is_sync::is_sync_a::make is the public interface for
       * creating new instances.
       */
      static sptr make();
    };

  } // namespace is_sync
} // namespace gr

#endif /* INCLUDED_IS_SYNC_IS_SYNC_A_H */

