/* -*- c++ -*- */
/*
 * Copyright 2019 gr-is_sync author.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef INCLUDED_IS_SYNC_IS_SYNC_A_IMPL_H
#define INCLUDED_IS_SYNC_IS_SYNC_A_IMPL_H

#include <is_sync/is_sync_a.h>

namespace gr {
  namespace is_sync {

    class is_sync_a_impl : public is_sync_a
    {
    private:
        static const bool synca_seq[];
        // Factor exponential smoothing average,
        // which is used for sync pattern detection
        const float f_average_alpha;
        const size_t d_history_length;
        float f_average;

        int get_marker_correlation(
            const bool marker_seq[],
            size_t pos,
            const float *samples,
            const float average);

     public:
        is_sync_a_impl();
        ~is_sync_a_impl();

        // Where all the action really happens
        int work(
            int noutput_items,
            gr_vector_const_void_star &input_items,
            gr_vector_void_star &output_items);
    };

  } // namespace is_sync
} // namespace gr

#endif /* INCLUDED_IS_SYNC_IS_SYNC_A_IMPL_H */

