/* -*- c++ -*- */
/*
 * gr-satnogs: SatNOGS GNU Radio Out-Of-Tree Module
 *
 *  Copyright (C) 2017,2018 Libre Space Foundation <http://librespacefoundation.org/>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gnuradio/io_signature.h>
#include "is_sync_a_impl.h"

namespace gr {
namespace is_sync {

    // Noaa apt sync pattern A
    // (see https://sourceforge.isae.fr/attachments/download/1813/apt_synch.gif)
    const bool is_sync_a_impl::synca_seq[] = {
        false, false, false, false,
        true, true, false, false,   // Pulse 1
        true, true, false, false,   // Pulse 2
        true, true, false, false,   // Pulse 3
        true, true, false, false,   // Pulse 4
        true, true, false, false,   // Pulse 5
        true, true, false, false,   // Pulse 6
        true, true, false, false,   // Pulse 7
        false, false, false, false,
        false, false, false, false
    };

    is_sync_a::sptr is_sync_a::make()
    {
        return gnuradio::get_initial_sptr(new is_sync_a_impl());
    }

    is_sync_a_impl::is_sync_a_impl()
      : gr::sync_block("is_sync_a",
            gr::io_signature::make(1, 1, sizeof(float)),
            gr::io_signature::make(1, 1, sizeof(unsigned int))
        ),
        d_history_length(40),
        f_average_alpha(0.25),
        f_average(0.0)
    {
    }

    is_sync_a_impl::~is_sync_a_impl()
    {
    }

    int is_sync_a_impl::get_marker_correlation(
        const bool marker_seq[],
        size_t pos,
        const float *samples,
        const float average)
    {
        // Initialize counters for 'hacky' correlation
        size_t count = 0;

        for (size_t i = 0; i < 40; i++) {
            // history of previous 39 samples + current one
            // -> start 39 samples in the past
            float sample = samples[pos - 39 + i];
            // Remove DC-offset (aka. the average value of the sync pattern)
            sample = sample - average;

            // Very basic 1/0 correlation between pattern constan and history
            if ((sample > 0 && marker_seq[i]) || (sample < 0 && !synca_seq[i]))
                count++;
        }

        return count;
    }

    int is_sync_a_impl::work(
        int noutput_items,
        gr_vector_const_void_star &input_items,
        gr_vector_void_star &output_items)
    {
        const float *in = (const float *)input_items[0];
        unsigned int *out = (unsigned int *)output_items[0];

        // Structure of in[]:
        // - d_history_length many historical samples
        // - noutput_items many samples to process

        const size_t start = d_history_length - 1;
        const size_t end = noutput_items + d_history_length - 1;

        float sum = 0.0f;
        for (size_t i = start; i < end; i++)
            sum += in[i];
        const float mean = sum / (end - start);

        for (size_t i = start; i < end; i++) {

            // Get the current sample
            const float sample = in[i];

            // Update exponential smoothing average used in sync pattern detection
            // f_average = f_average_alpha * sample + (1.0 - f_average_alpha) * f_average;

            // Get correlation with the sync pattern
            out[i] = get_marker_correlation(synca_seq, i, in, mean);
        }

        // Tell gnu radio how many samples were consumed (= all)
        return noutput_items;
    }

} /* namespace is_sync */
} /* namespace gr */

