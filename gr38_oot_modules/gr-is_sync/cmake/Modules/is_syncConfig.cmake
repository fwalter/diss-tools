INCLUDE(FindPkgConfig)
PKG_CHECK_MODULES(PC_IS_SYNC is_sync)

FIND_PATH(
    IS_SYNC_INCLUDE_DIRS
    NAMES is_sync/api.h
    HINTS $ENV{IS_SYNC_DIR}/include
        ${PC_IS_SYNC_INCLUDEDIR}
    PATHS ${CMAKE_INSTALL_PREFIX}/include
          /usr/local/include
          /usr/include
)

FIND_LIBRARY(
    IS_SYNC_LIBRARIES
    NAMES gnuradio-is_sync
    HINTS $ENV{IS_SYNC_DIR}/lib
        ${PC_IS_SYNC_LIBDIR}
    PATHS ${CMAKE_INSTALL_PREFIX}/lib
          ${CMAKE_INSTALL_PREFIX}/lib64
          /usr/local/lib
          /usr/local/lib64
          /usr/lib
          /usr/lib64
          )

include("${CMAKE_CURRENT_LIST_DIR}/is_syncTarget.cmake")

INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(IS_SYNC DEFAULT_MSG IS_SYNC_LIBRARIES IS_SYNC_INCLUDE_DIRS)
MARK_AS_ADVANCED(IS_SYNC_LIBRARIES IS_SYNC_INCLUDE_DIRS)
