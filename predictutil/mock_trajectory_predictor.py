import datetime

import ephem
from scipy import optimize

from .misc import StartTimeBasedDict


class SatelliteMockTrajectoryPredictor:

    # This ensures we always get the correct remapping offset.
    REMAP_TOLERANCE_SECONDS = 600  # 10 minutes may "time" be < start

    # NOTE: The new scenario should be as long or at maximum 1 week longer than
    # the old one because of TLE validity!

    def __init__(self, orig_sat_name, orig_tle_list, gs_lat, gs_lon, gs_alt,
                 contact_remap):
        self.sat_name = orig_sat_name
        # NOTE: This is still ordered by the un-remapped timestamps from the
        # old scenario!!
        self.tle_list = StartTimeBasedDict({
            int(start_time): tle
            for start_time, tle in orig_tle_list
        })
        self.gs = {
            "id": None,
            "lat": gs_lat,
            "lon": gs_lon,
            "alt": gs_alt,
        }
        # Sometimes there is no remap because there are no contacts.
        if contact_remap:
            # "Floatify" keys.
            contact_remap = {float(k): v for k, v in contact_remap.items()}
            # Sanity-check.
            last_t = -self.REMAP_TOLERANCE_SECONDS
            for k in sorted(contact_remap.keys()):
                assert k >= last_t + self.REMAP_TOLERANCE_SECONDS
                last_t = k
            # This is a StartTimeBasedDict returning an offset (new - old) for
            # a "new" timestamp. It has to be ensured that items are spaced
            # more than REMAP_TOLERANCE_SECONDS apart.
            self.remap = StartTimeBasedDict(contact_remap)
        self._init_sgp4_instances()

    def _init_sgp4_instances(self):
        self.sgp4_inst = StartTimeBasedDict({
            int(start_time): self._init_sgp4(
                self.sat_name,
                tle,
                self.gs["lat"],
                self.gs["lon"],
                self.gs["alt"],
            )
            for start_time, tle in self.tle_list.base_dict.items()
        })

    def _init_sgp4(self, sat_name, tle, gs_lat, gs_lon, gs_alt):
        sat = ephem.readtle(str(sat_name), *tle)
        obs = ephem.Observer()
        obs.lat = str(gs_lat)  # pylint: disable=assigning-non-slot
        obs.lon = str(gs_lon)  # pylint: disable=assigning-non-slot
        obs.elevation = int(gs_alt)  # pylint: disable=assigning-non-slot

        # NOTE: Returns values by old time!
        def _sgp4(time):
            obs.date = datetime.datetime.utcfromtimestamp(time)
            sat.compute(obs)
            return sat.alt, sat.az

        return _sgp4, sat, obs

    def _get_sgp4_inst(self, time):
        # NOTE: Contrary to the "normal" predictor we do not allow timestamps
        # in the future - every offset should be recorded in the remapping
        # list and a corresponding element shall be available.
        return self.sgp4_inst.get_entry_for(time)

    def get_sat_and_observer(self, time):
        raise NotImplementedError()

    def get_offset(self, new_time):
        # NOTE: IMPORTANT: time has to be within tolerance of the contact start
        time_offset = self.remap.get_entry_for(
            new_time + self.REMAP_TOLERANCE_SECONDS
        )
        return time_offset

    def get_sgp4(self, new_time):
        time_offset = self.get_offset(new_time)

        # NOTE: Returned function returns radians!
        sgp4, _, _ = self._get_sgp4_inst(new_time - time_offset)

        def sgp4_remapped(new_time_t):
            return sgp4(new_time_t - time_offset)

        return sgp4_remapped

    def get_tle(self, time):
        raise NotImplementedError()

    def predict_elevation(self, time):
        # NOTE: Returned elevation is in radians!
        sgp4 = self.get_sgp4(time)
        alt, _ = sgp4(time)
        return alt

    def predict_azimuth(self, time):
        # NOTE: Returned azimuth is in radians!
        sgp4 = self.get_sgp4(time)
        _, az = sgp4(time)
        return az

    def predict_max_elevation(self, start, end):
        # NOTE: Returned elevation is in radians!
        sgp4 = self.get_sgp4(start)
        bracket_interval = [
            start,
            (start + end) / 2,
            end,
        ]
        solution = optimize.minimize_scalar(
            lambda x: -sgp4(x)[0],
            bracket=bracket_interval,
            tol=.1,
        )
        assert solution.success
        return solution.x, sgp4(solution.x)[0]

    def predict_contacts(self, start_time, end_time, min_elevation):
        raise NotImplementedError()

    def get_contact_within(self, at_time, soft_start, soft_end,
                           start_elev, end_elev):
        # NOTE: *_elev is in radians!
        # NOTE: We use the soft start here b/c remap does not work otherwise!
        sgp4 = self.get_sgp4(soft_start)
        TOLERANCE_SECONDS = 30
        mid = (soft_start + soft_end) / 2
        try:
            start_zero, rr1 = optimize.brentq(
                lambda x: sgp4(x)[0] - start_elev,
                soft_start - TOLERANCE_SECONDS,
                mid,
                full_output=True,
            )
            end_zero, rr2 = optimize.brentq(
                lambda x: sgp4(x)[0] - end_elev,
                mid,
                soft_end + TOLERANCE_SECONDS,
                full_output=True,
            )
        except ValueError:
            return None, None
        if not rr1.converged or not rr2.converged:
            return None, None
        return start_zero, end_zero

    # Pickle support
    def __getstate__(self):
        # Copy the object's state from self.__dict__ which contains
        # all our instance attributes. Always use the dict.copy()
        # method to avoid modifying the original state.
        state = self.__dict__.copy()
        # Remove the unpicklable entries.
        del state["sgp4_inst"]
        return state

    # Un-pickle support
    def __setstate__(self, state):
        # Restore instance attributes (i.e., filename and lineno).
        self.__dict__.update(state)
        # Re-init the unpicklable entry
        self._init_sgp4_instances()
