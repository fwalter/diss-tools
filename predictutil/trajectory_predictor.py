import datetime

import ephem
from scipy import optimize

from tvgutil.ring_road.contact_plan import get_rr0_contact_tuples

from .misc import StartTimeBasedDict

# Allow a SGP4 instance up to 86400 seconds in the future
TRAJECTORY_PREDICTOR_ALLOW_FUTURE_SGP4_SECONDS = 86400


class SatelliteTrajectoryPredictor:

    def __init__(self, sat_name, tle_list, gs_lat, gs_lon, gs_alt):
        self.sat_name = sat_name
        self.tle_list = StartTimeBasedDict({
            int(start_time): tle
            for start_time, tle in tle_list
        })
        self.gs = {
            "id": None,
            "lat": gs_lat,
            "lon": gs_lon,
            "alt": gs_alt,
        }
        self._init_sgp4_instances()

    def _init_sgp4_instances(self):
        self.sgp4_inst = StartTimeBasedDict({
            int(start_time): self._init_sgp4(
                self.sat_name,
                tle,
                self.gs["lat"],
                self.gs["lon"],
                self.gs["alt"],
            )
            for start_time, tle in self.tle_list.base_dict.items()
        })

    def _init_sgp4(self, sat_name, tle, gs_lat, gs_lon, gs_alt):
        sat = ephem.readtle(str(sat_name), *tle)
        obs = ephem.Observer()
        obs.lat = str(gs_lat)  # pylint: disable=assigning-non-slot
        obs.lon = str(gs_lon)  # pylint: disable=assigning-non-slot
        obs.elevation = int(gs_alt)  # pylint: disable=assigning-non-slot

        def _sgp4(time):
            obs.date = datetime.datetime.utcfromtimestamp(time)
            sat.compute(obs)
            return sat.alt, sat.az

        return _sgp4, sat, obs

    def _get_sgp4_inst(self, time):
        try:
            return self.sgp4_inst.get_entry_for(time)
        except ValueError:
            return self.sgp4_inst.get_entry_for(
                time +
                TRAJECTORY_PREDICTOR_ALLOW_FUTURE_SGP4_SECONDS
            )

    def get_sat_and_observer(self, time):
        _, sat, obs = self._get_sgp4_inst(time)
        return sat, obs

    def get_offset(self, new_time):
        return 0

    def get_sgp4(self, time):
        # NOTE: Returned function returns radians!
        sgp4, _, _ = self._get_sgp4_inst(time)
        return sgp4

    def get_tle(self, time):
        try:
            return self.tle_list.get_entry_for(time)
        except ValueError:
            return self.tle_list.get_entry_for(
                time +
                TRAJECTORY_PREDICTOR_ALLOW_FUTURE_SGP4_SECONDS
            )

    def predict_elevation(self, time):
        # NOTE: Returned elevation is in radians!
        sgp4 = self.get_sgp4(time)
        alt, _ = sgp4(time)
        return alt

    def predict_azimuth(self, time):
        # NOTE: Returned azimuth is in radians!
        sgp4 = self.get_sgp4(time)
        _, az = sgp4(time)
        return az

    def predict_max_elevation(self, start, end):
        # NOTE: Returned elevation is in radians!
        sgp4 = self.get_sgp4(start)
        bracket_interval = [
            start,
            (start + end) / 2,
            end,
        ]
        solution = optimize.minimize_scalar(
            lambda x: -sgp4(x)[0],
            bracket=bracket_interval,
            tol=.1,
        )
        assert solution.success
        return solution.x, sgp4(solution.x)[0]

    def predict_contacts(self, start_time, end_time, min_elevation):
        # WARNING: This function uses an elevation in degrees!
        # NOTE: TLE at soft start_time used for prediction. Consider!
        contact_tuples = get_rr0_contact_tuples(
            [{"id": self.sat_name, "tle": self.get_tle(start_time)}],
            [self.gs],
            start_time,
            end_time - start_time,
            min_elevation=min_elevation,
            precision=.1,
            half_period=2700.,
            step=300.,
            strip_start_end=False,
        )
        return [(c[2], c[3]) for c in contact_tuples]

    def get_contact_within(self, at_time, soft_start, soft_end,
                           start_elev, end_elev):
        # NOTE: *_elev is in radians!
        sgp4 = self.get_sgp4(at_time)
        TOLERANCE_SECONDS = 30
        mid = (soft_start + soft_end) / 2
        try:
            start_zero, rr1 = optimize.brentq(
                lambda x: sgp4(x)[0] - start_elev,
                soft_start - TOLERANCE_SECONDS,
                mid,
                full_output=True,
            )
            end_zero, rr2 = optimize.brentq(
                lambda x: sgp4(x)[0] - end_elev,
                mid,
                soft_end + TOLERANCE_SECONDS,
                full_output=True,
            )
        except ValueError:
            return None, None
        if not rr1.converged or not rr2.converged:
            return None, None
        return start_zero, end_zero

    # Pickle support
    def __getstate__(self):
        # Copy the object's state from self.__dict__ which contains
        # all our instance attributes. Always use the dict.copy()
        # method to avoid modifying the original state.
        state = self.__dict__.copy()
        # Remove the unpicklable entries.
        del state["sgp4_inst"]
        return state

    # Un-pickle support
    def __setstate__(self, state):
        # Restore instance attributes (i.e., filename and lineno).
        self.__dict__.update(state)
        # Re-init the unpicklable entry
        self._init_sgp4_instances()
