class BasePredictor:
    # One predictor works at one local node for exactly one neighbor

    def __init__(self, trajectory_predictor):
        self.trajectory_predictor = trajectory_predictor

    def observe_contact(self, fc):
        # update metrics according to the Contact
        pass

    def export_metric_info(self, at_time):
        # return a list of (start, metrics) that can be serialized
        return []

    def import_metric_info(self, mi):
        # import a serialized state, possibly overwriting the current state
        pass
