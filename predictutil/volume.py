import math
import logging
import collections

import numpy as np
import scipy.optimize

from capacityutil import contact_estimator

from . import (
    WindowAverager,
    get_fc_volume,
    get_fc_volume_in_interval,
)
from .base_predictor import BasePredictor

logger = logging.getLogger(__name__)


class VolumePredictor(BasePredictor):

    def predict_data_rate_factors(self, at_time, soft_start, soft_end):
        # return a list of tuples representing volume characteristics:
        # (starting_at, bit_rate_factor)
        raise NotImplementedError()


class MinElevationVolumePredictor(VolumePredictor):

    def __init__(self, trajectory_predictor, min_elevation=5):
        super().__init__(trajectory_predictor)
        self.min_elevation = min_elevation

    def predict_data_rate_factors(self, at_time, soft_start, soft_end):
        elev_radian = self.min_elevation / 180 * math.pi
        (
            elev_start_time,
            elev_end_time,
        ) = self.trajectory_predictor.get_contact_within(
            # NOTE: We use the TLE available during planning of the associated
            # factual contact. This should resemble reality quite well.
            soft_start,
            soft_start,
            soft_end,
            elev_radian,
            elev_radian,
        )
        if not elev_start_time or not elev_end_time:
            return [(0, 0.0)]
        center_offset = (
            (elev_end_time + elev_start_time) / 2 -
            (soft_end + soft_start) / 2
        )
        # Return prediction moved into provided interval
        return [
            (0, 0.0),
            (elev_start_time - center_offset, 1.0),
            (elev_end_time - center_offset, 0.0),
        ]


class FactualVolumePredictor(VolumePredictor):

    def __init__(self, trajectory_predictor, link_block_size=12000):
        super().__init__(trajectory_predictor)
        self.link_block_size = link_block_size
        self.fcp = list()

    def predict_data_rate_factors(self, at_time, soft_start, soft_end):
        for start, end, fc_data_rate, volume in self.fcp:
            if soft_end > start and soft_start < end:
                max_predicted_volume = (soft_end - soft_start) * fc_data_rate
                drf = volume / max_predicted_volume
                return [
                    (0, 0.0),
                    (soft_start, drf),
                    (soft_end, 0.0),
                ]

        # NOTE: This predictor HAS to be used with instant knowledge assignment
        raise ValueError("contact not found in record - wrong KB?")

    def observe_contact(self, fc):
        self.fcp.append((
            fc.start_time,
            fc.end_time,
            fc.to_simple().bit_rate,
            get_fc_volume(
                fc,
                self.link_block_size,
            ),
        ))

    def export_metric_info(self, at_time):
        # NOTE: This is a reference. It will be updated AFTER it is returned.
        # This is the only predictor where this is expected behavior.
        return self.fcp

    def import_metric_info(self, mi):
        self.fcp = list(mi)


class XPredBasedVolumePredictor(VolumePredictor):

    # NOTE: xpred makes us independent of freq (partially),
    #       elevation, altitude (may be good for dist'ing to other sats)

    # A maximum BER of 1e-4 allows for successful block transmission with
    # P=0.86 for blocks of 1500 bits; 0.3 for 1500 bytes
    def __init__(self, trajectory_predictor,
                 max_ber=1.e-2, next_xpred_weight=.3, min_samples_xpred=10,
                 xpred_percentile=90, frequency=137.e6, sample_rate=1,
                 min_observed_contacts=1, min_elevation=5):
        super().__init__(trajectory_predictor)
        self.max_ber = max_ber
        self.next_xpred_weight = next_xpred_weight
        self.min_samples_xpred = min_samples_xpred
        self.xpred_percentile = xpred_percentile
        self.frequency = frequency
        self.sample_rate = sample_rate
        self.min_observed_contacts = min_observed_contacts
        self.default_predictor = MinElevationVolumePredictor(
            trajectory_predictor,
            min_elevation,
        )
        self.observed_contacts = 0
        self.xpred = None

    # xpred (the "new prediction constant") is a value updated and stored after
    # every prediction step, representing the history and used to derive the
    # next prediction -- xpred is the maximum calculated loss in dB at which
    # still a satisfactory BER (below max_ber) can be achieved

    def _predict_losses(self, start, end):
        sat, obs = self.trajectory_predictor.get_sat_and_observer(start)
        return list(contact_estimator.compute_over_contact(
            contact_estimator.estimate_losses,
            sat,
            obs,
            self.frequency,
            start,
            end,
            sample_rate=self.sample_rate,
        ))

    # XXX: first thought was to derive snr (eb/n0) from ber via the inverse of
    # ber for bfsk: 1/2erfc(sqrt(eb/2n0))
    # see: https://www.atlantarf.com/FSK_Modulation.php
    # issues: 1) might get to inf quickly (for ber == 0)
    #         2) we use the ber after coding for which this does not hold...
    # def _unused__derive_snrs_from_bers_bfsk(self, fc):
    #     return [
    #         2 * (scipy.special.erfcinv(2 * (
    #             fc.get_characteristics_at(ts).bit_error_rate
    #         )) ** 2)
    #         for ts in np.arange(
    #             fc.start_time,
    #             fc.endtime,
    #             1 / self.sample_rate,
    #         )
    #     ]

    def _calculate_next_xpred(self, predicted_losses, ber_samples):
        # calculate updated xpred which is used after the contact was observed
        assert len(predicted_losses) == len(ber_samples)
        last_xpred = self.xpred
        # NOTE: XXX: this is pessimistic - we want to be optimistic!
        # we want to determine the loss threshold after which comm is not
        # possible anymore...
        cur_xpred_candidates = [
            predicted_losses[i]
            for i, ber in enumerate(ber_samples)
            if ber < self.max_ber
        ]
        logger.debug("Inferred a comm. duration of %f seconds",
                     (1 / self.sample_rate) * len(cur_xpred_candidates))
        if len(cur_xpred_candidates) < self.min_samples_xpred:
            logger.debug("Too few rate samples in contact, not updating")
            return None
        cur_xpred = np.percentile(
            cur_xpred_candidates,
            self.xpred_percentile,
        )
        w = self.next_xpred_weight
        next_xpred = (
            (w * cur_xpred + (1 - w) * last_xpred)
            if last_xpred is not None else cur_xpred
        )
        logger.debug("Updating xpred to %f", next_xpred)
        return next_xpred

    def observe_contact(self, fc):
        predicted_losses = self._predict_losses(fc.start_time, fc.end_time)
        ber_samples = [
            fc.get_characteristics_at(ts).bit_error_rate
            for ts in np.arange(
                fc.start_time,
                fc.end_time,
                1 / self.sample_rate,
            )
        ]
        next_xpred = self._calculate_next_xpred(
            predicted_losses,
            ber_samples,
        )
        if next_xpred:
            self.xpred = next_xpred
            self.observed_contacts += 1

    def predict_data_rate_factors(self, at_time, soft_start, soft_end):
        if (self.observed_contacts < self.min_observed_contacts or
                self.xpred is None):
            return self.default_predictor.predict_data_rate_factors(
                at_time,
                soft_start,
                soft_end,
            )
        chars = [(0, 0.0)]
        losses = self._predict_losses(soft_start, soft_end)
        ber_achieved_bools = [
            val <= self.xpred
            for val in losses
        ]
        timestamps = list(np.arange(
            soft_start,
            soft_end,
            1 / self.sample_rate,
        ))
        could_communicate = False
        comm_time = 0
        for ts, can_communicate in zip(timestamps, ber_achieved_bools):
            if can_communicate != could_communicate:
                if could_communicate:
                    comm_time += ts - chars[-1][0]
                could_communicate = can_communicate
                chars.append((ts, 1.0 if can_communicate else 0.0))
        logger.debug("Predicted comm. duration of %f seconds", comm_time)
        return chars

    def export_metric_info(self, at_time):
        return self.xpred, self.observed_contacts

    def import_metric_info(self, mi):
        logger.debug("Importing xpred = %f sourced from %d contacts", *mi)
        self.xpred, self.observed_contacts = mi


class ExpFitVolumePredictor(VolumePredictor):

    # TODO: add az_granularity (default: 45) for considering azimuth

    def __init__(self, trajectory_predictor,
                 elev_granularity=6, max_ber=1.e-4, backlog_size=10,
                 char_granularity=30, min_ratio_val=1.e-1, min_buck_size=30,
                 min_observed_contacts=1, min_elevation=5):
        super().__init__(trajectory_predictor)
        self.elev_granularity = elev_granularity
        self.max_ber = max_ber
        self.backlog_size = backlog_size
        self.backlog = collections.deque(maxlen=int(backlog_size))
        self.char_granularity = char_granularity
        self.min_ratio_val = min_ratio_val
        self.min_buck_size = min_buck_size
        self.min_observed_contacts = min_observed_contacts
        self.default_predictor = MinElevationVolumePredictor(
            trajectory_predictor,
            min_elevation,
        )
        self.observed_contacts = 0
        self.fitfunc = lambda _: 0

    def observe_contact(self, fc):
        elev_gran = self.elev_granularity
        elevation_range = list(range(0, 90, elev_gran))
        ber_bucks = np.array([0 for _ in elevation_range])
        all_bucks = np.array([0 for _ in elevation_range])
        tp = self.trajectory_predictor
        for i, char in enumerate(fc.characteristics):
            char_start = max(fc.start_time, char.starting_at)
            char_end = (
                fc.characteristics[i + 1].starting_at
                if i + 1 < len(fc.characteristics)
                else fc.end_time
            )
            if char_end <= char_start:
                continue
            char_mean = 0.5 * (char_start + char_end)
            char_duration_sec = char_end - char_start
            elev = tp.predict_elevation(char_mean) / math.pi * 180
            if char.bit_error_rate < self.max_ber:
                ber_bucks[int(elev / elev_gran)] += char_duration_sec
            all_bucks[int(elev / elev_gran)] += char_duration_sec
        self.backlog.append((ber_bucks, all_bucks))
        self.observed_contacts += 1
        # Update the predictor function
        self.fitfunc = self._get_fit_func()

    def _get_fit_func(self):
        # returns a function that allows to calculate for which ratio of
        # seconds at the given elevation a communication is expected to be
        # possible

        elev_gran = self.elev_granularity
        elevation_range = np.array(range(0, 90, elev_gran))
        cum_ber_bucks = np.array([0 for _ in elevation_range])
        cum_all_bucks = np.array([0 for _ in elevation_range])

        for ber_bucks, all_bucks in self.backlog:
            cum_ber_bucks += ber_bucks
            cum_all_bucks += all_bucks

        indices = np.array([
            i
            for i, buck_size in enumerate(cum_all_bucks)
            if buck_size >= self.min_buck_size
        ])

        def fit_func(e, a, b):
            return a * (1 - np.exp(-b * e))

        try:
            param = scipy.optimize.curve_fit(
                fit_func,
                elevation_range[indices],
                cum_ber_bucks[indices] / cum_all_bucks[indices],
            )[0]
        except RuntimeError:
            return lambda _: 0

        # Sanity check: param[0] (a) is the maximum, param[1] (b) is the factor
        if param[0] < 1e-1 or param[0] > 1 or param[1] < 1e-6 or param[1] > .5:
            return lambda _: 0

        return lambda e: fit_func(e, param[0], param[1])

    def predict_data_rate_factors(self, at_time, soft_start, soft_end):
        if self.observed_contacts < self.min_observed_contacts:
            return self.default_predictor.predict_data_rate_factors(
                at_time,
                soft_start,
                soft_end,
            )
        chars = []
        for ts in np.arange(soft_start, soft_end, self.char_granularity):
            elev = self.trajectory_predictor.predict_elevation(
                ts + .5 * self.char_granularity
            ) * 180 / math.pi
            ratio = self.fitfunc(elev)
            # NOTE this expects we calculate the capacity in the router from
            # the characteristics array
            if ratio < self.min_ratio_val:
                chars.append((ts, 0.0))
            elif ratio > 1 - self.min_ratio_val:
                chars.append((ts, 1.0))
            else:
                mid = ts + ratio * self.char_granularity
                chars.append((ts, 1.0))
                chars.append((mid, 0.0))

        return [
            (ts, fac)
            for i, (ts, fac) in enumerate(chars)
            if i == 0 or abs(fac - chars[i - 1][1]) > 1.e-10
        ]

    def _to_int(self, arr):
        return [int(x) for x in arr]

    def export_metric_info(self, at_time):
        return [
            (
                self._to_int(ber_bucks),
                self._to_int(all_bucks),
            )
            for ber_bucks, all_bucks in self.backlog
        ], self.observed_contacts

    def import_metric_info(self, mi):
        self.backlog = collections.deque([
            (
                np.array(self._to_int(ber_bucks)),
                np.array(self._to_int(all_bucks)),
            )
            for ber_bucks, all_bucks in mi[0]
        ], maxlen=int(self.backlog_size))
        self.observed_contacts = mi[1]


class GoodBadStateVolumePredictor(VolumePredictor):

    # Predict DataRateFactors for Elevation (in degrees!)
    def predict_drf_elev(self, at_time, soft_start, soft_end, elev):
        elev_radian = elev / 180 * math.pi
        (
            elev_start_time,
            elev_end_time,
        ) = self.trajectory_predictor.get_contact_within(
            # NOTE: We use the TLE available during planning of the associated
            # factual contact. This should resemble reality quite well.
            soft_start,
            soft_start,
            soft_end,
            elev_radian,
            elev_radian,
        )
        if not elev_start_time or not elev_end_time:
            return [(0, 0.0)]
        center_offset = (
            (elev_end_time + elev_start_time) / 2 -
            (soft_end + soft_start) / 2
        )
        # Return prediction moved into provided interval
        return [
            (0, 0.0),
            (elev_start_time - center_offset, 1.0),
            (elev_end_time - center_offset, 0.0),
        ]

    def get_angular_sgp4(self, time):
        # Get a SGP4 returning degrees...
        sgp4 = self.trajectory_predictor.get_sgp4(time)

        def f(t):
            e, a = sgp4(t)
            return float(e) * 180 / math.pi, float(a) * 180 / math.pi

        return f

    def sample_contact(self, fc, min_good_ratio, min_bad_ratio, min_good_elev):
        SAMPLE_DURATION = 30  # seconds

        # NOTE: It is important that the FTVG only records link quality
        # reductions as an increased BER! The BR has to be constant.
        bit_rate = fc.get_characteristics_at(fc.start_time).bit_rate

        # We want the predictor associated to the FC. As start_time may be
        # imprecise we use the end to get it definitely.
        sgp4 = self.get_angular_sgp4(fc.end_time)

        t0 = fc.start_time
        samples = []
        max_mean_elev = 0
        while t0 < fc.end_time:
            t1 = t0 + SAMPLE_DURATION
            if t1 > fc.end_time:
                break
            maxvol = bit_rate * (t1 - t0)
            intvol = get_fc_volume_in_interval(
                fc,
                self.link_block_size,
                t0,
                t1,
            )
            ratio = intvol / maxvol
            elev0 = float(sgp4(t0)[0])
            elev1 = float(sgp4(t1)[0])
            min_elev = min(elev0, elev1)
            mean_elev = (elev0 + elev1) / 2
            max_mean_elev = max(max_mean_elev, mean_elev)
            samples.append((
                t0,
                t1,
                elev0,
                elev1,
                intvol,
                maxvol,
                # 2 == good sector, 1 == bad sector
                (
                    2
                    if ratio >= min_good_ratio and min_elev >= min_good_elev
                    else (1 if ratio >= min_bad_ratio else 0)
                ),
            ))
            t0 += SAMPLE_DURATION

        return samples, max_mean_elev

    def good_interval(self, samples):
        first = min(
            i for i, v in enumerate(samples)
            if v[6] == 2
        )
        last = max(
            i for i, v in enumerate(samples)
            if v[6] == 2
        )
        return first, last

    def bad_interval(self, samples):
        # We only start the bad interval if we see at least two bad samples.
        first = min(
            i for i, v in enumerate(samples[:-1])
            if v[6] >= 1 and samples[i + 1][6] >= 1
        )
        # ...same for the end of the interval.
        last = max(
            i for i, v in enumerate(samples[1:])
            if v[6] >= 1 and samples[i - 1][6] >= 1
        )
        return first, last


class TwoStateVolumePredictor(GoodBadStateVolumePredictor):

    def __init__(self, trajectory_predictor, link_block_size=12000,
                 averaging_window_size=10):
        super().__init__(trajectory_predictor)
        self.link_block_size = int(link_block_size)
        good_averager = WindowAverager(
            int(averaging_window_size),
            10,  # default elevation
            use_median=True,
        )
        q_averager = WindowAverager(
            int(averaging_window_size),
            1.0,  # default Q
            use_median=True,
        )
        self.good_averager = good_averager
        self.good_q_averager = q_averager
        self.observed_good_contacts = 0

    def observe_contact(self, fc):
        MIN_GOOD_RATIO = 0.5
        MIN_BAD_RATIO = 0.25  # not relevant here
        MIN_GOOD_ELEV = 0  # as this is only two-state, we go down to zero

        samples, max_mean_elev = self.sample_contact(
            fc,
            MIN_GOOD_RATIO,
            MIN_BAD_RATIO,
            MIN_GOOD_ELEV,
        )

        try:
            good_interval = self.good_interval(samples)
        except ValueError:
            return

        good_start_elev = samples[good_interval[0]][2]  # elev0 from sample
        good_end_elev = samples[good_interval[1]][3]  # elev1 from sample
        self.good_averager.add_value(good_start_elev)
        self.good_averager.add_value(good_end_elev)
        self.observed_good_contacts += 1

        # save how good "good" is
        good_int_volume = 0
        good_max_volume = 0
        good_samples = samples[good_interval[0]:(good_interval[1] + 1)]
        for _, _, _, _, intv, maxv, _ in good_samples:
            good_int_volume += intv
            good_max_volume += maxv
        self.good_q_averager.add_value(good_int_volume / good_max_volume)

    def predict_data_rate_factors(self, at_time, soft_start, soft_end):
        good_avg_min_elev = self.good_averager.get_average()
        drf_good = self.predict_drf_elev(
            at_time,
            soft_start,
            soft_end,
            good_avg_min_elev,
        )

        if len(drf_good) == 1:
            return drf_good

        # add good
        _, (start_good, _), (end_good, _) = drf_good

        return [
            (0.0, 0.0),
            (start_good, self.good_q_averager.get_average()),
            (end_good, 0.0),
        ]

    def export_metric_info(self, at_time):
        return [
            self.link_block_size,
            self.observed_good_contacts,
            self.good_averager.export_state(),
            self.good_q_averager.export_state(),
        ]

    def import_metric_info(self, mi):
        self.link_block_size = int(mi[0])
        self.observed_good_contacts = int(mi[1])
        self.good_averager.import_state(mi[2])
        self.good_q_averager.import_state(mi[3])


class TriStateVolumePredictor(GoodBadStateVolumePredictor):

    def __init__(self, trajectory_predictor, link_block_size=12000,
                 averaging_window_size=10, az_granularity=45):
        super().__init__(trajectory_predictor)
        self.link_block_size = int(link_block_size)
        self.az_granularity = int(az_granularity)
        good_averager = WindowAverager(
            int(averaging_window_size),
            10,  # default "good" elevation
            use_median=True,
        )
        q_averager = WindowAverager(
            int(averaging_window_size),
            1.0,  # default Q
            use_median=True,
        )
        bad_averager = WindowAverager(
            int(averaging_window_size),
            10,  # default "bad" elevation
            use_median=True,
        )
        self.good_averager = good_averager
        self.good_q_averager = q_averager.copy()
        self.observed_good_contacts = 0
        # bad
        self.bad_averagers = [
            bad_averager.copy()
            for _ in range(360 // self.az_granularity)
        ]
        self.bad_averager_misc = bad_averager
        self.bad_q_averager = q_averager.copy()
        self.observed_bad_contacts = [
            0 for _ in range(360 // self.az_granularity)
        ]

    def observe_contact(self, fc):
        MIN_GOOD_RATIO = 0.5
        MIN_BAD_RATIO = 0.25
        # NOTE: In the paper fading goes until 20. Evaluate to decrease,
        # 5 led to some nice results...
        MIN_GOOD_ELEV = 20  # XXX

        samples, max_mean_elev = self.sample_contact(
            fc,
            MIN_GOOD_RATIO,
            MIN_BAD_RATIO,
            MIN_GOOD_ELEV,
        )

        try:
            bad_interval = self.bad_interval(samples)
        except ValueError:
            return

        try:
            good_interval = self.good_interval(samples)
        except ValueError:
            good_interval = None

        # We want the predictor associated to the FC. As start_time may be
        # imprecise we use the end to get it definitely.
        sgp4 = self.get_angular_sgp4(fc.end_time)

        # at least a bad interval has been observed... -> record!
        start_az = float(sgp4(fc.start_time)[1])
        end_az = float(sgp4(fc.end_time)[1])
        start_az_i = int(start_az // self.az_granularity)
        end_az_i = int(end_az // self.az_granularity)

        bad_start_elev = samples[bad_interval[0]][2]  # elev0 from sample
        bad_end_elev = samples[bad_interval[1]][3]  # elev1 from sample

        self.bad_averagers[start_az_i].add_value(bad_start_elev)
        self.bad_averagers[end_az_i].add_value(bad_end_elev)
        self.bad_averager_misc.add_value(bad_start_elev)
        self.bad_averager_misc.add_value(bad_end_elev)
        self.observed_bad_contacts[start_az_i] += 1
        self.observed_bad_contacts[end_az_i] += 1

        # save how bad "bad" is
        bad_int_volume = 0
        bad_max_volume = 0
        bad_samples = samples[bad_interval[0]:(bad_interval[1] + 1)]
        for i, (_, _, _, _, intv, maxv, _) in enumerate(bad_samples):
            # NOTE: Following change could be considered
            # if good_interval and (i >= good_interval[0] or
            #                       i <= good_interval[1]):
            #     continue
            bad_int_volume += intv
            bad_max_volume += maxv
        if bad_max_volume:
            self.bad_q_averager.add_value(bad_int_volume / bad_max_volume)

        # no "good" interval detected
        if not good_interval:
            # Here we may check and eventually increase good minimum elevation,
            # e.g. if it has a value which should lead to a
            # "definitely-detectable" signal within this contact...
            # NOTE: Tested, same result with and without that change...
            # good_average_min_elev = self.good_averager.get_average()
            # if good_average_min_elev <= max_mean_elev:
            #     bad_mean_elev = (bad_start_elev + bad_end_elev) / 2
            #     self.good_averager.add_value(
            #         2 * good_average_min_elev - bad_mean_elev
            #     )
            return

        good_start_elev = samples[good_interval[0]][2]  # elev0 from sample
        good_end_elev = samples[good_interval[1]][3]  # elev1 from sample
        self.good_averager.add_value(good_start_elev)
        self.good_averager.add_value(good_end_elev)
        self.observed_good_contacts += 1

        # save how good "good" is
        good_int_volume = 0
        good_max_volume = 0
        good_samples = samples[good_interval[0]:(good_interval[1] + 1)]
        for _, _, _, _, intv, maxv, _ in good_samples:
            good_int_volume += intv
            good_max_volume += maxv
        self.good_q_averager.add_value(good_int_volume / good_max_volume)

    def _get_min_elev_bad(self, az):
        # minimum amount of sample contacts to use value
        MIN_OBSERVED_BAD_C = 3
        i = int(az // self.az_granularity)
        if self.observed_bad_contacts[i] >= MIN_OBSERVED_BAD_C:
            return self.bad_averagers[i].get_average()
        # Fallback: interpolate
        i_prev = (i - 1) % len(self.bad_averagers)
        i_next = (i + 1) % len(self.bad_averagers)
        if (self.observed_bad_contacts[i_prev] is not None and
                self.observed_bad_contacts[i_next] is not None and
                (self.observed_bad_contacts[i_prev] +
                 self.observed_bad_contacts[i] +
                 self.observed_bad_contacts[i_next]) >=
                MIN_OBSERVED_BAD_C):
            return (
                self.bad_averagers[i_prev].get_average() +
                self.bad_averagers[i].get_average() +
                self.bad_averagers[i_next].get_average()
            ) / 3
        # Fallback 2: interpolate over everything
        return self.bad_averager_misc.get_average()

    def predict_data_rate_factors(self, at_time, soft_start, soft_end):
        DRF_BAD_VAL = self.bad_q_averager.get_average()
        DRF_GOOD_VAL = self.good_q_averager.get_average()

        # We use the SGP4 instance used for predicting the start of the
        # contact.
        sgp4 = self.get_angular_sgp4(soft_start)

        _, start_az = sgp4(soft_start)
        _, end_az = sgp4(soft_end)

        min_elevation_bad_start = self._get_min_elev_bad(float(start_az))
        min_elevation_bad_end = self._get_min_elev_bad(float(end_az))
        good_avg_min_elev = max(
            min_elevation_bad_start,
            min_elevation_bad_end,
            self.good_averager.get_average(),
        )
        TOLERANCE_DEGREE = 1.
        max_elev_bad_limit = self.trajectory_predictor.predict_max_elevation(
            soft_start,
            soft_end,
        )[1] * 180 / math.pi - TOLERANCE_DEGREE
        min_elevation_bad_start = min(
            max_elev_bad_limit,
            min_elevation_bad_start,
        )
        min_elevation_bad_end = min(
            max_elev_bad_limit,
            min_elevation_bad_end,
        )

        if (abs(min_elevation_bad_start - max_elev_bad_limit) <= .1 and
                abs(min_elevation_bad_end - max_elev_bad_limit) <= .1):
            # both have been limited - no bad range -> fail
            return [(0.0, 0.0)]

        drf_bad_start = self.predict_drf_elev(
            at_time,
            soft_start,
            soft_end,
            min_elevation_bad_start,
        )
        drf_bad_end = self.predict_drf_elev(
            at_time,
            soft_start,
            soft_end,
            min_elevation_bad_end,
        )
        if len(drf_bad_start) == 1 or len(drf_bad_end) == 1:
            # nothing found for the bad sector, no need to check the good
            return [(0.0, 0.0)]

        drf_good = self.predict_drf_elev(
            at_time,
            soft_start,
            soft_end,
            good_avg_min_elev,
        )

        # unpack bad
        _, (start_bad, _), (_, _) = drf_bad_start
        _, (_, _), (end_bad, _) = drf_bad_end
        if len(drf_good) == 1:
            return [
                (0.0, 0.0),
                (start_bad, DRF_BAD_VAL),
                (end_bad, 0.0),
            ]

        # add good
        _, (start_good, _), (end_good, _) = drf_good
        assert start_bad <= start_good <= end_good <= end_bad
        return [
            (0.0, 0.0),
            (start_bad, DRF_BAD_VAL),
            (start_good, DRF_GOOD_VAL),
            (end_good, DRF_BAD_VAL),
            (end_bad, 0.0),
        ]

    def export_metric_info(self, at_time):
        return [
            self.link_block_size,
            self.az_granularity,
            self.observed_good_contacts,
            self.observed_bad_contacts,
            self.good_averager.export_state(),
            self.good_q_averager.export_state(),
            [a.export_state() for a in self.bad_averagers],
            self.bad_averager_misc.export_state(),
            self.bad_q_averager.export_state(),
        ]

    def import_metric_info(self, mi):
        self.link_block_size = int(mi[0])
        self.az_granularity = int(mi[1])
        self.observed_good_contacts = int(mi[2])
        az_items = len(list(range(360 // self.az_granularity)))
        assert len(mi[3]) == az_items
        self.observed_bad_contacts = [int(x) for x in mi[3]]
        self.good_averager.import_state(mi[4])
        self.good_q_averager.import_state(mi[5])
        assert len(mi[6]) == az_items
        for i, a in enumerate(self.bad_averagers):
            a.import_state(mi[6][i])
        self.bad_averager_misc.import_state(mi[7])
        self.bad_q_averager.import_state(mi[8])


VOLUME_PREDICTORS = {
    "minelev": (MinElevationVolumePredictor, ["min_elevation"]),
    "xpred": (XPredBasedVolumePredictor, [
        "max_ber",
        "frequency",
        "sample_rate",
        "next_xpred_weight",
        "min_samples_xpred",
        "xpred_percentile",
        "min_observed_contacts",
        "min_elevation",
    ]),
    "expfit": (ExpFitVolumePredictor, [
        "elev_granularity",
        "max_ber",
        "backlog_size",
        "char_granularity",
        "min_ratio_val",
        "min_buck_size",
        "min_observed_contacts",
        "min_elevation",
    ]),
    "twostate": (TwoStateVolumePredictor, [
        "link_block_size",
        "averaging_window_size",
    ]),
    "tristate": (TriStateVolumePredictor, [
        "link_block_size",
        "averaging_window_size",
        "az_granularity",
    ]),
    "factual": (FactualVolumePredictor, [
        "link_block_size",
    ]),
}
