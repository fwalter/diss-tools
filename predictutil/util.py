import itertools
import math

from tvgutil import contact_plan
from tvgutil.ring_road.contact_plan import get_rr0_contact_tuples

from .trajectory_predictor import SatelliteTrajectoryPredictor
from .mock_trajectory_predictor import SatelliteMockTrajectoryPredictor


def get_contact(cp, start, end):
    """Get the first contact from the list overlapping with the interval."""
    for c in cp:
        if c.end_time > start and c.start_time < end:
            return c
    return None


def predict_rr0_for_scenario(gs_list, sat_list, scenario_start, scenario_end,
                             min_elevation, strip_start_end=True):
    """Predict RR0 tuples for a multi-TLE scenario (sat has tle_list)."""
    # This function allows to use multiple sets of TLEs per node pair...
    blocked_until = scenario_start if strip_start_end else 0
    rr0_contacts = []
    last_contact_end = {}
    for sat in sat_list:
        if "tle_list" in sat and sat["tle_list"]:
            tle_invocations = [
                (
                    start,
                    min(scenario_end, sat["tle_list"][i + 1][0]) - start,
                    tle,
                )
                for i, (start, tle) in enumerate(sat["tle_list"][:-1])
                if start < scenario_end
            ]
            if sat["tle_list"][0][0] > scenario_start:
                # Prepend the time before the first contact
                tle_invocations = [(
                    scenario_start,
                    sat["tle_list"][0][0] - scenario_start,
                    sat["tle_list"][0][1],
                )] + tle_invocations
            if not tle_invocations:
                # add at least one entry
                tle_invocations = [(
                    scenario_start,
                    scenario_end - scenario_start,
                    sat["tle_list"][0][0],
                )]
            else:
                # add time after the last contact to last entry
                tle_invocations[-1] = (
                    tle_invocations[-1][0],
                    max(
                        scenario_end - scenario_start,
                        tle_invocations[-1][1],
                    ),
                    tle_invocations[-1][2],
                )
        else:
            tle_invocations = [(
                scenario_start,
                scenario_end - scenario_start,
                sat["tle"],
            )]
        for start, duration, tle in tle_invocations:
            next_tuples = get_rr0_contact_tuples(
                [{"id": sat["id"], "tle": tle}],
                gs_list,
                start,
                duration,
                min_elevation=min_elevation,
                precision=.1,
                half_period=2700.,
                step=300.,
                strip_start_end=False,
            )
            for gsid, satid, start, end in next_tuples:
                # Do not add overlapping or existing contacts
                if start < last_contact_end.get((gsid, satid), blocked_until):
                    continue
                if strip_start_end and end > scenario_end:
                    continue
                rr0_contacts.append((gsid, satid, start, end))
                last_contact_end[(gsid, satid)] = max(
                    last_contact_end.get((gsid, satid), blocked_until),
                    end,
                )
    return rr0_contacts


def get_pcp_for_scenario(scenario, scenario_start, scenario_end,
                         ftvg_filter=None, strip_start_end=True):
    gs_list = scenario["gslist"]
    sat_list = scenario["satlist"]

    # 1. Predict all contacts via SGP-4
    rr0_contacts = predict_rr0_for_scenario(
        gs_list,
        sat_list,
        scenario_start,
        scenario_end,
        0,  # NOTE: minelev for PCP is 0 because we modify the predictions!
        strip_start_end=strip_start_end,
    )

    # If an F-TVG is provided, we want to filter the predicted contacts to
    # only return those having an occurrence in the F-TVG (un-probabilistic).
    if ftvg_filter is not None:
        rr0_contacts = [
            (gs, sat, start, end)
            for gs, sat, start, end in rr0_contacts
            if get_contact(ftvg_filter.edges.get((gs, sat), []), start, end)
        ]

    # WARNING: uplink is from first entry of tuple to second (GS -> Sat)
    pcp = contact_plan.contact_tuples_to_pcp(
        rr0_contacts,
        0.0,
        # NOTE: These data rates are _after_ coding, see script 02.
        uplink_rate=scenario["bit_rate_uplink"],
        downlink_rate=scenario["bit_rate_downlink"],
    )

    return pcp


def get_trajectory_predictors_for_scenario(scenario):
    gs_list = scenario["gslist"]
    sat_list = scenario["satlist"]

    trajectory_predictors = {}

    for gs, sat in itertools.product(gs_list, sat_list):
        if "mock" in scenario and scenario["mock"]:
            orig_sat = scenario["mock"]["orig_sat"][sat["id"]]
            orig_gs = scenario["mock"]["orig_gs"][gs["id"]]
            tp = SatelliteMockTrajectoryPredictor(
                orig_sat_name=orig_sat["id"],
                orig_tle_list=orig_sat["tle_list"],
                gs_lat=orig_gs["lat"],
                gs_lon=orig_gs["lon"],
                gs_alt=orig_gs["alt"],
                contact_remap=scenario["mock"]["remap"][gs["id"]][sat["id"]],
            )
        else:
            tp = SatelliteTrajectoryPredictor(
                sat_name=sat["id"],
                tle_list=sat["tle_list"],
                gs_lat=gs["lat"],
                gs_lon=gs["lon"],
                gs_alt=gs["alt"],
            )
        trajectory_predictors[frozenset((gs["id"], sat["id"]))] = tp

    return trajectory_predictors


def add_characteristics_to_pc(pc, predicted_at, bit_rate,
                              confidence, bit_rate_factors,
                              overwrite=False):
    gen = contact_plan.PredictedContactGeneration(
        predicted_at,
        probability=confidence,
        characteristics=[
            contact_plan.PredictedContactCharacteristics(
                starting_at,
                bit_rate=(bit_rate * bit_rate_factor),
            )
            for starting_at, bit_rate_factor in bit_rate_factors
        ],
    )
    if overwrite:
        pc.generations = [gen]
    else:
        pc.generations.append(gen)


def get_fc_volume(factual_contact, block_size):
    return get_fc_volume_in_interval(
        factual_contact,
        block_size,
        factual_contact.start_time,
        factual_contact.end_time,
    )


def get_fc_volume_in_interval(factual_contact, block_size, start, end):
    assert start >= factual_contact.start_time
    assert end <= factual_contact.end_time
    volume = 0
    t = start
    char_i = -1
    char_count = len(factual_contact.characteristics)
    next_char_start = -math.inf
    while t < end:
        while next_char_start <= t:
            char_i += 1
            char_i_changed = True
            if char_i + 1 == char_count:
                next_char_start = math.inf
                break
            else:
                next_char_start = factual_contact.characteristics[
                    char_i + 1
                ].starting_at
        if char_i_changed:
            char = factual_contact.characteristics[char_i]
            p_block_successfully_transmitted = (
                (1 - char.bit_error_rate) ** block_size
            )
            tx_duration = block_size / char.bit_rate
            vol_inc = block_size * p_block_successfully_transmitted
            char_i_changed = False
        if t + tx_duration > end:
            break
        volume += vol_inc
        t += tx_duration
    return volume
