import bisect
import collections

import numpy as np


class StartTimeBasedDict:

    def __init__(self, base_dict):
        assert len(base_dict)
        self.base_dict = base_dict
        self.base_dict_keys = list(sorted(base_dict.keys()))

    def _get_key_index(self, time):
        if time < self.base_dict_keys[0]:
            raise ValueError(
                f"provided time is {self.base_dict_keys[0] - time} s "
                f"before start of first item"
            )
        return bisect.bisect_right(self.base_dict_keys, time) - 1

    def get_entry_for(self, time):
        return self.base_dict[self.base_dict_keys[
            self._get_key_index(time)
        ]]

    def get_entry_and_drop_predecessors(self, time):
        index = self._get_key_index(time)
        key = self.base_dict_keys[index]
        item = self.base_dict[key]
        for dropk in self.base_dict_keys[:index]:
            del self.base_dict[dropk]
        self.base_dict_keys[:index] = []
        assert self.base_dict_keys[0] == key
        assert key in self.base_dict
        assert self.base_dict[key] is item
        return item


class EwmaAverager:

    def __init__(self, weight, init_value):
        self.weight = weight
        self.value = init_value

    def add_value(self, value):
        self.value = (
            (1 - self.weight) * self.value +
            self.weight * value
        )

    def get_average(self):
        return self.value

    def export_state(self):
        return [self.weight, self.value]

    def import_state(self, val):
        self.weight = val[0]
        self.value = val[1]

    def copy(self):
        e = EwmaAverager(self.weight, self.value)
        e.import_state(self.export_state())
        return e


class WindowAverager:

    def __init__(self, window_size, default_value, use_median=True):
        self.window_size = int(window_size)
        self.default_value = default_value
        self.window = collections.deque(maxlen=self.window_size)
        self.use_median = use_median

    def add_value(self, value):
        self.window.append(value)

    def get_average(self):
        if not self.window:
            return self.default_value
        if self.use_median:
            return np.median(self.window)
        return sum(self.window) / len(self.window)

    def export_state(self):
        return [
            self.window_size,
            self.default_value,
            self.use_median,
            list(self.window),  # NOTE: it is important to copy here!
        ]

    def import_state(self, val):
        self.window_size = int(val[0])
        self.default_value = val[1]
        self.use_median = val[2]
        self.window = collections.deque(val[3], maxlen=self.window_size)

    def copy(self):
        e = WindowAverager(
            self.window_size,
            self.default_value,
            self.use_median,
        )
        e.import_state(self.export_state())
        return e
