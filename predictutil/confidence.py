import bisect
import collections
import itertools
import logging

from operator import attrgetter

from .base_predictor import BasePredictor

logger = logging.getLogger(__name__)


class ConfidencePredictor(BasePredictor):

    def __init__(self, pcp, trajectory_predictor):
        super().__init__(trajectory_predictor)
        # PCP contains all predicted contacts based on minelev
        self.pcp = list(sorted(pcp, key=attrgetter("start_time")))
        self.pcp_start = [c.start_time for c in self.pcp]

    def _get_predicted_contact(self, start_time, end_time):

        def _overlap(pc):
            return end_time > pc.start_time and start_time < pc.end_time

        index = bisect.bisect_left(self.pcp_start, start_time)

        if index < len(self.pcp) and _overlap(self.pcp[index]):
            return self.pcp[index]

        for i in itertools.count(1):
            if index - i < 0:
                break
            pc = self.pcp[index - i]
            if _overlap(pc):
                return pc
            if pc.end_time < start_time:
                break

        for i in itertools.count(1):
            if index + i >= len(self.pcp):
                break
            pc = self.pcp[index + i]
            if _overlap(pc):
                return pc
            if pc.start_time > end_time:
                break

        return None

    def predict_confidence(self, at_time, for_start_time, for_end_time):
        # return the expected probability of the contact
        # (= the reliability of the prediction)
        raise NotImplementedError()


class RatioBasedConfidencePredictor(ConfidencePredictor):

    def __init__(self, pcp, trajectory_predictor, window_size=10,
                 min_observed_contacts=5, default_confidence=1):
        super().__init__(pcp, trajectory_predictor)
        assert min_observed_contacts <= window_size
        self.window_size = window_size
        self.window = collections.deque(maxlen=int(window_size))
        self.min_observed_contacts = min_observed_contacts
        self.default_confidence = default_confidence
        self.last_observation = 0
        self.imported = False

    def _fill_up_observations(self, window, time):
        # NOTE: It is important to only run this once for a window for each
        # update to last_observation.
        index = bisect.bisect_left(self.pcp_start, self.last_observation)
        if index == len(self.pcp):
            # Nothing critical, it's normal to observe the last contact and
            # get predictions requested afterwards via the exporter...
            logger.warn(f"Observation at {self.last_observation} exceeds PCP")
            return
        for pc in self.pcp[index:]:
            if pc.end_time >= time:
                break
            if pc.start_time > self.last_observation:
                # Contact missed!
                window.append(False)

    def observe_contact(self, fc):
        pc = self._get_predicted_contact(fc.start_time, fc.end_time)
        if pc is not None:
            self._fill_up_observations(
                self.window,
                min(fc.start_time, pc.start_time),
            )
            self.window.append(True)
            self.last_observation = max(fc.end_time, pc.end_time)
        else:
            # WARNING: Non-matching contact? Do nothing!
            logger.warning(f"PC for {fc} not found")
            self._fill_up_observations(
                self.window,
                fc.start_time,
            )
            self.last_observation = fc.end_time

    def predict_confidence(self, at_time, for_start_time, for_end_time):
        if self.imported:
            return self.confidence_value
        wnd = self.window.copy()
        self._fill_up_observations(wnd, at_time)
        if len(wnd) < self.min_observed_contacts:
            return self.default_confidence
        # Ratio of True (observed) divided by all (predicted)
        return sum(wnd) / len(wnd)

    def export_metric_info(self, at_time):
        return self.predict_confidence(at_time, 0, 0)

    def import_metric_info(self, mi):
        self.confidence_value = mi
        self.imported = True


class EWMAConfidencePredictor(RatioBasedConfidencePredictor):

    def __init__(self, pcp, trajectory_predictor, window_size=64,
                 min_observed_contacts=0, p_0=1, w_obs=.1):
        super().__init__(pcp, trajectory_predictor, window_size,
                         min_observed_contacts, p_0)
        self.p_0 = p_0
        self.w_obs = w_obs

    def predict_confidence(self, at_time, for_start_time, for_end_time):
        if self.imported:
            return self.confidence_value
        wnd = self.window.copy()
        self._fill_up_observations(wnd, at_time)
        if len(wnd) < self.min_observed_contacts:
            return self.default_confidence
        p_cur = self.p_0
        for x_i in wnd:
            p_cur = self.w_obs * x_i + (1 - self.w_obs) * p_cur
        return p_cur


class DisabledConfidencePredictor(ConfidencePredictor):

    def __init__(self, pcp, trajectory_predictor):
        super().__init__(pcp, trajectory_predictor)

    def predict_confidence(self, at_time, for_start_time, for_end_time):
        return 1.0


CONFIDENCE_PREDICTORS = {
    "ratio": (
        RatioBasedConfidencePredictor,
        [
            "window_size",
            "min_observed_contacts",
            "default_confidence",
        ]),
    "ewma": (
        EWMAConfidencePredictor,
        [
            "window_size",
            "min_observed_contacts",
            "p_0",
            "w_obs",
        ]),
    "disabled": (DisabledConfidencePredictor, []),
}
