# diss-tools

This repository contains the source code used for the evaluation presented in the PhD thesis "Prediction-enhanced Routing in Disruption-tolerant Satellite Networks" by Dipl.-Ing. Felix Walter. Please consult the manuscript for additional details and direct your questions to: felix dot walter at tu-dresden dot de.

The scripts were used and tested on an up-to-date installation of Arch Linux.

## Requirements

* Python 3.7+
* Requirements from `requirements.txt`
* GNU Make (for automatically setting up the virtual environment)

**For contact analysis:**

* GNU Radio 3.8
* CMake
* a recent C++ compiler and libraries
* SWIG (for the Python bindings to the used GNU Radio out-of-tree modules)

## Getting Started

Initialize submodules, create a venv with all dependencies, and activate it:

```
git submodule update --init --recursive
make virtualenv
source .venv/bin/activate
```

If you do not want to perform analysis of actual satellite data from SatNOGS, `make virtualenv-without-gr` skips compiling and installing the GNU Radio modules:

```
git submodule update --init --recursive
make virtualenv-without-gr
source .venv/bin/activate
```

Now, check out the scriptes in the `diss` directory. They are numbered by the order in which they have to be executed.

## License

This code is licensed under the MIT license. See [LICENSE](LICENSE) for details.
