#!/usr/bin/env python
# encoding: utf-8

import sys
import json

import numpy as np

from tvgutil import tvg

if len(sys.argv) < 2:
    print("Usage: {} <TVG file> [file]".format(sys.argv[0]))
    sys.exit(1)

with open(sys.argv[1], "r") as tvg_file:
    tvg_json = json.load(tvg_file)

xtvg = tvg.from_serializable(tvg_json)
cplan = sorted(
    [c for _, clist in xtvg.edges.items() for c in clist],
    key=lambda c: c.end_time,
)

clashing = {}
for c1 in cplan:
    for c2 in cplan:
        if c1 == c2:
            continue
        if ((c1.tx_node == c2.tx_node or c1.rx_node == c2.rx_node) and
                c1.start_time < c2.end_time and c1.end_time > c2.start_time):
            if c1 in clashing:
                clashing[c1].append(c2)
            else:
                clashing[c1] = [c2]

for c1, clist in clashing.items():
    print("{} clashing with:".format(c1))
    for c2 in clist:
        print("    {}".format(c2))

print("Total clashing: {} of {} ({} %)".format(
    len(clashing),
    len(cplan),
    round(len(clashing) / len(cplan) * 100, 2),
))
print("Mean clashes per contact: {} (std = {})".format(
    round(np.mean([len(v) for _, v in clashing.items()]), 4),
    round(np.std([len(v) for _, v in clashing.items()]), 4),
))
