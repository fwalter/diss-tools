#!/bin/bash

NAME="$1"

set -euo pipefail

usage() {
    echo "Usage: $0 <scenario_name>" >&2
}

if [[ -z "$NAME" ]]; then
    usage
    exit 1
fi

python3 -m tvgutil.tools.create_rr_scenario --gs 5 --sats 5 --hotspots 2 --output "${NAME}_01_scenario.json"
python3 -m tvgutil.tools.create_rr_tvg --rr 0 --duration 864000 --minelev 10 --uplinkrate 9600 --downlinkrate 64000 --output "${NAME}_02_ptvg.json" "${NAME}_01_scenario.json"
python3 -m tvgutil.tools.convert_ptvg_to_ftvg --probabilistic --output "${NAME}_03_ftvg.json" "${NAME}_02_ptvg.json"
python3 -m tvgutil.tools.create_transmission_plan --interval 60 --minsize 500000 --lifetime 86400 --intervaldev 10 --maxgenpercent 5 --output "${NAME}_04_tplan.json" "${NAME}_02_ptvg.json"
