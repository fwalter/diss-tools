#!/bin/bash

set -euo pipefail

CPLAN_FILE="$1"

nodes_all=$(grep -F "a contact" "$CPLAN_FILE" | cut -d ' ' -f 6 | sort -h | uniq)

for n1 in $nodes_all; do
    for n2 in $nodes_all; do
        echo "d contact * $n1 $n2" | ionadmin > /dev/null
    done
done
