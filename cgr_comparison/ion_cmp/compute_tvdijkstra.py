#!/usr/bin/env python

import argparse
import math
import collections
import heapq

from tvgutil import contact_plan, tvg

from aiocgrsim import tvdijkstra


# load contact plan file with the format:
# a contact +<start> +<end> <from> <to> <rate> <range>
def cp_load(file_name, max_contacts=None):
    _contact_plan = []
    nodes = set()
    with open(file_name, 'r') as cf:
        for contact in cf.readlines():

            if contact[0] == '#':
                continue
            if not contact.startswith('a contact'):
                continue

            fields = contact.split(' ')[2:]  # ignore "a contact"
            start, end, frm, to, rate, owlt = map(int, fields)
            nodes.add(frm)
            nodes.add(to)
            _contact_plan.append(
                contact_plan.PredictedContact.simple(
                    start_time=start,
                    end_time=end,
                    tx_node=str(frm),
                    rx_node=str(to),
                    bit_rate=rate,
                    delay=0,
                )
            )
            if len(_contact_plan) == max_contacts:
                break

    return _contact_plan


def ptvg_to_simple_tvg(ptvg, generation_at):
    # Copy the PTVG to a simple edge-tuple representation
    edges = {}
    full_contact_list = []
    for edge, predicted_contact_list in ptvg.edges.items():
        tx_node, rx_node = edge
        cur_contact_list = []
        for predicted_contact in predicted_contact_list:
            # A hashable tuple representation, used for indexing
            # (This calculates an average bit rate.)
            contact_identifier = predicted_contact.to_simple(
                generation_at=generation_at,
                characteristics_at=None,
            )
            cur_contact_list.append(contact_identifier)
        edges[edge] = cur_contact_list
        full_contact_list.extend(cur_contact_list)
    return (
        tvg.TVG(
            ptvg.vertices.copy(),
            edges,
            contact_type=contact_plan.SimplePredictedContactTuple,
        ),
        full_contact_list
    )


def _main(args):
    cp = cp_load(args.CP_FILE)
    ptvg = tvg.from_contact_plan(cp)
    sptvg, spcl = ptvg_to_simple_tvg(ptvg, 0)

    for dest in sorted(ptvg.vertices, key=lambda x: int(x)):
        if dest == args.start_node:
            continue

        dr = tvdijkstra.tvdijkstra(
            sptvg,
            args.start_node,
            dest_node=dest,
            node_blacklist=set(),
            contact_blacklist=set(),
            start_time=0,
        )

        path_length, _, contact_path, eat = dr.get_path(dest)

        # Any route found?
        if not path_length:
            print(dest, "-")
        else:
            contact_path = list(contact_path)
            eat = list(eat)
            print(
                dest,
                contact_path[0].start_time,
                int(eat[-1]),
                "->".join(c.rx_node for c in contact_path),
            )


def _get_argument_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "CP_FILE",
        help="the contact plan file"
    )
    parser.add_argument(
        "--start-node",
        default="1",
        help="the start node for route searches"
    )
    return parser


if __name__ == "__main__":
    _main(_get_argument_parser().parse_args())
