#!/bin/bash

set -euo pipefail

CPLAN_FILE="$1"

nodes=$(grep -F "a contact" "$CPLAN_FILE" | cut -d ' ' -f 6 | sort -h | uniq | grep -vE "^1$")

cat "$CPLAN_FILE" | ionadmin > /dev/null

sleep 1

for n in $nodes; do
    echo "$n:: $(cgrfetch -d udp:127.0.0.1:4557 $n 2>&1 | grep -E "(HOP|PROPOSE|CHECK|ADD)" | tr -d "\n")"
done
