#!/usr/bin/env python3
# encoding: utf-8

import sys
import argparse
import json

from tvgutil import tvg


def _main(args):
    graph = tvg.from_serializable(json.load(args.INFILE))
    contacts = tvg.to_contact_plan(graph)
    scenario_start = min(c.start_time for c in contacts)

    out = args.output

    offset = args.start_offset - scenario_start
    nid_mapping = {v: i + 1 for i, v in enumerate(graph.vertices)}

    for pc in contacts:
        sc = pc.to_simple()
        t0 = int(round(sc.start_time + offset))
        t1 = int(round(sc.end_time + offset))
        tx_id = nid_mapping[sc.tx_node]
        rx_id = nid_mapping[sc.rx_node]
        out.write(
            f"a contact +{t0} +{t1} {tx_id} {rx_id} {int(sc.bit_rate / 8)} 1\n"
        )
        out.write(
            f"a range +{t0} +{t1} {tx_id} {rx_id} {sc.delay}\n"
        )

    out.close()


def _get_argument_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "INFILE",
        type=argparse.FileType("r"),
        default=sys.stdin,
        help="the input (F/P-)TVG",
    )
    parser.add_argument(
        "-o", "--output",
        type=argparse.FileType("w"),
        default=sys.stdout,
        help="the output (F/P-)TVG",
    )
    parser.add_argument(
        "--start-offset",
        type=int,
        default=3600,
        help="the start offset, from scenario start, in seconds",
    )
    return parser


if __name__ == "__main__":
    _main(_get_argument_parser().parse_args())
