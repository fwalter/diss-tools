#!/usr/bin/env python

import argparse


def _main(args):
    with open(args.ION_OUT_FILE, "r") as f:
        for line in f.readlines():
            larr = line.split()
            dest = larr[0].split(":")[0]

            hops = []
            i = 1
            while i < len(larr):
                if larr[i].startswith("HOP"):
                    hops.append(larr[i + 2].split(":")[1])
                    i += 3
                elif larr[i].startswith("CHECK"):
                    start_t = larr[i + 3].split(":")[1]
                    arr_t = larr[i + 4].split(":")[1]
                    break
                else:
                    i += 1

            print(
                dest,
                int(start_t) - args.offset,
                int(arr_t) - args.offset,
                "->".join(reversed(hops)),
            )


def _get_argument_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "ION_OUT_FILE",
        help="the file containing output of compute_ion_routes.sh"
    )
    parser.add_argument(
        "--offset",
        type=int,
        default=0,
        help="a time offset to be subtracted"
    )
    return parser


if __name__ == "__main__":
    _main(_get_argument_parser().parse_args())
