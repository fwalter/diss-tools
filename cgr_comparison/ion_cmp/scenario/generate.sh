python3 -m tvgutil.tools.create_rr_scenario --gs 5 --sats 5 --hotspots 2 --output "scenario.json"
python3 -m tvgutil.tools.create_rr_tvg --rr 0 --duration 43200 --minelev 10 --uplinkrate 9600 --downlinkrate 64000 --output "ptvg.json" "scenario.json"
python3 ../ptvg2ion.py "ptvg.json" > "cplan_ion.rc"
