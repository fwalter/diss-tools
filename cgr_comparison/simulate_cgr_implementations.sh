#!/bin/bash

NAME="$1"
ALGO="$2"

set -euo pipefail

usage() {
    echo "Usage: $0 <scenario_name> <algo>" >&2
    echo "Supported algos: aiocgr, pydtnsim" >&2
}

if [[ -z "$NAME" ]]; then
    usage
    exit 1
fi

AIODTNSIM_PATH="../.submodules/aiodtnsim"
DISS_PATH="../diss"

case "$ALGO" in
    aiocgr)
        python "${DISS_PATH}/06_simulation.py" --progress -a cgr --buffersize -1 -t "${NAME}_04_tplan.json" -p "${NAME}_02_ptvg.json" --savemsgops "${NAME}_msgops_aiocgr.log" "${NAME}_03_ftvg.json"
        ;;
    pydtnsim)
        python "${AIODTNSIM_PATH}/examples/pydtnsim_comparison/convert_v2_tvg_to_v1_tvg.py" -o "${NAME}_pydtnsim_v1_ptvg.json" "${NAME}_02_ptvg.json"
        python "${AIODTNSIM_PATH}/examples/pydtnsim_comparison/cgr_simulation_pydtnsim.py" -a scgr --savemsgops "${NAME}_msgops_pydtnsim.log" -v "${NAME}_pydtnsim_v1_ptvg.json" "${NAME}_04_tplan.json"
        ;;
    aiosabr)
        python "${DISS_PATH}/06_simulation.py" --progress -a sabr --buffersize -1 -t "${NAME}_04_tplan.json" -p "${NAME}_02_ptvg.json" --savemsgops "${NAME}_msgops_aiocgr.log" "${NAME}_03_ftvg.json"
        ;;
    ngsabr)
        python "${DISS_PATH}/06_simulation.py" --progress -a ngsabr --buffersize -1 -t "${NAME}_04_tplan.json" -p "${NAME}_02_ptvg.json" --savemsgops "${NAME}_msgops_aiocgr.log" "${NAME}_03_ftvg.json"
        ;;
    *)
        usage
        exit 1
        ;;
esac
