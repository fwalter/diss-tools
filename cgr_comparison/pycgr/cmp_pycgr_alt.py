#!/usr/bin/env python

"""Script using the tvdijkstra depicted in the manuscript."""

import argparse
import math
import collections
import heapq

from tvgutil import contact_plan, tvg

OWLT_MARGIN = 0

def tvdijkstra(graph, source, arrival_time, excluded_nodes, excluded_cts):
    # A mapping of node identifier to earliest arrival time.
    distance = {key: float("inf") for key in graph.vertices}
    distance[source] = arrival_time
    # A mapping of node identifier to contact for earliest arrival.
    taken_contact = {key: None for key in graph.vertices}
    # The queue is used to access all elements, ordered by distance.
    queue = [(arrival_time, source)]
    while queue:
        cur_dist, cur_node = queue.pop(0)
        # Neighbors: All nodes to which any contact exists from cur_node.
        for neigh_node in graph.vertices[cur_node]:
            # Traverse all contacts from cur_node to neigh
            for contact in graph.edges[(cur_node, neigh_node)]:
                # If contact excluded or bundle arrives too late, skip.
                if contact in excluded_cts or contact.end_time < cur_dist:
                    continue
                # SABR 3.2.4.1.1: Earliest Transmission Time
                ett = max(contact.start_time, cur_dist)
                # SABR 3.2.4.1.2: Earliest Arrival Time
                next_eat = ett + contact.delay + OWLT_MARGIN
                # If the new distance is smaller, update and add to queue.
                if next_eat < distance[neigh_node]:
                    distance[neigh_node] = next_eat
                    taken_contact[neigh_node] = contact
                    if neigh_node not in excluded_nodes:
                        queue.append((next_eat, neigh_node))
                        queue.sort()
                    # The contacts are ordered. All following are later.
                    break
    return distance, taken_contact


def get_route(taken_contact, destination):
    cur_node = destination
    contact_path = list()
    while taken_contact[cur_node] is not None:
        # Add contacts to the path in reverse order.
        contact_path.append(taken_contact[cur_node])
        # Get the sending node of the used contact.
        cur_node = taken_contact[cur_node].tx_node
    return list(reversed(contact_path))


def get_eats(distance, taken_contact, destination):
    cur_node = destination
    eats = list()
    while taken_contact[cur_node] is not None:
        # Add EAT in reverse order.
        eats.append(distance[cur_node])
        # Get the sending node of the used contact.
        cur_node = taken_contact[cur_node].tx_node
    return list(reversed(eats))


# load contact plan file with the format:
# a contact +<start> +<end> <from> <to> <rate> <range>
def cp_load(file_name, max_contacts=None):
    __contact_plan = []
    nodes = set()
    with open(file_name, 'r') as cf:
        for contact in cf.readlines():

            if contact[0] == '#':
                continue
            if not contact.startswith('a contact'):
                continue

            fields = contact.split(' ')[2:]  # ignore "a contact"
            start, end, frm, to, rate, owlt = map(int, fields)
            nodes.add(frm)
            nodes.add(to)
            __contact_plan.append(
                contact_plan.PredictedContact.simple(start_time=start, end_time=end, tx_node=str(frm), rx_node=str(to), bit_rate=rate, delay=0))
            if len(__contact_plan) == max_contacts:
                break

    print('Load contact plan: %s contacts were read.' % len(__contact_plan))
    # print(__contact_plan)
    return __contact_plan


def ptvg_to_simple_tvg(ptvg, generation_at):
    # Copy the PTVG to a simple edge-tuple representation
    edges = {}
    full_contact_list = []
    for edge, predicted_contact_list in ptvg.edges.items():
        tx_node, rx_node = edge
        cur_contact_list = []
        for predicted_contact in predicted_contact_list:
            # A hashable tuple representation, used for indexing
            # (This calculates an average bit rate.)
            contact_identifier = predicted_contact.to_simple(
                generation_at=generation_at,
                characteristics_at=None,
            )
            cur_contact_list.append(contact_identifier)
        edges[edge] = cur_contact_list
        full_contact_list.extend(cur_contact_list)
    return (
        tvg.TVG(
            ptvg.vertices.copy(),
            edges,
            contact_type=contact_plan.SimplePredictedContactTuple,
        ),
        full_contact_list
    )


def rgf_ended(ptvg, source_eid, dest_eid, excluded_eids, start_time):
    excluded_contacts = set()

    while True:
        _, taken_contact = tvdijkstra(
            graph=ptvg,
            source=source_eid,
            arrival_time=start_time,
            excluded_nodes=excluded_eids,
            excluded_cts=excluded_contacts,
        )
        contact_path = get_route(taken_contact, dest_eid)
        # Any route found?
        if not contact_path:
            break

        # Determine the time at which the route becomes invalid.
        route_end_time = math.inf
        limiting_contact_index = None
        for i, contact in enumerate(contact_path):
            if contact.end_time >= route_end_time:
                continue
            route_end_time = contact.end_time
            limiting_contact_index = i
        excluded_contacts.add(contact_path[limiting_contact_index])

        yield contact_path


def rgf_depleted(ptvg, source_eid, dest_eid, excluded_eids, start_time):
    excluded_contacts = set()

    while True:
        _, taken_contact = tvdijkstra(
            graph=ptvg,
            source=source_eid,
            arrival_time=start_time,
            excluded_nodes=excluded_eids,
            excluded_cts=excluded_contacts,
        )
        contact_path = get_route(taken_contact, dest_eid)
        # Any route found?
        if not contact_path:
            break

        # Determine the time at which the route becomes invalid.
        route_vol = math.inf
        limiting_contact_index = None
        for i, contact in enumerate(contact_path):
            vol = (contact.end_time - contact.start_time) * contact.bit_rate
            if vol >= route_vol:
                continue
            route_vol = vol
            limiting_contact_index = i
        excluded_contacts.add(contact_path[limiting_contact_index])

        yield contact_path


RouteInfo = collections.namedtuple("RouteInfo", ["contact_path", "arrival_times"])


def rgf_yen(ptvg, source_eid, dest_eid, excluded_eids, start_time, num_k):
    # The list of returned (best) routes.
    A = []
    # The heap of candidates for the next-best route.
    B = []

    # Copy the excluded EIDs to a set.
    excluded_eids = set(excluded_eids)
    # Search for the first-best route.
    distance, taken_contact = tvdijkstra(
        graph=ptvg,
        source=source_eid,
        arrival_time=start_time,
        excluded_nodes=excluded_eids,
        excluded_cts=set(),
    )
    contact_path = get_route(taken_contact, dest_eid)
    # Any route found?
    if not contact_path:
        return
    eat = get_eats(distance, taken_contact, dest_eid)
    # tvdijkstra does not use a notional root contact, so we emulate it by
    # adding "None" to the path with arrival time == start time.
    first_route = RouteInfo(
        contact_path,
        eat,
    )
    A.append(first_route)
    # Yield the contact path without the emulated notional root.
    yield first_route.contact_path

    for k in range(1, num_k):
        # Take apart the last-best route to find the next-best route.
        for i, spur_contact in enumerate(A[-1].contact_path):
            spur_node = spur_contact.tx_node
            # The path to the spur node (at which the spur contact starts).
            root_path = A[-1].contact_path[:i]
            arrival_times = A[-1].arrival_times[:i]
            arrival_time = arrival_times[-1] if arrival_times else start_time
            # Blacklist the path leading to the spur node, excluding the spur
            # contact as it is outgoing.
            contact_blacklist = set(root_path)
            # All contacts that are reachable from the same root path and
            # already contained in existing routes are blacklisted.
            # This also includes the spur contact.
            for route in A:
                if root_path == route.contact_path[:i]:
                    contact_blacklist.add(route.contact_path[i])
            # Blacklist nodes on the root path.
            node_blacklist = excluded_eids.copy()
            for contact in root_path:
                node_blacklist.add(contact.tx_node)
            # Search for the next-best route starting at the spur node.
            distance, taken_contact = tvdijkstra(
                graph=ptvg,
                source=spur_node,
                arrival_time=arrival_time,
                excluded_nodes=node_blacklist,
                excluded_cts=contact_blacklist,
            )
            contact_path = get_route(taken_contact, dest_eid)
            # If a new route has been found, add it to the heap.
            if contact_path:
                eat = get_eats(distance, taken_contact, dest_eid)
                heapq.heappush(B, (
                    distance[dest_eid],
                    RouteInfo(
                        root_path + contact_path,
                        arrival_times + eat,
                    )
                ))

        # No candidates found, terminate.
        if not B:
            return
        # Pop the best candidate from the heap, add it to A, and yield it.
        _, new_route = heapq.heappop(B)
        A.append(new_route)
        yield new_route.contact_path


def _main(args):
    cp = cp_load(args.CP_FILE)
    ptvg = tvg.from_contact_plan(cp)
    sptvg, spcl = ptvg_to_simple_tvg(ptvg, 0)
    print("ENDED")
    for route in rgf_ended(sptvg, "1", "5", set(), 0):
        print(route)
    print("\n\nDEPLETED")
    for route in rgf_depleted(sptvg, "1", "5", set(), 0):
        print(route)
    print("\n\nYEN")
    for route in rgf_yen(sptvg, "1", "5", set(), 0, 10000):
        print(route)


def _get_argument_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "CP_FILE",
        help="the contact plan file"
    )
    return parser


if __name__ == "__main__":
    _main(_get_argument_parser().parse_args())
